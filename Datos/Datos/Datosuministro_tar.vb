Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common

Public Class Datosuministro_tar
   Inherits conexion

  Public Function Insertar(ByVal suministro_tar As suministro_tar)
      Dim parametros As New List(Of DbParameter)
      Dim paramcod_suministro As DbParameter = cmd.CreateParameter()
      paramcod_suministro.Value = suministro_tar.cod_suministro
      paramcod_suministro.ParameterName = "cod_suministro"
      parametros.Add(paramcod_suministro)
      Dim parammanual As DbParameter = cmd.CreateParameter()
      parammanual.Value = suministro_tar.manual
      parammanual.ParameterName = "manual"
      parametros.Add(parammanual)
      Dim paramplan_tar As DbParameter = cmd.CreateParameter()
      paramplan_tar.Value = suministro_tar.plan_tar
      paramplan_tar.ParameterName = "plan_tar"
      parametros.Add(paramplan_tar)
      Dim paramvalor As DbParameter = cmd.CreateParameter()
      paramvalor.Value = suministro_tar.valor
      paramvalor.ParameterName = "valor"
      parametros.Add(paramvalor)
      Return ejecutaNonQuery("insertasuministro_tar", parametros)
  End Function

  Public Function InsertarRet(ByVal suministro_tar As suministro_tar)
      Dim dato As Integer = 0
      Try
          cnn = conecta()
          cmd = New SqlCommand("insertasuministro_tar")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
        Dim paramcod_suministro As DbParameter = cmd.CreateParameter()
        paramcod_suministro.Value = suministro_tar.cod_suministro
        paramcod_suministro.ParameterName = "cod_suministro"
        cmd.Parameters.Add(paramcod_suministro)
        Dim parammanual As DbParameter = cmd.CreateParameter()
        parammanual.Value = suministro_tar.manual
        parammanual.ParameterName = "manual"
        cmd.Parameters.Add(parammanual)
        Dim paramplan_tar As DbParameter = cmd.CreateParameter()
        paramplan_tar.Value = suministro_tar.plan_tar
        paramplan_tar.ParameterName = "plan_tar"
        cmd.Parameters.Add(paramplan_tar)
        Dim paramvalor As DbParameter = cmd.CreateParameter()
        paramvalor.Value = suministro_tar.valor
        paramvalor.ParameterName = "valor"
        cmd.Parameters.Add(paramvalor)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
            dato = Dr(0)
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return dato
  End Function

  Public Function Editar(ByVal suministro_tar As suministro_tar)
      Dim parametros As New List(Of DbParameter)
      Dim paramcod_suministro As DbParameter = cmd.CreateParameter()
      paramcod_suministro.Value = suministro_tar.cod_suministro
      paramcod_suministro.ParameterName = "cod_suministro"
      parametros.Add(paramcod_suministro)
      Dim parammanual As DbParameter = cmd.CreateParameter()
      parammanual.Value = suministro_tar.manual
      parammanual.ParameterName = "manual"
      parametros.Add(parammanual)
      Dim paramplan_tar As DbParameter = cmd.CreateParameter()
      paramplan_tar.Value = suministro_tar.plan_tar
      paramplan_tar.ParameterName = "plan_tar"
      parametros.Add(paramplan_tar)
      Dim paramvalor As DbParameter = cmd.CreateParameter()
      paramvalor.Value = suministro_tar.valor
      paramvalor.ParameterName = "valor"
      parametros.Add(paramvalor)
      Return ejecutaNonQuery("cambiarsuministro_tar", parametros)
  End Function

  Public Function Borrar(
      ByVal cod_suministro As string,
      ByVal manual As string,
      ByVal plan_tar As string)
      Dim parametros As New List(Of DbParameter)
      Dim paramcod_suministro As DbParameter = cmd.CreateParameter()
      paramcod_suministro.Value = cod_suministro
      paramcod_suministro.ParameterName = "cod_suministro"
      parametros.Add(paramcod_suministro)
      Dim parammanual As DbParameter = cmd.CreateParameter()
      parammanual.Value = manual
      parammanual.ParameterName = "manual"
      parametros.Add(parammanual)
      Dim paramplan_tar As DbParameter = cmd.CreateParameter()
      paramplan_tar.Value = plan_tar
      paramplan_tar.ParameterName = "plan_tar"
      parametros.Add(paramplan_tar)
      Return ejecutaNonQuery("eliminasuministro_tar", parametros)
  End Function

  Public Function Select_Where_suministro_tar(ByVal Cond As String) As List(Of suministro_tar)
      Dim Listsuministro_tar As New List(Of suministro_tar)
      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersuministro_tarByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsuministro_tar.Add(New suministro_tar(
                  Dr("cod_suministro"),
                  Dr("manual"),
                  Dr("plan_tar"),
                  Dr("valor")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsuministro_tar
  End Function

  Public Function Select_ById_suministro_tar(
      ByVal cod_suministro As string,
      ByVal manual As string,
      ByVal plan_tar As string) As List(Of suministro_tar)
      Dim Listsuministro_tar As New List(Of suministro_tar)
      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersuministro_tarById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramcod_suministro As DbParameter = cmd.CreateParameter()
      paramcod_suministro.Value = cod_suministro
      paramcod_suministro.ParameterName = "cod_suministro"
      cmd.Parameters.Add(paramcod_suministro)
      Dim parammanual As DbParameter = cmd.CreateParameter()
      parammanual.Value = manual
      parammanual.ParameterName = "manual"
      cmd.Parameters.Add(parammanual)
      Dim paramplan_tar As DbParameter = cmd.CreateParameter()
      paramplan_tar.Value = plan_tar
      paramplan_tar.ParameterName = "plan_tar"
      cmd.Parameters.Add(paramplan_tar)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsuministro_tar.Add(New suministro_tar(
                  Dr("cod_suministro"),
                  Dr("manual"),
                  Dr("plan_tar"),
                  Dr("valor")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsuministro_tar
  End Function

  Public Function Select_suministro_tar() As List(Of suministro_tar)
      Dim Listsuministro_tar As New List(Of suministro_tar)
      Try
         cnn = conecta()
          cmd = New SqlCommand("obtenersuministro_tar")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsuministro_tar.Add(New suministro_tar(
                  Dr("cod_suministro"),
                  Dr("manual"),
                  Dr("plan_tar"),
                  Dr("valor")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsuministro_tar
  End Function

End Class