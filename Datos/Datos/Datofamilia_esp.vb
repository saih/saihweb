Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common

Public Class Datofamilia_esp
   Inherits conexion

  Public Function Insertar(ByVal familia_esp As familia_esp)
      Dim parametros As New List(Of DbParameter)
      Dim paramid_familia As DbParameter = cmd.CreateParameter()
      paramid_familia.Value = familia_esp.id_familia
      paramid_familia.ParameterName = "id_familia"
      parametros.Add(paramid_familia)
      Dim paramcod_especialidad As DbParameter = cmd.CreateParameter()
      paramcod_especialidad.Value = familia_esp.cod_especialidad
      paramcod_especialidad.ParameterName = "cod_especialidad"
      parametros.Add(paramcod_especialidad)
      Dim paramcodpas As DbParameter = cmd.CreateParameter()
      paramcodpas.Value = familia_esp.codpas
      paramcodpas.ParameterName = "codpas"
      parametros.Add(paramcodpas)
      Dim paramfecha_adicion As DbParameter = cmd.CreateParameter()
      paramfecha_adicion.Value = familia_esp.fecha_adicion
      paramfecha_adicion.ParameterName = "fecha_adicion"
      parametros.Add(paramfecha_adicion)
      Dim paramusuario_creo As DbParameter = cmd.CreateParameter()
      paramusuario_creo.Value = familia_esp.usuario_creo
      paramusuario_creo.ParameterName = "usuario_creo"
      parametros.Add(paramusuario_creo)
      Dim paramfecha_modificacion As DbParameter = cmd.CreateParameter()
      paramfecha_modificacion.Value = familia_esp.fecha_modificacion
      paramfecha_modificacion.ParameterName = "fecha_modificacion"
      parametros.Add(paramfecha_modificacion)
      Dim paramusuario_modifico As DbParameter = cmd.CreateParameter()
      paramusuario_modifico.Value = familia_esp.usuario_modifico
      paramusuario_modifico.ParameterName = "usuario_modifico"
      parametros.Add(paramusuario_modifico)
      Return ejecutaNonQuery("insertafamilia_esp", parametros)
  End Function

  Public Function InsertarRet(ByVal familia_esp As familia_esp)
      Dim dato As Integer = 0
      Try
          cnn = conecta()
          cmd = New SqlCommand("insertafamilia_esp")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
        Dim paramid_familia As DbParameter = cmd.CreateParameter()
        paramid_familia.Value = familia_esp.id_familia
        paramid_familia.ParameterName = "id_familia"
        cmd.Parameters.Add(paramid_familia)
        Dim paramcod_especialidad As DbParameter = cmd.CreateParameter()
        paramcod_especialidad.Value = familia_esp.cod_especialidad
        paramcod_especialidad.ParameterName = "cod_especialidad"
        cmd.Parameters.Add(paramcod_especialidad)
        Dim paramcodpas As DbParameter = cmd.CreateParameter()
        paramcodpas.Value = familia_esp.codpas
        paramcodpas.ParameterName = "codpas"
        cmd.Parameters.Add(paramcodpas)
        Dim paramfecha_adicion As DbParameter = cmd.CreateParameter()
        paramfecha_adicion.Value = familia_esp.fecha_adicion
        paramfecha_adicion.ParameterName = "fecha_adicion"
        cmd.Parameters.Add(paramfecha_adicion)
        Dim paramusuario_creo As DbParameter = cmd.CreateParameter()
        paramusuario_creo.Value = familia_esp.usuario_creo
        paramusuario_creo.ParameterName = "usuario_creo"
        cmd.Parameters.Add(paramusuario_creo)
        Dim paramfecha_modificacion As DbParameter = cmd.CreateParameter()
        paramfecha_modificacion.Value = familia_esp.fecha_modificacion
        paramfecha_modificacion.ParameterName = "fecha_modificacion"
        cmd.Parameters.Add(paramfecha_modificacion)
        Dim paramusuario_modifico As DbParameter = cmd.CreateParameter()
        paramusuario_modifico.Value = familia_esp.usuario_modifico
        paramusuario_modifico.ParameterName = "usuario_modifico"
        cmd.Parameters.Add(paramusuario_modifico)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
            dato = Dr(0)
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return dato
  End Function

  Public Function Editar(ByVal familia_esp As familia_esp)
      Dim parametros As New List(Of DbParameter)
      Dim paramid_familia As DbParameter = cmd.CreateParameter()
      paramid_familia.Value = familia_esp.id_familia
      paramid_familia.ParameterName = "id_familia"
      parametros.Add(paramid_familia)
      Dim paramcod_especialidad As DbParameter = cmd.CreateParameter()
      paramcod_especialidad.Value = familia_esp.cod_especialidad
      paramcod_especialidad.ParameterName = "cod_especialidad"
      parametros.Add(paramcod_especialidad)
      Dim paramcodpas As DbParameter = cmd.CreateParameter()
      paramcodpas.Value = familia_esp.codpas
      paramcodpas.ParameterName = "codpas"
      parametros.Add(paramcodpas)
      Dim paramfecha_adicion As DbParameter = cmd.CreateParameter()
      paramfecha_adicion.Value = familia_esp.fecha_adicion
      paramfecha_adicion.ParameterName = "fecha_adicion"
      parametros.Add(paramfecha_adicion)
      Dim paramusuario_creo As DbParameter = cmd.CreateParameter()
      paramusuario_creo.Value = familia_esp.usuario_creo
      paramusuario_creo.ParameterName = "usuario_creo"
      parametros.Add(paramusuario_creo)
      Dim paramfecha_modificacion As DbParameter = cmd.CreateParameter()
      paramfecha_modificacion.Value = familia_esp.fecha_modificacion
      paramfecha_modificacion.ParameterName = "fecha_modificacion"
      parametros.Add(paramfecha_modificacion)
      Dim paramusuario_modifico As DbParameter = cmd.CreateParameter()
      paramusuario_modifico.Value = familia_esp.usuario_modifico
      paramusuario_modifico.ParameterName = "usuario_modifico"
      parametros.Add(paramusuario_modifico)
      Return ejecutaNonQuery("cambiarfamilia_esp", parametros)
  End Function

  Public Function Borrar(
      ByVal id_familia As integer,
      ByVal cod_especialidad As string)
      Dim parametros As New List(Of DbParameter)
      Dim paramid_familia As DbParameter = cmd.CreateParameter()
      paramid_familia.Value = id_familia
      paramid_familia.ParameterName = "id_familia"
      parametros.Add(paramid_familia)
      Dim paramcod_especialidad As DbParameter = cmd.CreateParameter()
      paramcod_especialidad.Value = cod_especialidad
      paramcod_especialidad.ParameterName = "cod_especialidad"
      parametros.Add(paramcod_especialidad)
      Return ejecutaNonQuery("eliminafamilia_esp", parametros)
  End Function

  Public Function Select_Where_familia_esp(ByVal Cond As String) As List(Of familia_esp)
      Dim Listfamilia_esp As New List(Of familia_esp)
      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenerfamilia_espByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listfamilia_esp.Add(New familia_esp(
                  Dr("id_familia"),
                  Dr("cod_especialidad"),
                  Dr("codpas"),
                  Dr("fecha_adicion"),
                  Dr("usuario_creo"),
                  Dr("fecha_modificacion"),
                  Dr("usuario_modifico")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listfamilia_esp
  End Function

  Public Function Select_ById_familia_esp(
      ByVal id_familia As integer,
      ByVal cod_especialidad As string) As List(Of familia_esp)
      Dim Listfamilia_esp As New List(Of familia_esp)
      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenerfamilia_espById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramid_familia As DbParameter = cmd.CreateParameter()
      paramid_familia.Value = id_familia
      paramid_familia.ParameterName = "id_familia"
      cmd.Parameters.Add(paramid_familia)
      Dim paramcod_especialidad As DbParameter = cmd.CreateParameter()
      paramcod_especialidad.Value = cod_especialidad
      paramcod_especialidad.ParameterName = "cod_especialidad"
      cmd.Parameters.Add(paramcod_especialidad)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listfamilia_esp.Add(New familia_esp(
                  Dr("id_familia"),
                  Dr("cod_especialidad"),
                  Dr("codpas"),
                  Dr("fecha_adicion"),
                  Dr("usuario_creo"),
                  Dr("fecha_modificacion"),
                  Dr("usuario_modifico")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listfamilia_esp
  End Function

  Public Function Select_familia_esp() As List(Of familia_esp)
      Dim Listfamilia_esp As New List(Of familia_esp)
      Try
         cnn = conecta()
          cmd = New SqlCommand("obtenerfamilia_esp")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listfamilia_esp.Add(New familia_esp(
                  Dr("id_familia"),
                  Dr("cod_especialidad"),
                  Dr("codpas"),
                  Dr("fecha_adicion"),
                  Dr("usuario_creo"),
                  Dr("fecha_modificacion"),
                  Dr("usuario_modifico")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listfamilia_esp
  End Function

End Class