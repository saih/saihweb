Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Configuration

Public Class Datosaih_lugar
   Inherits conexion

  Public Function Insertar(ByVal saih_lugar As saih_lugar)
      Dim parametros As New List(Of DbParameter)
      Dim paramTipo As DbParameter = cmd.CreateParameter()
      paramTipo.Value = saih_lugar.Tipo
      paramTipo.ParameterName = "Tipo"
      parametros.Add(paramTipo)
      Dim paramDescripcion As DbParameter = cmd.CreateParameter()
      paramDescripcion.Value = saih_lugar.Descripcion
      paramDescripcion.ParameterName = "Descripcion"
      parametros.Add(paramDescripcion)
      Dim paramId_Lugar As DbParameter = cmd.CreateParameter()
      paramId_Lugar.Value = saih_lugar.Id_Lugar
      paramId_Lugar.ParameterName = "Id_Lugar"
      parametros.Add(paramId_Lugar)
      Dim paramCodigo As DbParameter = cmd.CreateParameter()
      paramCodigo.Value = saih_lugar.Codigo
      paramCodigo.ParameterName = "Codigo"
      parametros.Add(paramCodigo)
      Return ejecutaNonQuery("insertasaih_lugar", parametros)
  End Function

  Public Function InsertarRet(ByVal saih_lugar As saih_lugar)
        Dim dato As Integer = 0
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("024")

      Try
          cnn = conecta()
          cmd = New SqlCommand("insertasaih_lugar")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
        Dim paramTipo As DbParameter = cmd.CreateParameter()
        paramTipo.Value = saih_lugar.Tipo
        paramTipo.ParameterName = "Tipo"
        cmd.Parameters.Add(paramTipo)
        Dim paramDescripcion As DbParameter = cmd.CreateParameter()
        paramDescripcion.Value = saih_lugar.Descripcion
        paramDescripcion.ParameterName = "Descripcion"
        cmd.Parameters.Add(paramDescripcion)
        Dim paramId_Lugar As DbParameter = cmd.CreateParameter()
        paramId_Lugar.Value = saih_lugar.Id_Lugar
        paramId_Lugar.ParameterName = "Id_Lugar"
        cmd.Parameters.Add(paramId_Lugar)
        Dim paramCodigo As DbParameter = cmd.CreateParameter()
        paramCodigo.Value = saih_lugar.Codigo
        paramCodigo.ParameterName = "Codigo"
        cmd.Parameters.Add(paramCodigo)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
            dato = Dr(0)
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return dato
  End Function

  Public Function Editar(ByVal saih_lugar As saih_lugar)
      Dim parametros As New List(Of DbParameter)
      Dim paramId As DbParameter = cmd.CreateParameter()
      paramId.Value = saih_lugar.Id
      paramId.ParameterName = "Id"
      parametros.Add(paramId)
      Dim paramTipo As DbParameter = cmd.CreateParameter()
      paramTipo.Value = saih_lugar.Tipo
      paramTipo.ParameterName = "Tipo"
      parametros.Add(paramTipo)
      Dim paramDescripcion As DbParameter = cmd.CreateParameter()
      paramDescripcion.Value = saih_lugar.Descripcion
      paramDescripcion.ParameterName = "Descripcion"
      parametros.Add(paramDescripcion)
      Dim paramId_Lugar As DbParameter = cmd.CreateParameter()
      paramId_Lugar.Value = saih_lugar.Id_Lugar
      paramId_Lugar.ParameterName = "Id_Lugar"
      parametros.Add(paramId_Lugar)
      Dim paramCodigo As DbParameter = cmd.CreateParameter()
      paramCodigo.Value = saih_lugar.Codigo
      paramCodigo.ParameterName = "Codigo"
      parametros.Add(paramCodigo)
      Return ejecutaNonQuery("cambiarsaih_lugar", parametros)
  End Function

  Public Function Borrar(
      ByVal Id As integer)
      Dim parametros As New List(Of DbParameter)
      Dim paramId As DbParameter = cmd.CreateParameter()
      paramId.Value = Id
      paramId.ParameterName = "Id"
      parametros.Add(paramId)
      Return ejecutaNonQuery("eliminasaih_lugar", parametros)
  End Function

  Public Function Select_Where_saih_lugar(ByVal Cond As String) As List(Of saih_lugar)
        Dim Listsaih_lugar As New List(Of saih_lugar)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("025")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_lugarByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_lugar.Add(New saih_lugar(
                  Dr("Id"),
                  Dr("Tipo"),
                  Dr("Descripcion"),
                  Dr("Id_Lugar"),
                  Dr("Codigo")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_lugar
  End Function

  Public Function Select_ById_saih_lugar(
      ByVal Id As integer) As List(Of saih_lugar)
        Dim Listsaih_lugar As New List(Of saih_lugar)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("025")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_lugarById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramId As DbParameter = cmd.CreateParameter()
      paramId.Value = Id
      paramId.ParameterName = "Id"
      cmd.Parameters.Add(paramId)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_lugar.Add(New saih_lugar(
                  Dr("Id"),
                  Dr("Tipo"),
                  Dr("Descripcion"),
                  Dr("Id_Lugar"),
                  Dr("Codigo")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_lugar
  End Function

  Public Function Select_saih_lugar() As List(Of saih_lugar)
        Dim Listsaih_lugar As New List(Of saih_lugar)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("025")

      Try
         cnn = conecta()
          cmd = New SqlCommand("obtenersaih_lugar")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_lugar.Add(New saih_lugar(
                  Dr("Id"),
                  Dr("Tipo"),
                  Dr("Descripcion"),
                  Dr("Id_Lugar"),
                  Dr("Codigo")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_lugar
  End Function

End Class