Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common

Public Class Datosuministro
   Inherits conexion

  Public Function Insertar(ByVal suministro As suministro)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = suministro.codigo
      paramcodigo.ParameterName = "codigo"
      parametros.Add(paramcodigo)
      Dim paramnombre As DbParameter = cmd.CreateParameter()
      paramnombre.Value = suministro.nombre
      paramnombre.ParameterName = "nombre"
      parametros.Add(paramnombre)
      Dim paramcodana As DbParameter = cmd.CreateParameter()
      paramcodana.Value = suministro.codana
      paramcodana.ParameterName = "codana"
      parametros.Add(paramcodana)
      Dim parampresentacion As DbParameter = cmd.CreateParameter()
      parampresentacion.Value = suministro.presentacion
      parampresentacion.ParameterName = "presentacion"
      parametros.Add(parampresentacion)
      Dim paramund_concentracion As DbParameter = cmd.CreateParameter()
      paramund_concentracion.Value = suministro.und_concentracion
      paramund_concentracion.ParameterName = "und_concentracion"
      parametros.Add(paramund_concentracion)
      Dim paramconcentracion As DbParameter = cmd.CreateParameter()
      paramconcentracion.Value = suministro.concentracion
      paramconcentracion.ParameterName = "concentracion"
      parametros.Add(paramconcentracion)
      Dim paramclase As DbParameter = cmd.CreateParameter()
      paramclase.Value = suministro.clase
      paramclase.ParameterName = "clase"
      parametros.Add(paramclase)
      Dim paramcodser As DbParameter = cmd.CreateParameter()
      paramcodser.Value = suministro.codser
      paramcodser.ParameterName = "codser"
      parametros.Add(paramcodser)
      Dim paramvalor As DbParameter = cmd.CreateParameter()
      paramvalor.Value = suministro.valor
      paramvalor.ParameterName = "valor"
      parametros.Add(paramvalor)
      Dim paramcod_centrocosto As DbParameter = cmd.CreateParameter()
      paramcod_centrocosto.Value = suministro.cod_centrocosto
      paramcod_centrocosto.ParameterName = "cod_centrocosto"
      parametros.Add(paramcod_centrocosto)
      Dim paramcod_undfunc As DbParameter = cmd.CreateParameter()
      paramcod_undfunc.Value = suministro.cod_undfunc
      paramcod_undfunc.ParameterName = "cod_undfunc"
      parametros.Add(paramcod_undfunc)
      Dim paramcod_material As DbParameter = cmd.CreateParameter()
      paramcod_material.Value = suministro.cod_material
      paramcod_material.ParameterName = "cod_material"
      parametros.Add(paramcod_material)
      Dim paramiva As DbParameter = cmd.CreateParameter()
      paramiva.Value = suministro.iva
      paramiva.ParameterName = "iva"
      parametros.Add(paramiva)
      Dim paramcod_generico As DbParameter = cmd.CreateParameter()
      paramcod_generico.Value = suministro.cod_generico
      paramcod_generico.ParameterName = "cod_generico"
      parametros.Add(paramcod_generico)
      Dim paramnom_generico As DbParameter = cmd.CreateParameter()
      paramnom_generico.Value = suministro.nom_generico
      paramnom_generico.ParameterName = "nom_generico"
      parametros.Add(paramnom_generico)
      Dim paramtipo_imp As DbParameter = cmd.CreateParameter()
      paramtipo_imp.Value = suministro.tipo_imp
      paramtipo_imp.ParameterName = "tipo_imp"
      parametros.Add(paramtipo_imp)
      Dim paramposo As DbParameter = cmd.CreateParameter()
      paramposo.Value = suministro.poso
      paramposo.ParameterName = "poso"
      parametros.Add(paramposo)
      Return ejecutaNonQuery("insertasuministro", parametros)
  End Function

  Public Function InsertarRet(ByVal suministro As suministro)
      Dim dato As Integer = 0
      Try
          cnn = conecta()
          cmd = New SqlCommand("insertasuministro")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
        Dim paramcodigo As DbParameter = cmd.CreateParameter()
        paramcodigo.Value = suministro.codigo
        paramcodigo.ParameterName = "codigo"
        cmd.Parameters.Add(paramcodigo)
        Dim paramnombre As DbParameter = cmd.CreateParameter()
        paramnombre.Value = suministro.nombre
        paramnombre.ParameterName = "nombre"
        cmd.Parameters.Add(paramnombre)
        Dim paramcodana As DbParameter = cmd.CreateParameter()
        paramcodana.Value = suministro.codana
        paramcodana.ParameterName = "codana"
        cmd.Parameters.Add(paramcodana)
        Dim parampresentacion As DbParameter = cmd.CreateParameter()
        parampresentacion.Value = suministro.presentacion
        parampresentacion.ParameterName = "presentacion"
        cmd.Parameters.Add(parampresentacion)
        Dim paramund_concentracion As DbParameter = cmd.CreateParameter()
        paramund_concentracion.Value = suministro.und_concentracion
        paramund_concentracion.ParameterName = "und_concentracion"
        cmd.Parameters.Add(paramund_concentracion)
        Dim paramconcentracion As DbParameter = cmd.CreateParameter()
        paramconcentracion.Value = suministro.concentracion
        paramconcentracion.ParameterName = "concentracion"
        cmd.Parameters.Add(paramconcentracion)
        Dim paramclase As DbParameter = cmd.CreateParameter()
        paramclase.Value = suministro.clase
        paramclase.ParameterName = "clase"
        cmd.Parameters.Add(paramclase)
        Dim paramcodser As DbParameter = cmd.CreateParameter()
        paramcodser.Value = suministro.codser
        paramcodser.ParameterName = "codser"
        cmd.Parameters.Add(paramcodser)
        Dim paramvalor As DbParameter = cmd.CreateParameter()
        paramvalor.Value = suministro.valor
        paramvalor.ParameterName = "valor"
        cmd.Parameters.Add(paramvalor)
        Dim paramcod_centrocosto As DbParameter = cmd.CreateParameter()
        paramcod_centrocosto.Value = suministro.cod_centrocosto
        paramcod_centrocosto.ParameterName = "cod_centrocosto"
        cmd.Parameters.Add(paramcod_centrocosto)
        Dim paramcod_undfunc As DbParameter = cmd.CreateParameter()
        paramcod_undfunc.Value = suministro.cod_undfunc
        paramcod_undfunc.ParameterName = "cod_undfunc"
        cmd.Parameters.Add(paramcod_undfunc)
        Dim paramcod_material As DbParameter = cmd.CreateParameter()
        paramcod_material.Value = suministro.cod_material
        paramcod_material.ParameterName = "cod_material"
        cmd.Parameters.Add(paramcod_material)
        Dim paramiva As DbParameter = cmd.CreateParameter()
        paramiva.Value = suministro.iva
        paramiva.ParameterName = "iva"
        cmd.Parameters.Add(paramiva)
        Dim paramcod_generico As DbParameter = cmd.CreateParameter()
        paramcod_generico.Value = suministro.cod_generico
        paramcod_generico.ParameterName = "cod_generico"
        cmd.Parameters.Add(paramcod_generico)
        Dim paramnom_generico As DbParameter = cmd.CreateParameter()
        paramnom_generico.Value = suministro.nom_generico
        paramnom_generico.ParameterName = "nom_generico"
        cmd.Parameters.Add(paramnom_generico)
        Dim paramtipo_imp As DbParameter = cmd.CreateParameter()
        paramtipo_imp.Value = suministro.tipo_imp
        paramtipo_imp.ParameterName = "tipo_imp"
        cmd.Parameters.Add(paramtipo_imp)
        Dim paramposo As DbParameter = cmd.CreateParameter()
        paramposo.Value = suministro.poso
        paramposo.ParameterName = "poso"
        cmd.Parameters.Add(paramposo)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
            dato = Dr(0)
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return dato
  End Function

  Public Function Editar(ByVal suministro As suministro)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = suministro.codigo
      paramcodigo.ParameterName = "codigo"
      parametros.Add(paramcodigo)
      Dim paramnombre As DbParameter = cmd.CreateParameter()
      paramnombre.Value = suministro.nombre
      paramnombre.ParameterName = "nombre"
      parametros.Add(paramnombre)
      Dim paramcodana As DbParameter = cmd.CreateParameter()
      paramcodana.Value = suministro.codana
      paramcodana.ParameterName = "codana"
      parametros.Add(paramcodana)
      Dim parampresentacion As DbParameter = cmd.CreateParameter()
      parampresentacion.Value = suministro.presentacion
      parampresentacion.ParameterName = "presentacion"
      parametros.Add(parampresentacion)
      Dim paramund_concentracion As DbParameter = cmd.CreateParameter()
      paramund_concentracion.Value = suministro.und_concentracion
      paramund_concentracion.ParameterName = "und_concentracion"
      parametros.Add(paramund_concentracion)
      Dim paramconcentracion As DbParameter = cmd.CreateParameter()
      paramconcentracion.Value = suministro.concentracion
      paramconcentracion.ParameterName = "concentracion"
      parametros.Add(paramconcentracion)
      Dim paramclase As DbParameter = cmd.CreateParameter()
      paramclase.Value = suministro.clase
      paramclase.ParameterName = "clase"
      parametros.Add(paramclase)
      Dim paramcodser As DbParameter = cmd.CreateParameter()
      paramcodser.Value = suministro.codser
      paramcodser.ParameterName = "codser"
      parametros.Add(paramcodser)
      Dim paramvalor As DbParameter = cmd.CreateParameter()
      paramvalor.Value = suministro.valor
      paramvalor.ParameterName = "valor"
      parametros.Add(paramvalor)
      Dim paramcod_centrocosto As DbParameter = cmd.CreateParameter()
      paramcod_centrocosto.Value = suministro.cod_centrocosto
      paramcod_centrocosto.ParameterName = "cod_centrocosto"
      parametros.Add(paramcod_centrocosto)
      Dim paramcod_undfunc As DbParameter = cmd.CreateParameter()
      paramcod_undfunc.Value = suministro.cod_undfunc
      paramcod_undfunc.ParameterName = "cod_undfunc"
      parametros.Add(paramcod_undfunc)
      Dim paramcod_material As DbParameter = cmd.CreateParameter()
      paramcod_material.Value = suministro.cod_material
      paramcod_material.ParameterName = "cod_material"
      parametros.Add(paramcod_material)
      Dim paramiva As DbParameter = cmd.CreateParameter()
      paramiva.Value = suministro.iva
      paramiva.ParameterName = "iva"
      parametros.Add(paramiva)
      Dim paramcod_generico As DbParameter = cmd.CreateParameter()
      paramcod_generico.Value = suministro.cod_generico
      paramcod_generico.ParameterName = "cod_generico"
      parametros.Add(paramcod_generico)
      Dim paramnom_generico As DbParameter = cmd.CreateParameter()
      paramnom_generico.Value = suministro.nom_generico
      paramnom_generico.ParameterName = "nom_generico"
      parametros.Add(paramnom_generico)
      Dim paramtipo_imp As DbParameter = cmd.CreateParameter()
      paramtipo_imp.Value = suministro.tipo_imp
      paramtipo_imp.ParameterName = "tipo_imp"
      parametros.Add(paramtipo_imp)
      Dim paramposo As DbParameter = cmd.CreateParameter()
      paramposo.Value = suministro.poso
      paramposo.ParameterName = "poso"
      parametros.Add(paramposo)
      Return ejecutaNonQuery("cambiarsuministro", parametros)
  End Function

  Public Function Borrar(
      ByVal codigo As string)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = codigo
      paramcodigo.ParameterName = "codigo"
      parametros.Add(paramcodigo)
      Return ejecutaNonQuery("eliminasuministro", parametros)
  End Function

  Public Function Select_Where_suministro(ByVal Cond As String) As List(Of suministro)
      Dim Listsuministro As New List(Of suministro)
      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersuministroByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsuministro.Add(New suministro(
                  Dr("codigo"),
                  Dr("nombre"),
                  Dr("codana"),
                  Dr("presentacion"),
                  Dr("und_concentracion"),
                  Dr("concentracion"),
                  Dr("clase"),
                  Dr("codser"),
                  Dr("valor"),
                  Dr("cod_centrocosto"),
                  Dr("cod_undfunc"),
                  Dr("cod_material"),
                  Dr("iva"),
                  Dr("cod_generico"),
                  Dr("nom_generico"),
                  Dr("tipo_imp"),
                  Dr("poso")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsuministro
  End Function

  Public Function Select_ById_suministro(
      ByVal codigo As string) As List(Of suministro)
      Dim Listsuministro As New List(Of suministro)
      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersuministroById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = codigo
      paramcodigo.ParameterName = "codigo"
      cmd.Parameters.Add(paramcodigo)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsuministro.Add(New suministro(
                  Dr("codigo"),
                  Dr("nombre"),
                  Dr("codana"),
                  Dr("presentacion"),
                  Dr("und_concentracion"),
                  Dr("concentracion"),
                  Dr("clase"),
                  Dr("codser"),
                  Dr("valor"),
                  Dr("cod_centrocosto"),
                  Dr("cod_undfunc"),
                  Dr("cod_material"),
                  Dr("iva"),
                  Dr("cod_generico"),
                  Dr("nom_generico"),
                  Dr("tipo_imp"),
                  Dr("poso")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsuministro
  End Function

  Public Function Select_suministro() As List(Of suministro)
      Dim Listsuministro As New List(Of suministro)
      Try
         cnn = conecta()
          cmd = New SqlCommand("obtenersuministro")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsuministro.Add(New suministro(
                  Dr("codigo"),
                  Dr("nombre"),
                  Dr("codana"),
                  Dr("presentacion"),
                  Dr("und_concentracion"),
                  Dr("concentracion"),
                  Dr("clase"),
                  Dr("codser"),
                  Dr("valor"),
                  Dr("cod_centrocosto"),
                  Dr("cod_undfunc"),
                  Dr("cod_material"),
                  Dr("iva"),
                  Dr("cod_generico"),
                  Dr("nom_generico"),
                  Dr("tipo_imp"),
                  Dr("poso")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsuministro
  End Function

End Class