Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Configuration

Public Class Datousuario_log
   Inherits conexion

  Public Function Insertar(ByVal usuario_log As usuario_log)
      Dim parametros As New List(Of DbParameter)
      Dim paramUsuario As DbParameter = cmd.CreateParameter()
      paramUsuario.Value = usuario_log.Usuario
      paramUsuario.ParameterName = "Usuario"
      parametros.Add(paramUsuario)
      Dim paramfecha As DbParameter = cmd.CreateParameter()
      paramfecha.Value = usuario_log.fecha
      paramfecha.ParameterName = "fecha"
      parametros.Add(paramfecha)
      Dim paramip As DbParameter = cmd.CreateParameter()
      paramip.Value = usuario_log.ip
      paramip.ParameterName = "ip"
      parametros.Add(paramip)
      Dim paramhost As DbParameter = cmd.CreateParameter()
      paramhost.Value = usuario_log.host
      paramhost.ParameterName = "host"
      parametros.Add(paramhost)
      Dim parammac As DbParameter = cmd.CreateParameter()
      parammac.Value = usuario_log.mac
      parammac.ParameterName = "mac"
      parametros.Add(parammac)
      Dim paramtipo As DbParameter = cmd.CreateParameter()
      paramtipo.Value = usuario_log.tipo
      paramtipo.ParameterName = "tipo"
      parametros.Add(paramtipo)
      Return ejecutaNonQuery("insertausuario_log", parametros)
  End Function

  Public Function InsertarRet(ByVal usuario_log As usuario_log)
        Dim dato As Integer = 0
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("004")

      Try
          cnn = conecta()
          cmd = New SqlCommand("insertausuario_log")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
        Dim paramUsuario As DbParameter = cmd.CreateParameter()
        paramUsuario.Value = usuario_log.Usuario
        paramUsuario.ParameterName = "Usuario"
        cmd.Parameters.Add(paramUsuario)
        Dim paramfecha As DbParameter = cmd.CreateParameter()
        paramfecha.Value = usuario_log.fecha
        paramfecha.ParameterName = "fecha"
        cmd.Parameters.Add(paramfecha)
        Dim paramip As DbParameter = cmd.CreateParameter()
        paramip.Value = usuario_log.ip
        paramip.ParameterName = "ip"
        cmd.Parameters.Add(paramip)
        Dim paramhost As DbParameter = cmd.CreateParameter()
        paramhost.Value = usuario_log.host
        paramhost.ParameterName = "host"
        cmd.Parameters.Add(paramhost)
        Dim parammac As DbParameter = cmd.CreateParameter()
        parammac.Value = usuario_log.mac
        parammac.ParameterName = "mac"
        cmd.Parameters.Add(parammac)
        Dim paramtipo As DbParameter = cmd.CreateParameter()
        paramtipo.Value = usuario_log.tipo
        paramtipo.ParameterName = "tipo"
        cmd.Parameters.Add(paramtipo)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
            dato = Dr(0)
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return dato
  End Function

  Public Function Editar(ByVal usuario_log As usuario_log)
      Dim parametros As New List(Of DbParameter)
      Dim paramid As DbParameter = cmd.CreateParameter()
      paramid.Value = usuario_log.id
      paramid.ParameterName = "id"
      parametros.Add(paramid)
      Dim paramUsuario As DbParameter = cmd.CreateParameter()
      paramUsuario.Value = usuario_log.Usuario
      paramUsuario.ParameterName = "Usuario"
      parametros.Add(paramUsuario)
      Dim paramfecha As DbParameter = cmd.CreateParameter()
      paramfecha.Value = usuario_log.fecha
      paramfecha.ParameterName = "fecha"
      parametros.Add(paramfecha)
      Dim paramip As DbParameter = cmd.CreateParameter()
      paramip.Value = usuario_log.ip
      paramip.ParameterName = "ip"
      parametros.Add(paramip)
      Dim paramhost As DbParameter = cmd.CreateParameter()
      paramhost.Value = usuario_log.host
      paramhost.ParameterName = "host"
      parametros.Add(paramhost)
      Dim parammac As DbParameter = cmd.CreateParameter()
      parammac.Value = usuario_log.mac
      parammac.ParameterName = "mac"
      parametros.Add(parammac)
      Dim paramtipo As DbParameter = cmd.CreateParameter()
      paramtipo.Value = usuario_log.tipo
      paramtipo.ParameterName = "tipo"
      parametros.Add(paramtipo)
      Return ejecutaNonQuery("cambiarusuario_log", parametros)
  End Function

  Public Function Borrar(
      ByVal id As integer)
      Dim parametros As New List(Of DbParameter)
      Dim paramid As DbParameter = cmd.CreateParameter()
      paramid.Value = id
      paramid.ParameterName = "id"
      parametros.Add(paramid)
      Return ejecutaNonQuery("eliminausuario_log", parametros)
  End Function

  Public Function Select_Where_usuario_log(ByVal Cond As String) As List(Of usuario_log)
        Dim Listusuario_log As New List(Of usuario_log)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("005")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenerusuario_logByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listusuario_log.Add(New usuario_log(
                  Dr("id"),
                  Dr("Usuario"),
                  Dr("fecha"),
                  Dr("ip"),
                  Dr("host"),
                  Dr("mac"),
                  Dr("tipo")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listusuario_log
  End Function

  Public Function Select_ById_usuario_log(
      ByVal id As integer) As List(Of usuario_log)
        Dim Listusuario_log As New List(Of usuario_log)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("005")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenerusuario_logById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramid As DbParameter = cmd.CreateParameter()
      paramid.Value = id
      paramid.ParameterName = "id"
      cmd.Parameters.Add(paramid)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listusuario_log.Add(New usuario_log(
                  Dr("id"),
                  Dr("Usuario"),
                  Dr("fecha"),
                  Dr("ip"),
                  Dr("host"),
                  Dr("mac"),
                  Dr("tipo")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listusuario_log
  End Function

  Public Function Select_usuario_log() As List(Of usuario_log)
        Dim Listusuario_log As New List(Of usuario_log)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("005")

      Try
         cnn = conecta()
          cmd = New SqlCommand("obtenerusuario_log")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listusuario_log.Add(New usuario_log(
                  Dr("id"),
                  Dr("Usuario"),
                  Dr("fecha"),
                  Dr("ip"),
                  Dr("host"),
                  Dr("mac"),
                  Dr("tipo")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listusuario_log
  End Function

End Class