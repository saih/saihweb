Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Configuration

Public Class Datosaih_unf
   Inherits conexion

  Public Function Insertar(ByVal saih_unf As saih_unf)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodunf As DbParameter = cmd.CreateParameter()
      paramcodunf.Value = saih_unf.codunf
      paramcodunf.ParameterName = "codunf"
      parametros.Add(paramcodunf)
      Dim paramnombre As DbParameter = cmd.CreateParameter()
      paramnombre.Value = saih_unf.nombre
      paramnombre.ParameterName = "nombre"
      parametros.Add(paramnombre)
      Return ejecutaNonQuery("insertasaih_unf", parametros)
  End Function

  Public Function InsertarRet(ByVal saih_unf As saih_unf)
        Dim dato As Integer = 0
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("008")

      Try
          cnn = conecta()
          cmd = New SqlCommand("insertasaih_unf")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
        Dim paramcodunf As DbParameter = cmd.CreateParameter()
        paramcodunf.Value = saih_unf.codunf
        paramcodunf.ParameterName = "codunf"
        cmd.Parameters.Add(paramcodunf)
        Dim paramnombre As DbParameter = cmd.CreateParameter()
        paramnombre.Value = saih_unf.nombre
        paramnombre.ParameterName = "nombre"
        cmd.Parameters.Add(paramnombre)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
            dato = Dr(0)
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return dato
  End Function

  Public Function Editar(ByVal saih_unf As saih_unf)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodunf As DbParameter = cmd.CreateParameter()
      paramcodunf.Value = saih_unf.codunf
      paramcodunf.ParameterName = "codunf"
      parametros.Add(paramcodunf)
      Dim paramnombre As DbParameter = cmd.CreateParameter()
      paramnombre.Value = saih_unf.nombre
      paramnombre.ParameterName = "nombre"
      parametros.Add(paramnombre)
      Return ejecutaNonQuery("cambiarsaih_unf", parametros)
  End Function

  Public Function Borrar(
      ByVal codunf As string)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodunf As DbParameter = cmd.CreateParameter()
      paramcodunf.Value = codunf
      paramcodunf.ParameterName = "codunf"
      parametros.Add(paramcodunf)
      Return ejecutaNonQuery("eliminasaih_unf", parametros)
  End Function

  Public Function Select_Where_saih_unf(ByVal Cond As String) As List(Of saih_unf)
        Dim Listsaih_unf As New List(Of saih_unf)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("009")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_unfByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_unf.Add(New saih_unf(
                  Dr("codunf"),
                  Dr("nombre")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_unf
  End Function

  Public Function Select_ById_saih_unf(
      ByVal codunf As string) As List(Of saih_unf)
        Dim Listsaih_unf As New List(Of saih_unf)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("009")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_unfById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramcodunf As DbParameter = cmd.CreateParameter()
      paramcodunf.Value = codunf
      paramcodunf.ParameterName = "codunf"
      cmd.Parameters.Add(paramcodunf)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_unf.Add(New saih_unf(
                  Dr("codunf"),
                  Dr("nombre")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_unf
  End Function

  Public Function Select_saih_unf() As List(Of saih_unf)
        Dim Listsaih_unf As New List(Of saih_unf)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("009")

      Try
         cnn = conecta()
          cmd = New SqlCommand("obtenersaih_unf")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_unf.Add(New saih_unf(
                  Dr("codunf"),
                  Dr("nombre")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_unf
  End Function

End Class