Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Configuration

Public Class Datosaih_pas
   Inherits conexion

  Public Function Insertar(ByVal saih_pas As saih_pas)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = saih_pas.codigo
      paramcodigo.ParameterName = "codigo"
      parametros.Add(paramcodigo)
      Dim paramidentificacion As DbParameter = cmd.CreateParameter()
      paramidentificacion.Value = saih_pas.identificacion
      paramidentificacion.ParameterName = "identificacion"
      parametros.Add(paramidentificacion)
      Dim paramid_lugarexp As DbParameter = cmd.CreateParameter()
      paramid_lugarexp.Value = saih_pas.id_lugarexp
      paramid_lugarexp.ParameterName = "id_lugarexp"
      parametros.Add(paramid_lugarexp)
      Dim paramnum_registro_med As DbParameter = cmd.CreateParameter()
      paramnum_registro_med.Value = saih_pas.num_registro_med
      paramnum_registro_med.ParameterName = "num_registro_med"
      parametros.Add(paramnum_registro_med)
      Dim paramnombres As DbParameter = cmd.CreateParameter()
      paramnombres.Value = saih_pas.nombres
      paramnombres.ParameterName = "nombres"
      parametros.Add(paramnombres)
      Dim paramdireccion As DbParameter = cmd.CreateParameter()
      paramdireccion.Value = saih_pas.direccion
      paramdireccion.ParameterName = "direccion"
      parametros.Add(paramdireccion)
      Dim paramtelefono As DbParameter = cmd.CreateParameter()
      paramtelefono.Value = saih_pas.telefono
      paramtelefono.ParameterName = "telefono"
      parametros.Add(paramtelefono)
      Dim paramcod_clase As DbParameter = cmd.CreateParameter()
      paramcod_clase.Value = saih_pas.cod_clase
      paramcod_clase.ParameterName = "cod_clase"
      parametros.Add(paramcod_clase)
      Dim paramcod_especialidad As DbParameter = cmd.CreateParameter()
      paramcod_especialidad.Value = saih_pas.cod_especialidad
      paramcod_especialidad.ParameterName = "cod_especialidad"
      parametros.Add(paramcod_especialidad)
      Dim paramdireccion_off As DbParameter = cmd.CreateParameter()
      paramdireccion_off.Value = saih_pas.direccion_off
      paramdireccion_off.ParameterName = "direccion_off"
      parametros.Add(paramdireccion_off)
      Dim paramtelefono_off As DbParameter = cmd.CreateParameter()
      paramtelefono_off.Value = saih_pas.telefono_off
      paramtelefono_off.ParameterName = "telefono_off"
      parametros.Add(paramtelefono_off)
      Dim paramnum_citas_dia As DbParameter = cmd.CreateParameter()
      paramnum_citas_dia.Value = saih_pas.num_citas_dia
      paramnum_citas_dia.ParameterName = "num_citas_dia"
      parametros.Add(paramnum_citas_dia)
      Dim paramestado As DbParameter = cmd.CreateParameter()
      paramestado.Value = saih_pas.estado
      paramestado.ParameterName = "estado"
      parametros.Add(paramestado)
      Dim paramduracion As DbParameter = cmd.CreateParameter()
      paramduracion.Value = saih_pas.duracion
      paramduracion.ParameterName = "duracion"
      parametros.Add(paramduracion)
      Dim paramfoto As DbParameter = cmd.CreateParameter()
      paramfoto.Value = saih_pas.foto
      paramfoto.ParameterName = "foto"
      parametros.Add(paramfoto)
      Dim paramfirma As DbParameter = cmd.CreateParameter()
      paramfirma.Value = saih_pas.firma
      paramfirma.ParameterName = "firma"
      parametros.Add(paramfirma)
      Dim paramhuella As DbParameter = cmd.CreateParameter()
      paramhuella.Value = saih_pas.huella
      paramhuella.ParameterName = "huella"
      parametros.Add(paramhuella)
      Return ejecutaNonQuery("insertasaih_pas", parametros)
  End Function

  Public Function InsertarRet(ByVal saih_pas As saih_pas)
        Dim dato As Integer = 0
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("022")

      Try
          cnn = conecta()
          cmd = New SqlCommand("insertasaih_pas")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
        Dim paramcodigo As DbParameter = cmd.CreateParameter()
        paramcodigo.Value = saih_pas.codigo
        paramcodigo.ParameterName = "codigo"
        cmd.Parameters.Add(paramcodigo)
        Dim paramidentificacion As DbParameter = cmd.CreateParameter()
        paramidentificacion.Value = saih_pas.identificacion
        paramidentificacion.ParameterName = "identificacion"
        cmd.Parameters.Add(paramidentificacion)
        Dim paramid_lugarexp As DbParameter = cmd.CreateParameter()
        paramid_lugarexp.Value = saih_pas.id_lugarexp
        paramid_lugarexp.ParameterName = "id_lugarexp"
        cmd.Parameters.Add(paramid_lugarexp)
        Dim paramnum_registro_med As DbParameter = cmd.CreateParameter()
        paramnum_registro_med.Value = saih_pas.num_registro_med
        paramnum_registro_med.ParameterName = "num_registro_med"
        cmd.Parameters.Add(paramnum_registro_med)
        Dim paramnombres As DbParameter = cmd.CreateParameter()
        paramnombres.Value = saih_pas.nombres
        paramnombres.ParameterName = "nombres"
        cmd.Parameters.Add(paramnombres)
        Dim paramdireccion As DbParameter = cmd.CreateParameter()
        paramdireccion.Value = saih_pas.direccion
        paramdireccion.ParameterName = "direccion"
        cmd.Parameters.Add(paramdireccion)
        Dim paramtelefono As DbParameter = cmd.CreateParameter()
        paramtelefono.Value = saih_pas.telefono
        paramtelefono.ParameterName = "telefono"
        cmd.Parameters.Add(paramtelefono)
        Dim paramcod_clase As DbParameter = cmd.CreateParameter()
        paramcod_clase.Value = saih_pas.cod_clase
        paramcod_clase.ParameterName = "cod_clase"
        cmd.Parameters.Add(paramcod_clase)
        Dim paramcod_especialidad As DbParameter = cmd.CreateParameter()
        paramcod_especialidad.Value = saih_pas.cod_especialidad
        paramcod_especialidad.ParameterName = "cod_especialidad"
        cmd.Parameters.Add(paramcod_especialidad)
        Dim paramdireccion_off As DbParameter = cmd.CreateParameter()
        paramdireccion_off.Value = saih_pas.direccion_off
        paramdireccion_off.ParameterName = "direccion_off"
        cmd.Parameters.Add(paramdireccion_off)
        Dim paramtelefono_off As DbParameter = cmd.CreateParameter()
        paramtelefono_off.Value = saih_pas.telefono_off
        paramtelefono_off.ParameterName = "telefono_off"
        cmd.Parameters.Add(paramtelefono_off)
        Dim paramnum_citas_dia As DbParameter = cmd.CreateParameter()
        paramnum_citas_dia.Value = saih_pas.num_citas_dia
        paramnum_citas_dia.ParameterName = "num_citas_dia"
        cmd.Parameters.Add(paramnum_citas_dia)
        Dim paramestado As DbParameter = cmd.CreateParameter()
        paramestado.Value = saih_pas.estado
        paramestado.ParameterName = "estado"
        cmd.Parameters.Add(paramestado)
        Dim paramduracion As DbParameter = cmd.CreateParameter()
        paramduracion.Value = saih_pas.duracion
        paramduracion.ParameterName = "duracion"
        cmd.Parameters.Add(paramduracion)
        Dim paramfoto As DbParameter = cmd.CreateParameter()
        paramfoto.Value = saih_pas.foto
        paramfoto.ParameterName = "foto"
        cmd.Parameters.Add(paramfoto)
        Dim paramfirma As DbParameter = cmd.CreateParameter()
        paramfirma.Value = saih_pas.firma
        paramfirma.ParameterName = "firma"
        cmd.Parameters.Add(paramfirma)
        Dim paramhuella As DbParameter = cmd.CreateParameter()
        paramhuella.Value = saih_pas.huella
        paramhuella.ParameterName = "huella"
        cmd.Parameters.Add(paramhuella)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
            dato = Dr(0)
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return dato
  End Function

  Public Function Editar(ByVal saih_pas As saih_pas)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = saih_pas.codigo
      paramcodigo.ParameterName = "codigo"
      parametros.Add(paramcodigo)
      Dim paramidentificacion As DbParameter = cmd.CreateParameter()
      paramidentificacion.Value = saih_pas.identificacion
      paramidentificacion.ParameterName = "identificacion"
      parametros.Add(paramidentificacion)
      Dim paramid_lugarexp As DbParameter = cmd.CreateParameter()
      paramid_lugarexp.Value = saih_pas.id_lugarexp
      paramid_lugarexp.ParameterName = "id_lugarexp"
      parametros.Add(paramid_lugarexp)
      Dim paramnum_registro_med As DbParameter = cmd.CreateParameter()
      paramnum_registro_med.Value = saih_pas.num_registro_med
      paramnum_registro_med.ParameterName = "num_registro_med"
      parametros.Add(paramnum_registro_med)
      Dim paramnombres As DbParameter = cmd.CreateParameter()
      paramnombres.Value = saih_pas.nombres
      paramnombres.ParameterName = "nombres"
      parametros.Add(paramnombres)
      Dim paramdireccion As DbParameter = cmd.CreateParameter()
      paramdireccion.Value = saih_pas.direccion
      paramdireccion.ParameterName = "direccion"
      parametros.Add(paramdireccion)
      Dim paramtelefono As DbParameter = cmd.CreateParameter()
      paramtelefono.Value = saih_pas.telefono
      paramtelefono.ParameterName = "telefono"
      parametros.Add(paramtelefono)
      Dim paramcod_clase As DbParameter = cmd.CreateParameter()
      paramcod_clase.Value = saih_pas.cod_clase
      paramcod_clase.ParameterName = "cod_clase"
      parametros.Add(paramcod_clase)
      Dim paramcod_especialidad As DbParameter = cmd.CreateParameter()
      paramcod_especialidad.Value = saih_pas.cod_especialidad
      paramcod_especialidad.ParameterName = "cod_especialidad"
      parametros.Add(paramcod_especialidad)
      Dim paramdireccion_off As DbParameter = cmd.CreateParameter()
      paramdireccion_off.Value = saih_pas.direccion_off
      paramdireccion_off.ParameterName = "direccion_off"
      parametros.Add(paramdireccion_off)
      Dim paramtelefono_off As DbParameter = cmd.CreateParameter()
      paramtelefono_off.Value = saih_pas.telefono_off
      paramtelefono_off.ParameterName = "telefono_off"
      parametros.Add(paramtelefono_off)
      Dim paramnum_citas_dia As DbParameter = cmd.CreateParameter()
      paramnum_citas_dia.Value = saih_pas.num_citas_dia
      paramnum_citas_dia.ParameterName = "num_citas_dia"
      parametros.Add(paramnum_citas_dia)
      Dim paramestado As DbParameter = cmd.CreateParameter()
      paramestado.Value = saih_pas.estado
      paramestado.ParameterName = "estado"
      parametros.Add(paramestado)
      Dim paramduracion As DbParameter = cmd.CreateParameter()
      paramduracion.Value = saih_pas.duracion
      paramduracion.ParameterName = "duracion"
      parametros.Add(paramduracion)
      Dim paramfoto As DbParameter = cmd.CreateParameter()
      paramfoto.Value = saih_pas.foto
      paramfoto.ParameterName = "foto"
      parametros.Add(paramfoto)
      Dim paramfirma As DbParameter = cmd.CreateParameter()
      paramfirma.Value = saih_pas.firma
      paramfirma.ParameterName = "firma"
      parametros.Add(paramfirma)
      Dim paramhuella As DbParameter = cmd.CreateParameter()
      paramhuella.Value = saih_pas.huella
      paramhuella.ParameterName = "huella"
      parametros.Add(paramhuella)
      Return ejecutaNonQuery("cambiarsaih_pas", parametros)
  End Function

  Public Function Borrar(
      ByVal codigo As string)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = codigo
      paramcodigo.ParameterName = "codigo"
      parametros.Add(paramcodigo)
      Return ejecutaNonQuery("eliminasaih_pas", parametros)
  End Function

  Public Function Select_Where_saih_pas(ByVal Cond As String) As List(Of saih_pas)
        Dim Listsaih_pas As New List(Of saih_pas)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("023")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_pasByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_pas.Add(New saih_pas(
                  Dr("codigo"),
                  Dr("identificacion"),
                  Dr("id_lugarexp"),
                  Dr("num_registro_med"),
                  Dr("nombres"),
                  Dr("direccion"),
                  Dr("telefono"),
                  Dr("cod_clase"),
                  Dr("cod_especialidad"),
                  Dr("direccion_off"),
                  Dr("telefono_off"),
                  Dr("num_citas_dia"),
                  Dr("estado"),
                  Dr("duracion"),
                  Dr("foto"),
                  Dr("firma"),
                  Dr("huella")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_pas
  End Function

  Public Function Select_ById_saih_pas(
      ByVal codigo As string) As List(Of saih_pas)
        Dim Listsaih_pas As New List(Of saih_pas)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("023")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_pasById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = codigo
      paramcodigo.ParameterName = "codigo"
      cmd.Parameters.Add(paramcodigo)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_pas.Add(New saih_pas(
                  Dr("codigo"),
                  Dr("identificacion"),
                  Dr("id_lugarexp"),
                  Dr("num_registro_med"),
                  Dr("nombres"),
                  Dr("direccion"),
                  Dr("telefono"),
                  Dr("cod_clase"),
                  Dr("cod_especialidad"),
                  Dr("direccion_off"),
                  Dr("telefono_off"),
                  Dr("num_citas_dia"),
                  Dr("estado"),
                  Dr("duracion"),
                  Dr("foto"),
                  Dr("firma"),
                  Dr("huella")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_pas
  End Function

  Public Function Select_saih_pas() As List(Of saih_pas)
        Dim Listsaih_pas As New List(Of saih_pas)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("023")

      Try
         cnn = conecta()
          cmd = New SqlCommand("obtenersaih_pas")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_pas.Add(New saih_pas(
                  Dr("codigo"),
                  Dr("identificacion"),
                  Dr("id_lugarexp"),
                  Dr("num_registro_med"),
                  Dr("nombres"),
                  Dr("direccion"),
                  Dr("telefono"),
                  Dr("cod_clase"),
                  Dr("cod_especialidad"),
                  Dr("direccion_off"),
                  Dr("telefono_off"),
                  Dr("num_citas_dia"),
                  Dr("estado"),
                  Dr("duracion"),
                  Dr("foto"),
                  Dr("firma"),
                  Dr("huella")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_pas
  End Function

End Class