Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common

Public Class Datoemp_contser
   Inherits conexion

  Public Function Insertar(ByVal emp_contser As emp_contser)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodemp As DbParameter = cmd.CreateParameter()
      paramcodemp.Value = emp_contser.codemp
      paramcodemp.ParameterName = "codemp"
      parametros.Add(paramcodemp)
      Dim paramcodser As DbParameter = cmd.CreateParameter()
      paramcodser.Value = emp_contser.codser
      paramcodser.ParameterName = "codser"
      parametros.Add(paramcodser)
      Dim parammanual As DbParameter = cmd.CreateParameter()
      parammanual.Value = emp_contser.manual
      parammanual.ParameterName = "manual"
      parametros.Add(parammanual)
      Dim paramporcentaje As DbParameter = cmd.CreateParameter()
      paramporcentaje.Value = emp_contser.porcentaje
      paramporcentaje.ParameterName = "porcentaje"
      parametros.Add(paramporcentaje)
      Dim paramnum_contrato As DbParameter = cmd.CreateParameter()
      paramnum_contrato.Value = emp_contser.num_contrato
      paramnum_contrato.ParameterName = "num_contrato"
      parametros.Add(paramnum_contrato)
      Dim paramniv_complejidad As DbParameter = cmd.CreateParameter()
      paramniv_complejidad.Value = emp_contser.niv_complejidad
      paramniv_complejidad.ParameterName = "niv_complejidad"
      parametros.Add(paramniv_complejidad)
      Return ejecutaNonQuery("insertaemp_contser", parametros)
  End Function

  Public Function InsertarRet(ByVal emp_contser As emp_contser)
      Dim dato As Integer = 0
      Try
          cnn = conecta()
          cmd = New SqlCommand("insertaemp_contser")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
        Dim paramcodemp As DbParameter = cmd.CreateParameter()
        paramcodemp.Value = emp_contser.codemp
        paramcodemp.ParameterName = "codemp"
        cmd.Parameters.Add(paramcodemp)
        Dim paramcodser As DbParameter = cmd.CreateParameter()
        paramcodser.Value = emp_contser.codser
        paramcodser.ParameterName = "codser"
        cmd.Parameters.Add(paramcodser)
        Dim parammanual As DbParameter = cmd.CreateParameter()
        parammanual.Value = emp_contser.manual
        parammanual.ParameterName = "manual"
        cmd.Parameters.Add(parammanual)
        Dim paramporcentaje As DbParameter = cmd.CreateParameter()
        paramporcentaje.Value = emp_contser.porcentaje
        paramporcentaje.ParameterName = "porcentaje"
        cmd.Parameters.Add(paramporcentaje)
        Dim paramnum_contrato As DbParameter = cmd.CreateParameter()
        paramnum_contrato.Value = emp_contser.num_contrato
        paramnum_contrato.ParameterName = "num_contrato"
        cmd.Parameters.Add(paramnum_contrato)
        Dim paramniv_complejidad As DbParameter = cmd.CreateParameter()
        paramniv_complejidad.Value = emp_contser.niv_complejidad
        paramniv_complejidad.ParameterName = "niv_complejidad"
        cmd.Parameters.Add(paramniv_complejidad)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
            dato = Dr(0)
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return dato
  End Function

  Public Function Editar(ByVal emp_contser As emp_contser)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodemp As DbParameter = cmd.CreateParameter()
      paramcodemp.Value = emp_contser.codemp
      paramcodemp.ParameterName = "codemp"
      parametros.Add(paramcodemp)
      Dim paramcodser As DbParameter = cmd.CreateParameter()
      paramcodser.Value = emp_contser.codser
      paramcodser.ParameterName = "codser"
      parametros.Add(paramcodser)
      Dim parammanual As DbParameter = cmd.CreateParameter()
      parammanual.Value = emp_contser.manual
      parammanual.ParameterName = "manual"
      parametros.Add(parammanual)
      Dim paramporcentaje As DbParameter = cmd.CreateParameter()
      paramporcentaje.Value = emp_contser.porcentaje
      paramporcentaje.ParameterName = "porcentaje"
      parametros.Add(paramporcentaje)
      Dim paramnum_contrato As DbParameter = cmd.CreateParameter()
      paramnum_contrato.Value = emp_contser.num_contrato
      paramnum_contrato.ParameterName = "num_contrato"
      parametros.Add(paramnum_contrato)
      Dim paramniv_complejidad As DbParameter = cmd.CreateParameter()
      paramniv_complejidad.Value = emp_contser.niv_complejidad
      paramniv_complejidad.ParameterName = "niv_complejidad"
      parametros.Add(paramniv_complejidad)
      Return ejecutaNonQuery("cambiaremp_contser", parametros)
  End Function

  Public Function Borrar(
      ByVal codemp As string,
      ByVal codser As string,
      ByVal manual As string)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodemp As DbParameter = cmd.CreateParameter()
      paramcodemp.Value = codemp
      paramcodemp.ParameterName = "codemp"
      parametros.Add(paramcodemp)
      Dim paramcodser As DbParameter = cmd.CreateParameter()
      paramcodser.Value = codser
      paramcodser.ParameterName = "codser"
      parametros.Add(paramcodser)
      Dim parammanual As DbParameter = cmd.CreateParameter()
      parammanual.Value = manual
      parammanual.ParameterName = "manual"
      parametros.Add(parammanual)
      Return ejecutaNonQuery("eliminaemp_contser", parametros)
  End Function

  Public Function Select_Where_emp_contser(ByVal Cond As String) As List(Of emp_contser)
      Dim Listemp_contser As New List(Of emp_contser)
      Try
          cnn = conecta()
          cmd = New SqlCommand("obteneremp_contserByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listemp_contser.Add(New emp_contser(
                  Dr("codemp"),
                  Dr("codser"),
                  Dr("manual"),
                  Dr("porcentaje"),
                  Dr("num_contrato"),
                  Dr("niv_complejidad")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listemp_contser
  End Function

  Public Function Select_ById_emp_contser(
      ByVal codemp As string,
      ByVal codser As string,
      ByVal manual As string) As List(Of emp_contser)
      Dim Listemp_contser As New List(Of emp_contser)
      Try
          cnn = conecta()
          cmd = New SqlCommand("obteneremp_contserById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramcodemp As DbParameter = cmd.CreateParameter()
      paramcodemp.Value = codemp
      paramcodemp.ParameterName = "codemp"
      cmd.Parameters.Add(paramcodemp)
      Dim paramcodser As DbParameter = cmd.CreateParameter()
      paramcodser.Value = codser
      paramcodser.ParameterName = "codser"
      cmd.Parameters.Add(paramcodser)
      Dim parammanual As DbParameter = cmd.CreateParameter()
      parammanual.Value = manual
      parammanual.ParameterName = "manual"
      cmd.Parameters.Add(parammanual)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listemp_contser.Add(New emp_contser(
                  Dr("codemp"),
                  Dr("codser"),
                  Dr("manual"),
                  Dr("porcentaje"),
                  Dr("num_contrato"),
                  Dr("niv_complejidad")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listemp_contser
  End Function

  Public Function Select_emp_contser() As List(Of emp_contser)
      Dim Listemp_contser As New List(Of emp_contser)
      Try
         cnn = conecta()
          cmd = New SqlCommand("obteneremp_contser")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listemp_contser.Add(New emp_contser(
                  Dr("codemp"),
                  Dr("codser"),
                  Dr("manual"),
                  Dr("porcentaje"),
                  Dr("num_contrato"),
                  Dr("niv_complejidad")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listemp_contser
  End Function

End Class