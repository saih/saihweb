Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common

Public Class Datocama
   Inherits conexion

  Public Function Insertar(ByVal cama As cama)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = cama.codigo
      paramcodigo.ParameterName = "codigo"
      parametros.Add(paramcodigo)
      Dim paramcodigo_area As DbParameter = cmd.CreateParameter()
      paramcodigo_area.Value = cama.codigo_area
      paramcodigo_area.ParameterName = "codigo_area"
      parametros.Add(paramcodigo_area)
      Dim paramnum_admision As DbParameter = cmd.CreateParameter()
      paramnum_admision.Value = cama.num_admision
      paramnum_admision.ParameterName = "num_admision"
      parametros.Add(paramnum_admision)
      Dim parammanual As DbParameter = cmd.CreateParameter()
      parammanual.Value = cama.manual
      parammanual.ParameterName = "manual"
      parametros.Add(parammanual)
      Dim paramcodpcd As DbParameter = cmd.CreateParameter()
      paramcodpcd.Value = cama.codpcd
      paramcodpcd.ParameterName = "codpcd"
      parametros.Add(paramcodpcd)
      Dim paramcodser As DbParameter = cmd.CreateParameter()
      paramcodser.Value = cama.codser
      paramcodser.ParameterName = "codser"
      parametros.Add(paramcodser)
      Return ejecutaNonQuery("camasinsertacama", parametros)
  End Function

  Public Function InsertarRet(ByVal cama As cama)
      Dim dato As Integer = 0
      Try
          cnn = conecta()
          cmd = New SqlCommand("camasinsertacama")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
        Dim paramcodigo As DbParameter = cmd.CreateParameter()
        paramcodigo.Value = cama.codigo
        paramcodigo.ParameterName = "codigo"
        cmd.Parameters.Add(paramcodigo)
        Dim paramcodigo_area As DbParameter = cmd.CreateParameter()
        paramcodigo_area.Value = cama.codigo_area
        paramcodigo_area.ParameterName = "codigo_area"
        cmd.Parameters.Add(paramcodigo_area)
        Dim paramnum_admision As DbParameter = cmd.CreateParameter()
        paramnum_admision.Value = cama.num_admision
        paramnum_admision.ParameterName = "num_admision"
        cmd.Parameters.Add(paramnum_admision)
        Dim parammanual As DbParameter = cmd.CreateParameter()
        parammanual.Value = cama.manual
        parammanual.ParameterName = "manual"
        cmd.Parameters.Add(parammanual)
        Dim paramcodpcd As DbParameter = cmd.CreateParameter()
        paramcodpcd.Value = cama.codpcd
        paramcodpcd.ParameterName = "codpcd"
        cmd.Parameters.Add(paramcodpcd)
        Dim paramcodser As DbParameter = cmd.CreateParameter()
        paramcodser.Value = cama.codser
        paramcodser.ParameterName = "codser"
        cmd.Parameters.Add(paramcodser)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
            dato = Dr(0)
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return dato
  End Function

  Public Function Editar(ByVal cama As cama)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = cama.codigo
      paramcodigo.ParameterName = "codigo"
      parametros.Add(paramcodigo)
      Dim paramcodigo_area As DbParameter = cmd.CreateParameter()
      paramcodigo_area.Value = cama.codigo_area
      paramcodigo_area.ParameterName = "codigo_area"
      parametros.Add(paramcodigo_area)
      Dim paramnum_admision As DbParameter = cmd.CreateParameter()
      paramnum_admision.Value = cama.num_admision
      paramnum_admision.ParameterName = "num_admision"
      parametros.Add(paramnum_admision)
      Dim parammanual As DbParameter = cmd.CreateParameter()
      parammanual.Value = cama.manual
      parammanual.ParameterName = "manual"
      parametros.Add(parammanual)
      Dim paramcodpcd As DbParameter = cmd.CreateParameter()
      paramcodpcd.Value = cama.codpcd
      paramcodpcd.ParameterName = "codpcd"
      parametros.Add(paramcodpcd)
      Dim paramcodser As DbParameter = cmd.CreateParameter()
      paramcodser.Value = cama.codser
      paramcodser.ParameterName = "codser"
      parametros.Add(paramcodser)
      Return ejecutaNonQuery("camascambiarcama", parametros)
  End Function

  Public Function Borrar(
      ByVal codigo As string)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = codigo
      paramcodigo.ParameterName = "codigo"
      parametros.Add(paramcodigo)
      Return ejecutaNonQuery("camaseliminacama", parametros)
  End Function

  Public Function Select_Where_cama(ByVal Cond As String) As List(Of cama)
      Dim Listcama As New List(Of cama)
      Try
          cnn = conecta()
          cmd = New SqlCommand("camasobtenercamaByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listcama.Add(New cama(
                  Dr("codigo"),
                  Dr("codigo_area"),
                  Dr("num_admision"),
                  Dr("manual"),
                  Dr("codpcd"),
                  Dr("codser")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listcama
  End Function

  Public Function Select_ById_cama(
      ByVal codigo As string) As List(Of cama)
      Dim Listcama As New List(Of cama)
      Try
          cnn = conecta()
          cmd = New SqlCommand("camasobtenercamaById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = codigo
      paramcodigo.ParameterName = "codigo"
      cmd.Parameters.Add(paramcodigo)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listcama.Add(New cama(
                  Dr("codigo"),
                  Dr("codigo_area"),
                  Dr("num_admision"),
                  Dr("manual"),
                  Dr("codpcd"),
                  Dr("codser")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listcama
  End Function

  Public Function Select_cama() As List(Of cama)
      Dim Listcama As New List(Of cama)
      Try
         cnn = conecta()
          cmd = New SqlCommand("camasobtenercama")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listcama.Add(New cama(
                  Dr("codigo"),
                  Dr("codigo_area"),
                  Dr("num_admision"),
                  Dr("manual"),
                  Dr("codpcd"),
                  Dr("codser")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listcama
  End Function

End Class