Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common

Public Class Datoexc_quirurjicas
   Inherits conexion

  Public Function Insertar(ByVal exc_quirurjicas As exc_quirurjicas)
      Dim parametros As New List(Of DbParameter)
      Dim parammanual As DbParameter = cmd.CreateParameter()
      parammanual.Value = exc_quirurjicas.manual
      parammanual.ParameterName = "manual"
      parametros.Add(parammanual)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = exc_quirurjicas.codigo
      paramcodigo.ParameterName = "codigo"
      parametros.Add(paramcodigo)
      Dim paramnombre As DbParameter = cmd.CreateParameter()
      paramnombre.Value = exc_quirurjicas.nombre
      paramnombre.ParameterName = "nombre"
      parametros.Add(paramnombre)
      Dim paramtipo_porcentaje As DbParameter = cmd.CreateParameter()
      paramtipo_porcentaje.Value = exc_quirurjicas.tipo_porcentaje
      paramtipo_porcentaje.ParameterName = "tipo_porcentaje"
      parametros.Add(paramtipo_porcentaje)
      Dim paramporc_honmed As DbParameter = cmd.CreateParameter()
      paramporc_honmed.Value = exc_quirurjicas.porc_honmed
      paramporc_honmed.ParameterName = "porc_honmed"
      parametros.Add(paramporc_honmed)
      Dim paramporc_honane As DbParameter = cmd.CreateParameter()
      paramporc_honane.Value = exc_quirurjicas.porc_honane
      paramporc_honane.ParameterName = "porc_honane"
      parametros.Add(paramporc_honane)
      Dim paramporc_honayu As DbParameter = cmd.CreateParameter()
      paramporc_honayu.Value = exc_quirurjicas.porc_honayu
      paramporc_honayu.ParameterName = "porc_honayu"
      parametros.Add(paramporc_honayu)
      Dim paramporc_dersal As DbParameter = cmd.CreateParameter()
      paramporc_dersal.Value = exc_quirurjicas.porc_dersal
      paramporc_dersal.ParameterName = "porc_dersal"
      parametros.Add(paramporc_dersal)
      Dim paramporc_dersalesp As DbParameter = cmd.CreateParameter()
      paramporc_dersalesp.Value = exc_quirurjicas.porc_dersalesp
      paramporc_dersalesp.ParameterName = "porc_dersalesp"
      parametros.Add(paramporc_dersalesp)
      Dim paramporc_matsum As DbParameter = cmd.CreateParameter()
      paramporc_matsum.Value = exc_quirurjicas.porc_matsum
      paramporc_matsum.ParameterName = "porc_matsum"
      parametros.Add(paramporc_matsum)
      Dim paramporc_honper As DbParameter = cmd.CreateParameter()
      paramporc_honper.Value = exc_quirurjicas.porc_honper
      paramporc_honper.ParameterName = "porc_honper"
      parametros.Add(paramporc_honper)
      Dim paramporc_paqquir As DbParameter = cmd.CreateParameter()
      paramporc_paqquir.Value = exc_quirurjicas.porc_paqquir
      paramporc_paqquir.ParameterName = "porc_paqquir"
      parametros.Add(paramporc_paqquir)
      Return ejecutaNonQuery("camasinsertaexc_quirurjicas", parametros)
  End Function

  Public Function InsertarRet(ByVal exc_quirurjicas As exc_quirurjicas)
      Dim dato As Integer = 0
      Try
          cnn = conecta()
          cmd = New SqlCommand("camasinsertaexc_quirurjicas")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
        Dim parammanual As DbParameter = cmd.CreateParameter()
        parammanual.Value = exc_quirurjicas.manual
        parammanual.ParameterName = "manual"
        cmd.Parameters.Add(parammanual)
        Dim paramcodigo As DbParameter = cmd.CreateParameter()
        paramcodigo.Value = exc_quirurjicas.codigo
        paramcodigo.ParameterName = "codigo"
        cmd.Parameters.Add(paramcodigo)
        Dim paramnombre As DbParameter = cmd.CreateParameter()
        paramnombre.Value = exc_quirurjicas.nombre
        paramnombre.ParameterName = "nombre"
        cmd.Parameters.Add(paramnombre)
        Dim paramtipo_porcentaje As DbParameter = cmd.CreateParameter()
        paramtipo_porcentaje.Value = exc_quirurjicas.tipo_porcentaje
        paramtipo_porcentaje.ParameterName = "tipo_porcentaje"
        cmd.Parameters.Add(paramtipo_porcentaje)
        Dim paramporc_honmed As DbParameter = cmd.CreateParameter()
        paramporc_honmed.Value = exc_quirurjicas.porc_honmed
        paramporc_honmed.ParameterName = "porc_honmed"
        cmd.Parameters.Add(paramporc_honmed)
        Dim paramporc_honane As DbParameter = cmd.CreateParameter()
        paramporc_honane.Value = exc_quirurjicas.porc_honane
        paramporc_honane.ParameterName = "porc_honane"
        cmd.Parameters.Add(paramporc_honane)
        Dim paramporc_honayu As DbParameter = cmd.CreateParameter()
        paramporc_honayu.Value = exc_quirurjicas.porc_honayu
        paramporc_honayu.ParameterName = "porc_honayu"
        cmd.Parameters.Add(paramporc_honayu)
        Dim paramporc_dersal As DbParameter = cmd.CreateParameter()
        paramporc_dersal.Value = exc_quirurjicas.porc_dersal
        paramporc_dersal.ParameterName = "porc_dersal"
        cmd.Parameters.Add(paramporc_dersal)
        Dim paramporc_dersalesp As DbParameter = cmd.CreateParameter()
        paramporc_dersalesp.Value = exc_quirurjicas.porc_dersalesp
        paramporc_dersalesp.ParameterName = "porc_dersalesp"
        cmd.Parameters.Add(paramporc_dersalesp)
        Dim paramporc_matsum As DbParameter = cmd.CreateParameter()
        paramporc_matsum.Value = exc_quirurjicas.porc_matsum
        paramporc_matsum.ParameterName = "porc_matsum"
        cmd.Parameters.Add(paramporc_matsum)
        Dim paramporc_honper As DbParameter = cmd.CreateParameter()
        paramporc_honper.Value = exc_quirurjicas.porc_honper
        paramporc_honper.ParameterName = "porc_honper"
        cmd.Parameters.Add(paramporc_honper)
        Dim paramporc_paqquir As DbParameter = cmd.CreateParameter()
        paramporc_paqquir.Value = exc_quirurjicas.porc_paqquir
        paramporc_paqquir.ParameterName = "porc_paqquir"
        cmd.Parameters.Add(paramporc_paqquir)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
            dato = Dr(0)
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return dato
  End Function

  Public Function Editar(ByVal exc_quirurjicas As exc_quirurjicas)
      Dim parametros As New List(Of DbParameter)
      Dim parammanual As DbParameter = cmd.CreateParameter()
      parammanual.Value = exc_quirurjicas.manual
      parammanual.ParameterName = "manual"
      parametros.Add(parammanual)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = exc_quirurjicas.codigo
      paramcodigo.ParameterName = "codigo"
      parametros.Add(paramcodigo)
      Dim paramnombre As DbParameter = cmd.CreateParameter()
      paramnombre.Value = exc_quirurjicas.nombre
      paramnombre.ParameterName = "nombre"
      parametros.Add(paramnombre)
      Dim paramtipo_porcentaje As DbParameter = cmd.CreateParameter()
      paramtipo_porcentaje.Value = exc_quirurjicas.tipo_porcentaje
      paramtipo_porcentaje.ParameterName = "tipo_porcentaje"
      parametros.Add(paramtipo_porcentaje)
      Dim paramporc_honmed As DbParameter = cmd.CreateParameter()
      paramporc_honmed.Value = exc_quirurjicas.porc_honmed
      paramporc_honmed.ParameterName = "porc_honmed"
      parametros.Add(paramporc_honmed)
      Dim paramporc_honane As DbParameter = cmd.CreateParameter()
      paramporc_honane.Value = exc_quirurjicas.porc_honane
      paramporc_honane.ParameterName = "porc_honane"
      parametros.Add(paramporc_honane)
      Dim paramporc_honayu As DbParameter = cmd.CreateParameter()
      paramporc_honayu.Value = exc_quirurjicas.porc_honayu
      paramporc_honayu.ParameterName = "porc_honayu"
      parametros.Add(paramporc_honayu)
      Dim paramporc_dersal As DbParameter = cmd.CreateParameter()
      paramporc_dersal.Value = exc_quirurjicas.porc_dersal
      paramporc_dersal.ParameterName = "porc_dersal"
      parametros.Add(paramporc_dersal)
      Dim paramporc_dersalesp As DbParameter = cmd.CreateParameter()
      paramporc_dersalesp.Value = exc_quirurjicas.porc_dersalesp
      paramporc_dersalesp.ParameterName = "porc_dersalesp"
      parametros.Add(paramporc_dersalesp)
      Dim paramporc_matsum As DbParameter = cmd.CreateParameter()
      paramporc_matsum.Value = exc_quirurjicas.porc_matsum
      paramporc_matsum.ParameterName = "porc_matsum"
      parametros.Add(paramporc_matsum)
      Dim paramporc_honper As DbParameter = cmd.CreateParameter()
      paramporc_honper.Value = exc_quirurjicas.porc_honper
      paramporc_honper.ParameterName = "porc_honper"
      parametros.Add(paramporc_honper)
      Dim paramporc_paqquir As DbParameter = cmd.CreateParameter()
      paramporc_paqquir.Value = exc_quirurjicas.porc_paqquir
      paramporc_paqquir.ParameterName = "porc_paqquir"
      parametros.Add(paramporc_paqquir)
      Return ejecutaNonQuery("camascambiarexc_quirurjicas", parametros)
  End Function

  Public Function Borrar(
      ByVal manual As string,
      ByVal codigo As string)
      Dim parametros As New List(Of DbParameter)
      Dim parammanual As DbParameter = cmd.CreateParameter()
      parammanual.Value = manual
      parammanual.ParameterName = "manual"
      parametros.Add(parammanual)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = codigo
      paramcodigo.ParameterName = "codigo"
      parametros.Add(paramcodigo)
      Return ejecutaNonQuery("camaseliminaexc_quirurjicas", parametros)
  End Function

  Public Function Select_Where_exc_quirurjicas(ByVal Cond As String) As List(Of exc_quirurjicas)
      Dim Listexc_quirurjicas As New List(Of exc_quirurjicas)
      Try
          cnn = conecta()
          cmd = New SqlCommand("camasobtenerexc_quirurjicasByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listexc_quirurjicas.Add(New exc_quirurjicas(
                  Dr("manual"),
                  Dr("codigo"),
                  Dr("nombre"),
                  Dr("tipo_porcentaje"),
                  Dr("porc_honmed"),
                  Dr("porc_honane"),
                  Dr("porc_honayu"),
                  Dr("porc_dersal"),
                  Dr("porc_dersalesp"),
                  Dr("porc_matsum"),
                  Dr("porc_honper"),
                  Dr("porc_paqquir")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listexc_quirurjicas
  End Function

  Public Function Select_ById_exc_quirurjicas(
      ByVal manual As string,
      ByVal codigo As string) As List(Of exc_quirurjicas)
      Dim Listexc_quirurjicas As New List(Of exc_quirurjicas)
      Try
          cnn = conecta()
          cmd = New SqlCommand("camasobtenerexc_quirurjicasById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim parammanual As DbParameter = cmd.CreateParameter()
      parammanual.Value = manual
      parammanual.ParameterName = "manual"
      cmd.Parameters.Add(parammanual)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = codigo
      paramcodigo.ParameterName = "codigo"
      cmd.Parameters.Add(paramcodigo)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listexc_quirurjicas.Add(New exc_quirurjicas(
                  Dr("manual"),
                  Dr("codigo"),
                  Dr("nombre"),
                  Dr("tipo_porcentaje"),
                  Dr("porc_honmed"),
                  Dr("porc_honane"),
                  Dr("porc_honayu"),
                  Dr("porc_dersal"),
                  Dr("porc_dersalesp"),
                  Dr("porc_matsum"),
                  Dr("porc_honper"),
                  Dr("porc_paqquir")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listexc_quirurjicas
  End Function

  Public Function Select_exc_quirurjicas() As List(Of exc_quirurjicas)
      Dim Listexc_quirurjicas As New List(Of exc_quirurjicas)
      Try
         cnn = conecta()
          cmd = New SqlCommand("camasobtenerexc_quirurjicas")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listexc_quirurjicas.Add(New exc_quirurjicas(
                  Dr("manual"),
                  Dr("codigo"),
                  Dr("nombre"),
                  Dr("tipo_porcentaje"),
                  Dr("porc_honmed"),
                  Dr("porc_honane"),
                  Dr("porc_honayu"),
                  Dr("porc_dersal"),
                  Dr("porc_dersalesp"),
                  Dr("porc_matsum"),
                  Dr("porc_honper"),
                  Dr("porc_paqquir")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listexc_quirurjicas
  End Function

End Class