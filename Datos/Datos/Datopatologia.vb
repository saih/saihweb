Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common

Public Class Datopatologia
   Inherits conexion

  Public Function Insertar(ByVal patologia As patologia)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = patologia.codigo
      paramcodigo.ParameterName = "codigo"
      parametros.Add(paramcodigo)
      Dim paramnombre As DbParameter = cmd.CreateParameter()
      paramnombre.Value = patologia.nombre
      paramnombre.ParameterName = "nombre"
      parametros.Add(paramnombre)
      Dim paramaplica_sexo As DbParameter = cmd.CreateParameter()
      paramaplica_sexo.Value = patologia.aplica_sexo
      paramaplica_sexo.ParameterName = "aplica_sexo"
      parametros.Add(paramaplica_sexo)
      Dim paramedad_inicial As DbParameter = cmd.CreateParameter()
      paramedad_inicial.Value = patologia.edad_inicial
      paramedad_inicial.ParameterName = "edad_inicial"
      parametros.Add(paramedad_inicial)
      Dim paramedad_final As DbParameter = cmd.CreateParameter()
      paramedad_final.Value = patologia.edad_final
      paramedad_final.ParameterName = "edad_final"
      parametros.Add(paramedad_final)
      Dim paramind_patep As DbParameter = cmd.CreateParameter()
      paramind_patep.Value = patologia.ind_patep
      paramind_patep.ParameterName = "ind_patep"
      parametros.Add(paramind_patep)
      Return ejecutaNonQuery("camasinsertapatologia", parametros)
  End Function

  Public Function InsertarRet(ByVal patologia As patologia)
      Dim dato As Integer = 0
      Try
          cnn = conecta()
          cmd = New SqlCommand("camasinsertapatologia")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
        Dim paramcodigo As DbParameter = cmd.CreateParameter()
        paramcodigo.Value = patologia.codigo
        paramcodigo.ParameterName = "codigo"
        cmd.Parameters.Add(paramcodigo)
        Dim paramnombre As DbParameter = cmd.CreateParameter()
        paramnombre.Value = patologia.nombre
        paramnombre.ParameterName = "nombre"
        cmd.Parameters.Add(paramnombre)
        Dim paramaplica_sexo As DbParameter = cmd.CreateParameter()
        paramaplica_sexo.Value = patologia.aplica_sexo
        paramaplica_sexo.ParameterName = "aplica_sexo"
        cmd.Parameters.Add(paramaplica_sexo)
        Dim paramedad_inicial As DbParameter = cmd.CreateParameter()
        paramedad_inicial.Value = patologia.edad_inicial
        paramedad_inicial.ParameterName = "edad_inicial"
        cmd.Parameters.Add(paramedad_inicial)
        Dim paramedad_final As DbParameter = cmd.CreateParameter()
        paramedad_final.Value = patologia.edad_final
        paramedad_final.ParameterName = "edad_final"
        cmd.Parameters.Add(paramedad_final)
        Dim paramind_patep As DbParameter = cmd.CreateParameter()
        paramind_patep.Value = patologia.ind_patep
        paramind_patep.ParameterName = "ind_patep"
        cmd.Parameters.Add(paramind_patep)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
            dato = Dr(0)
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return dato
  End Function

  Public Function Editar(ByVal patologia As patologia)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = patologia.codigo
      paramcodigo.ParameterName = "codigo"
      parametros.Add(paramcodigo)
      Dim paramnombre As DbParameter = cmd.CreateParameter()
      paramnombre.Value = patologia.nombre
      paramnombre.ParameterName = "nombre"
      parametros.Add(paramnombre)
      Dim paramaplica_sexo As DbParameter = cmd.CreateParameter()
      paramaplica_sexo.Value = patologia.aplica_sexo
      paramaplica_sexo.ParameterName = "aplica_sexo"
      parametros.Add(paramaplica_sexo)
      Dim paramedad_inicial As DbParameter = cmd.CreateParameter()
      paramedad_inicial.Value = patologia.edad_inicial
      paramedad_inicial.ParameterName = "edad_inicial"
      parametros.Add(paramedad_inicial)
      Dim paramedad_final As DbParameter = cmd.CreateParameter()
      paramedad_final.Value = patologia.edad_final
      paramedad_final.ParameterName = "edad_final"
      parametros.Add(paramedad_final)
      Dim paramind_patep As DbParameter = cmd.CreateParameter()
      paramind_patep.Value = patologia.ind_patep
      paramind_patep.ParameterName = "ind_patep"
      parametros.Add(paramind_patep)
      Return ejecutaNonQuery("camascambiarpatologia", parametros)
  End Function

  Public Function Borrar(
      ByVal codigo As string)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = codigo
      paramcodigo.ParameterName = "codigo"
      parametros.Add(paramcodigo)
      Return ejecutaNonQuery("camaseliminapatologia", parametros)
  End Function

  Public Function Select_Where_patologia(ByVal Cond As String) As List(Of patologia)
      Dim Listpatologia As New List(Of patologia)
      Try
          cnn = conecta()
          cmd = New SqlCommand("camasobtenerpatologiaByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listpatologia.Add(New patologia(
                  Dr("codigo"),
                  Dr("nombre"),
                  Dr("aplica_sexo"),
                  Dr("edad_inicial"),
                  Dr("edad_final"),
                  Dr("ind_patep")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listpatologia
  End Function

  Public Function Select_ById_patologia(
      ByVal codigo As string) As List(Of patologia)
      Dim Listpatologia As New List(Of patologia)
      Try
          cnn = conecta()
          cmd = New SqlCommand("camasobtenerpatologiaById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = codigo
      paramcodigo.ParameterName = "codigo"
      cmd.Parameters.Add(paramcodigo)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listpatologia.Add(New patologia(
                  Dr("codigo"),
                  Dr("nombre"),
                  Dr("aplica_sexo"),
                  Dr("edad_inicial"),
                  Dr("edad_final"),
                  Dr("ind_patep")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listpatologia
  End Function

  Public Function Select_patologia() As List(Of patologia)
      Dim Listpatologia As New List(Of patologia)
      Try
         cnn = conecta()
          cmd = New SqlCommand("camasobtenerpatologia")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listpatologia.Add(New patologia(
                  Dr("codigo"),
                  Dr("nombre"),
                  Dr("aplica_sexo"),
                  Dr("edad_inicial"),
                  Dr("edad_final"),
                  Dr("ind_patep")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listpatologia
  End Function

End Class