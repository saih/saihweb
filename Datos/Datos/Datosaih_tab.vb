Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Configuration

Public Class Datosaih_tab
   Inherits conexion

  Public Function Insertar(ByVal saih_tab As saih_tab)
      Dim parametros As New List(Of DbParameter)
      Dim paramdominio As DbParameter = cmd.CreateParameter()
      paramdominio.Value = saih_tab.dominio
      paramdominio.ParameterName = "dominio"
      parametros.Add(paramdominio)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = saih_tab.codigo
      paramcodigo.ParameterName = "codigo"
      parametros.Add(paramcodigo)
      Dim paramnombre As DbParameter = cmd.CreateParameter()
      paramnombre.Value = saih_tab.nombre
      paramnombre.ParameterName = "nombre"
      parametros.Add(paramnombre)
      Dim paramvalor As DbParameter = cmd.CreateParameter()
      paramvalor.Value = saih_tab.valor
      paramvalor.ParameterName = "valor"
      parametros.Add(paramvalor)
      Return ejecutaNonQuery("insertasaih_tab", parametros)
  End Function

  Public Function InsertarRet(ByVal saih_tab As saih_tab)
        Dim dato As Integer = 0
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("012")

      Try
          cnn = conecta()
          cmd = New SqlCommand("insertasaih_tab")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
        Dim paramdominio As DbParameter = cmd.CreateParameter()
        paramdominio.Value = saih_tab.dominio
        paramdominio.ParameterName = "dominio"
        cmd.Parameters.Add(paramdominio)
        Dim paramcodigo As DbParameter = cmd.CreateParameter()
        paramcodigo.Value = saih_tab.codigo
        paramcodigo.ParameterName = "codigo"
        cmd.Parameters.Add(paramcodigo)
        Dim paramnombre As DbParameter = cmd.CreateParameter()
        paramnombre.Value = saih_tab.nombre
        paramnombre.ParameterName = "nombre"
        cmd.Parameters.Add(paramnombre)
        Dim paramvalor As DbParameter = cmd.CreateParameter()
        paramvalor.Value = saih_tab.valor
        paramvalor.ParameterName = "valor"
        cmd.Parameters.Add(paramvalor)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
            dato = Dr(0)
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return dato
  End Function

  Public Function Editar(ByVal saih_tab As saih_tab)
      Dim parametros As New List(Of DbParameter)
      Dim paramdominio As DbParameter = cmd.CreateParameter()
      paramdominio.Value = saih_tab.dominio
      paramdominio.ParameterName = "dominio"
      parametros.Add(paramdominio)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = saih_tab.codigo
      paramcodigo.ParameterName = "codigo"
      parametros.Add(paramcodigo)
      Dim paramnombre As DbParameter = cmd.CreateParameter()
      paramnombre.Value = saih_tab.nombre
      paramnombre.ParameterName = "nombre"
      parametros.Add(paramnombre)
      Dim paramvalor As DbParameter = cmd.CreateParameter()
      paramvalor.Value = saih_tab.valor
      paramvalor.ParameterName = "valor"
      parametros.Add(paramvalor)
      Dim paramcsc As DbParameter = cmd.CreateParameter()
      paramcsc.Value = saih_tab.csc
      paramcsc.ParameterName = "csc"
      parametros.Add(paramcsc)
      Return ejecutaNonQuery("cambiarsaih_tab", parametros)
  End Function

  Public Function Borrar(
      ByVal dominio As string,
      ByVal codigo As string)
      Dim parametros As New List(Of DbParameter)
      Dim paramdominio As DbParameter = cmd.CreateParameter()
      paramdominio.Value = dominio
      paramdominio.ParameterName = "dominio"
      parametros.Add(paramdominio)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = codigo
      paramcodigo.ParameterName = "codigo"
      parametros.Add(paramcodigo)
      Return ejecutaNonQuery("eliminasaih_tab", parametros)
  End Function

  Public Function Select_Where_saih_tab(ByVal Cond As String) As List(Of saih_tab)
        Dim Listsaih_tab As New List(Of saih_tab)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("013")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_tabByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_tab.Add(New saih_tab(
                  Dr("dominio"),
                  Dr("codigo"),
                  Dr("nombre"),
                  Dr("valor"),
                  Dr("csc")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_tab
  End Function

  Public Function Select_ById_saih_tab(
      ByVal dominio As string,
      ByVal codigo As string) As List(Of saih_tab)
        Dim Listsaih_tab As New List(Of saih_tab)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("013")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenersaih_tabById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramdominio As DbParameter = cmd.CreateParameter()
      paramdominio.Value = dominio
      paramdominio.ParameterName = "dominio"
      cmd.Parameters.Add(paramdominio)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = codigo
      paramcodigo.ParameterName = "codigo"
      cmd.Parameters.Add(paramcodigo)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_tab.Add(New saih_tab(
                  Dr("dominio"),
                  Dr("codigo"),
                  Dr("nombre"),
                  Dr("valor"),
                  Dr("csc")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_tab
  End Function

  Public Function Select_saih_tab() As List(Of saih_tab)
        Dim Listsaih_tab As New List(Of saih_tab)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("013")

      Try
         cnn = conecta()
          cmd = New SqlCommand("obtenersaih_tab")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listsaih_tab.Add(New saih_tab(
                  Dr("dominio"),
                  Dr("codigo"),
                  Dr("nombre"),
                  Dr("valor"),
                  Dr("csc")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listsaih_tab
  End Function

End Class