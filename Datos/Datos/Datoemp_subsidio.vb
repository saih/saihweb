Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common

Public Class Datoemp_subsidio
   Inherits conexion

  Public Function Insertar(ByVal emp_subsidio As emp_subsidio)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodemp As DbParameter = cmd.CreateParameter()
      paramcodemp.Value = emp_subsidio.codemp
      paramcodemp.ParameterName = "codemp"
      parametros.Add(paramcodemp)
      Dim paramestrato As DbParameter = cmd.CreateParameter()
      paramestrato.Value = emp_subsidio.estrato
      paramestrato.ParameterName = "estrato"
      parametros.Add(paramestrato)
      Dim paramsubsidio As DbParameter = cmd.CreateParameter()
      paramsubsidio.Value = emp_subsidio.subsidio
      paramsubsidio.ParameterName = "subsidio"
      parametros.Add(paramsubsidio)
      Return ejecutaNonQuery("insertaemp_subsidio", parametros)
  End Function

  Public Function InsertarRet(ByVal emp_subsidio As emp_subsidio)
      Dim dato As Integer = 0
      Try
          cnn = conecta()
          cmd = New SqlCommand("insertaemp_subsidio")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
        Dim paramcodemp As DbParameter = cmd.CreateParameter()
        paramcodemp.Value = emp_subsidio.codemp
        paramcodemp.ParameterName = "codemp"
        cmd.Parameters.Add(paramcodemp)
        Dim paramestrato As DbParameter = cmd.CreateParameter()
        paramestrato.Value = emp_subsidio.estrato
        paramestrato.ParameterName = "estrato"
        cmd.Parameters.Add(paramestrato)
        Dim paramsubsidio As DbParameter = cmd.CreateParameter()
        paramsubsidio.Value = emp_subsidio.subsidio
        paramsubsidio.ParameterName = "subsidio"
        cmd.Parameters.Add(paramsubsidio)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
            dato = Dr(0)
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return dato
  End Function

  Public Function Editar(ByVal emp_subsidio As emp_subsidio)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodemp As DbParameter = cmd.CreateParameter()
      paramcodemp.Value = emp_subsidio.codemp
      paramcodemp.ParameterName = "codemp"
      parametros.Add(paramcodemp)
      Dim paramestrato As DbParameter = cmd.CreateParameter()
      paramestrato.Value = emp_subsidio.estrato
      paramestrato.ParameterName = "estrato"
      parametros.Add(paramestrato)
      Dim paramsubsidio As DbParameter = cmd.CreateParameter()
      paramsubsidio.Value = emp_subsidio.subsidio
      paramsubsidio.ParameterName = "subsidio"
      parametros.Add(paramsubsidio)
      Return ejecutaNonQuery("cambiaremp_subsidio", parametros)
  End Function

  Public Function Borrar(
      ByVal codemp As string,
      ByVal estrato As string)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodemp As DbParameter = cmd.CreateParameter()
      paramcodemp.Value = codemp
      paramcodemp.ParameterName = "codemp"
      parametros.Add(paramcodemp)
      Dim paramestrato As DbParameter = cmd.CreateParameter()
      paramestrato.Value = estrato
      paramestrato.ParameterName = "estrato"
      parametros.Add(paramestrato)
      Return ejecutaNonQuery("eliminaemp_subsidio", parametros)
  End Function

  Public Function Select_Where_emp_subsidio(ByVal Cond As String) As List(Of emp_subsidio)
      Dim Listemp_subsidio As New List(Of emp_subsidio)
      Try
          cnn = conecta()
          cmd = New SqlCommand("obteneremp_subsidioByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listemp_subsidio.Add(New emp_subsidio(
                  Dr("codemp"),
                  Dr("estrato"),
                  Dr("subsidio")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listemp_subsidio
  End Function

  Public Function Select_ById_emp_subsidio(
      ByVal codemp As string,
      ByVal estrato As string) As List(Of emp_subsidio)
      Dim Listemp_subsidio As New List(Of emp_subsidio)
      Try
          cnn = conecta()
          cmd = New SqlCommand("obteneremp_subsidioById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramcodemp As DbParameter = cmd.CreateParameter()
      paramcodemp.Value = codemp
      paramcodemp.ParameterName = "codemp"
      cmd.Parameters.Add(paramcodemp)
      Dim paramestrato As DbParameter = cmd.CreateParameter()
      paramestrato.Value = estrato
      paramestrato.ParameterName = "estrato"
      cmd.Parameters.Add(paramestrato)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listemp_subsidio.Add(New emp_subsidio(
                  Dr("codemp"),
                  Dr("estrato"),
                  Dr("subsidio")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listemp_subsidio
  End Function

  Public Function Select_emp_subsidio() As List(Of emp_subsidio)
      Dim Listemp_subsidio As New List(Of emp_subsidio)
      Try
         cnn = conecta()
          cmd = New SqlCommand("obteneremp_subsidio")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listemp_subsidio.Add(New emp_subsidio(
                  Dr("codemp"),
                  Dr("estrato"),
                  Dr("subsidio")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listemp_subsidio
  End Function

End Class