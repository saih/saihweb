Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common

Public Class Datocodtar_gq
   Inherits conexion

  Public Function Insertar(ByVal codtar_gq As codtar_gq)
      Dim parametros As New List(Of DbParameter)
      Dim parammanual As DbParameter = cmd.CreateParameter()
      parammanual.Value = codtar_gq.manual
      parammanual.ParameterName = "manual"
      parametros.Add(parammanual)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = codtar_gq.codigo
      paramcodigo.ParameterName = "codigo"
      parametros.Add(paramcodigo)
      Dim paramnombre As DbParameter = cmd.CreateParameter()
      paramnombre.Value = codtar_gq.nombre
      paramnombre.ParameterName = "nombre"
      parametros.Add(paramnombre)
      Dim paramhonmed As DbParameter = cmd.CreateParameter()
      paramhonmed.Value = codtar_gq.honmed
      paramhonmed.ParameterName = "honmed"
      parametros.Add(paramhonmed)
      Dim paramhonane As DbParameter = cmd.CreateParameter()
      paramhonane.Value = codtar_gq.honane
      paramhonane.ParameterName = "honane"
      parametros.Add(paramhonane)
      Dim paramdersal As DbParameter = cmd.CreateParameter()
      paramdersal.Value = codtar_gq.dersal
      paramdersal.ParameterName = "dersal"
      parametros.Add(paramdersal)
      Dim paramdersalesp As DbParameter = cmd.CreateParameter()
      paramdersalesp.Value = codtar_gq.dersalesp
      paramdersalesp.ParameterName = "dersalesp"
      parametros.Add(paramdersalesp)
      Dim paramdersalend As DbParameter = cmd.CreateParameter()
      paramdersalend.Value = codtar_gq.dersalend
      paramdersalend.ParameterName = "dersalend"
      parametros.Add(paramdersalend)
      Dim parammatsum As DbParameter = cmd.CreateParameter()
      parammatsum.Value = codtar_gq.matsum
      parammatsum.ParameterName = "matsum"
      parametros.Add(parammatsum)
      Dim parammataneloc As DbParameter = cmd.CreateParameter()
      parammataneloc.Value = codtar_gq.mataneloc
      parammataneloc.ParameterName = "mataneloc"
      parametros.Add(parammataneloc)
      Dim paramhonayu As DbParameter = cmd.CreateParameter()
      paramhonayu.Value = codtar_gq.honayu
      paramhonayu.ParameterName = "honayu"
      parametros.Add(paramhonayu)
      Dim paramhonper As DbParameter = cmd.CreateParameter()
      paramhonper.Value = codtar_gq.honper
      paramhonper.ParameterName = "honper"
      parametros.Add(paramhonper)
      Return ejecutaNonQuery("camasinsertacodtar_gq", parametros)
  End Function

  Public Function InsertarRet(ByVal codtar_gq As codtar_gq)
      Dim dato As Integer = 0
      Try
          cnn = conecta()
          cmd = New SqlCommand("camasinsertacodtar_gq")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
        Dim parammanual As DbParameter = cmd.CreateParameter()
        parammanual.Value = codtar_gq.manual
        parammanual.ParameterName = "manual"
        cmd.Parameters.Add(parammanual)
        Dim paramcodigo As DbParameter = cmd.CreateParameter()
        paramcodigo.Value = codtar_gq.codigo
        paramcodigo.ParameterName = "codigo"
        cmd.Parameters.Add(paramcodigo)
        Dim paramnombre As DbParameter = cmd.CreateParameter()
        paramnombre.Value = codtar_gq.nombre
        paramnombre.ParameterName = "nombre"
        cmd.Parameters.Add(paramnombre)
        Dim paramhonmed As DbParameter = cmd.CreateParameter()
        paramhonmed.Value = codtar_gq.honmed
        paramhonmed.ParameterName = "honmed"
        cmd.Parameters.Add(paramhonmed)
        Dim paramhonane As DbParameter = cmd.CreateParameter()
        paramhonane.Value = codtar_gq.honane
        paramhonane.ParameterName = "honane"
        cmd.Parameters.Add(paramhonane)
        Dim paramdersal As DbParameter = cmd.CreateParameter()
        paramdersal.Value = codtar_gq.dersal
        paramdersal.ParameterName = "dersal"
        cmd.Parameters.Add(paramdersal)
        Dim paramdersalesp As DbParameter = cmd.CreateParameter()
        paramdersalesp.Value = codtar_gq.dersalesp
        paramdersalesp.ParameterName = "dersalesp"
        cmd.Parameters.Add(paramdersalesp)
        Dim paramdersalend As DbParameter = cmd.CreateParameter()
        paramdersalend.Value = codtar_gq.dersalend
        paramdersalend.ParameterName = "dersalend"
        cmd.Parameters.Add(paramdersalend)
        Dim parammatsum As DbParameter = cmd.CreateParameter()
        parammatsum.Value = codtar_gq.matsum
        parammatsum.ParameterName = "matsum"
        cmd.Parameters.Add(parammatsum)
        Dim parammataneloc As DbParameter = cmd.CreateParameter()
        parammataneloc.Value = codtar_gq.mataneloc
        parammataneloc.ParameterName = "mataneloc"
        cmd.Parameters.Add(parammataneloc)
        Dim paramhonayu As DbParameter = cmd.CreateParameter()
        paramhonayu.Value = codtar_gq.honayu
        paramhonayu.ParameterName = "honayu"
        cmd.Parameters.Add(paramhonayu)
        Dim paramhonper As DbParameter = cmd.CreateParameter()
        paramhonper.Value = codtar_gq.honper
        paramhonper.ParameterName = "honper"
        cmd.Parameters.Add(paramhonper)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
            dato = Dr(0)
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return dato
  End Function

  Public Function Editar(ByVal codtar_gq As codtar_gq)
      Dim parametros As New List(Of DbParameter)
      Dim parammanual As DbParameter = cmd.CreateParameter()
      parammanual.Value = codtar_gq.manual
      parammanual.ParameterName = "manual"
      parametros.Add(parammanual)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = codtar_gq.codigo
      paramcodigo.ParameterName = "codigo"
      parametros.Add(paramcodigo)
      Dim paramnombre As DbParameter = cmd.CreateParameter()
      paramnombre.Value = codtar_gq.nombre
      paramnombre.ParameterName = "nombre"
      parametros.Add(paramnombre)
      Dim paramhonmed As DbParameter = cmd.CreateParameter()
      paramhonmed.Value = codtar_gq.honmed
      paramhonmed.ParameterName = "honmed"
      parametros.Add(paramhonmed)
      Dim paramhonane As DbParameter = cmd.CreateParameter()
      paramhonane.Value = codtar_gq.honane
      paramhonane.ParameterName = "honane"
      parametros.Add(paramhonane)
      Dim paramdersal As DbParameter = cmd.CreateParameter()
      paramdersal.Value = codtar_gq.dersal
      paramdersal.ParameterName = "dersal"
      parametros.Add(paramdersal)
      Dim paramdersalesp As DbParameter = cmd.CreateParameter()
      paramdersalesp.Value = codtar_gq.dersalesp
      paramdersalesp.ParameterName = "dersalesp"
      parametros.Add(paramdersalesp)
      Dim paramdersalend As DbParameter = cmd.CreateParameter()
      paramdersalend.Value = codtar_gq.dersalend
      paramdersalend.ParameterName = "dersalend"
      parametros.Add(paramdersalend)
      Dim parammatsum As DbParameter = cmd.CreateParameter()
      parammatsum.Value = codtar_gq.matsum
      parammatsum.ParameterName = "matsum"
      parametros.Add(parammatsum)
      Dim parammataneloc As DbParameter = cmd.CreateParameter()
      parammataneloc.Value = codtar_gq.mataneloc
      parammataneloc.ParameterName = "mataneloc"
      parametros.Add(parammataneloc)
      Dim paramhonayu As DbParameter = cmd.CreateParameter()
      paramhonayu.Value = codtar_gq.honayu
      paramhonayu.ParameterName = "honayu"
      parametros.Add(paramhonayu)
      Dim paramhonper As DbParameter = cmd.CreateParameter()
      paramhonper.Value = codtar_gq.honper
      paramhonper.ParameterName = "honper"
      parametros.Add(paramhonper)
      Return ejecutaNonQuery("camascambiarcodtar_gq", parametros)
  End Function

  Public Function Borrar(
      ByVal manual As string,
      ByVal codigo As string)
      Dim parametros As New List(Of DbParameter)
      Dim parammanual As DbParameter = cmd.CreateParameter()
      parammanual.Value = manual
      parammanual.ParameterName = "manual"
      parametros.Add(parammanual)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = codigo
      paramcodigo.ParameterName = "codigo"
      parametros.Add(paramcodigo)
      Return ejecutaNonQuery("camaseliminacodtar_gq", parametros)
  End Function

  Public Function Select_Where_codtar_gq(ByVal Cond As String) As List(Of codtar_gq)
      Dim Listcodtar_gq As New List(Of codtar_gq)
      Try
          cnn = conecta()
          cmd = New SqlCommand("camasobtenercodtar_gqByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listcodtar_gq.Add(New codtar_gq(
                  Dr("manual"),
                  Dr("codigo"),
                  Dr("nombre"),
                  Dr("honmed"),
                  Dr("honane"),
                  Dr("dersal"),
                  Dr("dersalesp"),
                  Dr("dersalend"),
                  Dr("matsum"),
                  Dr("mataneloc"),
                  Dr("honayu"),
                  Dr("honper")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listcodtar_gq
  End Function

  Public Function Select_ById_codtar_gq(
      ByVal manual As string,
      ByVal codigo As string) As List(Of codtar_gq)
      Dim Listcodtar_gq As New List(Of codtar_gq)
      Try
          cnn = conecta()
          cmd = New SqlCommand("camasobtenercodtar_gqById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim parammanual As DbParameter = cmd.CreateParameter()
      parammanual.Value = manual
      parammanual.ParameterName = "manual"
      cmd.Parameters.Add(parammanual)
      Dim paramcodigo As DbParameter = cmd.CreateParameter()
      paramcodigo.Value = codigo
      paramcodigo.ParameterName = "codigo"
      cmd.Parameters.Add(paramcodigo)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listcodtar_gq.Add(New codtar_gq(
                  Dr("manual"),
                  Dr("codigo"),
                  Dr("nombre"),
                  Dr("honmed"),
                  Dr("honane"),
                  Dr("dersal"),
                  Dr("dersalesp"),
                  Dr("dersalend"),
                  Dr("matsum"),
                  Dr("mataneloc"),
                  Dr("honayu"),
                  Dr("honper")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listcodtar_gq
  End Function

  Public Function Select_codtar_gq() As List(Of codtar_gq)
      Dim Listcodtar_gq As New List(Of codtar_gq)
      Try
         cnn = conecta()
          cmd = New SqlCommand("camasobtenercodtar_gq")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listcodtar_gq.Add(New codtar_gq(
                  Dr("manual"),
                  Dr("codigo"),
                  Dr("nombre"),
                  Dr("honmed"),
                  Dr("honane"),
                  Dr("dersal"),
                  Dr("dersalesp"),
                  Dr("dersalend"),
                  Dr("matsum"),
                  Dr("mataneloc"),
                  Dr("honayu"),
                  Dr("honper")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listcodtar_gq
  End Function

End Class