Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common

Public Class Datocajero
   Inherits conexion

  Public Function Insertar(ByVal cajero As cajero)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodigo_cajero As DbParameter = cmd.CreateParameter()
      paramcodigo_cajero.Value = cajero.codigo_cajero
      paramcodigo_cajero.ParameterName = "codigo_cajero"
      parametros.Add(paramcodigo_cajero)
      Dim paramusuario As DbParameter = cmd.CreateParameter()
      paramusuario.Value = cajero.usuario
      paramusuario.ParameterName = "usuario"
      parametros.Add(paramusuario)
      Dim parambase As DbParameter = cmd.CreateParameter()
      parambase.Value = cajero.base
      parambase.ParameterName = "base"
      parametros.Add(parambase)
      Dim paramfecha_apertura As DbParameter = cmd.CreateParameter()
      paramfecha_apertura.Value = cajero.fecha_apertura
      paramfecha_apertura.ParameterName = "fecha_apertura"
      parametros.Add(paramfecha_apertura)
      Dim paramsaldo As DbParameter = cmd.CreateParameter()
      paramsaldo.Value = cajero.saldo
      paramsaldo.ParameterName = "saldo"
      parametros.Add(paramsaldo)
      Dim paramestado As DbParameter = cmd.CreateParameter()
      paramestado.Value = cajero.estado
      paramestado.ParameterName = "estado"
      parametros.Add(paramestado)
      Return ejecutaNonQuery("camasinsertacajero", parametros)
  End Function

  Public Function InsertarRet(ByVal cajero As cajero)
      Dim dato As Integer = 0
      Try
          cnn = conecta()
          cmd = New SqlCommand("camasinsertacajero")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
        Dim paramcodigo_cajero As DbParameter = cmd.CreateParameter()
        paramcodigo_cajero.Value = cajero.codigo_cajero
        paramcodigo_cajero.ParameterName = "codigo_cajero"
        cmd.Parameters.Add(paramcodigo_cajero)
        Dim paramusuario As DbParameter = cmd.CreateParameter()
        paramusuario.Value = cajero.usuario
        paramusuario.ParameterName = "usuario"
        cmd.Parameters.Add(paramusuario)
        Dim parambase As DbParameter = cmd.CreateParameter()
        parambase.Value = cajero.base
        parambase.ParameterName = "base"
        cmd.Parameters.Add(parambase)
        Dim paramfecha_apertura As DbParameter = cmd.CreateParameter()
        paramfecha_apertura.Value = cajero.fecha_apertura
        paramfecha_apertura.ParameterName = "fecha_apertura"
        cmd.Parameters.Add(paramfecha_apertura)
        Dim paramsaldo As DbParameter = cmd.CreateParameter()
        paramsaldo.Value = cajero.saldo
        paramsaldo.ParameterName = "saldo"
        cmd.Parameters.Add(paramsaldo)
        Dim paramestado As DbParameter = cmd.CreateParameter()
        paramestado.Value = cajero.estado
        paramestado.ParameterName = "estado"
        cmd.Parameters.Add(paramestado)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
            dato = Dr(0)
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return dato
  End Function

  Public Function Editar(ByVal cajero As cajero)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodigo_cajero As DbParameter = cmd.CreateParameter()
      paramcodigo_cajero.Value = cajero.codigo_cajero
      paramcodigo_cajero.ParameterName = "codigo_cajero"
      parametros.Add(paramcodigo_cajero)
      Dim paramusuario As DbParameter = cmd.CreateParameter()
      paramusuario.Value = cajero.usuario
      paramusuario.ParameterName = "usuario"
      parametros.Add(paramusuario)
      Dim parambase As DbParameter = cmd.CreateParameter()
      parambase.Value = cajero.base
      parambase.ParameterName = "base"
      parametros.Add(parambase)
      Dim paramfecha_apertura As DbParameter = cmd.CreateParameter()
      paramfecha_apertura.Value = cajero.fecha_apertura
      paramfecha_apertura.ParameterName = "fecha_apertura"
      parametros.Add(paramfecha_apertura)
      Dim paramsaldo As DbParameter = cmd.CreateParameter()
      paramsaldo.Value = cajero.saldo
      paramsaldo.ParameterName = "saldo"
      parametros.Add(paramsaldo)
      Dim paramestado As DbParameter = cmd.CreateParameter()
      paramestado.Value = cajero.estado
      paramestado.ParameterName = "estado"
      parametros.Add(paramestado)
      Return ejecutaNonQuery("camascambiarcajero", parametros)
  End Function

  Public Function Borrar(
      ByVal codigo_cajero As string)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodigo_cajero As DbParameter = cmd.CreateParameter()
      paramcodigo_cajero.Value = codigo_cajero
      paramcodigo_cajero.ParameterName = "codigo_cajero"
      parametros.Add(paramcodigo_cajero)
      Return ejecutaNonQuery("camaseliminacajero", parametros)
  End Function

  Public Function Select_Where_cajero(ByVal Cond As String) As List(Of cajero)
      Dim Listcajero As New List(Of cajero)
      Try
          cnn = conecta()
          cmd = New SqlCommand("camasobtenercajeroByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listcajero.Add(New cajero(
                  Dr("codigo_cajero"),
                  Dr("usuario"),
                  Dr("base"),
                  Dr("fecha_apertura"),
                  Dr("saldo"),
                  Dr("estado")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listcajero
  End Function

  Public Function Select_ById_cajero(
      ByVal codigo_cajero As string) As List(Of cajero)
      Dim Listcajero As New List(Of cajero)
      Try
          cnn = conecta()
          cmd = New SqlCommand("camasobtenercajeroById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramcodigo_cajero As DbParameter = cmd.CreateParameter()
      paramcodigo_cajero.Value = codigo_cajero
      paramcodigo_cajero.ParameterName = "codigo_cajero"
      cmd.Parameters.Add(paramcodigo_cajero)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listcajero.Add(New cajero(
                  Dr("codigo_cajero"),
                  Dr("usuario"),
                  Dr("base"),
                  Dr("fecha_apertura"),
                  Dr("saldo"),
                  Dr("estado")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listcajero
  End Function

  Public Function Select_cajero() As List(Of cajero)
      Dim Listcajero As New List(Of cajero)
      Try
         cnn = conecta()
          cmd = New SqlCommand("camasobtenercajero")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listcajero.Add(New cajero(
                  Dr("codigo_cajero"),
                  Dr("usuario"),
                  Dr("base"),
                  Dr("fecha_apertura"),
                  Dr("saldo"),
                  Dr("estado")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listcajero
  End Function

End Class