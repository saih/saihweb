Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Configuration

Public Class DatoUsuario
   Inherits conexion

  Public Function Insertar(ByVal Usuario As Usuario)
      Dim parametros As New List(Of DbParameter)
      Dim paramUsuario As DbParameter = cmd.CreateParameter()
      paramUsuario.Value = Usuario.Usuario
      paramUsuario.ParameterName = "Usuario"
      parametros.Add(paramUsuario)
      Dim paramPassword As DbParameter = cmd.CreateParameter()
      paramPassword.Value = Usuario.Password
      paramPassword.ParameterName = "Password"
      parametros.Add(paramPassword)
      Dim paramNombre1 As DbParameter = cmd.CreateParameter()
      paramNombre1.Value = Usuario.Nombre1
      paramNombre1.ParameterName = "Nombre1"
      parametros.Add(paramNombre1)
      Dim paramNombre2 As DbParameter = cmd.CreateParameter()
      paramNombre2.Value = Usuario.Nombre2
      paramNombre2.ParameterName = "Nombre2"
      parametros.Add(paramNombre2)
      Dim paramApellido1 As DbParameter = cmd.CreateParameter()
      paramApellido1.Value = Usuario.Apellido1
      paramApellido1.ParameterName = "Apellido1"
      parametros.Add(paramApellido1)
      Dim paramApellido2 As DbParameter = cmd.CreateParameter()
      paramApellido2.Value = Usuario.Apellido2
      paramApellido2.ParameterName = "Apellido2"
      parametros.Add(paramApellido2)
      Dim paramCorreo As DbParameter = cmd.CreateParameter()
      paramCorreo.Value = Usuario.Correo
      paramCorreo.ParameterName = "Correo"
      parametros.Add(paramCorreo)
      Dim paramEstado As DbParameter = cmd.CreateParameter()
      paramEstado.Value = Usuario.Estado
      paramEstado.ParameterName = "Estado"
      parametros.Add(paramEstado)
      Dim paramIdentificacion As DbParameter = cmd.CreateParameter()
      paramIdentificacion.Value = Usuario.Identificacion
      paramIdentificacion.ParameterName = "Identificacion"
      parametros.Add(paramIdentificacion)
      Dim paramidperfil As DbParameter = cmd.CreateParameter()
      paramidperfil.Value = Usuario.idperfil
      paramidperfil.ParameterName = "idperfil"
      parametros.Add(paramidperfil)
      Dim paramrazonsocial As DbParameter = cmd.CreateParameter()
      paramrazonsocial.Value = Usuario.razonsocial
      paramrazonsocial.ParameterName = "razonsocial"
        parametros.Add(paramrazonsocial)
        Dim paramcod_pas As DbParameter = cmd.CreateParameter()
        paramcod_pas.Value = Usuario.cod_pas
        paramcod_pas.ParameterName = "cod_pas"
        parametros.Add(paramcod_pas)
      Return ejecutaNonQuery("insertaUsuario", parametros)
  End Function

  Public Function InsertarRet(ByVal Usuario As Usuario)
        Dim dato As Integer = 0
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("006")

      Try
          cnn = conecta()
          cmd = New SqlCommand("insertaUsuario")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
        Dim paramUsuario As DbParameter = cmd.CreateParameter()
        paramUsuario.Value = Usuario.Usuario
        paramUsuario.ParameterName = "Usuario"
        cmd.Parameters.Add(paramUsuario)
        Dim paramPassword As DbParameter = cmd.CreateParameter()
        paramPassword.Value = Usuario.Password
        paramPassword.ParameterName = "Password"
        cmd.Parameters.Add(paramPassword)
        Dim paramNombre1 As DbParameter = cmd.CreateParameter()
        paramNombre1.Value = Usuario.Nombre1
        paramNombre1.ParameterName = "Nombre1"
        cmd.Parameters.Add(paramNombre1)
        Dim paramNombre2 As DbParameter = cmd.CreateParameter()
        paramNombre2.Value = Usuario.Nombre2
        paramNombre2.ParameterName = "Nombre2"
        cmd.Parameters.Add(paramNombre2)
        Dim paramApellido1 As DbParameter = cmd.CreateParameter()
        paramApellido1.Value = Usuario.Apellido1
        paramApellido1.ParameterName = "Apellido1"
        cmd.Parameters.Add(paramApellido1)
        Dim paramApellido2 As DbParameter = cmd.CreateParameter()
        paramApellido2.Value = Usuario.Apellido2
        paramApellido2.ParameterName = "Apellido2"
        cmd.Parameters.Add(paramApellido2)
        Dim paramCorreo As DbParameter = cmd.CreateParameter()
        paramCorreo.Value = Usuario.Correo
        paramCorreo.ParameterName = "Correo"
        cmd.Parameters.Add(paramCorreo)
        Dim paramEstado As DbParameter = cmd.CreateParameter()
        paramEstado.Value = Usuario.Estado
        paramEstado.ParameterName = "Estado"
        cmd.Parameters.Add(paramEstado)
        Dim paramIdentificacion As DbParameter = cmd.CreateParameter()
        paramIdentificacion.Value = Usuario.Identificacion
        paramIdentificacion.ParameterName = "Identificacion"
        cmd.Parameters.Add(paramIdentificacion)
        Dim paramidperfil As DbParameter = cmd.CreateParameter()
        paramidperfil.Value = Usuario.idperfil
        paramidperfil.ParameterName = "idperfil"
        cmd.Parameters.Add(paramidperfil)
        Dim paramrazonsocial As DbParameter = cmd.CreateParameter()
        paramrazonsocial.Value = Usuario.razonsocial
        paramrazonsocial.ParameterName = "razonsocial"
            cmd.Parameters.Add(paramrazonsocial)
            Dim paramcod_pas As DbParameter = cmd.CreateParameter()
            paramcod_pas.Value = Usuario.cod_pas
            paramcod_pas.ParameterName = "cod_pas"
            cmd.Parameters.Add(paramcod_pas)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
            dato = Dr(0)
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return dato
  End Function

  Public Function Editar(ByVal Usuario As Usuario)
      Dim parametros As New List(Of DbParameter)
      Dim paramUsuario As DbParameter = cmd.CreateParameter()
      paramUsuario.Value = Usuario.Usuario
      paramUsuario.ParameterName = "Usuario"
      parametros.Add(paramUsuario)
      Dim paramPassword As DbParameter = cmd.CreateParameter()
      paramPassword.Value = Usuario.Password
      paramPassword.ParameterName = "Password"
      parametros.Add(paramPassword)
      Dim paramNombre1 As DbParameter = cmd.CreateParameter()
      paramNombre1.Value = Usuario.Nombre1
      paramNombre1.ParameterName = "Nombre1"
      parametros.Add(paramNombre1)
      Dim paramNombre2 As DbParameter = cmd.CreateParameter()
      paramNombre2.Value = Usuario.Nombre2
      paramNombre2.ParameterName = "Nombre2"
      parametros.Add(paramNombre2)
      Dim paramApellido1 As DbParameter = cmd.CreateParameter()
      paramApellido1.Value = Usuario.Apellido1
      paramApellido1.ParameterName = "Apellido1"
      parametros.Add(paramApellido1)
      Dim paramApellido2 As DbParameter = cmd.CreateParameter()
      paramApellido2.Value = Usuario.Apellido2
      paramApellido2.ParameterName = "Apellido2"
      parametros.Add(paramApellido2)
      Dim paramCorreo As DbParameter = cmd.CreateParameter()
      paramCorreo.Value = Usuario.Correo
      paramCorreo.ParameterName = "Correo"
      parametros.Add(paramCorreo)
      Dim paramEstado As DbParameter = cmd.CreateParameter()
      paramEstado.Value = Usuario.Estado
      paramEstado.ParameterName = "Estado"
      parametros.Add(paramEstado)
      Dim paramIdentificacion As DbParameter = cmd.CreateParameter()
      paramIdentificacion.Value = Usuario.Identificacion
      paramIdentificacion.ParameterName = "Identificacion"
      parametros.Add(paramIdentificacion)
      Dim paramidperfil As DbParameter = cmd.CreateParameter()
      paramidperfil.Value = Usuario.idperfil
      paramidperfil.ParameterName = "idperfil"
      parametros.Add(paramidperfil)
      Dim paramrazonsocial As DbParameter = cmd.CreateParameter()
      paramrazonsocial.Value = Usuario.razonsocial
      paramrazonsocial.ParameterName = "razonsocial"
        parametros.Add(paramrazonsocial)
        Dim paramcod_pas As DbParameter = cmd.CreateParameter()
        paramcod_pas.Value = Usuario.cod_pas
        paramcod_pas.ParameterName = "cod_pas"
        parametros.Add(paramcod_pas)
      Return ejecutaNonQuery("cambiarUsuario", parametros)
  End Function

  Public Function Borrar(
      ByVal Usuario As string)
      Dim parametros As New List(Of DbParameter)
      Dim paramUsuario As DbParameter = cmd.CreateParameter()
      paramUsuario.Value = Usuario
      paramUsuario.ParameterName = "Usuario"
      parametros.Add(paramUsuario)
      Return ejecutaNonQuery("eliminaUsuario", parametros)
  End Function

  Public Function Select_Where_Usuario(ByVal Cond As String) As List(Of Usuario)
        Dim ListUsuario As New List(Of Usuario)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("007")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenerUsuarioByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
                ListUsuario.Add(New Usuario(
                    Dr("Usuario"),
                    Dr("Password"),
                    Dr("Nombre1"),
                    Dr("Nombre2"),
                    Dr("Apellido1"),
                    Dr("Apellido2"),
                    Dr("Correo"),
                    Dr("Estado"),
                    Dr("Identificacion"),
                    Dr("idperfil"),
                    Dr("razonsocial"),
                    Dr("cod_pas")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return ListUsuario
  End Function

  Public Function Select_ById_Usuario(
      ByVal Usuario As string) As List(Of Usuario)
        Dim ListUsuario As New List(Of Usuario)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("007")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenerUsuarioById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramUsuario As DbParameter = cmd.CreateParameter()
      paramUsuario.Value = Usuario
      paramUsuario.ParameterName = "Usuario"
      cmd.Parameters.Add(paramUsuario)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
                ListUsuario.Add(New Usuario(
                    Dr("Usuario"),
                    Dr("Password"),
                    Dr("Nombre1"),
                    Dr("Nombre2"),
                    Dr("Apellido1"),
                    Dr("Apellido2"),
                    Dr("Correo"),
                    Dr("Estado"),
                    Dr("Identificacion"),
                    Dr("idperfil"),
                    Dr("razonsocial"),
                    Dr("cod_pas")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return ListUsuario
  End Function

  Public Function Select_Usuario() As List(Of Usuario)
        Dim ListUsuario As New List(Of Usuario)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("007")

      Try
         cnn = conecta()
          cmd = New SqlCommand("obtenerUsuario")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
                ListUsuario.Add(New Usuario(
                    Dr("Usuario"),
                    Dr("Password"),
                    Dr("Nombre1"),
                    Dr("Nombre2"),
                    Dr("Apellido1"),
                    Dr("Apellido2"),
                    Dr("Correo"),
                    Dr("Estado"),
                    Dr("Identificacion"),
                    Dr("idperfil"),
                    Dr("razonsocial"),
                    Dr("cod_pas")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return ListUsuario
  End Function

End Class