Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common

Public Class Datofamilia
   Inherits conexion

  Public Function Insertar(ByVal familia As familia)
      Dim parametros As New List(Of DbParameter)
      Dim paramdescripcion As DbParameter = cmd.CreateParameter()
      paramdescripcion.Value = familia.descripcion
      paramdescripcion.ParameterName = "descripcion"
      parametros.Add(paramdescripcion)
      Dim paramfecha_adicion As DbParameter = cmd.CreateParameter()
      paramfecha_adicion.Value = familia.fecha_adicion
      paramfecha_adicion.ParameterName = "fecha_adicion"
      parametros.Add(paramfecha_adicion)
      Dim paramusuario_creo As DbParameter = cmd.CreateParameter()
      paramusuario_creo.Value = familia.usuario_creo
      paramusuario_creo.ParameterName = "usuario_creo"
      parametros.Add(paramusuario_creo)
      Return ejecutaNonQuery("insertafamilia", parametros)
  End Function

  Public Function InsertarRet(ByVal familia As familia)
      Dim dato As Integer = 0
      Try
          cnn = conecta()
          cmd = New SqlCommand("insertafamilia")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
        Dim paramdescripcion As DbParameter = cmd.CreateParameter()
        paramdescripcion.Value = familia.descripcion
        paramdescripcion.ParameterName = "descripcion"
        cmd.Parameters.Add(paramdescripcion)
        Dim paramfecha_adicion As DbParameter = cmd.CreateParameter()
        paramfecha_adicion.Value = familia.fecha_adicion
        paramfecha_adicion.ParameterName = "fecha_adicion"
        cmd.Parameters.Add(paramfecha_adicion)
        Dim paramusuario_creo As DbParameter = cmd.CreateParameter()
        paramusuario_creo.Value = familia.usuario_creo
        paramusuario_creo.ParameterName = "usuario_creo"
        cmd.Parameters.Add(paramusuario_creo)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
            dato = Dr(0)
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return dato
  End Function

  Public Function Editar(ByVal familia As familia)
      Dim parametros As New List(Of DbParameter)
      Dim paramid As DbParameter = cmd.CreateParameter()
      paramid.Value = familia.id
      paramid.ParameterName = "id"
      parametros.Add(paramid)
      Dim paramdescripcion As DbParameter = cmd.CreateParameter()
      paramdescripcion.Value = familia.descripcion
      paramdescripcion.ParameterName = "descripcion"
      parametros.Add(paramdescripcion)
      Dim paramfecha_adicion As DbParameter = cmd.CreateParameter()
      paramfecha_adicion.Value = familia.fecha_adicion
      paramfecha_adicion.ParameterName = "fecha_adicion"
      parametros.Add(paramfecha_adicion)
      Dim paramusuario_creo As DbParameter = cmd.CreateParameter()
      paramusuario_creo.Value = familia.usuario_creo
      paramusuario_creo.ParameterName = "usuario_creo"
      parametros.Add(paramusuario_creo)
      Return ejecutaNonQuery("cambiarfamilia", parametros)
  End Function

  Public Function Borrar(
      ByVal id As integer)
      Dim parametros As New List(Of DbParameter)
      Dim paramid As DbParameter = cmd.CreateParameter()
      paramid.Value = id
      paramid.ParameterName = "id"
      parametros.Add(paramid)
      Return ejecutaNonQuery("eliminafamilia", parametros)
  End Function

  Public Function Select_Where_familia(ByVal Cond As String) As List(Of familia)
      Dim Listfamilia As New List(Of familia)
      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenerfamiliaByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listfamilia.Add(New familia(
                  Dr("id"),
                  Dr("descripcion"),
                  Dr("fecha_adicion"),
                  Dr("usuario_creo")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listfamilia
  End Function

  Public Function Select_ById_familia(
      ByVal id As integer) As List(Of familia)
      Dim Listfamilia As New List(Of familia)
      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenerfamiliaById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramid As DbParameter = cmd.CreateParameter()
      paramid.Value = id
      paramid.ParameterName = "id"
      cmd.Parameters.Add(paramid)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listfamilia.Add(New familia(
                  Dr("id"),
                  Dr("descripcion"),
                  Dr("fecha_adicion"),
                  Dr("usuario_creo")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listfamilia
  End Function

  Public Function Select_familia() As List(Of familia)
      Dim Listfamilia As New List(Of familia)
      Try
         cnn = conecta()
          cmd = New SqlCommand("obtenerfamilia")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listfamilia.Add(New familia(
                  Dr("id"),
                  Dr("descripcion"),
                  Dr("fecha_adicion"),
                  Dr("usuario_creo")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listfamilia
  End Function

End Class