Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Configuration

Public Class DatoUsuarioClave
   Inherits conexion

  Public Function Insertar(ByVal UsuarioClave As UsuarioClave)
      Dim parametros As New List(Of DbParameter)
      Dim paramUsuario As DbParameter = cmd.CreateParameter()
      paramUsuario.Value = UsuarioClave.Usuario
      paramUsuario.ParameterName = "Usuario"
      parametros.Add(paramUsuario)
      Dim paramfecha As DbParameter = cmd.CreateParameter()
      paramfecha.Value = UsuarioClave.fecha
      paramfecha.ParameterName = "fecha"
      parametros.Add(paramfecha)
      Dim paramclave As DbParameter = cmd.CreateParameter()
      paramclave.Value = UsuarioClave.clave
      paramclave.ParameterName = "clave"
      parametros.Add(paramclave)
      Return ejecutaNonQuery("insertaUsuarioClave", parametros)
  End Function

  Public Function InsertarRet(ByVal UsuarioClave As UsuarioClave)
        Dim dato As Integer = 0
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("002")

      Try
          cnn = conecta()
          cmd = New SqlCommand("insertaUsuarioClave")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
        Dim paramUsuario As DbParameter = cmd.CreateParameter()
        paramUsuario.Value = UsuarioClave.Usuario
        paramUsuario.ParameterName = "Usuario"
        cmd.Parameters.Add(paramUsuario)
        Dim paramfecha As DbParameter = cmd.CreateParameter()
        paramfecha.Value = UsuarioClave.fecha
        paramfecha.ParameterName = "fecha"
        cmd.Parameters.Add(paramfecha)
        Dim paramclave As DbParameter = cmd.CreateParameter()
        paramclave.Value = UsuarioClave.clave
        paramclave.ParameterName = "clave"
        cmd.Parameters.Add(paramclave)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
            dato = Dr(0)
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return dato
  End Function

  Public Function Editar(ByVal UsuarioClave As UsuarioClave)
      Dim parametros As New List(Of DbParameter)
      Dim paramid As DbParameter = cmd.CreateParameter()
      paramid.Value = UsuarioClave.id
      paramid.ParameterName = "id"
      parametros.Add(paramid)
      Dim paramUsuario As DbParameter = cmd.CreateParameter()
      paramUsuario.Value = UsuarioClave.Usuario
      paramUsuario.ParameterName = "Usuario"
      parametros.Add(paramUsuario)
      Dim paramfecha As DbParameter = cmd.CreateParameter()
      paramfecha.Value = UsuarioClave.fecha
      paramfecha.ParameterName = "fecha"
      parametros.Add(paramfecha)
      Dim paramclave As DbParameter = cmd.CreateParameter()
      paramclave.Value = UsuarioClave.clave
      paramclave.ParameterName = "clave"
      parametros.Add(paramclave)
      Return ejecutaNonQuery("cambiarUsuarioClave", parametros)
  End Function

  Public Function Borrar(
      ByVal id As integer)
      Dim parametros As New List(Of DbParameter)
      Dim paramid As DbParameter = cmd.CreateParameter()
      paramid.Value = id
      paramid.ParameterName = "id"
      parametros.Add(paramid)
      Return ejecutaNonQuery("eliminaUsuarioClave", parametros)
  End Function

  Public Function Select_Where_UsuarioClave(ByVal Cond As String) As List(Of UsuarioClave)
        Dim ListUsuarioClave As New List(Of UsuarioClave)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("003")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenerUsuarioClaveByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              ListUsuarioClave.Add(New UsuarioClave(
                  Dr("id"),
                  Dr("Usuario"),
                  Dr("fecha"),
                  Dr("clave")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return ListUsuarioClave
  End Function

  Public Function Select_ById_UsuarioClave(
      ByVal id As integer) As List(Of UsuarioClave)
        Dim ListUsuarioClave As New List(Of UsuarioClave)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("003")

      Try
          cnn = conecta()
          cmd = New SqlCommand("obtenerUsuarioClaveById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramid As DbParameter = cmd.CreateParameter()
      paramid.Value = id
      paramid.ParameterName = "id"
      cmd.Parameters.Add(paramid)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              ListUsuarioClave.Add(New UsuarioClave(
                  Dr("id"),
                  Dr("Usuario"),
                  Dr("fecha"),
                  Dr("clave")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return ListUsuarioClave
  End Function

  Public Function Select_UsuarioClave() As List(Of UsuarioClave)
        Dim ListUsuarioClave As New List(Of UsuarioClave)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim result As String = appSettings("003")

      Try
         cnn = conecta()
          cmd = New SqlCommand("obtenerUsuarioClave")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              ListUsuarioClave.Add(New UsuarioClave(
                  Dr("id"),
                  Dr("Usuario"),
                  Dr("fecha"),
                  Dr("clave")))
          End While
      Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", result).Show()
      Finally
          desconecta(cnn)
      End Try
      Return ListUsuarioClave
  End Function

End Class