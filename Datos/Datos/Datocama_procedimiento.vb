Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common

Public Class Datocama_procedimiento
   Inherits conexion

  Public Function Insertar(ByVal cama_procedimiento As cama_procedimiento)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodigo_cama As DbParameter = cmd.CreateParameter()
      paramcodigo_cama.Value = cama_procedimiento.codigo_cama
      paramcodigo_cama.ParameterName = "codigo_cama"
      parametros.Add(paramcodigo_cama)
      Dim parammanual As DbParameter = cmd.CreateParameter()
      parammanual.Value = cama_procedimiento.manual
      parammanual.ParameterName = "manual"
      parametros.Add(parammanual)
      Dim paramcodpcd As DbParameter = cmd.CreateParameter()
      paramcodpcd.Value = cama_procedimiento.codpcd
      paramcodpcd.ParameterName = "codpcd"
      parametros.Add(paramcodpcd)
      Return ejecutaNonQuery("camasinsertacama_procedimiento", parametros)
  End Function

  Public Function InsertarRet(ByVal cama_procedimiento As cama_procedimiento)
      Dim dato As Integer = 0
      Try
          cnn = conecta()
          cmd = New SqlCommand("camasinsertacama_procedimiento")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
        Dim paramcodigo_cama As DbParameter = cmd.CreateParameter()
        paramcodigo_cama.Value = cama_procedimiento.codigo_cama
        paramcodigo_cama.ParameterName = "codigo_cama"
        cmd.Parameters.Add(paramcodigo_cama)
        Dim parammanual As DbParameter = cmd.CreateParameter()
        parammanual.Value = cama_procedimiento.manual
        parammanual.ParameterName = "manual"
        cmd.Parameters.Add(parammanual)
        Dim paramcodpcd As DbParameter = cmd.CreateParameter()
        paramcodpcd.Value = cama_procedimiento.codpcd
        paramcodpcd.ParameterName = "codpcd"
        cmd.Parameters.Add(paramcodpcd)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
            dato = Dr(0)
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return dato
  End Function

  Public Function Editar(ByVal cama_procedimiento As cama_procedimiento)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodigo_cama As DbParameter = cmd.CreateParameter()
      paramcodigo_cama.Value = cama_procedimiento.codigo_cama
      paramcodigo_cama.ParameterName = "codigo_cama"
      parametros.Add(paramcodigo_cama)
      Dim parammanual As DbParameter = cmd.CreateParameter()
      parammanual.Value = cama_procedimiento.manual
      parammanual.ParameterName = "manual"
      parametros.Add(parammanual)
      Dim paramcodpcd As DbParameter = cmd.CreateParameter()
      paramcodpcd.Value = cama_procedimiento.codpcd
      paramcodpcd.ParameterName = "codpcd"
      parametros.Add(paramcodpcd)
      Return ejecutaNonQuery("camascambiarcama_procedimiento", parametros)
  End Function

  Public Function Borrar(
      ByVal codigo_cama As string,
      ByVal manual As string,
      ByVal codpcd As string)
      Dim parametros As New List(Of DbParameter)
      Dim paramcodigo_cama As DbParameter = cmd.CreateParameter()
      paramcodigo_cama.Value = codigo_cama
      paramcodigo_cama.ParameterName = "codigo_cama"
      parametros.Add(paramcodigo_cama)
      Dim parammanual As DbParameter = cmd.CreateParameter()
      parammanual.Value = manual
      parammanual.ParameterName = "manual"
      parametros.Add(parammanual)
      Dim paramcodpcd As DbParameter = cmd.CreateParameter()
      paramcodpcd.Value = codpcd
      paramcodpcd.ParameterName = "codpcd"
      parametros.Add(paramcodpcd)
      Return ejecutaNonQuery("camaseliminacama_procedimiento", parametros)
  End Function

  Public Function Select_Where_cama_procedimiento(ByVal Cond As String) As List(Of cama_procedimiento)
      Dim Listcama_procedimiento As New List(Of cama_procedimiento)
      Try
          cnn = conecta()
          cmd = New SqlCommand("camasobtenercama_procedimientoByWhere")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim param As DbParameter = cmd.CreateParameter()
          param.Value = Cond
          param.ParameterName = "where"
          cmd.Parameters.Add(param)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listcama_procedimiento.Add(New cama_procedimiento(
                  Dr("codigo_cama"),
                  Dr("manual"),
                  Dr("codpcd")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listcama_procedimiento
  End Function

  Public Function Select_ById_cama_procedimiento(
      ByVal codigo_cama As string,
      ByVal manual As string,
      ByVal codpcd As string) As List(Of cama_procedimiento)
      Dim Listcama_procedimiento As New List(Of cama_procedimiento)
      Try
          cnn = conecta()
          cmd = New SqlCommand("camasobtenercama_procedimientoById")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
      Dim paramcodigo_cama As DbParameter = cmd.CreateParameter()
      paramcodigo_cama.Value = codigo_cama
      paramcodigo_cama.ParameterName = "codigo_cama"
      cmd.Parameters.Add(paramcodigo_cama)
      Dim parammanual As DbParameter = cmd.CreateParameter()
      parammanual.Value = manual
      parammanual.ParameterName = "manual"
      cmd.Parameters.Add(parammanual)
      Dim paramcodpcd As DbParameter = cmd.CreateParameter()
      paramcodpcd.Value = codpcd
      paramcodpcd.ParameterName = "codpcd"
      cmd.Parameters.Add(paramcodpcd)
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listcama_procedimiento.Add(New cama_procedimiento(
                  Dr("codigo_cama"),
                  Dr("manual"),
                  Dr("codpcd")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listcama_procedimiento
  End Function

  Public Function Select_cama_procedimiento() As List(Of cama_procedimiento)
      Dim Listcama_procedimiento As New List(Of cama_procedimiento)
      Try
         cnn = conecta()
          cmd = New SqlCommand("camasobtenercama_procedimiento")
          cmd.CommandType = CommandType.StoredProcedure
          cmd.Connection = cnn
          Dim Dr As DbDataReader = cmd.ExecuteReader()
          While Dr.Read
              Listcama_procedimiento.Add(New cama_procedimiento(
                  Dr("codigo_cama"),
                  Dr("manual"),
                  Dr("codpcd")))
          End While
      Catch ex As Exception
          Ext.Net.X.Msg.Alert("Información", ex.Message).Show()
      Finally
          desconecta(cnn)
      End Try
      Return Listcama_procedimiento
  End Function

End Class