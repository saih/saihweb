﻿Imports System.Data

Public Class deTablas

    Public Shared Function itemTabla(ByVal Tabla As String, ByVal Cond As String) As Integer
        Dim item As Integer = 0
        Dim Query As String
        Query = "select isnull(MAX(item),0) + 1 from " & Tabla & " where " & Cond
        Dim cadenaCon As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("ConexionLocal").ConnectionString
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(cadenaCon)
        conn.Open()
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(Query, conn)
        Dim reader As SqlClient.SqlDataReader = cmd.ExecuteReader
        While (reader.Read)
            item = reader(0)
        End While
        conn.Close()
        Return item
    End Function

    Public Shared Function datoCont(ByVal Tabla As String, ByVal Cond As String) As Integer
        Dim item As Integer = 0
        Dim Query As String
        Query = "select count(*) from " & Tabla & " " & Cond
        Dim cadenaCon As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("ConexionLocal").ConnectionString
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(cadenaCon)
        conn.Open()
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(Query, conn)
        Dim reader As SqlClient.SqlDataReader = cmd.ExecuteReader
        While (reader.Read)
            item = reader(0)
        End While
        conn.Close()
        Return item
    End Function

    Public Shared Function maxCampoTabla(ByVal campo As String, ByVal Tabla As String, ByVal Cond As String) As Integer
        Dim item As Integer = 0
        Dim Query As String
        Query = "select isnull(MAX(" & campo & "),0) + 1 from " & Tabla & " where " & Cond
        Dim cadenaCon As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("ConexionLocal").ConnectionString
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(cadenaCon)
        conn.Open()
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(Query, conn)
        Dim reader As SqlClient.SqlDataReader = cmd.ExecuteReader
        While (reader.Read)
            item = reader(0)
        End While
        conn.Close()
        Return item
    End Function

    Public Shared Function maxCampoTabla1(ByVal campo As String, ByVal Tabla As String, ByVal Cond As String) As Integer
        Dim item As Integer = 0
        Dim Query As String
        Query = "select isnull(MAX(" & campo & "),0) from " & Tabla & " where " & Cond
        Dim cadenaCon As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("ConexionLocal").ConnectionString
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(cadenaCon)
        conn.Open()
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(Query, conn)
        Dim reader As SqlClient.SqlDataReader = cmd.ExecuteReader
        While (reader.Read)
            item = reader(0)
        End While
        conn.Close()
        Return item
    End Function


    Public Shared Function verfFechaNull(ByVal fecha As String) As Object
        If fecha = "null" Then
            Return Nothing
        End If
        Return fecha
    End Function


    Public Shared Function difFechas(ByVal part As String, ByVal fecha1 As String, ByVal fecha2 As String) As Double
        Dim vlrunitario As Double = 0
        Dim Query As String
        Query = "select datediff(" & part & ",'" & fecha1 & "','" & fecha2 & "')"
        Dim cadenaCon As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("ConexionLocal").ConnectionString
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(cadenaCon)
        conn.Open()
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(Query, conn)
        Dim reader As SqlClient.SqlDataReader = cmd.ExecuteReader
        Try
            While (reader.Read)
                vlrunitario = reader(0)
            End While
        Catch ex As Exception
            vlrunitario = 0
        End Try
        conn.Close()
        Return vlrunitario
    End Function

    Public Shared Function fechaActual() As String
        Dim fecha As String = ""
        Dim Query As String
        Query = "SELECT CONVERT (char(10), GETDATE(), 112)"
        Dim cadenaCon As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("ConexionLocal").ConnectionString
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(cadenaCon)
        conn.Open()
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(Query, conn)
        Dim reader As SqlClient.SqlDataReader = cmd.ExecuteReader
        Try
            While (reader.Read)
                fecha = reader(0)
            End While
        Catch ex As Exception
            fecha = ""
        End Try
        conn.Close()
        Return fecha
    End Function


    Public Shared Function DifAnios(fecha As String) As String
        Dim anios As String = ""
        Dim Query As String = "select floor(cast(datediff(day, '" & fecha & "', getdate()) as float)/365)"
        Dim cadenaCon As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("ConexionLocal").ConnectionString
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(cadenaCon)
        conn.Open()
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(Query, conn)
        Dim reader As SqlClient.SqlDataReader = cmd.ExecuteReader
        Try
            While (reader.Read)
                anios = reader(0)
            End While
        Catch ex As Exception
            fecha = ""
        End Try
        conn.Close()
        Return anios
    End Function

    Public Shared Function datoTabla(ByVal campo As String, ByVal tabla As String, ByVal cond As String) As String
        Dim dato As String = ""
        Dim Query As String
        Query = "SELECT " & campo & " from " & tabla & cond
        Dim cadenaCon As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("ConexionLocal").ConnectionString
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(cadenaCon)
        conn.Open()
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(Query, conn)
        Dim reader As SqlClient.SqlDataReader = cmd.ExecuteReader
        Try
            While (reader.Read)
                dato = reader(0)
            End While
        Catch ex As Exception
            dato = ""
        End Try
        conn.Close()
        Return dato
    End Function

    Public Shared Function datoSql(ByVal Query) As String
        Dim dato As String = ""
        Dim cadenaCon As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("ConexionLocal").ConnectionString
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(cadenaCon)
        conn.Open()
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(Query, conn)
        Dim reader As SqlClient.SqlDataReader = cmd.ExecuteReader
        Try
            While (reader.Read)
                dato = reader(0)
            End While
        Catch ex As Exception
            dato = ""
        End Try
        conn.Close()
        Return dato
    End Function

    Public Shared Function ejecutaSelectDr(ByVal sql As String) As SqlClient.SqlDataReader
        Dim cadenaCon As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("ConexionLocal").ConnectionString
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(cadenaCon)
        conn.Open()
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(sql, conn)
        Dim reader As SqlClient.SqlDataReader = cmd.ExecuteReader
        Return reader
    End Function

    Public Shared Function ValidaLugar(ByVal Id_Lugar As String) As String
        Dim count As Integer = 0
        Dim Query As String
        Query = "select count(*) from saih_lugar where id_lugar = " & Id_Lugar & " "
        Dim cadenaCon As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("ConexionLocal").ConnectionString
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(cadenaCon)
        conn.Open()
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(Query, conn)
        Dim reader As SqlClient.SqlDataReader = cmd.ExecuteReader
        While (reader.Read)
            count = reader(0)
        End While
        conn.Close()
        Return count
    End Function

    Public Shared Function DescripcionLugar(ByVal Id_Lugar As String) As String
        Dim descripcion As String = ""
        Dim Query As String
        Query = "select Descripcion from saih_lugar where id = " & Id_Lugar & " "
        Dim cadenaCon As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("ConexionLocal").ConnectionString
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(cadenaCon)
        conn.Open()
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(Query, conn)
        Dim reader As SqlClient.SqlDataReader = cmd.ExecuteReader
        While (reader.Read)
            descripcion = reader(0)
        End While
        conn.Close()
        Return descripcion
    End Function

    Public Shared Function DescripcionLugarCiudad(ByVal Id_Lugar As String) As String
        Dim descripcion As String = ""
        Dim Query As String
        Query = "select a.descripcion + ' (' + b.descripcion + ')' from saih_lugar a inner join saih_lugar b on a.id_lugar = b.id where a.id = " & Id_Lugar & " "
        Dim cadenaCon As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("ConexionLocal").ConnectionString
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(cadenaCon)
        conn.Open()
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(Query, conn)
        Dim reader As SqlClient.SqlDataReader = cmd.ExecuteReader
        While (reader.Read)
            descripcion = reader(0)
        End While
        conn.Close()
        Return descripcion
    End Function

    Public Shared Function edadPaciente(ByVal numhis As String) As String
        Dim dato As String = ""
        Dim cadenaCon As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("ConexionLocal").ConnectionString
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(cadenaCon)
        conn.Open()
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("select dbo.FN_EDAD_PACIENTE('" & numhis & "')", conn)
        Dim reader As SqlClient.SqlDataReader = cmd.ExecuteReader
        Try
            While (reader.Read)
                dato = reader(0)
            End While
        Catch ex As Exception
            dato = ""
        End Try
        conn.Close()
        Return dato
    End Function

End Class
