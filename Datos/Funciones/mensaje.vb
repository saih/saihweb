﻿Public Class mensaje
    Private _codigo As String
    Private _texto As String
    Private _valor As String

    Public Property Codigo As String
        Get
            Return _codigo
        End Get
        Set(value As String)
            _codigo = value
        End Set
    End Property

    Public Property Texto As String
        Get
            Return _texto
        End Get
        Set(value As String)
            _texto = value
        End Set
    End Property

    Public Property Valor As String
        Get
            Return _valor
        End Get
        Set(value As String)
            _valor = value
        End Set
    End Property
End Class
