Public Class saih_apas
     Private _codpre As string
     Private _nropme As string
     Private _codpas As string
     Private _fecha_adicion As string
     Private _fecha_modificacion As string
     Private _usuario_creo As string
     Private _usuario_modifico As string
     Private _fecha_inicial As string
     Private _fecha_final As string
     Private _consultorio As string
     Private _intervalo As integer
     Private _codser As string
     Private _estado As string
     Private _motivo_cancelacion As string

     Public Property codpre() As string
        Get
            Return _codpre
        End Get
        Set(ByVal value As string)
            _codpre = value
        End Set
     End Property

     Public Property nropme() As string
        Get
            Return _nropme
        End Get
        Set(ByVal value As string)
            _nropme = value
        End Set
     End Property

     Public Property codpas() As string
        Get
            Return _codpas
        End Get
        Set(ByVal value As string)
            _codpas = value
        End Set
     End Property

     Public Property fecha_adicion() As string
        Get
            Return _fecha_adicion
        End Get
        Set(ByVal value As string)
            _fecha_adicion = value
        End Set
     End Property

     Public Property fecha_modificacion() As string
        Get
            Return _fecha_modificacion
        End Get
        Set(ByVal value As string)
            _fecha_modificacion = value
        End Set
     End Property

     Public Property usuario_creo() As string
        Get
            Return _usuario_creo
        End Get
        Set(ByVal value As string)
            _usuario_creo = value
        End Set
     End Property

     Public Property usuario_modifico() As string
        Get
            Return _usuario_modifico
        End Get
        Set(ByVal value As string)
            _usuario_modifico = value
        End Set
     End Property

     Public Property fecha_inicial() As string
        Get
            Return _fecha_inicial
        End Get
        Set(ByVal value As string)
            _fecha_inicial = value
        End Set
     End Property

     Public Property fecha_final() As string
        Get
            Return _fecha_final
        End Get
        Set(ByVal value As string)
            _fecha_final = value
        End Set
     End Property

     Public Property consultorio() As string
        Get
            Return _consultorio
        End Get
        Set(ByVal value As string)
            _consultorio = value
        End Set
     End Property

     Public Property intervalo() As integer
        Get
            Return _intervalo
        End Get
        Set(ByVal value As integer)
            _intervalo = value
        End Set
     End Property

     Public Property codser() As string
        Get
            Return _codser
        End Get
        Set(ByVal value As string)
            _codser = value
        End Set
     End Property

     Public Property estado() As string
        Get
            Return _estado
        End Get
        Set(ByVal value As string)
            _estado = value
        End Set
     End Property

     Public Property motivo_cancelacion() As string
        Get
            Return _motivo_cancelacion
        End Get
        Set(ByVal value As string)
            _motivo_cancelacion = value
        End Set
     End Property

    Public Sub New(
      ByVal codpre As string,
      ByVal nropme As string,
      ByVal codpas As string,
      ByVal fecha_adicion As string,
      ByVal fecha_modificacion As string,
      ByVal usuario_creo As string,
      ByVal usuario_modifico As string,
      ByVal fecha_inicial As string,
      ByVal fecha_final As string,
      ByVal consultorio As string,
      ByVal intervalo As integer,
      ByVal codser As string,
      ByVal estado As string,
      ByVal motivo_cancelacion As string
    )
      Me.codpre = codpre
      Me.nropme = nropme
      Me.codpas = codpas
      Me.fecha_adicion = fecha_adicion
      Me.fecha_modificacion = fecha_modificacion
      Me.usuario_creo = usuario_creo
      Me.usuario_modifico = usuario_modifico
      Me.fecha_inicial = fecha_inicial
      Me.fecha_final = fecha_final
      Me.consultorio = consultorio
      Me.intervalo = intervalo
      Me.codser = codser
      Me.estado = estado
      Me.motivo_cancelacion = motivo_cancelacion
    End Sub

    Public Sub New()
    End Sub
End Class