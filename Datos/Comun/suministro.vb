Public Class suministro
     Private _codigo As string
     Private _nombre As string
     Private _codana As string
     Private _presentacion As string
     Private _und_concentracion As string
     Private _concentracion As string
     Private _clase As string
     Private _codser As string
     Private _valor As decimal
     Private _cod_centrocosto As string
     Private _cod_undfunc As string
     Private _cod_material As string
     Private _iva As decimal
     Private _cod_generico As string
     Private _nom_generico As string
     Private _tipo_imp As string
     Private _poso As string

     Public Property codigo() As string
        Get
            Return _codigo
        End Get
        Set(ByVal value As string)
            _codigo = value
        End Set
     End Property

     Public Property nombre() As string
        Get
            Return _nombre
        End Get
        Set(ByVal value As string)
            _nombre = value
        End Set
     End Property

     Public Property codana() As string
        Get
            Return _codana
        End Get
        Set(ByVal value As string)
            _codana = value
        End Set
     End Property

     Public Property presentacion() As string
        Get
            Return _presentacion
        End Get
        Set(ByVal value As string)
            _presentacion = value
        End Set
     End Property

     Public Property und_concentracion() As string
        Get
            Return _und_concentracion
        End Get
        Set(ByVal value As string)
            _und_concentracion = value
        End Set
     End Property

     Public Property concentracion() As string
        Get
            Return _concentracion
        End Get
        Set(ByVal value As string)
            _concentracion = value
        End Set
     End Property

     Public Property clase() As string
        Get
            Return _clase
        End Get
        Set(ByVal value As string)
            _clase = value
        End Set
     End Property

     Public Property codser() As string
        Get
            Return _codser
        End Get
        Set(ByVal value As string)
            _codser = value
        End Set
     End Property

     Public Property valor() As decimal
        Get
            Return _valor
        End Get
        Set(ByVal value As decimal)
            _valor = value
        End Set
     End Property

     Public Property cod_centrocosto() As string
        Get
            Return _cod_centrocosto
        End Get
        Set(ByVal value As string)
            _cod_centrocosto = value
        End Set
     End Property

     Public Property cod_undfunc() As string
        Get
            Return _cod_undfunc
        End Get
        Set(ByVal value As string)
            _cod_undfunc = value
        End Set
     End Property

     Public Property cod_material() As string
        Get
            Return _cod_material
        End Get
        Set(ByVal value As string)
            _cod_material = value
        End Set
     End Property

     Public Property iva() As decimal
        Get
            Return _iva
        End Get
        Set(ByVal value As decimal)
            _iva = value
        End Set
     End Property

     Public Property cod_generico() As string
        Get
            Return _cod_generico
        End Get
        Set(ByVal value As string)
            _cod_generico = value
        End Set
     End Property

     Public Property nom_generico() As string
        Get
            Return _nom_generico
        End Get
        Set(ByVal value As string)
            _nom_generico = value
        End Set
     End Property

     Public Property tipo_imp() As string
        Get
            Return _tipo_imp
        End Get
        Set(ByVal value As string)
            _tipo_imp = value
        End Set
     End Property

     Public Property poso() As string
        Get
            Return _poso
        End Get
        Set(ByVal value As string)
            _poso = value
        End Set
     End Property

    Public Sub New(
      ByVal codigo As string,
      ByVal nombre As string,
      ByVal codana As string,
      ByVal presentacion As string,
      ByVal und_concentracion As string,
      ByVal concentracion As string,
      ByVal clase As string,
      ByVal codser As string,
      ByVal valor As decimal,
      ByVal cod_centrocosto As string,
      ByVal cod_undfunc As string,
      ByVal cod_material As string,
      ByVal iva As decimal,
      ByVal cod_generico As string,
      ByVal nom_generico As string,
      ByVal tipo_imp As string,
      ByVal poso As string
    )
      Me.codigo = codigo
      Me.nombre = nombre
      Me.codana = codana
      Me.presentacion = presentacion
      Me.und_concentracion = und_concentracion
      Me.concentracion = concentracion
      Me.clase = clase
      Me.codser = codser
      Me.valor = valor
      Me.cod_centrocosto = cod_centrocosto
      Me.cod_undfunc = cod_undfunc
      Me.cod_material = cod_material
      Me.iva = iva
      Me.cod_generico = cod_generico
      Me.nom_generico = nom_generico
      Me.tipo_imp = tipo_imp
      Me.poso = poso
    End Sub

    Public Sub New()
    End Sub
End Class