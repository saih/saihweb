Public Class saih_ser
     Private _codser As string
     Private _nombre As string
     Private _tippro As string

     Public Property codser() As string
        Get
            Return _codser
        End Get
        Set(ByVal value As string)
            _codser = value
        End Set
     End Property

     Public Property nombre() As string
        Get
            Return _nombre
        End Get
        Set(ByVal value As string)
            _nombre = value
        End Set
     End Property

     Public Property tippro() As string
        Get
            Return _tippro
        End Get
        Set(ByVal value As string)
            _tippro = value
        End Set
     End Property

    Public Sub New(
      ByVal codser As string,
      ByVal nombre As string,
      ByVal tippro As string
    )
      Me.codser = codser
      Me.nombre = nombre
      Me.tippro = tippro
    End Sub

    Public Sub New()
    End Sub
End Class