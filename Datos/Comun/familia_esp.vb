Public Class familia_esp
     Private _id_familia As integer
     Private _cod_especialidad As string
     Private _codpas As string
     Private _fecha_adicion As string
     Private _usuario_creo As string
     Private _fecha_modificacion As string
     Private _usuario_modifico As string

     Public Property id_familia() As integer
        Get
            Return _id_familia
        End Get
        Set(ByVal value As integer)
            _id_familia = value
        End Set
     End Property

     Public Property cod_especialidad() As string
        Get
            Return _cod_especialidad
        End Get
        Set(ByVal value As string)
            _cod_especialidad = value
        End Set
     End Property

     Public Property codpas() As string
        Get
            Return _codpas
        End Get
        Set(ByVal value As string)
            _codpas = value
        End Set
     End Property

     Public Property fecha_adicion() As string
        Get
            Return _fecha_adicion
        End Get
        Set(ByVal value As string)
            _fecha_adicion = value
        End Set
     End Property

     Public Property usuario_creo() As string
        Get
            Return _usuario_creo
        End Get
        Set(ByVal value As string)
            _usuario_creo = value
        End Set
     End Property

     Public Property fecha_modificacion() As string
        Get
            Return _fecha_modificacion
        End Get
        Set(ByVal value As string)
            _fecha_modificacion = value
        End Set
     End Property

     Public Property usuario_modifico() As string
        Get
            Return _usuario_modifico
        End Get
        Set(ByVal value As string)
            _usuario_modifico = value
        End Set
     End Property

    Public Sub New(
      ByVal id_familia As integer,
      ByVal cod_especialidad As string,
      ByVal codpas As string,
      ByVal fecha_adicion As string,
      ByVal usuario_creo As string,
      ByVal fecha_modificacion As string,
      ByVal usuario_modifico As string
    )
      Me.id_familia = id_familia
      Me.cod_especialidad = cod_especialidad
      Me.codpas = codpas
      Me.fecha_adicion = fecha_adicion
      Me.usuario_creo = usuario_creo
      Me.fecha_modificacion = fecha_modificacion
      Me.usuario_modifico = usuario_modifico
    End Sub

    Public Sub New()
    End Sub
End Class