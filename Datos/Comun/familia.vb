Public Class familia
     Private _id As integer
     Private _descripcion As string
     Private _fecha_adicion As string
     Private _usuario_creo As string

     Public Property id() As integer
        Get
            Return _id
        End Get
        Set(ByVal value As integer)
            _id = value
        End Set
     End Property

     Public Property descripcion() As string
        Get
            Return _descripcion
        End Get
        Set(ByVal value As string)
            _descripcion = value
        End Set
     End Property

     Public Property fecha_adicion() As string
        Get
            Return _fecha_adicion
        End Get
        Set(ByVal value As string)
            _fecha_adicion = value
        End Set
     End Property

     Public Property usuario_creo() As string
        Get
            Return _usuario_creo
        End Get
        Set(ByVal value As string)
            _usuario_creo = value
        End Set
     End Property

    Public Sub New(
      ByVal id As integer,
      ByVal descripcion As string,
      ByVal fecha_adicion As string,
      ByVal usuario_creo As string
    )
      Me.id = id
      Me.descripcion = descripcion
      Me.fecha_adicion = fecha_adicion
      Me.usuario_creo = usuario_creo
    End Sub

    Public Sub New()
    End Sub
End Class