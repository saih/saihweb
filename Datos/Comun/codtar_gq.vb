Public Class codtar_gq
     Private _manual As string
     Private _codigo As string
     Private _nombre As string
     Private _honmed As string
     Private _honane As string
     Private _dersal As string
     Private _dersalesp As string
     Private _dersalend As string
     Private _matsum As string
     Private _mataneloc As string
     Private _honayu As string
     Private _honper As string

     Public Property manual() As string
        Get
            Return _manual
        End Get
        Set(ByVal value As string)
            _manual = value
        End Set
     End Property

     Public Property codigo() As string
        Get
            Return _codigo
        End Get
        Set(ByVal value As string)
            _codigo = value
        End Set
     End Property

     Public Property nombre() As string
        Get
            Return _nombre
        End Get
        Set(ByVal value As string)
            _nombre = value
        End Set
     End Property

     Public Property honmed() As string
        Get
            Return _honmed
        End Get
        Set(ByVal value As string)
            _honmed = value
        End Set
     End Property

     Public Property honane() As string
        Get
            Return _honane
        End Get
        Set(ByVal value As string)
            _honane = value
        End Set
     End Property

     Public Property dersal() As string
        Get
            Return _dersal
        End Get
        Set(ByVal value As string)
            _dersal = value
        End Set
     End Property

     Public Property dersalesp() As string
        Get
            Return _dersalesp
        End Get
        Set(ByVal value As string)
            _dersalesp = value
        End Set
     End Property

     Public Property dersalend() As string
        Get
            Return _dersalend
        End Get
        Set(ByVal value As string)
            _dersalend = value
        End Set
     End Property

     Public Property matsum() As string
        Get
            Return _matsum
        End Get
        Set(ByVal value As string)
            _matsum = value
        End Set
     End Property

     Public Property mataneloc() As string
        Get
            Return _mataneloc
        End Get
        Set(ByVal value As string)
            _mataneloc = value
        End Set
     End Property

     Public Property honayu() As string
        Get
            Return _honayu
        End Get
        Set(ByVal value As string)
            _honayu = value
        End Set
     End Property

     Public Property honper() As string
        Get
            Return _honper
        End Get
        Set(ByVal value As string)
            _honper = value
        End Set
     End Property

    Public Sub New(
      ByVal manual As string,
      ByVal codigo As string,
      ByVal nombre As string,
      ByVal honmed As string,
      ByVal honane As string,
      ByVal dersal As string,
      ByVal dersalesp As string,
      ByVal dersalend As string,
      ByVal matsum As string,
      ByVal mataneloc As string,
      ByVal honayu As string,
      ByVal honper As string
    )
      Me.manual = manual
      Me.codigo = codigo
      Me.nombre = nombre
      Me.honmed = honmed
      Me.honane = honane
      Me.dersal = dersal
      Me.dersalesp = dersalesp
      Me.dersalend = dersalend
      Me.matsum = matsum
      Me.mataneloc = mataneloc
      Me.honayu = honayu
      Me.honper = honper
    End Sub

    Public Sub New()
    End Sub
End Class