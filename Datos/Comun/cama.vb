Public Class cama
     Private _codigo As string
     Private _codigo_area As string
     Private _num_admision As string
     Private _manual As string
     Private _codpcd As string
     Private _codser As string

     Public Property codigo() As string
        Get
            Return _codigo
        End Get
        Set(ByVal value As string)
            _codigo = value
        End Set
     End Property

     Public Property codigo_area() As string
        Get
            Return _codigo_area
        End Get
        Set(ByVal value As string)
            _codigo_area = value
        End Set
     End Property

     Public Property num_admision() As string
        Get
            Return _num_admision
        End Get
        Set(ByVal value As string)
            _num_admision = value
        End Set
     End Property

     Public Property manual() As string
        Get
            Return _manual
        End Get
        Set(ByVal value As string)
            _manual = value
        End Set
     End Property

     Public Property codpcd() As string
        Get
            Return _codpcd
        End Get
        Set(ByVal value As string)
            _codpcd = value
        End Set
     End Property

     Public Property codser() As string
        Get
            Return _codser
        End Get
        Set(ByVal value As string)
            _codser = value
        End Set
     End Property

    Public Sub New(
      ByVal codigo As string,
      ByVal codigo_area As string,
      ByVal num_admision As string,
      ByVal manual As string,
      ByVal codpcd As string,
      ByVal codser As string
    )
      Me.codigo = codigo
      Me.codigo_area = codigo_area
      Me.num_admision = num_admision
      Me.manual = manual
      Me.codpcd = codpcd
      Me.codser = codser
    End Sub

    Public Sub New()
    End Sub
End Class