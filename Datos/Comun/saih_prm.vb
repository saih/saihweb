Public Class saih_prm
     Private _codpre As string
     Private _lugar As string
     Private _numhis As string
     Private _nropac As string
     Private _numfac As string
     Private _nrocta As string
     Private _nroord As string
     Private _nronci As string
     Private _nroglo As string
     Private _nroges As string
     Private _nrocit As string
     Private _norage As string
     Private _norres As string
     Private _nroepi As string
     Private _nroiqu As string
     Private _indhuella As integer
     Private _nrorem As string
     Private _nroacm As string
     Private _nrofor As string
     Private _nropro As string
     Private _nropme As string
     Private _nrofur As string
     Private _id_lugar As integer
     Private _con_pago_ser_no As string
     Private _cod_abono_fac As string
     Private _cod_pago_act As string
     Private _cod_recaudo_copagos As string
     Private _cns_num_cuenta_cobro As string
     Private _cns_pag_cuenta_cobro As string
     Private _cod_recaudo_cuota_moderadora As string
     Private _HoraExpedicion As integer
     Private _HoraInicioAtencion As string
     Private _IntervaloCitas As integer
     Private _codurgencias As string
     Private _codambulatorias As string
     Private _codespecialidad As string
     Private _observaciones As string

     Public Property codpre() As string
        Get
            Return _codpre
        End Get
        Set(ByVal value As string)
            _codpre = value
        End Set
     End Property

     Public Property lugar() As string
        Get
            Return _lugar
        End Get
        Set(ByVal value As string)
            _lugar = value
        End Set
     End Property

     Public Property numhis() As string
        Get
            Return _numhis
        End Get
        Set(ByVal value As string)
            _numhis = value
        End Set
     End Property

     Public Property nropac() As string
        Get
            Return _nropac
        End Get
        Set(ByVal value As string)
            _nropac = value
        End Set
     End Property

     Public Property numfac() As string
        Get
            Return _numfac
        End Get
        Set(ByVal value As string)
            _numfac = value
        End Set
     End Property

     Public Property nrocta() As string
        Get
            Return _nrocta
        End Get
        Set(ByVal value As string)
            _nrocta = value
        End Set
     End Property

     Public Property nroord() As string
        Get
            Return _nroord
        End Get
        Set(ByVal value As string)
            _nroord = value
        End Set
     End Property

     Public Property nronci() As string
        Get
            Return _nronci
        End Get
        Set(ByVal value As string)
            _nronci = value
        End Set
     End Property

     Public Property nroglo() As string
        Get
            Return _nroglo
        End Get
        Set(ByVal value As string)
            _nroglo = value
        End Set
     End Property

     Public Property nroges() As string
        Get
            Return _nroges
        End Get
        Set(ByVal value As string)
            _nroges = value
        End Set
     End Property

     Public Property nrocit() As string
        Get
            Return _nrocit
        End Get
        Set(ByVal value As string)
            _nrocit = value
        End Set
     End Property

     Public Property norage() As string
        Get
            Return _norage
        End Get
        Set(ByVal value As string)
            _norage = value
        End Set
     End Property

     Public Property norres() As string
        Get
            Return _norres
        End Get
        Set(ByVal value As string)
            _norres = value
        End Set
     End Property

     Public Property nroepi() As string
        Get
            Return _nroepi
        End Get
        Set(ByVal value As string)
            _nroepi = value
        End Set
     End Property

     Public Property nroiqu() As string
        Get
            Return _nroiqu
        End Get
        Set(ByVal value As string)
            _nroiqu = value
        End Set
     End Property

     Public Property indhuella() As integer
        Get
            Return _indhuella
        End Get
        Set(ByVal value As integer)
            _indhuella = value
        End Set
     End Property

     Public Property nrorem() As string
        Get
            Return _nrorem
        End Get
        Set(ByVal value As string)
            _nrorem = value
        End Set
     End Property

     Public Property nroacm() As string
        Get
            Return _nroacm
        End Get
        Set(ByVal value As string)
            _nroacm = value
        End Set
     End Property

     Public Property nrofor() As string
        Get
            Return _nrofor
        End Get
        Set(ByVal value As string)
            _nrofor = value
        End Set
     End Property

     Public Property nropro() As string
        Get
            Return _nropro
        End Get
        Set(ByVal value As string)
            _nropro = value
        End Set
     End Property

     Public Property nropme() As string
        Get
            Return _nropme
        End Get
        Set(ByVal value As string)
            _nropme = value
        End Set
     End Property

     Public Property nrofur() As string
        Get
            Return _nrofur
        End Get
        Set(ByVal value As string)
            _nrofur = value
        End Set
     End Property

     Public Property id_lugar() As integer
        Get
            Return _id_lugar
        End Get
        Set(ByVal value As integer)
            _id_lugar = value
        End Set
     End Property

     Public Property con_pago_ser_no() As string
        Get
            Return _con_pago_ser_no
        End Get
        Set(ByVal value As string)
            _con_pago_ser_no = value
        End Set
     End Property

     Public Property cod_abono_fac() As string
        Get
            Return _cod_abono_fac
        End Get
        Set(ByVal value As string)
            _cod_abono_fac = value
        End Set
     End Property

     Public Property cod_pago_act() As string
        Get
            Return _cod_pago_act
        End Get
        Set(ByVal value As string)
            _cod_pago_act = value
        End Set
     End Property

     Public Property cod_recaudo_copagos() As string
        Get
            Return _cod_recaudo_copagos
        End Get
        Set(ByVal value As string)
            _cod_recaudo_copagos = value
        End Set
     End Property

     Public Property cns_num_cuenta_cobro() As string
        Get
            Return _cns_num_cuenta_cobro
        End Get
        Set(ByVal value As string)
            _cns_num_cuenta_cobro = value
        End Set
     End Property

     Public Property cns_pag_cuenta_cobro() As string
        Get
            Return _cns_pag_cuenta_cobro
        End Get
        Set(ByVal value As string)
            _cns_pag_cuenta_cobro = value
        End Set
     End Property

     Public Property cod_recaudo_cuota_moderadora() As string
        Get
            Return _cod_recaudo_cuota_moderadora
        End Get
        Set(ByVal value As string)
            _cod_recaudo_cuota_moderadora = value
        End Set
     End Property

     Public Property HoraExpedicion() As integer
        Get
            Return _HoraExpedicion
        End Get
        Set(ByVal value As integer)
            _HoraExpedicion = value
        End Set
     End Property

     Public Property HoraInicioAtencion() As string
        Get
            Return _HoraInicioAtencion
        End Get
        Set(ByVal value As string)
            _HoraInicioAtencion = value
        End Set
     End Property

     Public Property IntervaloCitas() As integer
        Get
            Return _IntervaloCitas
        End Get
        Set(ByVal value As integer)
            _IntervaloCitas = value
        End Set
     End Property

     Public Property codurgencias() As string
        Get
            Return _codurgencias
        End Get
        Set(ByVal value As string)
            _codurgencias = value
        End Set
     End Property

     Public Property codambulatorias() As string
        Get
            Return _codambulatorias
        End Get
        Set(ByVal value As string)
            _codambulatorias = value
        End Set
     End Property

     Public Property codespecialidad() As string
        Get
            Return _codespecialidad
        End Get
        Set(ByVal value As string)
            _codespecialidad = value
        End Set
     End Property

     Public Property observaciones() As string
        Get
            Return _observaciones
        End Get
        Set(ByVal value As string)
            _observaciones = value
        End Set
     End Property

    Public Sub New(
      ByVal codpre As string,
      ByVal lugar As string,
      ByVal numhis As string,
      ByVal nropac As string,
      ByVal numfac As string,
      ByVal nrocta As string,
      ByVal nroord As string,
      ByVal nronci As string,
      ByVal nroglo As string,
      ByVal nroges As string,
      ByVal nrocit As string,
      ByVal norage As string,
      ByVal norres As string,
      ByVal nroepi As string,
      ByVal nroiqu As string,
      ByVal indhuella As integer,
      ByVal nrorem As string,
      ByVal nroacm As string,
      ByVal nrofor As string,
      ByVal nropro As string,
      ByVal nropme As string,
      ByVal nrofur As string,
      ByVal id_lugar As integer,
      ByVal con_pago_ser_no As string,
      ByVal cod_abono_fac As string,
      ByVal cod_pago_act As string,
      ByVal cod_recaudo_copagos As string,
      ByVal cns_num_cuenta_cobro As string,
      ByVal cns_pag_cuenta_cobro As string,
      ByVal cod_recaudo_cuota_moderadora As string,
      ByVal HoraExpedicion As integer,
      ByVal HoraInicioAtencion As string,
      ByVal IntervaloCitas As integer,
      ByVal codurgencias As string,
      ByVal codambulatorias As string,
      ByVal codespecialidad As string,
      ByVal observaciones As string
    )
      Me.codpre = codpre
      Me.lugar = lugar
      Me.numhis = numhis
      Me.nropac = nropac
      Me.numfac = numfac
      Me.nrocta = nrocta
      Me.nroord = nroord
      Me.nronci = nronci
      Me.nroglo = nroglo
      Me.nroges = nroges
      Me.nrocit = nrocit
      Me.norage = norage
      Me.norres = norres
      Me.nroepi = nroepi
      Me.nroiqu = nroiqu
      Me.indhuella = indhuella
      Me.nrorem = nrorem
      Me.nroacm = nroacm
      Me.nrofor = nrofor
      Me.nropro = nropro
      Me.nropme = nropme
      Me.nrofur = nrofur
      Me.id_lugar = id_lugar
      Me.con_pago_ser_no = con_pago_ser_no
      Me.cod_abono_fac = cod_abono_fac
      Me.cod_pago_act = cod_pago_act
      Me.cod_recaudo_copagos = cod_recaudo_copagos
      Me.cns_num_cuenta_cobro = cns_num_cuenta_cobro
      Me.cns_pag_cuenta_cobro = cns_pag_cuenta_cobro
      Me.cod_recaudo_cuota_moderadora = cod_recaudo_cuota_moderadora
      Me.HoraExpedicion = HoraExpedicion
      Me.HoraInicioAtencion = HoraInicioAtencion
      Me.IntervaloCitas = IntervaloCitas
      Me.codurgencias = codurgencias
      Me.codambulatorias = codambulatorias
      Me.codespecialidad = codespecialidad
      Me.observaciones = observaciones
    End Sub

    Public Sub New()
    End Sub
End Class