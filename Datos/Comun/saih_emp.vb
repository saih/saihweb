Public Class saih_emp
     Private _codigo As string
     Private _nit As string
     Private _razonsocial As string
     Private _direccion As string
     Private _telefono As string
     Private _fax As string
     Private _contacto As string
     Private _tipo_fact As string
     Private _cod_contable As string
     Private _cod_empFctProc As string
     Private _cod_empSolCuentas As string

     Public Property codigo() As string
        Get
            Return _codigo
        End Get
        Set(ByVal value As string)
            _codigo = value
        End Set
     End Property

     Public Property nit() As string
        Get
            Return _nit
        End Get
        Set(ByVal value As string)
            _nit = value
        End Set
     End Property

     Public Property razonsocial() As string
        Get
            Return _razonsocial
        End Get
        Set(ByVal value As string)
            _razonsocial = value
        End Set
     End Property

     Public Property direccion() As string
        Get
            Return _direccion
        End Get
        Set(ByVal value As string)
            _direccion = value
        End Set
     End Property

     Public Property telefono() As string
        Get
            Return _telefono
        End Get
        Set(ByVal value As string)
            _telefono = value
        End Set
     End Property

     Public Property fax() As string
        Get
            Return _fax
        End Get
        Set(ByVal value As string)
            _fax = value
        End Set
     End Property

     Public Property contacto() As string
        Get
            Return _contacto
        End Get
        Set(ByVal value As string)
            _contacto = value
        End Set
     End Property

     Public Property tipo_fact() As string
        Get
            Return _tipo_fact
        End Get
        Set(ByVal value As string)
            _tipo_fact = value
        End Set
     End Property

     Public Property cod_contable() As string
        Get
            Return _cod_contable
        End Get
        Set(ByVal value As string)
            _cod_contable = value
        End Set
     End Property

     Public Property cod_empFctProc() As string
        Get
            Return _cod_empFctProc
        End Get
        Set(ByVal value As string)
            _cod_empFctProc = value
        End Set
     End Property

     Public Property cod_empSolCuentas() As string
        Get
            Return _cod_empSolCuentas
        End Get
        Set(ByVal value As string)
            _cod_empSolCuentas = value
        End Set
     End Property

    Public Sub New(
      ByVal codigo As string,
      ByVal nit As string,
      ByVal razonsocial As string,
      ByVal direccion As string,
      ByVal telefono As string,
      ByVal fax As string,
      ByVal contacto As string,
      ByVal tipo_fact As string,
      ByVal cod_contable As string,
      ByVal cod_empFctProc As string,
      ByVal cod_empSolCuentas As string
    )
      Me.codigo = codigo
      Me.nit = nit
      Me.razonsocial = razonsocial
      Me.direccion = direccion
      Me.telefono = telefono
      Me.fax = fax
      Me.contacto = contacto
      Me.tipo_fact = tipo_fact
      Me.cod_contable = cod_contable
      Me.cod_empFctProc = cod_empFctProc
      Me.cod_empSolCuentas = cod_empSolCuentas
    End Sub

    Public Sub New()
    End Sub
End Class