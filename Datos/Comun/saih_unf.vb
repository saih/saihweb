Public Class saih_unf
     Private _codunf As string
     Private _nombre As string

     Public Property codunf() As string
        Get
            Return _codunf
        End Get
        Set(ByVal value As string)
            _codunf = value
        End Set
     End Property

     Public Property nombre() As string
        Get
            Return _nombre
        End Get
        Set(ByVal value As string)
            _nombre = value
        End Set
     End Property

    Public Sub New(
      ByVal codunf As string,
      ByVal nombre As string
    )
      Me.codunf = codunf
      Me.nombre = nombre
    End Sub

    Public Sub New()
    End Sub
End Class