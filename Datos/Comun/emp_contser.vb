Public Class emp_contser
     Private _codemp As string
     Private _codser As string
     Private _manual As string
     Private _porcentaje As decimal
     Private _num_contrato As string
     Private _niv_complejidad As string

     Public Property codemp() As string
        Get
            Return _codemp
        End Get
        Set(ByVal value As string)
            _codemp = value
        End Set
     End Property

     Public Property codser() As string
        Get
            Return _codser
        End Get
        Set(ByVal value As string)
            _codser = value
        End Set
     End Property

     Public Property manual() As string
        Get
            Return _manual
        End Get
        Set(ByVal value As string)
            _manual = value
        End Set
     End Property

     Public Property porcentaje() As decimal
        Get
            Return _porcentaje
        End Get
        Set(ByVal value As decimal)
            _porcentaje = value
        End Set
     End Property

     Public Property num_contrato() As string
        Get
            Return _num_contrato
        End Get
        Set(ByVal value As string)
            _num_contrato = value
        End Set
     End Property

     Public Property niv_complejidad() As string
        Get
            Return _niv_complejidad
        End Get
        Set(ByVal value As string)
            _niv_complejidad = value
        End Set
     End Property

    Public Sub New(
      ByVal codemp As string,
      ByVal codser As string,
      ByVal manual As string,
      ByVal porcentaje As decimal,
      ByVal num_contrato As string,
      ByVal niv_complejidad As string
    )
      Me.codemp = codemp
      Me.codser = codser
      Me.manual = manual
      Me.porcentaje = porcentaje
      Me.num_contrato = num_contrato
      Me.niv_complejidad = niv_complejidad
    End Sub

    Public Sub New()
    End Sub
End Class