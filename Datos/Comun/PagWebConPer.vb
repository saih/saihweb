Public Class PagWebConPer
     Private _id As integer
     Private _id_PaginaWebControl As integer
     Private _idperfil As string
     Private _accion As string

     Public Property id() As integer
        Get
            Return _id
        End Get
        Set(ByVal value As integer)
            _id = value
        End Set
     End Property

     Public Property id_PaginaWebControl() As integer
        Get
            Return _id_PaginaWebControl
        End Get
        Set(ByVal value As integer)
            _id_PaginaWebControl = value
        End Set
     End Property

     Public Property idperfil() As string
        Get
            Return _idperfil
        End Get
        Set(ByVal value As string)
            _idperfil = value
        End Set
     End Property

     Public Property accion() As string
        Get
            Return _accion
        End Get
        Set(ByVal value As string)
            _accion = value
        End Set
     End Property

    Public Sub New(
      ByVal id As integer,
      ByVal id_PaginaWebControl As integer,
      ByVal idperfil As string,
      ByVal accion As string
    )
      Me.id = id
      Me.id_PaginaWebControl = id_PaginaWebControl
      Me.idperfil = idperfil
      Me.accion = accion
    End Sub

    Public Sub New()
    End Sub
End Class