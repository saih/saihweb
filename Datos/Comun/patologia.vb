Public Class patologia
     Private _codigo As string
     Private _nombre As string
     Private _aplica_sexo As string
     Private _edad_inicial As integer
     Private _edad_final As integer
     Private _ind_patep As integer

     Public Property codigo() As string
        Get
            Return _codigo
        End Get
        Set(ByVal value As string)
            _codigo = value
        End Set
     End Property

     Public Property nombre() As string
        Get
            Return _nombre
        End Get
        Set(ByVal value As string)
            _nombre = value
        End Set
     End Property

     Public Property aplica_sexo() As string
        Get
            Return _aplica_sexo
        End Get
        Set(ByVal value As string)
            _aplica_sexo = value
        End Set
     End Property

     Public Property edad_inicial() As integer
        Get
            Return _edad_inicial
        End Get
        Set(ByVal value As integer)
            _edad_inicial = value
        End Set
     End Property

     Public Property edad_final() As integer
        Get
            Return _edad_final
        End Get
        Set(ByVal value As integer)
            _edad_final = value
        End Set
     End Property

     Public Property ind_patep() As integer
        Get
            Return _ind_patep
        End Get
        Set(ByVal value As integer)
            _ind_patep = value
        End Set
     End Property

    Public Sub New(
      ByVal codigo As string,
      ByVal nombre As string,
      ByVal aplica_sexo As string,
      ByVal edad_inicial As integer,
      ByVal edad_final As integer,
      ByVal ind_patep As integer
    )
      Me.codigo = codigo
      Me.nombre = nombre
      Me.aplica_sexo = aplica_sexo
      Me.edad_inicial = edad_inicial
      Me.edad_final = edad_final
      Me.ind_patep = ind_patep
    End Sub

    Public Sub New()
    End Sub
End Class