Public Class cajero
     Private _codigo_cajero As string
     Private _usuario As string
     Private _base As decimal
     Private _fecha_apertura As string
     Private _saldo As decimal
     Private _estado As string

     Public Property codigo_cajero() As string
        Get
            Return _codigo_cajero
        End Get
        Set(ByVal value As string)
            _codigo_cajero = value
        End Set
     End Property

     Public Property usuario() As string
        Get
            Return _usuario
        End Get
        Set(ByVal value As string)
            _usuario = value
        End Set
     End Property

     Public Property base() As decimal
        Get
            Return _base
        End Get
        Set(ByVal value As decimal)
            _base = value
        End Set
     End Property

     Public Property fecha_apertura() As string
        Get
            Return _fecha_apertura
        End Get
        Set(ByVal value As string)
            _fecha_apertura = value
        End Set
     End Property

     Public Property saldo() As decimal
        Get
            Return _saldo
        End Get
        Set(ByVal value As decimal)
            _saldo = value
        End Set
     End Property

     Public Property estado() As string
        Get
            Return _estado
        End Get
        Set(ByVal value As string)
            _estado = value
        End Set
     End Property

    Public Sub New(
      ByVal codigo_cajero As string,
      ByVal usuario As string,
      ByVal base As decimal,
      ByVal fecha_apertura As string,
      ByVal saldo As decimal,
      ByVal estado As string
    )
      Me.codigo_cajero = codigo_cajero
      Me.usuario = usuario
      Me.base = base
      Me.fecha_apertura = fecha_apertura
      Me.saldo = saldo
      Me.estado = estado
    End Sub

    Public Sub New()
    End Sub
End Class