Public Class emp_subsidio
     Private _codemp As string
     Private _estrato As string
     Private _subsidio As decimal

     Public Property codemp() As string
        Get
            Return _codemp
        End Get
        Set(ByVal value As string)
            _codemp = value
        End Set
     End Property

     Public Property estrato() As string
        Get
            Return _estrato
        End Get
        Set(ByVal value As string)
            _estrato = value
        End Set
     End Property

     Public Property subsidio() As decimal
        Get
            Return _subsidio
        End Get
        Set(ByVal value As decimal)
            _subsidio = value
        End Set
     End Property

    Public Sub New(
      ByVal codemp As string,
      ByVal estrato As string,
      ByVal subsidio As decimal
    )
      Me.codemp = codemp
      Me.estrato = estrato
      Me.subsidio = subsidio
    End Sub

    Public Sub New()
    End Sub
End Class