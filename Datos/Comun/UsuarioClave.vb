Public Class UsuarioClave
     Private _id As integer
     Private _Usuario As string
     Private _fecha As string
     Private _clave As string

     Public Property id() As integer
        Get
            Return _id
        End Get
        Set(ByVal value As integer)
            _id = value
        End Set
     End Property

     Public Property Usuario() As string
        Get
            Return _Usuario
        End Get
        Set(ByVal value As string)
            _Usuario = value
        End Set
     End Property

     Public Property fecha() As string
        Get
            Return _fecha
        End Get
        Set(ByVal value As string)
            _fecha = value
        End Set
     End Property

     Public Property clave() As string
        Get
            Return _clave
        End Get
        Set(ByVal value As string)
            _clave = value
        End Set
     End Property

    Public Sub New(
      ByVal id As integer,
      ByVal Usuario As string,
      ByVal fecha As string,
      ByVal clave As string
    )
      Me.id = id
      Me.Usuario = Usuario
      Me.fecha = fecha
      Me.clave = clave
    End Sub

    Public Sub New()
    End Sub
End Class