Public Class menu_perfil
     Private _idperfil As string
     Private _descripcion As string
     Private _Perfil_Csc As integer
     Private _manAtenciones As integer

     Public Property idperfil() As string
        Get
            Return _idperfil
        End Get
        Set(ByVal value As string)
            _idperfil = value
        End Set
     End Property

     Public Property descripcion() As string
        Get
            Return _descripcion
        End Get
        Set(ByVal value As string)
            _descripcion = value
        End Set
     End Property

     Public Property Perfil_Csc() As integer
        Get
            Return _Perfil_Csc
        End Get
        Set(ByVal value As integer)
            _Perfil_Csc = value
        End Set
     End Property

     Public Property manAtenciones() As integer
        Get
            Return _manAtenciones
        End Get
        Set(ByVal value As integer)
            _manAtenciones = value
        End Set
     End Property

    Public Sub New(
      ByVal idperfil As string,
      ByVal descripcion As string,
      ByVal Perfil_Csc As integer,
      ByVal manAtenciones As integer
    )
      Me.idperfil = idperfil
      Me.descripcion = descripcion
      Me.Perfil_Csc = Perfil_Csc
      Me.manAtenciones = manAtenciones
    End Sub

    Public Sub New()
    End Sub
End Class