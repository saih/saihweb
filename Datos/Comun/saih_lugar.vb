Public Class saih_lugar
     Private _Id As integer
     Private _Tipo As integer
     Private _Descripcion As string
     Private _Id_Lugar As integer
     Private _Codigo As string

     Public Property Id() As integer
        Get
            Return _Id
        End Get
        Set(ByVal value As integer)
            _Id = value
        End Set
     End Property

     Public Property Tipo() As integer
        Get
            Return _Tipo
        End Get
        Set(ByVal value As integer)
            _Tipo = value
        End Set
     End Property

     Public Property Descripcion() As string
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As string)
            _Descripcion = value
        End Set
     End Property

     Public Property Id_Lugar() As integer
        Get
            Return _Id_Lugar
        End Get
        Set(ByVal value As integer)
            _Id_Lugar = value
        End Set
     End Property

     Public Property Codigo() As string
        Get
            Return _Codigo
        End Get
        Set(ByVal value As string)
            _Codigo = value
        End Set
     End Property

    Public Sub New(
      ByVal Id As integer,
      ByVal Tipo As integer,
      ByVal Descripcion As string,
      ByVal Id_Lugar As integer,
      ByVal Codigo As string
    )
      Me.Id = Id
      Me.Tipo = Tipo
      Me.Descripcion = Descripcion
      Me.Id_Lugar = Id_Lugar
      Me.Codigo = Codigo
    End Sub

    Public Sub New()
    End Sub
End Class