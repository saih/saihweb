Public Class exc_quirurjicas
     Private _manual As string
     Private _codigo As string
     Private _nombre As string
     Private _tipo_porcentaje As string
     Private _porc_honmed As decimal
     Private _porc_honane As decimal
     Private _porc_honayu As decimal
     Private _porc_dersal As decimal
     Private _porc_dersalesp As decimal
     Private _porc_matsum As decimal
     Private _porc_honper As decimal
     Private _porc_paqquir As decimal

     Public Property manual() As string
        Get
            Return _manual
        End Get
        Set(ByVal value As string)
            _manual = value
        End Set
     End Property

     Public Property codigo() As string
        Get
            Return _codigo
        End Get
        Set(ByVal value As string)
            _codigo = value
        End Set
     End Property

     Public Property nombre() As string
        Get
            Return _nombre
        End Get
        Set(ByVal value As string)
            _nombre = value
        End Set
     End Property

     Public Property tipo_porcentaje() As string
        Get
            Return _tipo_porcentaje
        End Get
        Set(ByVal value As string)
            _tipo_porcentaje = value
        End Set
     End Property

     Public Property porc_honmed() As decimal
        Get
            Return _porc_honmed
        End Get
        Set(ByVal value As decimal)
            _porc_honmed = value
        End Set
     End Property

     Public Property porc_honane() As decimal
        Get
            Return _porc_honane
        End Get
        Set(ByVal value As decimal)
            _porc_honane = value
        End Set
     End Property

     Public Property porc_honayu() As decimal
        Get
            Return _porc_honayu
        End Get
        Set(ByVal value As decimal)
            _porc_honayu = value
        End Set
     End Property

     Public Property porc_dersal() As decimal
        Get
            Return _porc_dersal
        End Get
        Set(ByVal value As decimal)
            _porc_dersal = value
        End Set
     End Property

     Public Property porc_dersalesp() As decimal
        Get
            Return _porc_dersalesp
        End Get
        Set(ByVal value As decimal)
            _porc_dersalesp = value
        End Set
     End Property

     Public Property porc_matsum() As decimal
        Get
            Return _porc_matsum
        End Get
        Set(ByVal value As decimal)
            _porc_matsum = value
        End Set
     End Property

     Public Property porc_honper() As decimal
        Get
            Return _porc_honper
        End Get
        Set(ByVal value As decimal)
            _porc_honper = value
        End Set
     End Property

     Public Property porc_paqquir() As decimal
        Get
            Return _porc_paqquir
        End Get
        Set(ByVal value As decimal)
            _porc_paqquir = value
        End Set
     End Property

    Public Sub New(
      ByVal manual As string,
      ByVal codigo As string,
      ByVal nombre As string,
      ByVal tipo_porcentaje As string,
      ByVal porc_honmed As decimal,
      ByVal porc_honane As decimal,
      ByVal porc_honayu As decimal,
      ByVal porc_dersal As decimal,
      ByVal porc_dersalesp As decimal,
      ByVal porc_matsum As decimal,
      ByVal porc_honper As decimal,
      ByVal porc_paqquir As decimal
    )
      Me.manual = manual
      Me.codigo = codigo
      Me.nombre = nombre
      Me.tipo_porcentaje = tipo_porcentaje
      Me.porc_honmed = porc_honmed
      Me.porc_honane = porc_honane
      Me.porc_honayu = porc_honayu
      Me.porc_dersal = porc_dersal
      Me.porc_dersalesp = porc_dersalesp
      Me.porc_matsum = porc_matsum
      Me.porc_honper = porc_honper
      Me.porc_paqquir = porc_paqquir
    End Sub

    Public Sub New()
    End Sub
End Class