Public Class cama_procedimiento
     Private _codigo_cama As string
     Private _manual As string
     Private _codpcd As string

     Public Property codigo_cama() As string
        Get
            Return _codigo_cama
        End Get
        Set(ByVal value As string)
            _codigo_cama = value
        End Set
     End Property

     Public Property manual() As string
        Get
            Return _manual
        End Get
        Set(ByVal value As string)
            _manual = value
        End Set
     End Property

     Public Property codpcd() As string
        Get
            Return _codpcd
        End Get
        Set(ByVal value As string)
            _codpcd = value
        End Set
     End Property

    Public Sub New(
      ByVal codigo_cama As string,
      ByVal manual As string,
      ByVal codpcd As string
    )
      Me.codigo_cama = codigo_cama
      Me.manual = manual
      Me.codpcd = codpcd
    End Sub

    Public Sub New()
    End Sub
End Class