Public Class Usuario
     Private _Usuario As string
     Private _Password As string
     Private _Nombre1 As string
     Private _Nombre2 As string
     Private _Apellido1 As string
     Private _Apellido2 As string
     Private _Correo As string
     Private _Estado As integer
     Private _Identificacion As string
     Private _idperfil As string
     Private _razonsocial As string
     Private _cod_pas As string

     Public Property Usuario() As string
        Get
            Return _Usuario
        End Get
        Set(ByVal value As string)
            _Usuario = value
        End Set
     End Property

     Public Property Password() As string
        Get
            Return _Password
        End Get
        Set(ByVal value As string)
            _Password = value
        End Set
     End Property

     Public Property Nombre1() As string
        Get
            Return _Nombre1
        End Get
        Set(ByVal value As string)
            _Nombre1 = value
        End Set
     End Property

     Public Property Nombre2() As string
        Get
            Return _Nombre2
        End Get
        Set(ByVal value As string)
            _Nombre2 = value
        End Set
     End Property

     Public Property Apellido1() As string
        Get
            Return _Apellido1
        End Get
        Set(ByVal value As string)
            _Apellido1 = value
        End Set
     End Property

     Public Property Apellido2() As string
        Get
            Return _Apellido2
        End Get
        Set(ByVal value As string)
            _Apellido2 = value
        End Set
     End Property

     Public Property Correo() As string
        Get
            Return _Correo
        End Get
        Set(ByVal value As string)
            _Correo = value
        End Set
     End Property

     Public Property Estado() As integer
        Get
            Return _Estado
        End Get
        Set(ByVal value As integer)
            _Estado = value
        End Set
     End Property

     Public Property Identificacion() As string
        Get
            Return _Identificacion
        End Get
        Set(ByVal value As string)
            _Identificacion = value
        End Set
     End Property

     Public Property idperfil() As string
        Get
            Return _idperfil
        End Get
        Set(ByVal value As string)
            _idperfil = value
        End Set
     End Property

     Public Property razonsocial() As string
        Get
            Return _razonsocial
        End Get
        Set(ByVal value As string)
            _razonsocial = value
        End Set
     End Property

     Public Property cod_pas() As string
        Get
            Return _cod_pas
        End Get
        Set(ByVal value As string)
            _cod_pas = value
        End Set
     End Property

    Public Sub New(
      ByVal Usuario As string,
      ByVal Password As string,
      ByVal Nombre1 As string,
      ByVal Nombre2 As string,
      ByVal Apellido1 As string,
      ByVal Apellido2 As string,
      ByVal Correo As string,
      ByVal Estado As integer,
      ByVal Identificacion As string,
      ByVal idperfil As string,
      ByVal razonsocial As string,
      ByVal cod_pas As string
    )
      Me.Usuario = Usuario
      Me.Password = Password
      Me.Nombre1 = Nombre1
      Me.Nombre2 = Nombre2
      Me.Apellido1 = Apellido1
      Me.Apellido2 = Apellido2
      Me.Correo = Correo
      Me.Estado = Estado
      Me.Identificacion = Identificacion
      Me.idperfil = idperfil
      Me.razonsocial = razonsocial
      Me.cod_pas = cod_pas
    End Sub

    Public Sub New()
    End Sub
End Class