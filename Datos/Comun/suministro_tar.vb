Public Class suministro_tar
     Private _cod_suministro As string
     Private _manual As string
     Private _plan_tar As string
     Private _valor As decimal

     Public Property cod_suministro() As string
        Get
            Return _cod_suministro
        End Get
        Set(ByVal value As string)
            _cod_suministro = value
        End Set
     End Property

     Public Property manual() As string
        Get
            Return _manual
        End Get
        Set(ByVal value As string)
            _manual = value
        End Set
     End Property

     Public Property plan_tar() As string
        Get
            Return _plan_tar
        End Get
        Set(ByVal value As string)
            _plan_tar = value
        End Set
     End Property

     Public Property valor() As decimal
        Get
            Return _valor
        End Get
        Set(ByVal value As decimal)
            _valor = value
        End Set
     End Property

    Public Sub New(
      ByVal cod_suministro As string,
      ByVal manual As string,
      ByVal plan_tar As string,
      ByVal valor As decimal
    )
      Me.cod_suministro = cod_suministro
      Me.manual = manual
      Me.plan_tar = plan_tar
      Me.valor = valor
    End Sub

    Public Sub New()
    End Sub
End Class