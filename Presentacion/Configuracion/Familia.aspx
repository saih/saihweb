﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Familia.aspx.vb" Inherits="Presentacion.Familia" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <link href="../public/css/estilos.css" type="text/css" rel="stylesheet" />
    <title></title>
    <script type="text/javascript" src="./../public/js/jquery-1.9.0.js"></script>
    <script type="text/javascript" src="./../public/js/jquery.timer.js"></script>
    <script type="text/javascript" src="./../public/js/app.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" />        
    <ext:Store 
        ID="stFamilia" 
        runat="server" 
        AutoLoad="true">
        <Reader>
            <ext:JsonReader IDProperty="id">
                <Fields>
                    <ext:RecordField Name="id" />
                    <ext:RecordField Name="descripcion" />
                    <ext:RecordField Name="fecha_adicion" />
                </Fields>
            </ext:JsonReader>
        </Reader>
    </ext:Store>

    <ext:Store 
        ID="stHis" 
        runat="server" 
        AutoLoad="true">
        <Reader>
            <ext:JsonReader IDProperty="numhis">
                <Fields>
                    <ext:RecordField Name="id_familia" />
                    <ext:RecordField Name="numhis" />
                    <ext:RecordField Name="tipo_identificacion" />
                    <ext:RecordField Name="identificacion" />
                    <ext:RecordField Name="razonsocial" />
                    <ext:RecordField Name="rol" />
                </Fields>
            </ext:JsonReader>
        </Reader>
    </ext:Store>

    <ext:Store 
        ID="stEsp" 
        runat="server" 
        AutoLoad="true">
        <Reader>
            <ext:JsonReader IDProperty="cod_especialidad">
                <Fields>
                    <ext:RecordField Name="id_familia" />
                    <ext:RecordField Name="cod_especialidad" />
                    <ext:RecordField Name="especialidad" />
                    <ext:RecordField Name="identificacion" />
                    <ext:RecordField Name="nombres" />
                </Fields>
            </ext:JsonReader>
        </Reader>
    </ext:Store>

    <ext:Panel ID="Panel3" runat="server" Frame="true" PaddingSummary="5px 5px 0" AutoWidth="true" Height="650" ButtonAlign="Center" Style="text-align: left" Title="Grupos Familiares">
        <Items>
            <ext:FieldSet ID="FieldSet3" runat="server" Title="Campos de Busqueda" Padding="10" Height="60">
                <Items>
                    <ext:Container ID="Container10" runat="server" Layout="ColumnLayout" Height="30">
                        <Items>                           
                            <ext:Container ID="Container1" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25">
                                <Items>
                                    <ext:TextField runat="server" ID="txtCodigoBsq" FieldLabel="Codigo" />
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container3" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25">
                                <Items>
                                    <ext:TextField runat="server" ID="txtDescripcionBsq" FieldLabel="Descripción" AnchorHorizontal="99%" />
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container4" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25">
                                <Items>
                                    <ext:TextField runat="server" ID="txtNumHisBsq" FieldLabel="Identificación" />
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container2" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".2">
                                <Items>
                                    <ext:Button runat="server" ID="btnBuscar" Text="Buscar" Icon="ApplicationGo" >
                                        <DirectEvents>
                                            <Click OnEvent="btnBuscar_DirectClick">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:Container>
                </Items>
            </ext:FieldSet>
            <ext:GridPanel
                ID="grdFamilia"
                runat="server"                                            
                Margins="0 0 5 5"
                Icon="TransmitGo"
                Frame="true"
                Height="200"
                StoreId="stFamilia"
                Title="Grupos Familiares"
                >
                <ColumnModel ID="ColumnModel3" runat="server">
                    <Columns>
                        <ext:Column Header="Codigo" DataIndex="id" />
                        <ext:Column Header="Descripción" DataIndex="descripcion" Width="380"/>
                        <ext:Column Header="Fecha Adición" DataIndex="fecha_adicion" Width="180" />                                                    
                    </Columns>                                
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" SingleSelect="true" >
                        <DirectEvents>
                            <RowSelect OnEvent="rowSelect" Buffer="100">
                                <ExtraParams>                                    
                                    <ext:Parameter Name="ID" Value="this.getSelected().id" Mode="Raw" />
                                </ExtraParams>
                            </RowSelect>
                        </DirectEvents>
                    </ext:RowSelectionModel>
                </SelectionModel>
                <LoadMask ShowMask="true" Msg="Loading...." />
                <BottomBar>
                    <ext:StatusBar runat="server" ID="StatusBar2">
                        <Items>                                                        
                            <ext:Button runat="server" Text="Nuevo Grupo Familiar " ID="btnNuevoGruFamiliar" Icon="Add" />
                            <ext:Button runat="server" Text="Editar Grupo Familiar" ID="btnEditaGruFamiliar" Icon="ApplicationEdit" />
                            <ext:Button runat="server" Text="Eliminar Grupo Familiar" ID="btnEliminaGruFamiliar" Icon="ApplicationDelete" />
                        </Items>
                    </ext:StatusBar>
                </BottomBar>
            </ext:GridPanel>
            <ext:Container ID="Container6" runat="server" Layout="ColumnLayout" Height="350">
                <Items>
                    <ext:Container ID="Container64" runat="server" LabelAlign="Left" Layout="FormLayout" ColumnWidth=".5">
                        <Items>
                            <ext:GridPanel
                                ID="grdHis"
                                runat="server"                                            
                                Margins="0 0 5 5"
                                Icon="TransmitGo"
                                Frame="true"
                                Height="300"
                                StoreId="stHis"
                                Title="Pacientes"
                            >
                                <ColumnModel ID="ColumnModel1" runat="server">
                                    <Columns>                        
                                        <ext:Column Header="id_familia" DataIndex="id_familia" Hidden="true" />
                                        <ext:Column Header="Código" DataIndex="numhis" Hidden="true"/>                                        
                                        <ext:Column Header="Identificación" DataIndex="identificacion" />
                                        <ext:Column Header="Tipo" DataIndex="tipo_identificacion" Width="60" />
                                        <ext:Column Header="Nombre" DataIndex="razonsocial" Width="190"/>
                                        <ext:Column Header="Rol" DataIndex="rol" />
                                    </Columns>                                
                                </ColumnModel>
                                <SelectionModel>
                                    <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" SingleSelect="true" >
                                        <DirectEvents>
                                            <RowSelect OnEvent="rowSelectHis" Buffer="100">
                                                <ExtraParams>                                    
                                                    <ext:Parameter Name="ID" Value="this.getSelected().id" Mode="Raw" />
                                                </ExtraParams>
                                            </RowSelect>
                                        </DirectEvents>
                                    </ext:RowSelectionModel>
                                </SelectionModel>
                                <LoadMask ShowMask="true" Msg="Loading...." />
                                <BottomBar>
                                    <ext:StatusBar runat="server" ID="StatusBar1">
                                        <Items>                                                        
                                            <ext:Button runat="server" Text="Nuevo Paciente" ID="btnNuevoPaciente" Icon="Add" />
                                            <ext:Button runat="server" Text="Editar Paciente" ID="btnEditaPaciente" Icon="ApplicationEdit" />
                                            <ext:Button runat="server" Text="Eliminar Paciente" ID="btnEliminaPaciente" Icon="ApplicationDelete" />
                                        </Items>
                                    </ext:StatusBar>
                                </BottomBar>
                            </ext:GridPanel>
                        </Items>
                    </ext:Container>
                    <ext:Container ID="Container5" runat="server" LabelAlign="Left" Layout="FormLayout" ColumnWidth=".5">
                        <Items>
                            <ext:GridPanel
                                ID="grdEsp"
                                runat="server"                                            
                                Margins="0 0 5 5"
                                Icon="TransmitGo"
                                Frame="true"
                                Height="300"
                                StoreId="stEsp"
                                Title="Especialidad"
                            >
                                <ColumnModel ID="ColumnModel2" runat="server">
                                    <Columns>                        
                                        <ext:Column Header="id_familia" DataIndex="id_familia" Hidden="true" />
                                        <ext:Column Header="Código" DataIndex="cod_especialidad" Hidden="true" />
                                        <ext:Column Header="Especialidad" DataIndex="especialidad" Width="180" />
                                        <ext:Column Header="Doc.Medico" DataIndex="identificacion" />                        
                                        <ext:Column Header="Medico" DataIndex="nombres" Width="250"/>                                        
                                    </Columns>                                
                                </ColumnModel>
                                <SelectionModel>
                                    <ext:RowSelectionModel ID="RowSelectionModel3" runat="server" SingleSelect="true" >
                                        <DirectEvents>
                                            <RowSelect OnEvent="rowSelectEsp" Buffer="100">
                                                <ExtraParams>                                    
                                                    <ext:Parameter Name="ID" Value="this.getSelected().id" Mode="Raw" />
                                                </ExtraParams>
                                            </RowSelect>
                                        </DirectEvents>
                                    </ext:RowSelectionModel>
                                </SelectionModel>
                                <LoadMask ShowMask="true" Msg="Loading...." />
                                <BottomBar>
                                    <ext:StatusBar runat="server" ID="StatusBar3">
                                        <Items>                                                        
                                            <ext:Button runat="server" Text="Nueva Especialidad" ID="btnNuevaEspecialidad" Icon="Add" />
                                            <ext:Button runat="server" Text="Editar Especialidad" ID="btnEditaEspecialidad" Icon="ApplicationEdit" />
                                            <ext:Button runat="server" Text="Eliminar Especialidad" ID="btnEliminaEspecialidad" Icon="ApplicationDelete" />
                                        </Items>
                                    </ext:StatusBar>
                                </BottomBar>
                            </ext:GridPanel>
                        </Items>
                    </ext:Container>
                </Items>
            </ext:Container>
        </Items>
    </ext:Panel>


    <ext:Window 
        ID="vntFamilia"
        runat="server"
        Icon="ApplicationFormEdit"
        Width="450"
        Height="250"
        Hidden="true" 
        Modal="true"            
        Title="Grupo Familiar"
        Constrain="true">
        <Items>
            <ext:Panel ID="Panel1" 
            runat="server" 
            Title=""
            AnchorHorizontal="100%"
            Height="220"
            Layout="Fit">
            <Items>
                <ext:FormPanel 
                    ID="StatusForm" 
                    runat="server"
                    LabelWidth="75"
                    ButtonAlign="Right"
                    Border="false"
                    PaddingSummary="10px 10px 10px">
                    <Defaults>                        
                        <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                        <ext:Parameter Name="SelectOnFocus" Value="true" Mode="Raw" />
                        <ext:Parameter Name="MsgTarget" Value="side" />
                    </Defaults>
                    <Items>
                        <ext:TextField runat="server" FieldLabel="Codigo" ID="txtId" Disabled="true"  />
                        <ext:TextField runat="server" ID="txtDescripcion" FieldLabel="Descripción" AnchorHorizontal="90%" BlankText="Descripción Requerida" DataIndex="descripcion" />
                        <ext:TextField runat="server" FieldLabel="Fecha Add" ID="txtUsuarioAdicion" DataIndex="usuario_creo" Disabled="true"  />
                        <ext:TextField runat="server" FieldLabel="Usuario Add" ID="txtFechaAdicion" DataIndex="fecha_adicion" Disabled="true"  />
                    </Items>
                    <Buttons>
                        <ext:Button ID="btnGuardaFamilia" runat="server" Text="Guardar" Icon="Disk">
                            <DirectEvents>
                                <Click 
                                    OnEvent="btnGuardaFamilia_DirectClick" 
                                    Before="var valid= #{StatusForm}.getForm().isValid(); if (valid) {#{FormStatusBar}.showBusy('Guardando Formulario...');} return valid;">
                                    <EventMask 
                                        ShowMask="true" 
                                        MinDelay="1000" 
                                        Target="CustomTarget" 
                                        CustomTarget="={#{StatusForm}.getEl()}" 
                                        />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                        <ext:Button runat="server" ID="btnCerrarVntFamilia" Text="Cerrar" />
                    </Buttons>
                </ext:FormPanel>
            </Items>
            <BottomBar>
                <ext:StatusBar ID="FormStatusBar" runat="server" DefaultText="Ready">
                    <Plugins>
                        <ext:ValidationStatus ID="ValidationStatus1" 
                            runat="server" 
                            FormPanelID="StatusForm" 
                            ValidIcon="Accept" 
                            ErrorIcon="Exclamation" 
                            ShowText="El Formulario tiene Errores (Click para ver Detalles)"
                            HideText="Click para Ocultar Errores"
                            />
                    </Plugins>
                </ext:StatusBar>
            </BottomBar>
        </ext:Panel>
        </Items>
    </ext:Window>


    <ext:Window 
        ID="vntHis"
        runat="server"
        Icon="ApplicationFormEdit"
        Width="450"
        Height="250"
        Hidden="true" 
        Modal="true"            
        Title="Pacientes"
        Constrain="true">
        <Items>
            <ext:Panel ID="Panel2" 
            runat="server" 
            Title=""
            AnchorHorizontal="100%"
            Height="220"
            Layout="Fit">
            <Items>
                <ext:FormPanel 
                    ID="StatusFormHis" 
                    runat="server"
                    LabelWidth="75"
                    ButtonAlign="Right"
                    Border="false"
                    PaddingSummary="10px 10px 10px">
                    <Defaults>                        
                        <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                        <ext:Parameter Name="SelectOnFocus" Value="true" Mode="Raw" />
                        <ext:Parameter Name="MsgTarget" Value="side" />
                    </Defaults>
                    <Items>
						<ext:Label ID="lblIdentificacion" runat="server" FieldLabel="Doc.Identidad" />
						<ext:Label ID="lblNumHistoria" runat="server" FieldLabel="Nro.Historia" />
                        <ext:ComboBox
							ID="CbPaciente"
							FieldLabel="Paciente"
							runat="server"
							DisplayField="razonsocial"
							ValueField="numhis"
							TypeAhead="false"
							LoadingText="Buscando...."
							Width="300"
							PageSize="10"
							HideTrigger="true"
							ItemSelector="div.search-item"
							MinChars="6"
							AllowBlank="false"                                            
							>
							<Store>
								<ext:Store runat="server" AutoLoad="false" ID="stCbPaciente">
									<Proxy>
										<ext:HttpProxy Method="POST" Url=".././Funciones/Paciente.ashx" />
									</Proxy>
									<Reader>
										<ext:JsonReader Root="paciente" TotalProperty="total">
											<Fields>
												<ext:RecordField Name="numhis" />
												<ext:RecordField Name="tipo_identificacion" />
												<ext:RecordField Name="identificacion" />
												<ext:RecordField Name="razonsocial" />                                                                
											</Fields>                                                        
										</ext:JsonReader>
									</Reader>                                                
								</ext:Store>
							</Store>
							<Template runat="server" ID="ctl1788">
								<Html>
									<tpl for=".">
										<div class="search-item">
											<h3><span>{tipo_identificacion}-{identificacion}</span>{razonsocial}</h3>							                               
										</div>                                                        
									</tpl>                                                    
								</Html>                                            
							</Template>
						</ext:ComboBox>
                        <ext:ComboBox ID="cbRol" runat="server"  FieldLabel="Rol" AllowBlank="false" AnchorHorizontal="95%" DataIndex="rol">
                            <Items>
                                <ext:ListItem Text="Padre" Value="padre" />
                                <ext:ListItem Text="Madre" Value="madre" />
                                <ext:ListItem Text="Hijo(a)" Value="hijo" />
                                <ext:ListItem Text="Otro" Value="otro" />
                            </Items>
                        </ext:ComboBox>
                    </Items>
                    <Buttons>
                        <ext:Button ID="btnGuardarPaciente" runat="server" Text="Guardar" Icon="Disk">
                            <DirectEvents>
                                <Click 
                                    OnEvent="btnGuardarPaciente_DirectClick" 
                                    Before="var valid= #{StatusFormHis}.getForm().isValid(); if (valid) {#{FormStatusBarHis}.showBusy('Guardando Formulario...');} return valid;">
                                    <EventMask 
                                        ShowMask="true" 
                                        MinDelay="1000" 
                                        Target="CustomTarget" 
                                        CustomTarget="={#{StatusFormHis}.getEl()}" 
                                        />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                        <ext:Button runat="server" ID="btnCerrarVntPaciente" Text="Cerrar" />
                    </Buttons>
                </ext:FormPanel>
            </Items>
            <BottomBar>
                <ext:StatusBar ID="FormStatusBarHis" runat="server" DefaultText="Ready">
                    <Plugins>
                        <ext:ValidationStatus ID="ValidationStatus2" 
                            runat="server" 
                            FormPanelID="StatusFormHis" 
                            ValidIcon="Accept" 
                            ErrorIcon="Exclamation" 
                            ShowText="El Formulario tiene Errores (Click para ver Detalles)"
                            HideText="Click para Ocultar Errores"
                            />
                    </Plugins>
                </ext:StatusBar>
            </BottomBar>
        </ext:Panel>
        </Items>
    </ext:Window>


    <ext:Window 
        ID="vntEsp"
        runat="server"
        Icon="ApplicationFormEdit"
        Width="450"
        Height="250"
        Hidden="true" 
        Modal="true"            
        Title="Especialidades"
        Constrain="true">
        <Items>
            <ext:Panel ID="Panel4" 
            runat="server" 
            Title=""
            AnchorHorizontal="100%"
            Height="220"
            Layout="Fit">
            <Items>
                <ext:FormPanel 
                    ID="StatusFormEsp" 
                    runat="server"
                    LabelWidth="75"
                    ButtonAlign="Right"
                    Border="false"
                    PaddingSummary="10px 10px 10px">
                    <Defaults>                        
                        <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                        <ext:Parameter Name="SelectOnFocus" Value="true" Mode="Raw" />
                        <ext:Parameter Name="MsgTarget" Value="side" />
                    </Defaults>
                    <Items>
						<ext:ComboBox     
	                        ID="cbCodEspecialidad"                                                                              
	                        runat="server" 
	                        Shadow="Drop" 
	                        Mode="Local" 
	                        TriggerAction="All" 
	                        ForceSelection="true"
	                        DisplayField="nombre"
	                        ValueField="codigo"	
	                        EmptyText="Seleccione Especialidad"
	                        AnchorHorizontal="99%"
	                        AllowBlank="false"
                            FieldLabel="Especialidad"
                            DataIndex="cod_especialidad"
	                        >
	                        <Store>
		                        <ext:Store ID="stCodEspecialidad" runat="server" AutoLoad="true">
			                        <Reader>
				                        <ext:JsonReader IDProperty="codigo">
					                        <Fields>
						                        <ext:RecordField Name="codigo" />
						                        <ext:RecordField Name="nombre" />
					                        </Fields>
				                        </ext:JsonReader>
			                        </Reader>            
		                        </ext:Store>    
	                        </Store>    
                        </ext:ComboBox>
                        <ext:ComboBox     
	                        ID="cbPAsistencial"                                                                              
	                        runat="server" 
	                        Shadow="Drop" 
	                        Mode="Local" 
	                        TriggerAction="All" 
	                        ForceSelection="true"
	                        DisplayField="nombres"
	                        ValueField="codigo"	
	                        EmptyText="Seleccione Profesional"
	                        AnchorHorizontal="99%"
	                        FieldLabel="Medico"
                            AllowBlank="false"
                            DataIndex="codpas"
	                        >
	                        <Store>
		                        <ext:Store ID="stPAsistencial" runat="server" AutoLoad="true">
			                        <Reader>
				                        <ext:JsonReader IDProperty="codigo">
					                        <Fields>
						                        <ext:RecordField Name="codigo" />
						                        <ext:RecordField Name="nombres" />
					                        </Fields>
				                        </ext:JsonReader>
			                        </Reader>            
		                        </ext:Store>    
	                        </Store>    
                        </ext:ComboBox>
                    </Items>
                    <Buttons>
                        <ext:Button ID="btnGuardarEspecialidad" runat="server" Text="Guardar" Icon="Disk">
                            <DirectEvents>
                                <Click 
                                    OnEvent="btnGuardarEspecialidad_DirectClick" 
                                    Before="var valid= #{StatusFormEsp}.getForm().isValid(); if (valid) {#{FormStatusBarEsp}.showBusy('Guardando Formulario...');} return valid;">
                                    <EventMask 
                                        ShowMask="true" 
                                        MinDelay="1000" 
                                        Target="CustomTarget" 
                                        CustomTarget="={#{StatusFormEsp}.getEl()}" 
                                        />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                        <ext:Button runat="server" ID="btnCerrarVntEspecialidad" Text="Cerrar" />
                    </Buttons>
                </ext:FormPanel>
            </Items>
            <BottomBar>
                <ext:StatusBar ID="FormStatusBarEsp" runat="server" DefaultText="Ready">
                    <Plugins>
                        <ext:ValidationStatus ID="ValidationStatus3" 
                            runat="server" 
                            FormPanelID="StatusFormEsp" 
                            ValidIcon="Accept" 
                            ErrorIcon="Exclamation" 
                            ShowText="El Formulario tiene Errores (Click para ver Detalles)"
                            HideText="Click para Ocultar Errores"
                            />
                    </Plugins>
                </ext:StatusBar>
            </BottomBar>
        </ext:Panel>
        </Items>
    </ext:Window>

    </form>
</body>
</html>
