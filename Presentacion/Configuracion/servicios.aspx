﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="servicios.aspx.vb" Inherits="Presentacion.servicios" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    
    <ext:ResourceManager ID="ResourceManager2" runat="server" />
        
    <ext:Store 
        ID="stSer" 
        runat="server" 
        AutoLoad="true">
        <Reader>
            <ext:JsonReader IDProperty="codser">
                <Fields>
                    <ext:RecordField Name="codser" />                    
                    <ext:RecordField Name="nombre" />                    
                </Fields>
            </ext:JsonReader>
        </Reader>
    </ext:Store>
    <ext:Panel ID="Panel3" runat="server" Frame="true" PaddingSummary="5px 5px 0" AutoWidth="true" Height="650" ButtonAlign="Center" Style="text-align: left" Title="Servicios">
        <Items>
            <ext:FieldSet ID="FieldSet3" runat="server" Title="Campos de Busqueda" Padding="10" Height="60">
                <Items>
                    <ext:Container ID="Container10" runat="server" Layout="ColumnLayout" Height="30">
                        <Items>
                            <ext:Container ID="Container1" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25">
                                <Items>
                                    <ext:TextField runat="server" ID="txtCodSerBsq" FieldLabel="Codigo" />
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container3" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25">
                                <Items>
                                    <ext:TextField runat="server" ID="txtNombreBsq" FieldLabel="Nombre" AnchorHorizontal="99%" />
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container2" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".2">
                                <Items>
                                    <ext:Button runat="server" ID="btnBuscar" Text="Buscar" Icon="ApplicationGo" >
                                        <DirectEvents>
                                            <Click OnEvent="btnBuscar_DirectClick" Before="">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:Container>
                </Items>
            </ext:FieldSet>
            <ext:GridPanel
                ID="grdSer"
                runat="server"                                            
                Margins="0 0 5 5"
                Icon="TransmitGo"
                Frame="true"
                Height="400"
                StoreId="stSer"
                Title="Servicios"
                >
                <ColumnModel ID="ColumnModel3" runat="server">
                    <Columns>                        
                        <ext:Column Header="Codigo" DataIndex="codser" />                            
                        <ext:Column Header="Nombre" DataIndex="nombre" Width="380"/>
                    </Columns>                                
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" SingleSelect="true" >
                        <DirectEvents>
                            <RowSelect OnEvent="rowSelect" Buffer="100">
                                <ExtraParams>                                    
                                    <ext:Parameter Name="ID" Value="this.getSelected().id" Mode="Raw" />
                                </ExtraParams>
                            </RowSelect>
                        </DirectEvents>
                    </ext:RowSelectionModel>
                </SelectionModel>
                <LoadMask ShowMask="true" Msg="Loading...." />
                <BottomBar>
                    <ext:StatusBar runat="server" ID="StatusBar2">
                        <Items>                                                        
                            <ext:Button runat="server" Text="Nuevo Servicio" ID="btnNuevoServicio" Icon="Add" />
                            <ext:Button runat="server" Text="Editar Servicio" ID="btnEditaServicio" Icon="ApplicationEdit" />
                            <ext:Button runat="server" Text="Eliminar Servicio" ID="btnEliminaServicio" Icon="ApplicationDelete" />
                                
                        </Items>
                    </ext:StatusBar>
                </BottomBar>
            </ext:GridPanel>
        </Items>
    </ext:Panel>


    <ext:Window 
        ID="vntSer"
        runat="server"
        Icon="ApplicationFormEdit"
        Width="480"
        Height="250"
        Hidden="true" 
        Modal="true"            
        Title="Servicio"
        Constrain="true">
        <Items>
            <ext:Panel ID="Panel1" 
            runat="server" 
            Title=""
            AnchorHorizontal="100%"
            Height="220"
            Layout="Fit">
            <Items>
                <ext:FormPanel 
                    ID="StatusForm" 
                    runat="server"
                    LabelWidth="100"
                    ButtonAlign="Right"
                    Border="false"
                    PaddingSummary="10px 10px 10px">
                    <Defaults>                        
                        <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                        <ext:Parameter Name="SelectOnFocus" Value="true" Mode="Raw" />
                        <ext:Parameter Name="MsgTarget" Value="side" />
                    </Defaults>
                    <Items>                        
                        <ext:TextField runat="server" ID="txtCodSer" FieldLabel="Codigo" AnchorHorizontal="90%" BlankText="Codigo Requerido" MaxLength="4" MinLength="2" />
                        <ext:TextField runat="server" ID="txtNombre" FieldLabel="Nombre" AnchorHorizontal="90%" BlankText="Nombre Requerido" MaxLength="100" MinLength="5"/>
                        <ext:ComboBox     
	                            ID="cbTipProc"
	                            runat="server" 
	                            Shadow="Drop" 
	                            Mode="Local" 
	                            TriggerAction="All" 
	                            ForceSelection="true"
	                            DisplayField="nombre"
	                            ValueField="codigo"	
	                            EmptyText="Seleccione Tipo de Procedimiento"
	                            AnchorHorizontal="99%"
	                            AllowBlank="false"
                                FieldLabel="Tip.Procedimiento"
	                            >
	                            <Store>
		                            <ext:Store ID="stTipProc" runat="server" AutoLoad="true">
			                            <Reader>
				                            <ext:JsonReader IDProperty="codigo">
					                            <Fields>
						                            <ext:RecordField Name="codigo" />
						                            <ext:RecordField Name="nombre" />
					                            </Fields>
				                            </ext:JsonReader>
			                            </Reader>            
		                            </ext:Store>    
	                            </Store>    
                            </ext:ComboBox>
                    </Items>
                    <Buttons>
                        <ext:Button ID="btnGuardaSer" runat="server" Text="Guardar" Icon="Disk">
                            <DirectEvents>
                                <Click 
                                    OnEvent="btnGuardaSer_DirectClick" 
                                    Before="var valid= #{StatusForm}.getForm().isValid(); if (valid) {#{FormStatusBar}.showBusy('Guardando Formulario...');} return valid;">
                                    <EventMask 
                                        ShowMask="true" 
                                        MinDelay="1000" 
                                        Target="CustomTarget" 
                                        CustomTarget="={#{StatusForm}.getEl()}" 
                                        />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                        <ext:Button runat="server" ID="btnCerrarSer" Text="Cerrar" />
                    </Buttons>
                </ext:FormPanel>
            </Items>
            <BottomBar>
                <ext:StatusBar ID="FormStatusBar" runat="server" DefaultText="Ready">
                    <Plugins>
                        <ext:ValidationStatus ID="ValidationStatus1" 
                            runat="server" 
                            FormPanelID="StatusForm" 
                            ValidIcon="Accept" 
                            ErrorIcon="Exclamation" 
                            ShowText="El Formulario tiene Errores (Click para ver Detalles)"
                            HideText="Click para Ocultar Errores"
                            />
                    </Plugins>
                </ext:StatusBar>
            </BottomBar>
        </ext:Panel>
        </Items>
    </ext:Window>
    
    </form>
</body>
</html>
