﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="personalasistencial.aspx.vb" Inherits="Presentacion.personalasistencial" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
    <ext:Store 
        ID="stPas" 
        runat="server" 
        AutoLoad="true">
        <Reader>
            <ext:JsonReader IDProperty="codigo">
                <Fields>
                    <ext:RecordField Name="codigo" />                    
                    <ext:RecordField Name="identificacion" />
                    <ext:RecordField Name="nombres" />
                </Fields>
            </ext:JsonReader>
        </Reader>
    </ext:Store>   
    <ext:Panel ID="Panel3" runat="server" Frame="true" PaddingSummary="5px 5px 0" AutoWidth="true" Height="650" ButtonAlign="Center" Style="text-align: left" Title="Unidades Funcionales">
        <Items>
            <ext:FieldSet ID="FieldSet3" runat="server" Title="Campos de Busqueda" Padding="10" Height="60">
                <Items>
                    <ext:Container ID="Container10" runat="server" Layout="ColumnLayout" Height="30">
                        <Items>
                            <ext:Container ID="Container1" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25">
                                <Items>
                                    <ext:TextField runat="server" ID="txtCodigoBsq" FieldLabel="Código" />
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container2" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25">
                                <Items>
                                    <ext:TextField runat="server" ID="txtIdentificacionBsq" FieldLabel="Identificación" />
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container3" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3">
                                <Items>
                                    <ext:TextField runat="server" ID="txtNombresBsq" FieldLabel="Nombres" AnchorHorizontal="99%" />
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container4" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".2">
                                <Items>
                                    <ext:Button runat="server" ID="btnBuscar" Text="Buscar" Icon="ApplicationGo" >
                                        <DirectEvents>
                                            <Click OnEvent="btnBuscar_DirectClick" Before="">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:Container>
                </Items>
            </ext:FieldSet>
            <ext:GridPanel
                ID="grdPas"
                runat="server"                                            
                Margins="0 0 5 5"
                Icon="TransmitGo"
                Frame="true"
                Height="400"
                StoreId="stPas"
                Title="Personal Asistencial"
                >
                <ColumnModel ID="ColumnModel3" runat="server">
                    <Columns>                        
                        <ext:Column Header="Código" DataIndex="codigo" />
                        <ext:Column Header="Identificación" DataIndex="identificacion" />
                        <ext:Column Header="Nombre" DataIndex="nombres" Width="380"/>
                    </Columns>                                
                </ColumnModel>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" SingleSelect="true" >
                        <DirectEvents>
                            <RowSelect OnEvent="rowSelect" Buffer="100">
                                <ExtraParams>                                    
                                    <ext:Parameter Name="ID" Value="this.getSelected().id" Mode="Raw" />
                                </ExtraParams>
                            </RowSelect>
                        </DirectEvents>
                    </ext:RowSelectionModel>
                </SelectionModel>
                <LoadMask ShowMask="true" Msg="Loading...." />
                <BottomBar>
                    <ext:StatusBar runat="server" ID="StatusBar2">
                        <Items>                                                        
                            <ext:Button runat="server" Text="Nuevo Personal Asistencial" ID="btnNuevaPAsistencial" Icon="Add" />
                            <ext:Button runat="server" Text="Editar Personal Asistencial" ID="btnEditaPAsistencial" Icon="ApplicationEdit" />
                            <ext:Button runat="server" Text="Eliminar Personal Asistencial" ID="btnEliminaPAsistencial" Icon="ApplicationDelete" />
                        </Items>
                    </ext:StatusBar>
                </BottomBar>
            </ext:GridPanel>
        </Items>
    </ext:Panel>


    <ext:Store 
        ID="stPaser" 
        runat="server" 
        AutoLoad="true">
        <Reader>
            <ext:JsonReader >
                <Fields>
                    <ext:RecordField Name="codpas" />                    
                    <ext:RecordField Name="codser" />
                    <ext:RecordField Name="nombre" />
                </Fields>
            </ext:JsonReader>
        </Reader>
    </ext:Store>

    <ext:Window 
        ID="vntPas"
        runat="server"
        Icon="ApplicationFormEdit"
        Width="750"
        Height="580"
        Hidden="true" 
        Modal="true"            
        Title="Personal Asistencial Medicos, Especialistas, Odontologos y Otros"
        Constrain="true">
        <Items>
            <ext:Panel ID="Panel1" 
            runat="server" 
            Title=""
            AnchorHorizontal="100%"
            Height="550"
            Layout="Fit">
            <Items>
                <ext:FormPanel 
                    ID="StatusForm" 
                    runat="server"
                    LabelWidth="75"
                    ButtonAlign="Right"
                    Border="false"
                    PaddingSummary="10px 10px 10px">
                    <Defaults>                        
                        <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                        <ext:Parameter Name="SelectOnFocus" Value="true" Mode="Raw" />
                        <ext:Parameter Name="MsgTarget" Value="side" />
                    </Defaults>
                    <Items>                        
                        <ext:Container ID="Container5" runat="server" Layout="ColumnLayout" Height="30" >
                            <Items>
                                <ext:Container ID="Container6" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:TextField runat="server" ID="txtCodigo" FieldLabel="Código" AnchorHorizontal="80%" AllowBlank="false" MinLength="3" MaxLength="10" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container7" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:TextField runat="server" ID="txtNum_registro_med" FieldLabel="Registro.Med#" AnchorHorizontal="80%" MaxLength="30" />                                        
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container8" runat="server" Layout="ColumnLayout" Height="30" >
                            <Items>
                                <ext:Container ID="Container9" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:TextField runat="server" ID="txtIdentificacion" FieldLabel="Cedula" AnchorHorizontal="80%" AllowBlank="false" MinLength="4" MaxLength="15" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container11" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:ComboBox
                                            ID="cbIdLugarExp"
                                            FieldLabel="De"
                                            runat="server"
                                            DisplayField="DescTotal"
                                            ValueField="Id_Ciudad"
                                            TypeAhead="false"
                                            LoadingText="Buscando...."
                                            AnchorHorizontal="90%"
                                            PageSize="10"
                                            HideTrigger="true"
                                            ItemSelector="div.search-item"        
                                            MinChars="3"
                                            AllowBlank="false"
                                            BlankText="La zona es requerida"
                                            EmptyText="Seleccione Zona..."
                                            >
                                            <Store>
                                                <ext:Store runat="server" AutoLoad="false" ID="Store1">
                                                    <Proxy>
                                                        <ext:HttpProxy Method="POST" Url=".././Funciones/UbicacionLugar.ashx" />
                                                    </Proxy>
                                                    <Reader>
                                                        <ext:JsonReader Root="BarrioZona" TotalProperty="total">
                                                            <Fields>
                                                                <ext:RecordField Name="Id_Ciudad" />
                                                                <ext:RecordField Name="Descripcion" />    
                                                                <ext:RecordField Name="Desc_Dpto" />                                                                                                                         
                                                                <ext:RecordField Name="DescTotal" />
                                                            </Fields>                                                        
                                                        </ext:JsonReader>
                                                    </Reader>                                                
                                                </ext:Store>
                                            </Store>
                                            <Template runat="server" ID="Template2">
                                                <Html>
                                                    <tpl for=".">
                                                        <div class="search-item">
							                                <h3>{Descripcion}</h3>
                                                            {Desc_Dpto}
						                                    </div>                                                        
                                                    </tpl>                                                    
                                                </Html>                                            
                                            </Template>                                            
                                        </ext:ComboBox>
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container12" runat="server" Layout="ColumnLayout" Height="30" >
                            <Items>
                                <ext:Container ID="Container13" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".9" >
                                    <Items>
                                        <ext:TextField runat="server" ID="txtNombres" FieldLabel="Nombres" AnchorHorizontal="99%" AllowBlank="false" MinLength="10" MaxLength="100" />
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container14" runat="server" Layout="ColumnLayout" Height="30" >
                            <Items>
                                <ext:Container ID="Container15" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".9" >
                                    <Items>
                                        <ext:TextField runat="server" ID="txtDireccion" FieldLabel="Dirección" AnchorHorizontal="99%" AllowBlank="false" MinLength="5" MaxLength="50" />
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container16" runat="server" Layout="ColumnLayout" Height="30" >
                            <Items>
                                <ext:Container ID="Container17" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".9" >
                                    <Items>
                                        <ext:TextField runat="server" ID="txtTelefono" FieldLabel="Telefono" AnchorHorizontal="50%" AllowBlank="false" MinLength="5" MaxLength="20" />
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container18" runat="server" Layout="ColumnLayout" Height="30" >
                            <Items>
                                <ext:Container ID="Container19" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".9" >
                                    <Items>
                                        <ext:ComboBox     
	                                        ID="cbCodClase"                                                                              
	                                        runat="server" 
	                                        Shadow="Drop" 
	                                        Mode="Local" 
	                                        TriggerAction="All" 
	                                        ForceSelection="true"
	                                        DisplayField="nombre"
	                                        ValueField="codigo"	
	                                        EmptyText="Seleccione Clase"
	                                        AnchorHorizontal="70%"
	                                        AllowBlank="false"
                                            FieldLabel="Clase"
	                                        >
	                                        <Store>
		                                        <ext:Store ID="stCodClase" runat="server" AutoLoad="true">
			                                        <Reader>
				                                        <ext:JsonReader IDProperty="codigo">
					                                        <Fields>
						                                        <ext:RecordField Name="codigo" />
						                                        <ext:RecordField Name="nombre" />
					                                        </Fields>
				                                        </ext:JsonReader>
			                                        </Reader>            
		                                        </ext:Store>    
	                                        </Store>    
                                        </ext:ComboBox>
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container20" runat="server" Layout="ColumnLayout" Height="30" >
                            <Items>
                                <ext:Container ID="Container21" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".9" >
                                    <Items>
                                        <ext:ComboBox     
	                                        ID="cbCodEspecialidad"                                                                              
	                                        runat="server" 
	                                        Shadow="Drop" 
	                                        Mode="Local" 
	                                        TriggerAction="All" 
	                                        ForceSelection="true"
	                                        DisplayField="nombre"
	                                        ValueField="codigo"	
	                                        EmptyText="Seleccione Especialidad"
	                                        AnchorHorizontal="70%"
	                                        AllowBlank="false"
                                            FieldLabel="Especialidad"
	                                        >
	                                        <Store>
		                                        <ext:Store ID="stCodEspecialidad" runat="server" AutoLoad="true">
			                                        <Reader>
				                                        <ext:JsonReader IDProperty="codigo">
					                                        <Fields>
						                                        <ext:RecordField Name="codigo" />
						                                        <ext:RecordField Name="nombre" />
					                                        </Fields>
				                                        </ext:JsonReader>
			                                        </Reader>            
		                                        </ext:Store>    
	                                        </Store>    
                                        </ext:ComboBox>
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container22" runat="server" Layout="ColumnLayout" Height="30" >
                            <Items>
                                <ext:Container ID="Container23" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".9" >
                                    <Items>
                                        <ext:TextField runat="server" ID="txtDireccion_off" FieldLabel="Dir.Oficina" AnchorHorizontal="99%" />
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container24" runat="server" Layout="ColumnLayout" Height="30" >
                            <Items>
                                <ext:Container ID="Container25" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:TextField runat="server" ID="txtTelefono_off" FieldLabel="Telefono" AnchorHorizontal="90%" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container26" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:NumberField runat="server" ID="txtNum_citas_dia" FieldLabel="Citas Día" AllowBlank="false" />
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container27" runat="server" Layout="ColumnLayout" Height="30" >
                            <Items>
                                <ext:Container ID="Container28" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:ComboBox ID="cbEstado" runat="server"  FieldLabel="Estado" AllowBlank="false">                                        
                                            <Items>
                                                <ext:ListItem Text="Activo" Value="A" />
                                                <ext:ListItem Text="Retirado" Value="R" />                                                
                                            </Items>
                                        </ext:ComboBox>
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container29" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:NumberField runat="server" ID="txtDuracion" FieldLabel="Duracción" AllowBlank="false" />
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container31" runat="server" Layout="ColumnLayout" Height="30" >
                            <Items>
                                <ext:Container ID="Container30" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3" >
                                    <Items>
                                        <ext:Label Text="Servicios Asociados" runat="server" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container32" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".4" >
                                    <Items>
                                        <ext:ComboBox     
	                                        ID="cbTipServicio"                                                                              
	                                        runat="server" 
	                                        Shadow="Drop" 
	                                        Mode="Local" 
	                                        TriggerAction="All" 
	                                        ForceSelection="true"
	                                        DisplayField="nombre"
	                                        ValueField="codser"	
	                                        EmptyText="Seleccione Servicio"
	                                        AnchorHorizontal="99%"	                                        
	                                        >
	                                        <Store>
		                                        <ext:Store ID="stTipServicio" runat="server" AutoLoad="true">
			                                        <Reader>
				                                        <ext:JsonReader IDProperty="codser">
					                                        <Fields>
						                                        <ext:RecordField Name="codser" />
						                                        <ext:RecordField Name="nombre" />
					                                        </Fields>
				                                        </ext:JsonReader>
			                                        </Reader>            
		                                        </ext:Store>    
	                                        </Store>    
                                        </ext:ComboBox>
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container33" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3" >
                                    <Items>
                                        <ext:Button runat="server" ID="btnSerAsociado" Text="Asociar" Icon="BuildingLink" />
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                        <ext:GridPanel
                            ID="grdPaser"
                            runat="server"                                            
                            Margins="0 0 5 5"
                            Icon="TransmitGo"
                            Frame="true"
                            Height="150"
                            StoreId="stPaser"
                            Title="Servicios Asociados"
                            >
                            <ColumnModel ID="ColumnModel1" runat="server">
                                <Columns>                        
                                    <ext:Column Header="codpas" DataIndex="codpas" />
                                    <ext:Column Header="codser" DataIndex="codser" />
                                    <ext:Column Header="Nombre" DataIndex="nombre" Width="380"/>
                                </Columns>                                
                            </ColumnModel>
                            <SelectionModel>
                                <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" SingleSelect="true" >
                                    <DirectEvents>
                                        <RowSelect OnEvent="rowSelectPSer" Buffer="100">
                                            <ExtraParams>                                    
                                                <ext:Parameter Name="codser" Value="this.getSelected().get('codser')" Mode="Raw" />
                                            </ExtraParams>
                                        </RowSelect>
                                    </DirectEvents>
                                </ext:RowSelectionModel>
                            </SelectionModel>
                            <LoadMask ShowMask="true" Msg="Loading...." />                
                        </ext:GridPanel>
                    </Items>
                    <Buttons>
                        <ext:Button ID="btnGuardaPas" runat="server" Text="Guardar" Icon="Disk">
                            <DirectEvents>
                                <Click 
                                    OnEvent="btnGuardaPas_DirectClick" 
                                    Before="var valid= #{StatusForm}.getForm().isValid(); if (valid) {#{FormStatusBar}.showBusy('Guardando Formulario...');} return valid;">
                                    <EventMask 
                                        ShowMask="true" 
                                        MinDelay="1000" 
                                        Target="CustomTarget" 
                                        CustomTarget="={#{StatusForm}.getEl()}" 
                                        />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                        <ext:Button runat="server" ID="btnCerrarPas" Text="Cerrar" />
                    </Buttons>
                </ext:FormPanel>
            </Items>
            <BottomBar>
                <ext:StatusBar ID="FormStatusBar" runat="server" DefaultText="Ready">
                    <Plugins>
                        <ext:ValidationStatus ID="ValidationStatus1" 
                            runat="server" 
                            FormPanelID="StatusForm" 
                            ValidIcon="Accept" 
                            ErrorIcon="Exclamation" 
                            ShowText="El Formulario tiene Errores (Click para ver Detalles)"
                            HideText="Click para Ocultar Errores"
                            />
                    </Plugins>
                </ext:StatusBar>
            </BottomBar>
        </ext:Panel>
        </Items>
    </ext:Window>
    </form>
</body>
</html>
