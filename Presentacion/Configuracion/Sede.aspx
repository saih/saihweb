﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Sede.aspx.vb" Inherits="Presentacion.Sede" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
        <ext:Store 
            ID="stPrm" 
            runat="server" 
            AutoLoad="true">
            <Reader>
                <ext:JsonReader IDProperty="codpre">
                    <Fields>
                        <ext:RecordField Name="codpre" />
                        <ext:RecordField Name="lugar" />
                    </Fields>
                </ext:JsonReader>
            </Reader>
        </ext:Store>

        <ext:Panel ID="Panel3" runat="server" Frame="true" PaddingSummary="5px 5px 0" AutoWidth="true" Height="650" ButtonAlign="Center" Style="text-align: left" Title="Parametros">
            <Items>
                <ext:FieldSet ID="FieldSet3" runat="server" Title="Campos de Busqueda" Padding="10" Height="60">
                    <Items>
                        <ext:Container ID="Container10" runat="server" Layout="ColumnLayout" Height="30">
                            <Items>
                                <ext:Container ID="Container8" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25">
                                    <Items>
                                        <ext:TextField runat="server" id="txtCodPreBsq" FieldLabel="Codigo" />
                                    </Items>    
                                </ext:Container>
                                <ext:Container ID="Container2" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".35">
                                    <Items>
                                         <ext:TextField runat="server" id="txtLugarBsq" FieldLabel="Lugar" />                                       
                                    </Items>
                                </ext:Container>                                
                                <ext:Container ID="Container5" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".2">
                                    <Items>
                                        <ext:Button runat="server" ID="btnBuscar" Text="Buscar" Icon="ApplicationGo" >
                                            <DirectEvents>
                                                <Click OnEvent="btnBuscar_DirectClick" Before="">
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>                        
                    </Items>
                </ext:FieldSet>
                <ext:GridPanel
                    ID="grdPrm"
                    runat="server"                                            
                    Margins="0 0 5 5"
                    Icon="TransmitGo"
                    Frame="true"
                    Height="400"
                    StoreId="stPrm"
                    Title="Sedes"
                    >
                    <ColumnModel ID="ColumnModel3" runat="server">
                        <Columns>                        
                            <ext:Column Header="Código" DataIndex="codpre" />
                            <ext:Column Header="Lugar" DataIndex="lugar" Width="380"/>
                        </Columns>                                
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" SingleSelect="true" >
                            <DirectEvents>
                                <RowSelect OnEvent="rowSelect" Buffer="100">
                                    <ExtraParams>                                    
                                        <ext:Parameter Name="ID" Value="this.getSelected().id" Mode="Raw" />
                                    </ExtraParams>
                                </RowSelect>
                            </DirectEvents>
                        </ext:RowSelectionModel>
                    </SelectionModel>
                    <LoadMask ShowMask="true" Msg="Loading...." />
                    <BottomBar>
                        <ext:StatusBar runat="server" ID="StatusBar2">
                            <Items>                                                        
                                <ext:Button runat="server" Text="Nuevo Sede" ID="btnNuevaSede" Icon="Add" />
                                <ext:Button runat="server" Text="Editar Sede" ID="btnEditaSede" Icon="ApplicationEdit" />
                                <ext:Button runat="server" Text="Eliminar Sede" ID="btnEliminaSede" Icon="ApplicationDelete" />
                            </Items>
                        </ext:StatusBar>
                    </BottomBar>
                </ext:GridPanel>
            </Items>
        </ext:Panel>

        <ext:Window 
        ID="vntPrm"
        runat="server"
        Icon="ApplicationFormEdit"
        Width="950"
        Height="695"
        Hidden="true" 
        Modal="true"            
        Title="Parametros"
        Constrain="true">
        <Items>
            <ext:Panel ID="Panel1" 
            runat="server" 
            Title=""
            AnchorHorizontal="100%"
            Height="665"
            Layout="Fit">
            <Items>
                <ext:FormPanel 
                    ID="StatusForm" 
                    runat="server"                    
                    ButtonAlign="Right"
                    Border="false"
                    PaddingSummary="3px 3px 3px">
                    <Defaults>                        
                        <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                        <ext:Parameter Name="SelectOnFocus" Value="true" Mode="Raw" />
                        <ext:Parameter Name="MsgTarget" Value="side" />
                    </Defaults>
                    <Items>                        
                         <ext:Container ID="Container1" runat="server" Layout="ColumnLayout" Height="25" >
                            <Items>
                                <ext:Container ID="Container6" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".4" >
                                    <Items>
                                        <ext:Label runat="server" ID="lblParametros" Text="Parametros Iniciales" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container17" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".4" >
                                    <Items>
                                        <ext:ComboBox
                                            ID="cbIdLugar"
                                            FieldLabel="Municipio"
                                            runat="server"
                                            DisplayField="DescTotal"
                                            ValueField="Id_Ciudad"
                                            TypeAhead="false"
                                            LoadingText="Buscando...."
                                            AnchorHorizontal="90%"
                                            PageSize="10"
                                            HideTrigger="true"
                                            ItemSelector="div.search-item"
                                            MinChars="3"
                                            AllowBlank="false"
                                            BlankText="Municipio de Sucursal"
                                            EmptyText="Seleccione Municipio de Sucursal..."
                                            DataIndex="id_lugar"
                                            >
                                            <Store>
                                                <ext:Store runat="server" AutoLoad="false" ID="Store1">
                                                    <Proxy>
                                                        <ext:HttpProxy Method="POST" Url=".././Funciones/UbicacionLugar.ashx" />
                                                    </Proxy>
                                                    <Reader>
                                                        <ext:JsonReader Root="BarrioZona" TotalProperty="total">
                                                            <Fields>
                                                                <ext:RecordField Name="Id_Ciudad" />
                                                                <ext:RecordField Name="Descripcion" />    
                                                                <ext:RecordField Name="Desc_Dpto" />                                                                                                                         
                                                                <ext:RecordField Name="DescTotal" />
                                                            </Fields>                                                        
                                                        </ext:JsonReader>
                                                    </Reader>                                                
                                                </ext:Store>
                                            </Store>
                                            <Template runat="server" ID="Template2">
                                                <Html>
                                                    <tpl for=".">
                                                        <div class="search-item">
							                                <h3>{Descripcion}</h3>
                                                            {Desc_Dpto}
						                                    </div>                                                        
                                                    </tpl>                                                    
                                                </Html>                                            
                                            </Template>                                            
                                        </ext:ComboBox>
                                    </Items>
                                </ext:Container>
                                
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container3" runat="server" Layout="ColumnLayout" Height="25" >
                            <Items>
                                <ext:Container ID="Container4" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3" >
                                    <Items>
                                        <ext:TextField runat="server" ID="txtCodPre" DataIndex="codpre" FieldLabel="Prefijo.Lugar.Base" AnchorHorizontal="80%" AllowBlank="false" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container7" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".4" >
                                    <Items>
                                        <ext:TextField runat="server" ID="txtLugar" DataIndex="lugar" FieldLabel="Lugar.Base" AnchorHorizontal="90%" AllowBlank="false" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container16" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".4" >
                                    <Items>
                                        <ext:Checkbox runat="server" ID="ckIndHuella" DataIndex="indhuella" BoxLabel="HC.firmada sin Huelleros" />
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container9" runat="server" Layout="ColumnLayout" Height="210" >
                            <Items>
                                <ext:Container ID="Container11" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:FieldSet ID="FieldSet1" runat="server" Title="Parametros de Facturación" Padding="3" Height="207">
                                            <Items>
                                                <ext:NumberField runat="server" ID="txtNumFac" DataIndex="numfac" FieldLabel="Cns Int.Facturas" AllowBlank="false" AnchorHorizontal="95%" />
                                                <ext:NumberField runat="server" ID="txtNroPac" DataIndex="nropac" FieldLabel="Cns.Pac.Atendidos" AllowBlank="false" AnchorHorizontal="95%" />
                                                <ext:NumberField runat="server" ID="txtNronci" DataIndex="nronci" FieldLabel="Cns.Ing.Caja" AllowBlank="false"  AnchorHorizontal="95%"/>
                                                <ext:TextField runat="server" ID="txtConPagoSerNo" DataIndex="con_pago_ser_no" FieldLabel="Con.Pago.Ser.No" EmptyText="Concepto Pago de Servicios no Cubiertos" AllowBlank="false" AnchorHorizontal="95%" />
                                                <ext:TextField runat="server" ID="txtCodAbonoFac" DataIndex="cod_abono_fac" FieldLabel="Con.Abono.Fac" EmptyText="Codigo Concepto Abono a Facturas" AllowBlank="false" AnchorHorizontal="95%" />
                                                <ext:TextField runat="server" ID="txtCodPagoAct" DataIndex="cod_pago_act" FieldLabel="Con.Pago.Act" EmptyText="Codigo Concepto de Pago de Actas" AllowBlank="false" AnchorHorizontal="95%" />
                                            </Items>
                                        </ext:FieldSet>
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container12" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:FieldSet ID="FieldSet2" runat="server" Title="Parametros de Cartera" Padding="3" Height="207">
                                            <Items>
                                                <ext:NumberField runat="server" ID="txtCnsNumCuentaCobro" DataIndex="cns_num_cuenta_cobro" FieldLabel="Cns.Num.Cta.Cobro" AllowBlank="false"  AnchorHorizontal="95%"/>
                                                <ext:NumberField runat="server" ID="txtCnsPagCuentaCobro" DataIndex="cns_pag_cuenta_cobro" FieldLabel="Cns.Pag.Cta.Cobro" AllowBlank="false"  AnchorHorizontal="95%"/>
                                                <ext:NumberField runat="server" ID="txtNroGlo" DataIndex="nroglo" FieldLabel="Cns.Glosas.Rec" AllowBlank="false"  AnchorHorizontal="95%"/>
                                                <ext:NumberField runat="server" ID="txtNroGes" DataIndex="nroges" FieldLabel="Cns.Ges.Cobro" AllowBlank="false"  AnchorHorizontal="95%"/>
                                                <ext:NumberField runat="server" ID="txtNroAcm" DataIndex="nroacm" FieldLabel="Cns.A.Compromiso" AllowBlank="false"  AnchorHorizontal="95%"/>
                                                <ext:TextField runat="server" ID="txtCodRecaudoCopagos" DataIndex="cod_recaudo_copagos" FieldLabel="Con.Rec.Coopagos" EmptyText="Codigo Concepto de Recaudo de Copagos" AllowBlank="false" AnchorHorizontal="95%" />
                                                <ext:TextField runat="server" ID="txtCodRecaudoCuotaModeradora" DataIndex="cod_recaudo_cuota_moderadora" FieldLabel="Con.Rec.C.Mod" EmptyText="Codigo Concepto de Recaudo Cuota Moderadora" AllowBlank="false" AnchorHorizontal="95%" />
                                            </Items>
                                        </ext:FieldSet>
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container13" runat="server" Layout="ColumnLayout" Height="215" >
                            <Items>
                                <ext:Container ID="Container14" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:FieldSet ID="FieldSet4" runat="server" Title="Parametros de Historias Clinicas" Padding="3" Height="207">
                                            <Items>
                                                <ext:NumberField runat="server" ID="txtNumHis" DataIndex="numhis" FieldLabel="Cns.Ident.Pacientes" AllowBlank="false" AnchorHorizontal="95%"/>
                                                <ext:NumberField runat="server" ID="txtNroEpi" DataIndex="nroepi" FieldLabel="Cns.Epicrisis" AllowBlank="false"  AnchorHorizontal="95%"/>
                                                <ext:NumberField runat="server" ID="txtNroIqu" DataIndex="nroiqu" FieldLabel="Cns.Inf.Quirurgicos" AllowBlank="false"  AnchorHorizontal="95%"/>
                                                <ext:NumberField runat="server" ID="txtNorRes" DataIndex="norres" FieldLabel="Cns.Res.Examenes" AllowBlank="false"  AnchorHorizontal="95%"/>
                                                <ext:NumberField runat="server" ID="txtNroRem" DataIndex="nrorem" FieldLabel="Cns.Remisiones" AllowBlank="false"  AnchorHorizontal="95%"/>
                                                <ext:NumberField runat="server" ID="txtNroPme" DataIndex="nropme" FieldLabel="Cns.Prog.Medicos" AllowBlank="false"  AnchorHorizontal="95%"/>
                                                <ext:NumberField runat="server" ID="txtNroFur" DataIndex="nrofur" FieldLabel="Cns.FURIPS" AllowBlank="false"  AnchorHorizontal="95%"/>                                                
                                            </Items>
                                        </ext:FieldSet>
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container15" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:FieldSet ID="FieldSet5" runat="server" Title="Parametros de Citas Medicas" Padding="3" Height="207">
                                            <Items>
                                                <ext:NumberField runat="server" ID="txtNroCta" DataIndex="nrocta" FieldLabel="Cns.Citas" AllowBlank="false"  AnchorHorizontal="95%"/>
                                                <ext:NumberField runat="server" ID="txtNroAge" DataIndex="norage" FieldLabel="Cns.Agenda.Citas" AllowBlank="false"  AnchorHorizontal="95%"/>
                                                <ext:NumberField runat="server" ID="txtNroOrd" DataIndex="nroord" FieldLabel="Cns.Ordenes" AllowBlank="false"  AnchorHorizontal="95%"/>
                                                <ext:NumberField runat="server" ID="txtHoraExpedicion" DataIndex="HoraExpedicion" FieldLabel="Horas.Exp.Citas" AllowBlank="false"  AnchorHorizontal="95%"/>
                                                <ext:NumberField runat="server" ID="txtHoraInicioAtencion" DataIndex="HoraInicioAtencion" FieldLabel="Hora.Ini.Aten" AllowBlank="false"  AnchorHorizontal="95%"/>
                                                <ext:NumberField runat="server" ID="txtIntervaloCitas" DataIndex="IntervaloCitas" FieldLabel="Min.Intervalo" AllowBlank="false"  AnchorHorizontal="95%"/>
                                            </Items>
                                        </ext:FieldSet>
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                        <ext:FieldSet ID="FieldSet6" runat="server" Title="Codigos x Defecto en Admisiones" Padding="3" Height="50">
                            <Items>                                
                                <ext:Container ID="Container18" runat="server" Layout="ColumnLayout" Height="25" >
                                    <Items>
                                        <ext:Container ID="Container19" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3" >
                                            <Items>
                                                <ext:TextField runat="server" ID="txtCodurgencias" DataIndex="codurgencias" FieldLabel="Cod.Urgencias" AnchorHorizontal="80%" AllowBlank="false" />
                                            </Items>
                                        </ext:Container>
                                        <ext:Container ID="Container20" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".4" >
                                            <Items>
                                                <ext:TextField runat="server" ID="txtCodambulatorias" DataIndex="codambulatorias" FieldLabel="Cod.Ambulatorias" AnchorHorizontal="90%" AllowBlank="false" />
                                            </Items>
                                        </ext:Container>
                                        <ext:Container ID="Container21" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".4" >
                                            <Items>
                                                <ext:TextField runat="server" ID="txtCodespecialidad" DataIndex="codespecialidad" FieldLabel="Cod.Especialidad" AnchorHorizontal="90%" AllowBlank="false" />
                                            </Items>
                                        </ext:Container>
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:FieldSet>
                        <ext:Container ID="Container22" runat="server" Layout="ColumnLayout" Height="25" >
                            <Items>
                                <ext:Container ID="Container23" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3" >
                                    <Items>
                                        <ext:NumberField runat="server" ID="txtNrofor" DataIndex="nrofor" FieldLabel="Cns.Form.Medica" AllowBlank="false"  AnchorHorizontal="95%"/>
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container24" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3" >
                                    <Items>
                                        <ext:NumberField runat="server" ID="txtNropro" DataIndex="nropro" FieldLabel="Cns.Procedimiento" AllowBlank="false"  AnchorHorizontal="60%"/>
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container25" runat="server" Layout="ColumnLayout" Height="60" >
                        <Items>
                            <ext:Container ID="Container26" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth="1" >
                                <Items>
                                    <ext:TextArea runat="server" FieldLabel="Observaciones" ID="txtObservaciones" DataIndex="observaciones" AnchorHorizontal="100%" Height="50" />
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:Container>
                    </Items>
                    <Buttons>
                        <ext:Button ID="btnGuardaPrm" runat="server" Text="Guardar" Icon="Disk">
                            <DirectEvents>
                                <Click 
                                    OnEvent="btnGuardaPrm_DirectClick" 
                                    Before="var valid= #{StatusForm}.getForm().isValid(); if (valid) {#{FormStatusBar}.showBusy('Guardando Formulario...');} return valid;">
                                    <EventMask 
                                        ShowMask="true" 
                                        MinDelay="1000" 
                                        Target="CustomTarget" 
                                        CustomTarget="={#{StatusForm}.getEl()}" 
                                        />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                        <ext:Button runat="server" ID="btnCerrarPrm" Text="Cerrar" />
                    </Buttons>
                </ext:FormPanel>
            </Items>
            <BottomBar>
                <ext:StatusBar ID="FormStatusBar" runat="server" DefaultText="Ready">
                    <Plugins>
                        <ext:ValidationStatus ID="ValidationStatus1" 
                            runat="server" 
                            FormPanelID="StatusForm" 
                            ValidIcon="Accept" 
                            ErrorIcon="Exclamation" 
                            ShowText="El Formulario tiene Errores (Click para ver Detalles)"
                            HideText="Click para Ocultar Errores"
                            />
                    </Plugins>
                </ext:StatusBar>
            </BottomBar>
        </ext:Panel>
        </Items>
    </ext:Window>


    </form>
</body>
</html>
