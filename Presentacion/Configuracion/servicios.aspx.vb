﻿Imports Ext.Net

Public Class servicios
    Inherits System.Web.UI.Page

    Protected Property Accion As String
        Get
            Return ViewState("Accion")
        End Get
        Set(value As String)
            ViewState.Add("Accion", value)
        End Set
    End Property

    Protected Property codser As String
        Get
            Return ViewState("codser")
        End Get
        Set(value As String)
            ViewState.Add("codser", value)
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim seg As New Seguridad(Me, Session("PERFIL"))
            cargarSer()
        End If
    End Sub

    Protected Sub rowSelect(ByVal sender As Object, ByVal e As DirectEventArgs)
        codser = e.ExtraParams("ID")
    End Sub

    Private Function cargarSer()
        Dim negSer As New Negocio.Negociosaih_ser
        Dim sWhere As String = " where codser <> ''"
        If txtCodSerBsq.Text <> "" Then
            sWhere &= " and codser = '" & txtCodSerBsq.Text & "'"
        End If
        If txtNombreBsq.Text <> "" Then
            sWhere &= " and nombre like '%" & txtNombreBsq.Text & "%'"
        End If
        stSer.DataSource = negSer.Obtenersaih_serbyWhere(sWhere)
        stSer.DataBind()
        Return True
    End Function

    Private Sub btnNuevoServicio_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnNuevoServicio.DirectClick
        Accion = "Insert"
        Funciones.cargaStoreDominio(stTipProc, Datos.ConstantesUtil.DOMINIO_TIPO_PROCEDIMIENTO)
        txtCodSer.Clear()
        txtNombre.Clear()
        cbTipProc.Clear()
        vntSer.Show()
    End Sub

    Private Sub btnEditaServicio_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEditaServicio.DirectClick
        Accion = "Edit"
        Dim negSer As New Negocio.Negociosaih_ser
        Dim ser As New Datos.saih_ser
        ser = negSer.Obtenersaih_serById(codser)
        Funciones.cargaStoreDominio(stTipProc, Datos.ConstantesUtil.DOMINIO_TIPO_PROCEDIMIENTO)
        txtCodSer.Text = ser.codser
        txtNombre.Text = ser.nombre
        cbTipProc.Value = ser.tippro
        vntSer.Show()
    End Sub

    <DirectMethod()>
    Public Function eliminarSer()
        Dim negSer As New Negocio.Negociosaih_ser
        negSer.Eliminasaih_ser(codser)
        cargarSer()
        Return True
    End Function

    Private Sub btnEliminaServicio_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEliminaServicio.DirectClick
        Dim js As String = "Ext.MessageBox.confirm('Confirmación', '¿Esta seguro de eliminar?', " & _
           "function (btn) { " & _
                " if (btn == 'yes') " & _
                "   Ext.net.DirectMethods.eliminarSer(); " & _
                " });"
        Ext.Net.X.Js.AddScript(js)
    End Sub

    Protected Sub btnBuscar_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        cargarSer()
    End Sub

    Protected Sub btnGuardaSer_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        Dim negSer As New Negocio.Negociosaih_ser
        If Accion = "Insert" Then
            If negSer.Altasaih_ser(txtCodSer.Text, txtNombre.Text, cbTipProc.Value) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                vntSer.Hide()
                cargarSer()
            End If
        Else
            If negSer.Editasaih_ser(codser, txtNombre.Text, cbTipProc.Value) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                vntSer.Hide()
                cargarSer()
            End If
        End If
    End Sub

    Private Sub btnCerrarSer_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnCerrarSer.DirectClick
        vntSer.Hide()
    End Sub
End Class