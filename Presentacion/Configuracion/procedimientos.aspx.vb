﻿Imports Ext.Net

Public Class procedimientos
    Inherits System.Web.UI.Page

#Region "Variables ViewState"
    Protected Property manual As String
        Get
            Return ViewState("manual")
        End Get
        Set(value As String)
            ViewState.Add("manual", value)
        End Set
    End Property

    Protected Property idTar As String
        Get
            Return ViewState("idTar")
        End Get
        Set(value As String)
            ViewState.Add("idTar", value)
        End Set
    End Property

    Protected Property codigo As String
        Get
            Return ViewState("codigo")
        End Get
        Set(value As String)
            ViewState.Add("codigo", value)
        End Set
    End Property

    Protected Property Accion As String
        Get
            Return ViewState("Accion")
        End Get
        Set(value As String)
            ViewState.Add("Accion", value)
        End Set
    End Property

    Protected Property AccionTar As String
        Get
            Return ViewState("AccionTar")
        End Get
        Set(value As String)
            ViewState.Add("AccionTar", value)
        End Set
    End Property
#End Region
    

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim seg As New Seguridad(Me, Session("PERFIL"))
        End If
    End Sub

#Region "Procedimientos"
    Private Function cargarProcedimientos()
        Dim sWhere As String = " where codigo <> ''"
        Dim negProcedimiento As New Negocio.Negociosaih_pcd

        If txtCodigoBsq.Text = "" And txtNombreBsq.Text = "" Then
            Ext.Net.X.Msg.Alert("Información", "Se debe ingresar algun campo de busqueda").Show()
            Return False
        End If

        If txtCodigoBsq.Text <> "" Then
            sWhere &= " and codigo = '" & txtCodigoBsq.Text & "'"
        End If

        If txtNombreBsq.Text <> "" Then
            sWhere &= " and nombre like '%" & txtNombreBsq.Text & "%'"
        End If

        stProcedimientos.DataSource = negProcedimiento.Obtenersaih_pcdbyWhere(sWhere)
        stProcedimientos.DataBind()
        Return True
    End Function

    Protected Sub btnBuscar_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        cargarProcedimientos()
    End Sub

    Protected Sub rowSelect(ByVal sender As Object, ByVal e As DirectEventArgs)
        Dim negProcedimiento As New Negocio.Negociosaih_pcd
        Dim procedimiento As New Datos.saih_pcd
        manual = e.ExtraParams("manual")
        codigo = e.ExtraParams("codigo")
        procedimiento = negProcedimiento.Obtenersaih_pcdById(manual, codigo)
    End Sub

    Private Sub btnNuevoProcedimiento_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnNuevoProcedimiento.DirectClick
        Accion = "Insert"
        cargaInformacionProc()
        cbManTar.Clear()
        txtCodigo.Clear()
        txtNombre.Clear()
        cbNivelComplejidad.Clear()
        cbCCosRel.Clear()
        cbAplicaSexos.Clear()
        txtapledadinicio.Clear()
        txtapledadfin.Clear()
        txtcodgrupoq.Clear()
        txtvalor.Clear()
        txtvalsmv.Clear()
        txtporincremento.Clear()
        ckcobhonmed.Clear()
        ckcobdersal.Clear()
        ckcobmatins.Clear()
        ckcobhonayu.Clear()
        ckcobhonanes.Clear()
        ckconhonper.Clear()
        cbFinalidad.Clear()
        cbUnidadFuncional.Clear()
        txtObservaciones.Clear()
        cbTipServicio.Clear()
        lkTarifas.Disabled = True
        vntProcedimiento.Show()
    End Sub

    Private Sub btnEditaProcedimiento_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEditaProcedimiento.DirectClick
        Accion = "Edit"
        cargaInformacionProc()
        Dim negProcedimiento As New Negocio.Negociosaih_pcd
        Dim procedimiento As New Datos.saih_pcd
        procedimiento = negProcedimiento.Obtenersaih_pcdById(manual, codigo)
        cbManTar.Value = procedimiento.manual
        txtCodigo.Text = procedimiento.codigo
        txtNombre.Text = procedimiento.nombre
        cbNivelComplejidad.Value = procedimiento.ncomplejidad
        cbCCosRel.Value = procedimiento.cencosto
        cbAplicaSexos.Value = procedimiento.aplsex
        txtapledadinicio.Value = procedimiento.apledadinicio
        txtapledadfin.Value = procedimiento.apledadfin
        txtcodgrupoq.Text = procedimiento.codgrupoq
        txtvalor.Value = procedimiento.valor
        txtvalsmv.Value = procedimiento.valsmv
        txtporincremento.Value = procedimiento.porincremento
        ckcobhonmed.Value = Funciones.IntToBool(procedimiento.cobhonmed)
        ckcobdersal.Value = Funciones.IntToBool(procedimiento.cobdersal)
        ckcobmatins.Value = Funciones.IntToBool(procedimiento.cobmatins)
        ckcobhonayu.Value = Funciones.IntToBool(procedimiento.cobhonayu)
        ckcobhonanes.Value = Funciones.IntToBool(procedimiento.cobhonanes)
        ckconhonper.Value = Funciones.IntToBool(procedimiento.conhonper)
        cbUnidadFuncional.Value = procedimiento.codunif
        txtObservaciones.Text = procedimiento.observaciones
        cbTipServicio.Value = procedimiento.codser
        cbFinalidad.Value = procedimiento.codfp
        lkTarifas.Disabled = False
        vntProcedimiento.Show()
    End Sub

    Private Function cargaInformacionProc()
        Dim negUnf As New Negocio.Negociosaih_unf
        Dim negSer As New Negocio.Negociosaih_ser
        stTipServicio.DataSource = negSer.Obtenersaih_ser
        stTipServicio.DataBind()
        stUnidadFuncional.DataSource = negUnf.Obtenersaih_unf
        stUnidadFuncional.DataBind()
        Funciones.cargaStoreDominio(stManTar, Datos.ConstantesUtil.DOMINIO_MANUAL_TARIFARIO)
        Funciones.cargaStoreDominio(stNivelComplejidad, Datos.ConstantesUtil.DOMINIO_NIVEL_COMPLEJIDAD)
        Funciones.cargaStoreDominioIn(stFinalidad, Datos.ConstantesUtil.DOMINIO_FINALIDAD_PROCEDIMIENTO & "','" & Datos.ConstantesUtil.DOMINIO_FINALIDAD_CONSULTA)
        Funciones.cargaStoreDominio(stCCostoRel, Datos.ConstantesUtil.DOMINIO_CENTRO_COSTO)
        Funciones.cargaStoreDominio(stAplicaSexos, Datos.ConstantesUtil.DOMINIO_APLICA_SEXO)
        Return True
    End Function

    Private Sub btnCerrarProc_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnCerrarProc.DirectClick
        vntProcedimiento.Hide()
    End Sub

    Protected Sub btnGuardaProc_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        Dim negProcedimientos As New Negocio.Negociosaih_pcd
        If Accion = "Insert" Then
            If negProcedimientos.Altasaih_pcd(cbManTar.Value, txtCodigo.Text, txtNombre.Text, cbNivelComplejidad.Value, cbCCosRel.Value, cbAplicaSexos.Value,
                                           txtapledadinicio.Value, txtapledadfin.Value, txtcodgrupoq.Text, txtvalor.Value,
                                           txtvalsmv.Value, txtporincremento.Value, Funciones.BoolToInt(ckcobhonmed.Value),
                                           Funciones.BoolToInt(ckcobdersal.Value), Funciones.BoolToInt(ckcobmatins.Value),
                                           Funciones.BoolToInt(ckcobhonayu.Value), Funciones.BoolToInt(ckcobhonanes.Value),
                                           Funciones.BoolToInt(ckconhonper.Value), cbUnidadFuncional.Value, cbFinalidad.Value,
                                           txtObservaciones.Text, cbTipServicio.Value) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                cargarProcedimientos()
                vntProcedimiento.Hide()
            End If
        Else
            If negProcedimientos.Editasaih_pcd(cbManTar.Value, txtCodigo.Text, txtNombre.Text, cbNivelComplejidad.Value, cbCCosRel.Value, cbAplicaSexos.Value,
                                       txtapledadinicio.Value, txtapledadfin.Value, txtcodgrupoq.Text, txtvalor.Value,
                                       txtvalsmv.Value, txtporincremento.Value, Funciones.BoolToInt(ckcobhonmed.Value),
                                       Funciones.BoolToInt(ckcobdersal.Value), Funciones.BoolToInt(ckcobmatins.Value),
                                       Funciones.BoolToInt(ckcobhonayu.Value), Funciones.BoolToInt(ckcobhonanes.Value),
                                       Funciones.BoolToInt(ckconhonper.Value), cbUnidadFuncional.Value, cbFinalidad.Value,
                                       txtObservaciones.Text, cbTipServicio.Value) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                cargarProcedimientos()
                vntProcedimiento.Hide()
            End If
        End If
    End Sub

    <DirectMethod()>
    Public Function eliminarProc()
        Dim negProcedimientos As New Negocio.Negociosaih_pcd
        negProcedimientos.Eliminasaih_pcd(manual, codigo)
        cargarProcedimientos()
        Return True
    End Function

    Private Sub btnEliminaProcedimiento_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEliminaProcedimiento.DirectClick
        Dim js As String = "Ext.MessageBox.confirm('Confirmación', '¿Esta seguro de eliminar?', " & _
           "function (btn) { " & _
                " if (btn == 'yes') " & _
                "   Ext.net.DirectMethods.eliminarProc(); " & _
                " });"
        Ext.Net.X.Js.AddScript(js)
    End Sub
#End Region
    
#Region "Tarifas"

    Private Sub lkTarifas_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles lkTarifas.DirectClick
        Funciones.cargaStoreDominio(stManual, Datos.ConstantesUtil.DOMINIO_MANUAL_TARIFARIO)
        Funciones.cargaStoreDominio(stPlan, Datos.ConstantesUtil.DOMINIO_PLANES)
        cargarTar()
        vntTarifas.Show()
    End Sub

    Function cargarTar()
        Dim negTar As New Negocio.Negociosaih_tpr
        stTar.DataSource = negTar.Obtenersaih_tprbyWhere(" where codpro = '" & codigo & "'")
        stTar.DataBind()
        Return True
    End Function

    <DirectMethod()>
    Public Function insertaTar(id_tar As String, manual_tar As String, id_plan As String,
                               valor As Decimal, valsmv As Decimal, porincremento As Decimal)
        Dim negTar As New Negocio.Negociosaih_tpr
        If id_tar = 0 Then
            negTar.Altasaih_tpr(codigo, manual_tar, id_tar, id_plan, "", valor, valsmv, porincremento)
        Else
            negTar.Editasaih_tpr(codigo, manual_tar, id_tar, id_plan, "", valor, valsmv, porincremento)
        End If
        cargarTar()
        Return True
    End Function

    Protected Sub rowSelectTar(ByVal sender As Object, ByVal e As DirectEventArgs)
        idTar = e.ExtraParams("id")
    End Sub

    <DirectMethod()>
    Public Function cancelarTar()
        cargarTar()
        Return True
    End Function

    <DirectMethod()>
    Public Function eliminarTar()
        Dim negTar As New Negocio.Negociosaih_tpr
        negTar.Eliminasaih_tpr(idTar)
        cargarTar()
        Return True
    End Function

    <DirectMethod()>
    Public Sub btnEliminaTar_DirectClick(id_tar As String)
        idTar = id_tar
        Dim js As String = "Ext.MessageBox.confirm('Confirmación', '¿Esta seguro de eliminar?', " & _
           "function (btn) { " & _
                " if (btn == 'yes') " & _
                "   Ext.net.DirectMethods.eliminarTar(); " & _
                " });"
        Ext.Net.X.Js.AddScript(js)
    End Sub

#End Region
End Class