﻿Imports Ext.Net

Public Class tabDominios
    Inherits System.Web.UI.Page

    Protected Property csc As String
        Get
            Return Me.ViewState("csc")
        End Get
        Set(ByVal value As String)
            Me.ViewState.Add("csc", value)
        End Set
    End Property

    Protected Property codigo As String
        Get
            Return Me.ViewState("codigo")
        End Get
        Set(ByVal value As String)
            Me.ViewState.Add("codigo", value)
        End Set
    End Property

    Protected Property dominio As String
        Get
            Return Me.ViewState("dominio")
        End Get
        Set(ByVal value As String)
            Me.ViewState.Add("dominio", value)
        End Set
    End Property

    Protected Property Accion As String
        Get
            Return ViewState("Accion")
        End Get
        Set(value As String)
            ViewState.Add("Accion", value)
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim seg As New Seguridad(Me, Session("PERFIL"))
            cargaDominios()
            cargaTabs()
        End If
    End Sub

    Protected Sub rowSelect(ByVal sender As Object, ByVal e As DirectEventArgs)
        csc = e.ExtraParams("ID")
        Dim negTab As New Negocio.Negociosaih_tab
        Dim tab As New Datos.saih_tab
        tab = negTab.Obtenersaih_tabByCsc(csc)
        dominio = tab.dominio
        codigo = tab.codigo
    End Sub

    Private Function cargaTabs()
        Dim sWhere As String = " where csc <> 0 "
        Dim negTab As New Negocio.Negociosaih_tab

        If cbDominio.Value <> "" And cbDominio.Value <> Nothing Then
            sWhere &= " and dominio = '" & cbDominio.Value & "'"
        End If

        If txtCodigoBsq.Text <> "" Then
            sWhere &= " and codigo like '%" & txtCodigoBsq.Text & "%'"
        End If

        If txtNombreBsq.Text <> "" Then
            sWhere &= " and nombre like '%" & txtNombreBsq.Text & "%'"
        End If

        stTab.DataSource = negTab.Obtenersaih_tabbyWhere(sWhere)
        stTab.DataBind()
        Return True
    End Function

    Private Function cargaDominios()
        Dim spFunc As New Datos.spFunciones
        stDominio.DataSource = spFunc.executaSelect("select distinct(dominio) dominio from saih_tab")
        stDominio.DataBind()
        Return True
    End Function

    Protected Sub btnBuscar_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        cargaTabs()
    End Sub

    Private Sub btnNuevoDominio_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnNuevoDominio.DirectClick
        Accion = "Insert"
        txtDominio.Clear()
        txtCodigo.Clear()
        txtNombre.Clear()
        txtValor.Clear()
        vntTab.Show()
    End Sub

    Private Sub btnEditaDominio_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEditaDominio.DirectClick
        Accion = "Edit"
        Dim negTab As New Negocio.Negociosaih_tab
        Dim tab As Datos.saih_tab
        tab = negTab.Obtenersaih_tabById(dominio, codigo)
        txtDominio.Text = tab.dominio
        txtCodigo.Text = tab.codigo
        txtNombre.Text = tab.nombre
        txtValor.Text = tab.valor
        vntTab.Show()
    End Sub

    Protected Sub FormSave(sender As Object, e As Ext.Net.DirectEventArgs)

    End Sub

    Protected Sub btnGuardaTab_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        Dim negTab As New Negocio.Negociosaih_tab
        If Accion = "Insert" Then
            If negTab.Altasaih_tab(txtDominio.Text, txtCodigo.Text, txtNombre.Text, txtValor.Text, 0) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                cargaDominios()
                cargaTabs()
                vntTab.Hide()
            End If
        End If
        If Accion = "Edit" Then
            If negTab.Editasaih_tab(dominio, codigo, txtNombre.Text, txtValor.Text, csc) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                cargaDominios()
                cargaTabs()
                vntTab.Hide()
            End If
        End If
    End Sub

    Private Sub btnCerrarTab_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnCerrarTab.DirectClick
        vntTab.Hide()
    End Sub
End Class