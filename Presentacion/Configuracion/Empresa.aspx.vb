﻿Imports Ext.Net

Public Class Empresa
    Inherits System.Web.UI.Page

#Region "ViewState"
    Protected Property Accion As String
        Get
            Return ViewState("Accion")
        End Get
        Set(value As String)
            ViewState.Add("Accion", value)
        End Set
    End Property

    Protected Property codigo As String
        Get
            Return ViewState("codigo")
        End Get
        Set(value As String)
            ViewState.Add("codigo", value)
        End Set
    End Property

    Protected Property num_contrato As String
        Get
            Return ViewState("num_contrato")
        End Get
        Set(value As String)
            ViewState.Add("num_contrato", value)
        End Set
    End Property


    Protected Property AccionEmc As String
        Get
            Return ViewState("AccionEmc")
        End Get
        Set(value As String)
            ViewState.Add("AccionEmc", value)
        End Set
    End Property

#End Region
    

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim seg As New Seguridad(Me, Session("PERFIL"))
        End If
    End Sub

#Region "Empresas"
    Protected Sub rowSelect(ByVal sender As Object, ByVal e As DirectEventArgs)
        codigo = e.ExtraParams("ID")
    End Sub

    Private Function cargaEmpresa()
        Dim sWhere As String = " where codigo <> ''"
        Dim negEmp As New Negocio.Negociosaih_emp

        If txtCodigoBsq.Text <> "" Then
            sWhere &= " and codigo = '" & txtCodigo.Text & "'"
        End If

        If txtIdentificacionBsq.Text <> "" Then
            sWhere &= " and nit = '" & txtIdentificacionBsq.Text & "'"
        End If

        If txtRazonSocialBsq.Text <> "" Then
            sWhere &= " and razonsocial like '%" & txtRazonSocialBsq.Text & "%'"
        End If

        stEmp.DataSource = negEmp.Obtenersaih_empbyWhere(sWhere)
        stEmp.DataBind()

        Return True
    End Function

    Protected Sub btnBuscar_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        cargaEmpresa()
    End Sub

    Protected Sub btnGuardaEmp_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        Dim negEmp As New Negocio.Negociosaih_emp
        Dim emp As New Datos.saih_emp
        Reflexion.valoresForma(emp, vntEmp)
        If Accion = "Insert" Then
            If negEmp.Altasaih_emp(emp) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                vntEmp.Hide()
                cargaEmpresa()
            End If
        Else
            If negEmp.Editasaih_emp(emp) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                vntEmp.Hide()
                cargaEmpresa()
            End If
        End If
    End Sub

    Private Sub btnCerrarEmp_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnCerrarEmp.DirectClick
        vntEmp.Hide()
    End Sub

    <DirectMethod()>
    Public Function eliminarEmp()
        Dim negEmp As New Negocio.Negociosaih_emp
        negEmp.Eliminasaih_emp(codigo)
        cargaEmpresa()
        Return True
    End Function

    Private Sub btnbtnEliminaEmpresa_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEliminaEmpresa.DirectClick
        Dim js As String = "Ext.MessageBox.confirm('Confirmación', '¿Esta seguro de eliminar?', " & _
           "function (btn) { " & _
                " if (btn == 'yes') " & _
                "   Ext.net.DirectMethods.eliminarEmp(); " & _
                " });"
        Ext.Net.X.Js.AddScript(js)
    End Sub

    Private Sub btnNuevaEmpresa_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnNuevaEmpresa.DirectClick
        Accion = "Insert"
        codigo = ""
        cargarContratos()
        cargarCombos()
        cargarContSer()
        cargarSubsidio()
        presentacionUtil.limpiarObjetos(vntEmp)
        vntEmp.Show()
    End Sub

    Private Sub btnEditaEmpresa_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEditaEmpresa.DirectClick
        Accion = "Edit"
        Dim negEmp As New Negocio.Negociosaih_emp
        Dim emp As New Datos.saih_emp
        emp = negEmp.Obtenersaih_empById(codigo)
        cargarCombos()
        cargarContratos()
        cargarContSer()
        cargarSubsidio()
        Reflexion.formaValores(emp, vntEmp)
        cbEntidadDeSalud.Value = emp.codigo
        cargaCombosServicios()
        cargarCombosSubsidios()
        vntEmp.Show()
    End Sub

    Private Function cargarCombos()
        Dim negEmp As New Negocio.Negociosaih_emp

        stEmpFctProc.DataSource = negEmp.Obtenersaih_empbyWhere(" where codigo <> '" & codigo & "'")
        stEmpFctProc.DataBind()

        stEmpSolCuentas.DataSource = negEmp.Obtenersaih_empbyWhere(" where codigo <> '" & codigo & "'")
        stEmpSolCuentas.DataBind()

        Funciones.cargaStoreDominio(stEntidadDeSalud, Datos.ConstantesUtil.DOMINIO_ENTIDADES_SALUD)
        Return True
    End Function

    Private Sub cbEntidadDeSalud_DirectSelect(sender As Object, e As Ext.Net.DirectEventArgs) Handles cbEntidadDeSalud.DirectSelect
        Dim negTab As New Negocio.Negociosaih_tab
        Dim tab As New Datos.saih_tab
        tab = negTab.Obtenersaih_tabById(Datos.ConstantesUtil.DOMINIO_ENTIDADES_SALUD, cbEntidadDeSalud.Value)
        txtCodigo.Text = tab.codigo
        txtRazonSocial.Text = tab.nombre
    End Sub
#End Region

#Region "Contratos"

    Protected Sub rowSelectEmc(ByVal sender As Object, ByVal e As DirectEventArgs)
        num_contrato = e.ExtraParams("num_contrato")
    End Sub

    Private Function cargarContratos()
        Dim spFunc As New Datos.spFunciones
        Dim sql As String
        sql = "select codemp,num_contrato,nombre,case when estado = 1 then 'Activo' " & _
              "else 'Inactivo' end estado,dbo.FN_FECHA_SOLA(fecha) fecha, " & _
              "dbo.FN_FECHA_SOLA(fecha_inicial) fecha_inicial, dbo.FN_FECHA_SOLA(fecha_final) " & _
              "fecha_final,monto from saih_emc where codemp = '" & codigo & "'"
        stEmc.DataSource = spFunc.executaSelect(sql)
        stEmc.DataBind()
        Return True
    End Function

    Private Sub btnNuevoContrato_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnNuevoContrato.DirectClick
        AccionEmc = "Insert"
        presentacionUtil.limpiarObjetos(vntEmc)
        vntEmc.Show()
    End Sub

    Private Sub btnEditaContrato_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEditaContrato.DirectClick
        AccionEmc = "Edit"
        Dim negEmc As New Negocio.Negociosaih_emc
        Dim emc As New Datos.saih_emc
        emc = negEmc.Obtenersaih_emcById(codigo, num_contrato)
        Reflexion.formaValores(emc, vntEmc)
        vntEmc.Show()
    End Sub

    <DirectMethod()>
    Public Function eliminarEmc()
        Dim negEmc As New Negocio.Negociosaih_emc
        negEmc.Eliminasaih_emc(codigo, num_contrato)
        cargarContratos()
        Return True
    End Function

    Private Sub btnEliminaContrato_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEliminaContrato.DirectClick
        Dim js As String = "Ext.MessageBox.confirm('Confirmación', '¿Esta seguro de eliminar?', " & _
           "function (btn) { " & _
                " if (btn == 'yes') " & _
                "   Ext.net.DirectMethods.eliminarEmc(); " & _
                " });"
        Ext.Net.X.Js.AddScript(js)
    End Sub

    Protected Sub btnGuardaEmc_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        Dim negEmc As New Negocio.Negociosaih_emc
        Dim emc As New Datos.saih_emc
        Reflexion.valoresForma(emc, vntEmc)
        emc.codemp = codigo
        If Datos.deTablas.difFechas("dd", emc.fecha_inicial, emc.fecha_final) < 0 Then
            Ext.Net.X.Msg.Alert("Información", "La fecha Final es Menor a la inicial").Show()
            Return
        End If
        If AccionEmc = "Insert" Then
            If negEmc.Altasaih_emc(emc) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                vntEmc.Hide()
                cargarContratos()
            End If
        Else
            If negEmc.Editasaih_emc(emc) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                vntEmc.Hide()
                cargarContratos()
            End If
        End If
    End Sub

    Private Sub btnCerrarEmc_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnCerrarEmc.DirectClick
        vntEmc.Hide()
    End Sub

#End Region

#Region "Convenios y Servicios"
    Private Function cargaCombosServicios()
        Funciones.cargaStoreDominio(stManual, Datos.ConstantesUtil.DOMINIO_MANUAL_TARIFARIO)
        Funciones.cargaStoreDominio(stNivelComplejidad, Datos.ConstantesUtil.DOMINIO_NIVEL_COMPLEJIDAD)
        Dim negSer As New Negocio.Negociosaih_ser
        stTipServicio.DataSource = negSer.Obtenersaih_ser
        stTipServicio.DataBind()
        Return True
    End Function

    <DirectMethod()>
    Public Function cancelarContSer()
        cargarContSer()
        Return True
    End Function

    <DirectMethod()>
    Public Function eliminarContSer(codser As String, manual As String)
        Dim js As String = "Ext.MessageBox.confirm('Confirmación', '¿Esta seguro de eliminar?', " & _
           "function (btn) { " & _
                " if (btn == 'yes') " & _
                "   Ext.net.DirectMethods.eliminaContSer('" & codser & "','" & manual & "'); " & _
                " });"
        Ext.Net.X.Js.AddScript(js)
        
        Return True
    End Function

    <DirectMethod()>
    Public Function eliminaContSer(codser As String, manual As String)
        Dim negContSer As New Negocio.Negocioemp_contser
        negContSer.Eliminaemp_contser(codigo, codser, manual)
        cargarContSer()
        Return True
    End Function

    <DirectMethod()>
    Public Function editarContSer(codser As String, manual As String, porcentaje As String, num_contrato As String,
                                                  niv_complejidad As String, es_nueva As String)

        Dim contSer As New Datos.emp_contser
        Dim negContSer As New Negocio.Negocioemp_contser
        contSer.codemp = codigo
        contSer.codser = codser
        contSer.manual = manual
        contSer.porcentaje = porcentaje
        contSer.num_contrato = num_contrato
        contSer.niv_complejidad = niv_complejidad

        If es_nueva = "1" Then
            If negContSer.Altaemp_contser(contSer) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                cargarContSer()
            End If
        Else
            If negContSer.Editaemp_contser(contSer) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                cargarContSer()
            End If
        End If

        Return True
    End Function

    Private Function cargarContSer()
        Dim negContSer As New Negocio.Negocioemp_contser
        stContSer.DataSource = negContSer.Obteneremp_contserbyWhere(" where codemp = '" & codigo & "'")
        stContSer.DataBind()
        Return True
    End Function

#End Region


#Region "Subsidios"

    Private Function cargarCombosSubsidios()
        Funciones.cargaStoreDominio(stEstrato, Datos.ConstantesUtil.DOMINIO_ESTRATO)
        Return True
    End Function

    <DirectMethod()>
    Public Function cancelarSubsidio()
        cargarSubsidio()
        Return True
    End Function

    <DirectMethod()>
    Public Function eliminarSubsidio(estrato As String)
        Dim js As String = "Ext.MessageBox.confirm('Confirmación', '¿Esta seguro de eliminar?', " & _
           "function (btn) { " & _
                " if (btn == 'yes') " & _
                "   Ext.net.DirectMethods.eliminaSubsidio('" & estrato & "'); " & _
                " });"
        Ext.Net.X.Js.AddScript(js)

        Return True
    End Function

    <DirectMethod()>
    Public Function eliminaSubsidio(estrato As String)
        Dim negSubsidio As New Negocio.Negocioemp_subsidio
        negSubsidio.Eliminaemp_subsidio(codigo, estrato)
        cargarSubsidio()
        Return True
    End Function

    <DirectMethod()>
    Public Function editarSubsidio(estrato As String, subsidio As String, es_nueva As String)

        Dim subs As New Datos.emp_subsidio
        Dim negSubsidio As New Negocio.Negocioemp_subsidio
        subs.codemp = codigo
        subs.estrato = estrato
        subs.subsidio = subsidio

        If es_nueva = "1" Then
            If negSubsidio.Altaemp_subsidio(subs) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                cargarSubsidio()
            End If
        Else
            If negSubsidio.Editaemp_subsidio(subs) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                cargarSubsidio()
            End If
        End If

        Return True
    End Function

    Private Function cargarSubsidio()
        Dim negSubsidio As New Negocio.Negocioemp_Subsidio
        stSubsidio.DataSource = negSubsidio.Obteneremp_SubsidiobyWhere(" where codemp = '" & codigo & "'")
        stSubsidio.DataBind()
        Return True
    End Function


#End Region
End Class