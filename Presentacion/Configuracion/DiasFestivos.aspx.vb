﻿Imports Ext.Net

Public Class DiasFestivos
    Inherits System.Web.UI.Page

    Protected Property idDiaFestivo As Integer
        Get
            Return ViewState("idDiaFestivo")
        End Get
        Set(value As Integer)
            ViewState.Add("idDiaFestivo", value)
        End Set
    End Property

    Protected Property Accion As String
        Get
            Return ViewState("Accion")
        End Get
        Set(value As String)
            ViewState.Add("Accion", value)
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim seg As New Seguridad(Me, Session("PERFIL"))
            cargaDiasFestivos()
        End If
    End Sub

    Protected Sub rowSelect(ByVal sender As Object, ByVal e As DirectEventArgs)
        idDiaFestivo = e.ExtraParams("ID")
    End Sub

    Private Function cargaDiasFestivos()
        Dim negDiaFestivo As New Negocio.Negociosaih_dfes
        Dim sWhere As String = " where id <> 0 "
        If txtDiaBsq.Text <> "01/01/0001 0:00:00" And txtDiaBsq.Text <> "" Then
            sWhere &= " and dia = '" & Funciones.Fecha(txtDiaBsq.Text) & "'"
        End If
        If txtDescripcion.Text <> "" Then
            sWhere &= " and descripcion like '%" & txtDescripcionBsq.Text & "%'"
        End If
        stDiasFestivos.DataSource = negDiaFestivo.Obtenersaih_dfesbyWhere(sWhere & " order by dia desc")
        stDiasFestivos.DataBind()
        Return True
    End Function

    Private Sub btnNuevaDiaFestivo_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnNuevaDiaFestivo.DirectClick
        Accion = "Insert"
        txtDescripcion.Clear()
        txtDia.Clear()
        VntDiasFestivos.Show()
    End Sub

    Private Sub btnEditaDiaFestivo_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEditaDiaFestivo.DirectClick
        Dim negDiaFestivo As New Negocio.Negociosaih_dfes
        Dim diaFestivo As New Datos.saih_dfes
        Accion = "Edit"
        diaFestivo = negDiaFestivo.Obtenersaih_dfesById(idDiaFestivo)
        txtDescripcion.Text = diaFestivo.descripcion
        txtDia.Text = diaFestivo.dia
        VntDiasFestivos.Show()
    End Sub

    Private Sub btnGuardaDiaFestivo_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnGuardaDiaFestivo.DirectClick
        Dim negDiaFestivo As New Negocio.Negociosaih_dfes
        If Accion = "Insert" Then
            If negDiaFestivo.Altasaih_dfes(0, Funciones.Fecha(txtDia.Text), txtDescripcion.Text) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                cargaDiasFestivos()
                VntDiasFestivos.Hide()
            End If
        Else
            If Accion = "Edit" Then
                If negDiaFestivo.Editasaih_dfes(idDiaFestivo, Funciones.Fecha(txtDia.Text), txtDescripcion.Text) Then
                    Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                    cargaDiasFestivos()
                    VntDiasFestivos.Hide()
                End If
            End If
        End If
    End Sub

    Private Sub btnCerrarDiaFestivo_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnCerrarDiaFestivo.DirectClick
        VntDiasFestivos.Hide()
    End Sub

    Private Sub btnBuscar_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnBuscar.DirectClick
        cargaDiasFestivos()
    End Sub

    <DirectMethod()>
    Public Function eliminarDFes()
        Dim negDiaFestivo As New Negocio.Negociosaih_dfes
        negDiaFestivo.Eliminasaih_dfes(idDiaFestivo)
        cargaDiasFestivos()
        Return True
    End Function

    Private Sub btnEliminaDiaFestivo_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEliminaDiaFestivo.DirectClick
        Dim js As String = "Ext.MessageBox.confirm('Confirmación', '¿Esta seguro de eliminar?', " & _
           "function (btn) { " & _
                " if (btn == 'yes') " & _
                "   Ext.net.DirectMethods.eliminarDFes(); " & _
                " });"
        Ext.Net.X.Js.AddScript(js)
    End Sub
End Class