﻿Imports System.IO

Public Class hospital
    Inherits System.Web.UI.Page

    Protected Property Accion As String
        Get
            Return ViewState("Accion")
        End Get
        Set(value As String)
            ViewState.Add("Accion", value)
        End Set
    End Property

    Protected Property codEnt As String
        Get
            Return ViewState("codEnt")
        End Get
        Set(value As String)
            ViewState.Add("codEnt", value)
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim seg As New Seguridad(Me, Session("PERFIL"))
            codEnt = Datos.deTablas.datoTabla("codent", "saih_hos", "")
            Accion = "Insert"
            If codEnt <> "" Then
                Accion = "Update"
                cargaHospital()
            End If
            Funciones.cargaStoreDominio(stTipoDocumento, Datos.ConstantesUtil.DOMINIO_TIPO_IDENTIFICACION)
            Funciones.cargaStoreDominio(stRegimen, Datos.ConstantesUtil.DOMINIO_REGIMEN)
        End If
    End Sub

    Private Function cargaHospital()
        Dim negHospital As New Negocio.Negociosaih_hos
        Dim hospital As New Datos.saih_hos
        hospital = negHospital.Obtenersaih_hosById(codEnt)
        txtCodEnt.Text = hospital.codent
        cbTipDoc.Value = hospital.tipdoc
        txtNroDoc.Text = hospital.nrodoc
        txtNombre.Text = hospital.nombre
        txtDireccion.Text = hospital.direccion
        txtTelefono.Text = hospital.telefono
        ckindtriage.Value = Funciones.IntToBool(hospital.indtriage)
        ckindimpformula.Value = Funciones.IntToBool(hospital.indimpformula)
        ckindfechaglosa.Value = Funciones.IntToBool(hospital.indfechaglosa)
        txtGerente.Text = hospital.gerente
        txtSubGerente.Text = hospital.subgerente
        txtResDian.Text = hospital.resdian
        txtFechaResolucion.Text = hospital.fecha_resolucion
        txtNumInicial.Value = hospital.num_inicial
        txtNumFinal.Value = hospital.num_final
        txtNumFactura.Value = hospital.num_factura
        cbRegimen.Value = hospital.regimen
        txtPrefijo.Text = hospital.prefijo
        Return True
    End Function

    Protected Sub btnGuardar_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        Dim negHospital As New Negocio.Negociosaih_hos
        If Accion = "Insert" Then
            If negHospital.Altasaih_hos(txtCodEnt.Text, cbTipDoc.Value, txtNroDoc.Text, txtNombre.Text, txtDireccion.Text,
                                           txtTelefono.Text, "", txtResDian.Text, Funciones.Fecha(txtFechaResolucion.Text),
                                           txtNumInicial.Value, txtNumFinal.Value, txtNumFactura.Value, txtPrefijo.Text,
                                           cbRegimen.Value, txtGerente.Text, txtSubGerente.Text, Funciones.BoolToInt(ckindtriage.Value),
                                           Funciones.BoolToInt(ckindimpformula.Value), Funciones.BoolToInt(ckindfechaglosa.Value)) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
            End If
        End If
        If Accion = "Update" Then
            If negHospital.Editasaih_hos(codEnt, cbTipDoc.Value, txtNroDoc.Text, txtNombre.Text, txtDireccion.Text,
                                           txtTelefono.Text, "", txtResDian.Text, Funciones.Fecha(txtFechaResolucion.Text),
                                           txtNumInicial.Value, txtNumFinal.Value, txtNumFactura.Value, txtPrefijo.Text,
                                           cbRegimen.Value, txtGerente.Text, txtSubGerente.Text, Funciones.BoolToInt(ckindtriage.Value),
                                           Funciones.BoolToInt(ckindimpformula.Value), Funciones.BoolToInt(ckindfechaglosa.Value)) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
            End If
        End If
    End Sub

    Protected Sub btnCarga_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        If Archivo.HasFile Then
            Dim FileName As String = Path.GetFileName(Archivo.PostedFile.FileName)
            Dim Extension As String = Path.GetExtension(Archivo.PostedFile.FileName)
            Dim FolderPath As String = ConfigurationManager.AppSettings("FolderPath")

            Dim FilePath As String = MapPath("~/Imagenes/logo.png")
            Try
                Archivo.PostedFile.SaveAs(FilePath)
                Ext.Net.X.Msg.Alert("Información", "Logo Cargado Correctamente").Show()
            Catch ex As Exception
                Ext.Net.X.Msg.Alert("Error", "Se encontro un error el proceso").Show()
            End Try


        End If

    End Sub
End Class