﻿Imports Ext.Net
Imports System.Xml
Imports System.Xml.Xsl

Public Class AgendaInformes
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then

            Dim negPas As New Negocio.Negociosaih_pas
            Funciones.cargaStoreDominio(stCodEspecialidad, Datos.ConstantesUtil.DOMINIO_ESPECIALIDADES)
            Funciones.cargaStoreDominio(stEntidadSalud, Datos.ConstantesUtil.DOMINIO_ENTIDADES_SALUD)
            cbTipoDescarga.Value = "pdf"
            stPAsistencial.DataSource = negPas.Obtenersaih_pas
            stPAsistencial.DataBind()
        End If
    End Sub

    Private Function cargaReporte(rpt As String)
        Dim urlJasper As String = System.Web.Configuration.WebConfigurationManager.AppSettings("urlJasper").ToString
        Dim url As String
        urlJasper = urlJasper.Replace("#", "&")
        If cbTipoDescarga.Value <> "pdf" Then
            urlJasper = urlJasper.Replace("pdf", cbTipoDescarga.Value)
        End If

        If cbTipoDescarga.Value = "xlsx" Then
            exportaExcel(rpt)
        Else
            Dim especialidadc As String = "&especialidadc=" & If(cbCodEspecialidad.Value = "", "0", cbCodEspecialidad.Value)
            Dim identificacionpac As String = "&identificacionpac=" & If(txtIdentificacion.Text = "", "0", txtIdentificacion.Text)
            Dim nombremed As String = "&nombremed=" & If(cbPAsistencial.Value = "", "0", cbPAsistencial.Value)
            Dim nombremp As String = "&nombremp=" & If(cbEntidadSalud.Value = "", "0", cbEntidadSalud.Value)
            Dim pccodigo As String = "&pccodigo=" & If(CbProcedimiento.Value = "", "0", CbProcedimiento.Value)
            Dim fechainicio As String = "&fechainicio=" & If(txtFechaIni.Text = "", "0", Funciones.FechaRpt(txtFechaIni.Text))
            Dim fechafin As String = "&fechafin=" & If(txtFechaFin.Text = Nothing, "0", Funciones.FechaRpt(txtFechaFin.Text))
            Dim horaInicio As String = "&horaInicio=" & If(txtHoraInicio.Text = "-10675199.02:48:05.4775808", "00:00:00", txtHoraInicio.Text & ":00")
            Dim horaFin As String = "&horaFin=" & If(txtHoraFinal.Text = "-10675199.02:48:05.4775808", "23:59:59", txtHoraFinal.Text & ":59")
            url = urlJasper & especialidadc & identificacionpac & nombremed & nombremp & pccodigo & fechainicio & fechafin & horaInicio & horaFin & _
                "&reportUnit=%2Freports%2FSAIH%2F" & rpt & "#page=1&zoom=auto,-146,792"
            Dim js As String
            js = "impresionReporte('" & url & "');"
            Ext.Net.X.Js.AddScript(js)
        End If

        
        Return True
    End Function

    Private Function exportaExcel(rpt As String)
        Dim query As String
        Dim spFunc As New Datos.spFunciones

        Dim especialidadc As String = "'" & If(cbCodEspecialidad.Value = "", "0", cbCodEspecialidad.Value) & "'"
        Dim identificacionpac As String = "'" & If(txtIdentificacion.Text = "", "0", txtIdentificacion.Text) & "'"
        Dim nombremed As String = "'" & If(cbPAsistencial.Value = "", "0", cbPAsistencial.Value) & "'"
        Dim nombremp As String = "'" & If(cbEntidadSalud.Value = "", "0", cbEntidadSalud.Value) & "'"
        Dim pccodigo As String = "'" & If(CbProcedimiento.Value = "", "0", CbProcedimiento.Value) & "'"
        Dim fechainicio As String = "'" & If(txtFechaIni.Text = "", "0", Funciones.FechaRpt(txtFechaIni.Text)) & "'"
        Dim fechafin As String = "'" & If(txtFechaFin.Text = Nothing, "0", Funciones.FechaRpt(txtFechaFin.Text)) & "'"
        Dim horaInicio As String = "'" & If(txtHoraInicio.Text = "-10675199.02:48:05.4775808", "00:00:00", txtHoraInicio.Text & ":00") & "'"
        Dim horaFin As String = "'" & If(txtHoraFinal.Text = "-10675199.02:48:05.4775808", "23:59:59", txtHoraFinal.Text & ":59") & "'"


        If rpt = "rptCitasEspecialidad" Then
            query = " select CAST(a.fecha_cita as date) As Fecha, pas.codigo as CodMedi, pas.nombres as" & _
                    " nombres,h.numhis as Historia, RIGHT(a.fecha_cita,7) as Hora,h.primer_nombre+' '+ h.primer_apellido as NombrePaciente, h.telefono as Tel," & _
                    " p.nombre as Procedimiento, datediff(year,h.fecha_nacimiento,GETDATE()) as Edad, e.razonsocial as Empresa," & _
                    " l.codigo municipio,t.nombre especialidad,dbo.FN_FECHA_SOLA(a.fecha_cita) + pas.codigo grupo" & _
                    " from saih_age a" & _
                    " left join saih_pcd p on a.codpcd= p.codigo" & _
                    " left join saih_pas pas on a.codpas = pas.codigo" & _
                    " left join saih_emp e on a.codemp= e.codigo" & _
                    " left join saih_his h on a.numhis = h.numhis" & _
                    " left join saih_tab t on pas.cod_especialidad=t.codigo and t.dominio='ESPECIALID'" & _
                    " left join saih_lugar l on h.id_lugar = l.id" & _
                    " where (cast(a.fecha_cita as date) between " & fechainicio & " and " & fechafin & ")" & _
                    " and (t.codigo = " & especialidadc & " or " & especialidadc & "='0')" & _
                    " and (h.identificacion = " & identificacionpac & " or " & identificacionpac & "='0')" & _
                    " and (a.codpas = " & nombremed & " or " & nombremed & "='0')" & _
                    " and (e.razonsocial = " & nombremp & " or " & nombremp & "='0')" & _
                    " and (p.codigo = " & pccodigo & " or " & pccodigo & " ='0')" & _
                    " and (cast(a.fecha_cita as time) between cast(" & horaInicio & " as time) and cast(" & horaFin & " as time))" & _
                    " and a.estado='Programada' order by Fecha,CodMedi"

            Dim queryPr As String = "select a.nroage,e.razonsocial empresa,pas.nombres medico,t.nombre especialidad, " & _
                            "a.fecha_modificacion fecha_asignacion,a.fecha_cita, " & _
                            "a.numhis identificacion,h.razonsocial paciente,a.codpcd,p.nombre nom_procedimiento,a.estado,a.usuario_modifico " & _
                            "from saih_age a " & _
                            " left join saih_pcd p on a.codpcd= p.codigo " & _
                            " left join saih_pas pas on a.codpas = pas.codigo " & _
                            " left join saih_emp e on a.codemp= e.codigo " & _
                            " left join saih_his h on a.numhis = h.numhis " & _
                            " left join saih_tab t on pas.cod_especialidad=t.codigo and t.dominio='ESPECIALID' " & _
                            " left join saih_lugar l on h.id_lugar = l.id " & _
                            " where (cast(a.fecha_cita as date) between " & fechainicio & " and " & fechafin & ")" & _
                            " and (t.codigo = " & especialidadc & " or " & especialidadc & "='0')" & _
                            " and (h.identificacion = " & identificacionpac & " or " & identificacionpac & "='0')" & _
                            " and (a.codpas = " & nombremed & " or " & nombremed & "='0')" & _
                            " and (e.razonsocial = " & nombremp & " or " & nombremp & "='0')" & _
                            " and (p.codigo = " & pccodigo & " or " & pccodigo & " ='0')" & _
                            " and (cast(a.fecha_cita as time) between cast(" & horaInicio & " as time) and cast(" & horaFin & " as time)) " & _
                            " and a.estado = 'Programada' and a.codpcd <> '' "

            Dim ds As DataSet = spFunc.executaSelect(queryPr)

            'Datos.clDtsToExcel.Convert(ds, Me.Response, Me.grdEncCitXUs)

            Datos.clDtsToExcel.ConvertTitulos(ds, Me.Response, Me.grdEncCitXUs, 1, NomArch:="Reporte",
                                                    parCabeceraRepor:=Me.getCabeceraRepor(1, "REPORTE DISPOSITIVOS MEDICOS", CType(Date.Today, String)))


            'Dim reader1 As New Ext.Net.ReaderCollection
            'Dim reader As New JsonReader
            'Dim store As New Store

            'stExcel.Reader.Add(reader)


            'For Each Column As DataColumn In ds.Tables(0).Columns
            '    Dim columna As New Column
            '    reader.Fields.Add(Column.ColumnName)
            '    columna.Header = Column.ColumnName
            '    columna.DataIndex = Column.ColumnName
            '    grdExcel.ColumnModel.Columns.Add(columna)
            'Next

            'stExcel.DataSource = ds
            'stExcel.DataBind()

        End If


        Return True
    End Function

    Public Function getCabeceraRepor(ByVal parIdPagina As Integer, ByVal parTituloRepor As String, ByVal parFchGeneraRepor As String _
                                    ) As String
        Dim cabecera As New Text.StringBuilder
        If (parIdPagina = 1) Then
            cabecera.Append("<table>")
            cabecera.Append("<tr>")
            cabecera.Append("<td align='left' colspan='5'")
            cabecera.Append("<b>" & parTituloRepor & "</b>")
            cabecera.Append("</td>")
            cabecera.Append("<td align='left'>")
            cabecera.Append("<b>&nbsp;</b>")
            cabecera.Append("</td>")
            cabecera.Append("<td align='left'>")
            cabecera.Append("<b>&nbsp;</b>")
            cabecera.Append("</td>")
            cabecera.Append("<td align='left'>")
            cabecera.Append("<b>&nbsp;</b>")
            cabecera.Append("</td>")
            cabecera.Append("<td align='left'>")
            cabecera.Append("<b>&nbsp;</b>")
            cabecera.Append("</td>")
            cabecera.Append("<td align='left'>")
            cabecera.Append("<b>&nbsp;</b>")
            cabecera.Append("</td>")
            cabecera.Append("<td align='left'>")
            cabecera.Append("<b>&nbsp;</b>")
            cabecera.Append("</td>")
            cabecera.Append("<td align='right'  Width='500'>")
            cabecera.Append("<br><b> Fecha Creación </b> " & parFchGeneraRepor & "</b>")
            cabecera.Append("</td>")
            cabecera.Append("</tr>")
            cabecera.Append("<tr>")
            cabecera.Append("<td align='left' colspan='5'")
            cabecera.Append("<br><b>  </b> " & "<br>")
            cabecera.Append("</td>")
            cabecera.Append("<td align='left'>")
            cabecera.Append("<br><b>  </b> " & "<br>")
            cabecera.Append("</td>")
            cabecera.Append("</tr>")
            cabecera.Append("<tr>")
            cabecera.Append("<td align='left' colspan='5'")
            cabecera.Append("<br><b>  </b> " & "<br>")
            cabecera.Append("</td>")
            cabecera.Append("</tr>")
            cabecera.Append("<tr>")
            cabecera.Append("<td align='left'>")
            cabecera.Append("</td>")
            cabecera.Append("</tr>")
            cabecera.Append("</table>")
            cabecera.Append("<br>")
        End If
        Return cabecera.ToString
    End Function


    Private Sub btnAgeXEsp_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnAgeXEsp.DirectClick
        If txtFechaIni.Text = "01/01/0001 0:00:00" Or txtFechaFin.Text = "01/01/0001 0:00:00" Then
            Ext.Net.X.Msg.Alert("Información", "Sebe seleccionar un rango de fechas").Show()
            Return
        End If
        cargaReporte("rptCitasEspecialidad")
    End Sub

    Private Sub btnAgeXPax_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnAgeXPax.DirectClick
        If txtFechaIni.Text = "01/01/0001 0:00:00" Or txtFechaFin.Text = "01/01/0001 0:00:00" Then
            Ext.Net.X.Msg.Alert("Información", "Sebe seleccionar un rango de fechas").Show()
            Return
        End If
        If txtIdentificacion.Text = "" Then
            Ext.Net.X.Msg.Alert("Información", "Debe Ingresar la Identificación del paciente").Show()
            Return
        End If
        cargaReporte("rptCitasXPaciente")
    End Sub

    Private Sub BtnAgeMed_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles BtnAgeMed.DirectClick
        If txtFechaIni.Text = "01/01/0001 0:00:00" Or txtFechaFin.Text = "01/01/0001 0:00:00" Then
            Ext.Net.X.Msg.Alert("Información", "Sebe seleccionar un rango de fechas").Show()
            Return
        End If
        If cbPAsistencial.Value = "" Then
            Ext.Net.X.Msg.Alert("Información", "El Profesional es Requerido").Show()
            Return
        End If
        cargaReporte("rptCitasMedico")
    End Sub

    Private Sub btnAgeDiariaxMed_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnAgeDiariaxMed.DirectClick
        If txtFechaIni.Text = "01/01/0001 0:00:00" Or txtFechaFin.Text = "01/01/0001 0:00:00" Then
            Ext.Net.X.Msg.Alert("Información", "Sebe seleccionar un rango de fechas").Show()
            Return
        End If
        'If cbPAsistencial.Value = "" Then
        '    Ext.Net.X.Msg.Alert("Información", "El Profesional es Requerido").Show()
        '    Return
        'End If
        cargaReporte("rptCitasMedicoXEsp")
    End Sub

    Private Sub btnLimpiaParametros_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnLimpiaParametros.DirectClick
        presentacionUtil.limpiarObjetos(TabPaciente)
    End Sub

    Private Sub btnAgeCanc_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnAgeCanc.DirectClick
        If txtFechaIni.Text = "01/01/0001 0:00:00" Or txtFechaFin.Text = "01/01/0001 0:00:00" Then
            Ext.Net.X.Msg.Alert("Información", "Sebe seleccionar un rango de fechas").Show()
            Return
        End If
        cargaReporte("rptCitasCanceladas")
    End Sub


    Protected Sub ToExcel(ByVal sender As Object, ByVal e As EventArgs)
        Dim json As String = GridData.Value.ToString()
        Dim eSubmit As New StoreSubmitDataEventArgs(json, Nothing)
        Dim xml As XmlNode = eSubmit.Xml

        Me.Response.Clear()
        Me.Response.ContentType = "application/vnd.ms-excel"
        Me.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls")
        Dim xtExcel As New XslCompiledTransform()
        xtExcel.Load(Server.MapPath("Excel.xsl"))
        xtExcel.Transform(xml, Nothing, Me.Response.OutputStream)
        Me.Response.[End]()
    End Sub

    Protected Sub btnReporte_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)

        If txtFechaIni.Text = "01/01/0001 0:00:00" Or txtFechaFin.Text = "01/01/0001 0:00:00" Then
            Ext.Net.X.Msg.Alert("Información", "Sebe seleccionar un rango de fechas").Show()
            Return
        End If

        Dim spFunc As New Datos.spFunciones

        Dim especialidadc As String = "'" & If(cbCodEspecialidad.Value = "", "0", cbCodEspecialidad.Value) & "'"
        Dim identificacionpac As String = "'" & If(txtIdentificacion.Text = "", "0", txtIdentificacion.Text) & "'"
        Dim nombremed As String = "'" & If(cbPAsistencial.Value = "", "0", cbPAsistencial.Value) & "'"
        Dim nombremp As String = "'" & If(cbEntidadSalud.Value = "", "0", cbEntidadSalud.Value) & "'"
        Dim pccodigo As String = "'" & If(CbProcedimiento.Value = "", "0", CbProcedimiento.Value) & "'"
        Dim fechainicio As String = "'" & If(txtFechaIni.Text = "", "0", Funciones.FechaRpt(txtFechaIni.Text)) & "'"
        Dim fechafin As String = "'" & If(txtFechaFin.Text = Nothing, "0", Funciones.FechaRpt(txtFechaFin.Text)) & "'"
        Dim horaInicio As String = "'" & If(txtHoraInicio.Text = "-10675199.02:48:05.4775808", "00:00:00", txtHoraInicio.Text & ":00") & "'"
        Dim horaFin As String = "'" & If(txtHoraFinal.Text = "-10675199.02:48:05.4775808", "23:59:59", txtHoraFinal.Text & ":59") & "'"
        Dim usuario As String = "'" & If(txtUsuario.Text = "", "0", txtUsuario.Text) & "'"
        Dim queryPr As String

        If cbReporte.Value = "1" Then
            queryPr = "select a.nroage,e.razonsocial empresa,pas.nombres medico,t.nombre especialidad, " & _
                            "a.fecha_modificacion fecha_asignacion,a.fecha_cita, " & _
                            "a.numhis identificacion,h.razonsocial paciente,a.codpcd,p.nombre nom_procedimiento,a.estado,a.usuario_modifico, " & _
                            "l.Descripcion municipio " & _
                            "from saih_age a " & _
                            " left join saih_pcd p on a.codpcd= p.codigo " & _
                            " left join saih_pas pas on a.codpas = pas.codigo " & _
                            " left join saih_emp e on a.codemp= e.codigo " & _
                            " left join saih_his h on a.numhis = h.numhis " & _
                            " left join saih_tab t on pas.cod_especialidad=t.codigo and t.dominio='ESPECIALID' " & _
                            " left join saih_lugar l on h.id_lugar = l.id " & _
                            " where (cast(a.fecha_cita as date) between " & fechainicio & " and " & fechafin & ")" & _
                            " and (t.codigo = " & especialidadc & " or " & especialidadc & "='0')" & _
                            " and (h.identificacion = " & identificacionpac & " or " & identificacionpac & "='0')" & _
                            " and (a.codpas = " & nombremed & " or " & nombremed & "='0')" & _
                            " and (e.codigo = " & nombremp & " or " & nombremp & "='0')" & _
                            " and (p.codigo = " & pccodigo & " or " & pccodigo & " ='0')" & _
                            " and (a.usuario_modifico = " & usuario & " or " & usuario & " ='0')" & _
                            " and (cast(a.fecha_cita as time) between cast(" & horaInicio & " as time) and cast(" & horaFin & " as time)) " & _
                            " and a.codpcd <> '' "

            Dim ds As DataSet = spFunc.executaSelect(queryPr)

            Datos.clDtsToExcel.Convert(ds, Me.Response, Me.grdEncCitXUs)
        End If

        If cbReporte.Value = "2" Then
            queryPr = " select a.nroage as Nro_Cita, a.fecha_cita  Feccit,RIGHT(a.fecha_cita,7) as Horcit,e.razonsocial" & _
                     " as Entidad,l.Descripcion as Municipio,concat(h.tipo_identificacion,h.identificacion) as Identificacion," & _
                     " concat(h.primer_apellido," & _
                     "  ' ',h.segundo_apellido,' ', h.primer_nombre,' ',h.segundo_nombre) as Paciente,pas.nombres as NombreMedico," & _
                     "  t.nombre as Especialidad,a.usuario_modifico as Usuario,a.estado as Estado" & _
                     " from saih_age as a" & _
                     " left join saih_prm p on a.codpre= p.codpre" & _
                     " join saih_emp e on a.codemp= e.codigo" & _
                     " join saih_lugar l on p.id_lugar = l.Id" & _
                     " join saih_his h on a.numhis = h.numhis" & _
                     " join saih_pas pas on a.codpas = pas.codigo" & _
                     " join saih_tab t on pas.cod_especialidad=t.codigo and t.dominio='ESPECIALID' " & _
                     " where (cast(a.fecha_cita as date) between " & fechainicio & " and " & fechafin & ")" & _
                     " and (cast(a.fecha_cita as time) between cast(" & horaInicio & " as time) and cast(" & horaFin & " as time)) " & _
                     " and (e.codigo = " & nombremp & " or " & nombremp & "='0')"

            Dim ds As DataSet = spFunc.executaSelect(queryPr)

            Datos.clDtsToExcel.Convert(ds, Me.Response, Me.grdEncCitExpecialidad)

        End If

        If cbReporte.Value = "3" Then
            queryPr = " select e.razonsocial as Entidad,p.codser as Serv,a.fecha_modificacion as FechaSolicitud," & _
                    " pas.nombres as NombreMedico, concat(h.primer_apellido," & _
                    "  ' ',h.segundo_apellido,' ', h.primer_nombre,' ',h.segundo_nombre) as Paciente, cast(a.fecha_cita as DATE)as FechaCita," & _
                    " DATEDIFF(dd,a.fecha_modificacion,a.fecha_cita) as Oportunidad, '5' as MAX_ACEPTABLE" & _
                    " from saih_age a" & _
                    " left join saih_pcd p on a.codpcd= p.codigo" & _
                    " join saih_emp e on a.codemp= e.codigo" & _
                    " join saih_pas pas on a.codpas = pas.codigo" & _
                    " join saih_his h on a.numhis = h.numhis where " & _
                    "(cast(a.fecha_cita as date) between " & fechainicio & " and " & fechafin & ")" & _
                    " and (cast(a.fecha_cita as time) between cast(" & horaInicio & " as time) and cast(" & horaFin & " as time)) " & _
                    " and (e.codigo = " & nombremp & " or " & nombremp & "='0')" & _
                    " and p.codser = '25' and pas.cod_especialidad = 'MG'"

            Dim ds As DataSet = spFunc.executaSelect(queryPr)

            Datos.clDtsToExcel.Convert(ds, Me.Response, Me.grdEncOportunidad)


        End If

        If cbReporte.Value = "4" Then
            queryPr = " select e.razonsocial as Entidad,p.codser as Serv,a.fecha_modificacion as FechaSolicitud," & _
                    " pas.nombres as NombreMedico, concat(h.primer_apellido," & _
                    "  ' ',h.segundo_apellido,' ', h.primer_nombre,' ',h.segundo_nombre) as Paciente, cast(a.fecha_cita as DATE)as FechaCita," & _
                    " DATEDIFF(dd,a.fecha_modificacion,a.fecha_cita) as Oportunidad, '30' as MAX_ACEPTABLE" & _
                    " from saih_age a" & _
                    " left join saih_pcd p on a.codpcd= p.codigo" & _
                    " join saih_emp e on a.codemp= e.codigo" & _
                    " join saih_pas pas on a.codpas = pas.codigo" & _
                    " join saih_his h on a.numhis = h.numhis where " & _
                    "(cast(a.fecha_cita as date) between " & fechainicio & " and " & fechafin & ")" & _
                    " and (cast(a.fecha_cita as time) between cast(" & horaInicio & " as time) and cast(" & horaFin & " as time)) " & _
                    " and (e.codigo = " & nombremp & " or " & nombremp & "='0')" & _
                    " and p.codser = '45' and pas.cod_especialidad = '387'"

            Dim ds As DataSet = spFunc.executaSelect(queryPr)

            Datos.clDtsToExcel.Convert(ds, Me.Response, Me.grdEncOportunidad)
        End If

        If cbReporte.Value = "5" Then
            queryPr = " select e.razonsocial as Entidad,p.codser as Serv,a.fecha_modificacion as FechaSolicitud," & _
                    " pas.nombres as NombreMedico, concat(h.primer_apellido," & _
                    "  ' ',h.segundo_apellido,' ', h.primer_nombre,' ',h.segundo_nombre) as Paciente, cast(a.fecha_cita as DATE)as FechaCita," & _
                    " DATEDIFF(dd,a.fecha_modificacion,a.fecha_cita) as Oportunidad, '15' as MAX_ACEPTABLE" & _
                    " from saih_age a" & _
                    " left join saih_pcd p on a.codpcd= p.codigo" & _
                    " join saih_emp e on a.codemp= e.codigo" & _
                    " join saih_pas pas on a.codpas = pas.codigo" & _
                    " join saih_his h on a.numhis = h.numhis where " & _
                    "(cast(a.fecha_cita as date) between " & fechainicio & " and " & fechafin & ")" & _
                    " and (cast(a.fecha_cita as time) between cast(" & horaInicio & " as time) and cast(" & horaFin & " as time)) " & _
                    " and (e.codigo = " & nombremp & " or " & nombremp & "='0')" & _
                    " and p.codser = '45' and (pas.cod_especialidad = '340' or pas.cod_especialidad = '341')"

            Dim ds As DataSet = spFunc.executaSelect(queryPr)

            Datos.clDtsToExcel.Convert(ds, Me.Response, Me.grdEncOportunidad)
        End If

        If cbReporte.Value = "6" Then
            queryPr = " select e.razonsocial as Entidad,p.codser as Serv,a.fecha_modificacion as FechaSolicitud," & _
                    " pas.nombres as NombreMedico, concat(h.primer_apellido," & _
                    "  ' ',h.segundo_apellido,' ', h.primer_nombre,' ',h.segundo_nombre) as Paciente, cast(a.fecha_cita as DATE)as FechaCita," & _
                    " DATEDIFF(dd,a.fecha_modificacion,a.fecha_cita) as Oportunidad, '5' as MAX_ACEPTABLE" & _
                    " from saih_age a" & _
                    " left join saih_pcd p on a.codpcd= p.codigo" & _
                    " join saih_emp e on a.codemp= e.codigo" & _
                    " join saih_pas pas on a.codpas = pas.codigo" & _
                    " join saih_his h on a.numhis = h.numhis where " & _
                    "(cast(a.fecha_cita as date) between " & fechainicio & " and " & fechafin & ")" & _
                    " and (cast(a.fecha_cita as time) between cast(" & horaInicio & " as time) and cast(" & horaFin & " as time)) " & _
                    " and (e.codigo = " & nombremp & " or " & nombremp & "='0')" & _
                    " and p.codser = '45' and (pas.cod_especialidad = '550' or pas.cod_especialidad = '551')"

            Dim ds As DataSet = spFunc.executaSelect(queryPr)

            Datos.clDtsToExcel.Convert(ds, Me.Response, Me.grdEncOportunidad)
        End If

        If cbReporte.Value = "7" Then
            queryPr = " select e.razonsocial as Entidad,p.codser as Serv,a.fecha_modificacion as FechaSolicitud," & _
                    " pas.nombres as NombreMedico, concat(h.primer_apellido," & _
                    "  ' ',h.segundo_apellido,' ', h.primer_nombre,' ',h.segundo_nombre) as Paciente, cast(a.fecha_cita as DATE)as FechaCita,DATEDIFF(dd,a.fecha_modificacion,a.fecha_cita) as Oportunidad, '20' as MAX_ACEPTABLE" & _
                    " from saih_age a" & _
                    " left join saih_pcd p on a.codpcd= p.codigo" & _
                    " join saih_emp e on a.codemp= e.codigo" & _
                    " join saih_pas pas on a.codpas = pas.codigo" & _
                    " join saih_his h on a.numhis = h.numhis where " & _
                    "(cast(a.fecha_cita as date) between " & fechainicio & " and " & fechafin & ")" & _
                    " and (cast(a.fecha_cita as time) between cast(" & horaInicio & " as time) and cast(" & horaFin & " as time)) " & _
                    " and (e.codigo = " & nombremp & " or " & nombremp & "='0')" & _
                    " and pas.cod_especialidad in ('130' ,'131','132','133','134','135','137','138','139','140','141','142','143','144','145','146','147','148','149')"

            Dim ds As DataSet = spFunc.executaSelect(queryPr)

            Datos.clDtsToExcel.Convert(ds, Me.Response, Me.grdEncOportunidad)
        End If

        If cbReporte.Value = "8" Then
            queryPr = " select e.razonsocial as Entidad,p.codser as Serv,a.fecha_modificacion as FechaSolicitud," & _
                    " pas.nombres as NombreMedico, concat(h.primer_apellido," & _
                    "  ' ',h.segundo_apellido,' ', h.primer_nombre,' ',h.segundo_nombre) as Paciente, cast(a.fecha_cita as DATE)as FechaCita," & _
                    " DATEDIFF(dd,a.fecha_modificacion,a.fecha_cita) as Oportunidad, '5' as MAX_ACEPTABLE" & _
                    " from saih_age a" & _
                    " left join saih_pcd p on a.codpcd= p.codigo" & _
                    " join saih_emp e on a.codemp= e.codigo" & _
                    " join saih_pas pas on a.codpas = pas.codigo" & _
                    " join saih_his h on a.numhis = h.numhis where " & _
                    "(cast(a.fecha_cita as date) between " & fechainicio & " and " & fechafin & ")" & _
                    " and (cast(a.fecha_cita as time) between cast(" & horaInicio & " as time) and cast(" & horaFin & " as time)) " & _
                    " and (e.codigo = " & nombremp & " or " & nombremp & "='0')" & _
                    " and pas.cod_especialidad in ('460' ,'461','462','463')"

            Dim ds As DataSet = spFunc.executaSelect(queryPr)

            Datos.clDtsToExcel.Convert(ds, Me.Response, Me.grdEncOportunidad)
        End If

        If cbReporte.Value = "9" Then
            queryPr = " select e.razonsocial as Entidad,p.codser as Serv,a.fecha_modificacion as FechaSolicitud," & _
                    " pas.nombres as NombreMedico, concat(h.primer_apellido," & _
                    "  ' ',h.segundo_apellido,' ', h.primer_nombre,' ',h.segundo_nombre) as Paciente, cast(a.fecha_cita as DATE)as FechaCita," & _
                    " DATEDIFF(dd,a.fecha_modificacion,a.fecha_cita) as Oportunidad, '3' as MAX_ACEPTABLE" & _
                    " from saih_age a" & _
                    " left join saih_pcd p on a.codpcd= p.codigo" & _
                    " join saih_emp e on a.codemp= e.codigo" & _
                    " join saih_pas pas on a.codpas = pas.codigo" & _
                    " join saih_his h on a.numhis = h.numhis where " & _
                    "(cast(a.fecha_cita as date) between " & fechainicio & " and " & fechafin & ")" & _
                    " and (cast(a.fecha_cita as time) between cast(" & horaInicio & " as time) and cast(" & horaFin & " as time)) " & _
                    " and (e.codigo = " & nombremp & " or " & nombremp & "='0')" & _
                    " and p.codser in ('03' ,'04','05')"

            Dim ds As DataSet = spFunc.executaSelect(queryPr)

            Datos.clDtsToExcel.Convert(ds, Me.Response, Me.grdEncOportunidad)
        End If

        If cbReporte.Value = "10" Then
            queryPr = " select e.razonsocial as Entidad,p.codser as Serv,a.fecha_modificacion as FechaSolicitud," & _
                    " pas.nombres as NombreMedico, concat(h.primer_apellido," & _
                    "  ' ',h.segundo_apellido,' ', h.primer_nombre,' ',h.segundo_nombre) as Paciente, cast(a.fecha_cita as DATE)as FechaCita," & _
                    " DATEDIFF(dd,a.fecha_modificacion,a.fecha_cita) as Oportunidad, '1' as MAX_ACEPTABLE" & _
                    " from saih_age a" & _
                    " left join saih_pcd p on a.codpcd= p.codigo" & _
                    " join saih_emp e on a.codemp= e.codigo" & _
                    " join saih_pas pas on a.codpas = pas.codigo" & _
                    " join saih_his h on a.numhis = h.numhis where " & _
                    "(cast(a.fecha_cita as date) between " & fechainicio & " and " & fechafin & ")" & _
                    " and (cast(a.fecha_cita as time) between cast(" & horaInicio & " as time) and cast(" & horaFin & " as time)) " & _
                    " and (e.codigo = " & nombremp & " or " & nombremp & "='0')" & _
                    " and (pas.cod_especialidad = '353' or pas.cod_especialidad = '352')"

            Dim ds As DataSet = spFunc.executaSelect(queryPr)

            Datos.clDtsToExcel.Convert(ds, Me.Response, Me.grdEncOportunidad)
        End If

    End Sub
End Class