﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AgendaInformes.aspx.vb" Inherits="Presentacion.AgendaInformes" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../public/css/estilos.css" type="text/css" rel="stylesheet" />   
    <script src="../public/js/jquery-1.9.0.js" type="text/javascript"></script>
    <script type="text/javascript" src="./../public/js/app.js"></script>
    <script type="text/javascript">
        function impresionReporte(url) {
            var ventana = parent.Ext.getCmp('VentanaImpresion');
            //console.log(url);
            ventana.setTitle('Impresión Documento');
            ventana.loadIFrame({ url: url });
            ventana.show();
        };

        function limpiarImpresion(url) {
            var ventana = parent.Ext.getCmp('VentanaImpresion');
            ventana.setTitle('Impresión Documento');
            ventana.loadIFrame({ url: url })
        }

        var saveData = function () {
            GridData.setValue(Ext.encode(grdExcel.getRowsValues({ selectedOnly: false })));
        };
    </script>

    
</head>
<body>
    <form id="form1" runat="server">
        <ext:ResourceManager ID="ResourceManager1" runat="server" />


        <ext:Store ID="stExcel" runat="server" AutoLoad="false"  >             
        </ext:Store>


        <asp:GridView ID="grdEncCitXUs" runat="server" Visible="False" >
            <Columns>
                <asp:BoundField DataField="nroage" HeaderText="nroage"  />
                <asp:BoundField DataField="empresa" HeaderText="empresa"  />
                <asp:BoundField DataField="medico" HeaderText="medico"  />
                <asp:BoundField DataField="especialidad" HeaderText="especialidad"  />
                <asp:BoundField DataField="fecha_asignacion" HeaderText="fecha_asignacion"  />
                <asp:BoundField DataField="fecha_cita" HeaderText="fecha_cita"  />
                <asp:BoundField DataField="identificacion" HeaderText="identificacion"  />            
                <asp:BoundField DataField="paciente" HeaderText="paciente"  />
                <asp:BoundField DataField="codpcd" HeaderText="codpcd"  /> 
                <asp:BoundField DataField="nom_procedimiento" HeaderText="nom_procedimiento"  />
                <asp:BoundField DataField="estado" HeaderText="estado"  />            
                <asp:BoundField DataField="usuario_modifico" HeaderText="usuario_modifico"  />
                <asp:BoundField DataField="municipio" HeaderText="municipio"  />
            </Columns>
        </asp:GridView>

        <asp:GridView ID="grdEncCitExpecialidad" runat="server" Visible="False" >
            <Columns>
                <asp:BoundField DataField="Nro_Cita" HeaderText="Nro_Cita"  />
                <asp:BoundField DataField="Feccit" HeaderText="Feccit"  />
                <asp:BoundField DataField="Horcit" HeaderText="Horcit"  />
                <asp:BoundField DataField="Entidad" HeaderText="Entidad"  />
                <asp:BoundField DataField="Municipio" HeaderText="Municipio"  />
                <asp:BoundField DataField="Identificacion" HeaderText="Identificacion"  />
                <asp:BoundField DataField="Paciente" HeaderText="Paciente"  />            
                <asp:BoundField DataField="NombreMedico" HeaderText="NombreMedico"  />                
                <asp:BoundField DataField="Especialidad" HeaderText="Especialidad"  />                
                <asp:BoundField DataField="Usuario" HeaderText="Usuario"  />                
                <asp:BoundField DataField="Estado" HeaderText="Estado"  />                
            </Columns>
        </asp:GridView>

        <asp:GridView ID="grdEncOportunidad" runat="server" Visible="False" >
            <Columns>
                <asp:BoundField DataField="Entidad" HeaderText="Entidad"  />
                <asp:BoundField DataField="Serv" HeaderText="Serv"  />
                <asp:BoundField DataField="FechaSolicitud" HeaderText="FechaSolicitud"  />
                <asp:BoundField DataField="NombreMedico" HeaderText="NombreMedico"  />
                <asp:BoundField DataField="Paciente" HeaderText="Paciente"  />
                <asp:BoundField DataField="FechaCita" HeaderText="FechaCita"  />
                <asp:BoundField DataField="Oportunidad" HeaderText="Oportunidad"  />            
                <asp:BoundField DataField="MAX_ACEPTABLE" HeaderText="MAX_ACEPTABLE"  />                
            </Columns>
        </asp:GridView>

        <ext:Hidden ID="GridData" runat="server" />

        <ext:TabPanel ID="TabPaciente" runat="server" ActiveTabIndex="0" Border="false" Title="Center" ResizeTabs="true">
            <Items>
                <ext:Panel ID="PGeneral" runat="server" Closable="False" Title="General de Agenda de Citas Medicas Programadas" Hidden="False" >
                    <Items>
                        <ext:Container ID="Container9" runat="server" Layout="ColumnLayout" Height="210" >
                            <Items>                                
                                <ext:Container ID="Container3" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".85" >
                                    <Items>
                                        <ext:Panel runat="server" ID="panel1" Padding="10" >
                                            <Items>
                                                <ext:Container ID="Container4" runat="server" Layout="ColumnLayout" Height="30" >
                                                    <Items>
                                                        <ext:Container ID="Container5" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".2">
                                                            <Items>
                                                                <ext:DateField runat="server" ID="txtFechaIni" FieldLabel="Desde" AnchorHorizontal="99%" />
                                                            </Items>
                                                        </ext:Container>
                                                        <ext:Container ID="Container1" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".2">
                                                            <Items>
                                                                <ext:DateField runat="server" ID="txtFechaFin" FieldLabel="Hasta" AnchorHorizontal="99%" />
                                                            </Items>
                                                        </ext:Container>
                                                        <ext:Container ID="Container2" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".4">
                                                            <Items>
                                                                <ext:ComboBox     
	                                                                ID="cbCodEspecialidad"                                                                              
	                                                                runat="server" 
	                                                                Shadow="Drop" 
	                                                                Mode="Local" 
	                                                                TriggerAction="All" 
	                                                                ForceSelection="true"
	                                                                DisplayField="nombre"
	                                                                ValueField="codigo"	
	                                                                EmptyText="Seleccione Especialidad"
	                                                                AnchorHorizontal="90%"
	                                                                AllowBlank="false"
                                                                    FieldLabel="Especialidad"
	                                                                >
	                                                                <Store>
		                                                                <ext:Store ID="stCodEspecialidad" runat="server" AutoLoad="true">
			                                                                <Reader>
				                                                                <ext:JsonReader IDProperty="codigo">
					                                                                <Fields>
						                                                                <ext:RecordField Name="codigo" />
						                                                                <ext:RecordField Name="nombre" />
					                                                                </Fields>
				                                                                </ext:JsonReader>
			                                                                </Reader>            
		                                                                </ext:Store>    
	                                                                </Store>    
                                                                </ext:ComboBox>
                                                            </Items>
                                                        </ext:Container>
                                                        <ext:Container ID="Container6" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".2">
                                                            <Items>
                                                                <ext:TextField runat="server" FieldLabel="Doc.Paciente" ID="txtIdentificacion" AnchorHorizontal="99%"/>
                                                            </Items>
                                                        </ext:Container>
                                                    </Items>
                                                </ext:Container>
                                                <ext:Container ID="Container7" runat="server" Layout="ColumnLayout" Height="30" >
                                                    <Items>
                                                        <ext:Container ID="Container8" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".4">
                                                            <Items>
                                                                <ext:ComboBox     
	                                                                ID="cbPAsistencial"                                                                              
	                                                                runat="server" 
	                                                                Shadow="Drop" 
	                                                                Mode="Local" 
	                                                                TriggerAction="All" 
	                                                                ForceSelection="true"
	                                                                DisplayField="nombres"
	                                                                ValueField="codigo"	
	                                                                EmptyText="Seleccione Profesional"
	                                                                AnchorHorizontal="99%"
	                                                                FieldLabel="Profesional"                                                                    
                                                                    DataIndex="codpas"
	                                                                >
	                                                                <Store>
		                                                                <ext:Store ID="stPAsistencial" runat="server" AutoLoad="true">
			                                                                <Reader>
				                                                                <ext:JsonReader IDProperty="codigo">
					                                                                <Fields>
						                                                                <ext:RecordField Name="codigo" />
						                                                                <ext:RecordField Name="nombres" />
					                                                                </Fields>
				                                                                </ext:JsonReader>
			                                                                </Reader>            
		                                                                </ext:Store>    
	                                                                </Store>    
                                                                </ext:ComboBox>
                                                            </Items>
                                                        </ext:Container>
                                                        <ext:Container ID="Container10" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".2" >
                                                            <Items>
                                                                <ext:TimeField runat="server" ID="txtHoraInicio" FieldLabel="H.Inicial" AllowBlank="false" AnchorHorizontal="90%" />
                                                            </Items>
                                                        </ext:Container>
                                                        <ext:Container ID="Container12" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".2" >
                                                            <Items>
                                                                <ext:TimeField runat="server" ID="txtHoraFinal" FieldLabel="H.Final" AllowBlank="false" AnchorHorizontal="90%"/>
                                                            </Items>
                                                        </ext:Container>
                                                        <ext:Container ID="Container13" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".2" >
                                                            <Items>
                                                            </Items>
                                                        </ext:Container>
                                                    </Items>
                                                </ext:Container>
                                                <ext:Container ID="Container14" runat="server" Layout="ColumnLayout" Height="30" >
                                                    <Items>
                                                        <ext:Container ID="Container15" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".8">
                                                            <Items>
                                                                <ext:ComboBox     
	                                                                ID="cbEntidadSalud"                                                                              
	                                                                runat="server" 
	                                                                Shadow="Drop" 
	                                                                Mode="Local" 
	                                                                TriggerAction="All" 
	                                                                ForceSelection="true"
	                                                                DisplayField="nombre"
	                                                                ValueField="codigo"	
	                                                                EmptyText="Seleccione Empresa"
	                                                                AnchorHorizontal="95%"	                                                                
                                                                    FieldLabel="Empresa"
                                                                    DataIndex="codemp"
	                                                                >
	                                                                <Store>
		                                                                <ext:Store ID="stEntidadSalud" runat="server" AutoLoad="true">
			                                                                <Reader>
				                                                                <ext:JsonReader IDProperty="codigo">
					                                                                <Fields>
						                                                                <ext:RecordField Name="codigo" />
						                                                                <ext:RecordField Name="nombre" />
					                                                                </Fields>
				                                                                </ext:JsonReader>
			                                                                </Reader>            
		                                                                </ext:Store>    
	                                                                </Store>    
                                                                </ext:ComboBox>
                                                            </Items>
                                                        </ext:Container>
                                                        <ext:Container ID="Container16" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".2">
                                                            <Items>
                                                            </Items>
                                                        </ext:Container>
                                                    </Items>
                                                </ext:Container>
                                                <ext:Container ID="Container17" runat="server" Layout="ColumnLayout" Height="30" >
                                                    <Items>
                                                        <ext:Container ID="Container18" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5">
                                                            <Items>
                                                                <ext:ComboBox
                                                                    ID="CbProcedimiento"
                                                                    FieldLabel="Procedimiento"
                                                                    runat="server"
                                                                    DisplayField="nombre"
                                                                    ValueField="codigo"
                                                                    TypeAhead="false"
                                                                    LoadingText="Buscando...."
                                                                    Width="300"
                                                                    PageSize="10"
                                                                    HideTrigger="true"
                                                                    ItemSelector="div.search-item"
                                                                    MinChars="3"                                                                    
                                                                    >
                                                                    <Store>
                                                                        <ext:Store runat="server" AutoLoad="false" ID="stCbProcedimiento">
                                                                            <Proxy>
                                                                                <ext:HttpProxy Method="POST" Url=".././Funciones/Procedimientos.ashx" />
                                                                            </Proxy>
                                                                            <Reader>
                                                                                <ext:JsonReader Root="procedimiento" TotalProperty="total">
                                                                                    <Fields>
                                                                                        <ext:RecordField Name="codigo" />
                                                                                        <ext:RecordField Name="nombre" />                                                                                                                                                     
                                                                                    </Fields>                                                        
                                                                                </ext:JsonReader>
                                                                            </Reader>                                                
                                                                        </ext:Store>
                                                                    </Store>
                                                                    <Template runat="server" ID="ctl1788">
                                                                        <Html>
                                                                            <tpl for=".">
                                                                                <div class="search-item">
							                                                        <h3><span>{codigo}</span>{nombre}</h3>							                               
						                                                        </div>                                                        
                                                                            </tpl>                                                    
                                                                        </Html>                                            
                                                                    </Template>
                                                                </ext:ComboBox>
                                                            </Items>
                                                        </ext:Container>
                                                        <ext:Container ID="Container20" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3">
                                                            <Items>
                                                                <ext:ComboBox ID="cbTipoDescarga" runat="server"  FieldLabel="Exportar en" AllowBlank="false" AnchorHorizontal="95%">
                                                                    <Items>
                                                                        <ext:ListItem Text="Pdf" Value="pdf" />
                                                                        <ext:ListItem Text="Excel" Value="xlsx" />
                                                                        <ext:ListItem Text="Word" Value="docx" />
                                                                        <ext:ListItem Text="Csv" Value="csv" />
                                                                        <ext:ListItem Text="Rtf" Value="rtf" />
                                                                        <ext:ListItem Text="Odt" Value="odt" />
                                                                    </Items>
                                                                </ext:ComboBox>
                                                            </Items>
                                                        </ext:Container>
                                                        
                                                        <ext:Container ID="Container19" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".2">
                                                            <Items>
                                                                <ext:Button runat="server" ID="btnLimpiaParametros" Text="Limpiar" Icon="Delete" />
                                                            </Items>
                                                        </ext:Container>
                                                    </Items>
                                                </ext:Container>     
                                                <ext:Container ID="Container21" runat="server" Layout="ColumnLayout" Height="30" >
                                                    <Items>
                                                        <ext:Container ID="Container22" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3">
                                                            <Items>
                                                                <ext:TextField runat="server" ID="txtUsuario" FieldLabel="Usuario" />
                                                            </Items>                                           
                                                        </ext:Container>
                                                        <ext:Container ID="Container23" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".7">
			                                                <Items>
				                                                <ext:ComboBox ID="cbReporte" runat="server"  FieldLabel="Informe" AllowBlank="false" AnchorHorizontal="95%">
					                                                <Items>
						                                                <ext:ListItem Text="CITAS ASIGNADAS POR USUARIO" Value="1" />
						                                                <ext:ListItem Text="CITAS POR ESPECIALIDAD Y PROCEDENCIA" Value="2" />
						                                                <ext:ListItem Text="OPORTUNIDAD POR PACIENTE POR CONSULTA MEDICINA GENERAL" Value="3" />
						                                                <ext:ListItem Text="OPORTUNIDAD POR PACIENTE POR CONSULTA MEDICINA INTERNA" Value="4" />
						                                                <ext:ListItem Text="OPORTUNIDAD POR PACIENTE POR CONSULTA DE GINECOLOGIA" Value="5" />
						                                                <ext:ListItem Text="OPORTUNIDAD POR PACIENTE POR CONSULTA DE PEDIATRIA" Value="6" />						
						                                                <ext:ListItem Text="OPORTUNIDAD POR PACIENTE POR CONSULTA DE CIRUGIA GENERAL" Value="7" />
						                                                <ext:ListItem Text="OPORTUNIDAD POR PACIENTE POR CONSULTA DE ODONTOLOGIA" Value="8" />
						                                                <ext:ListItem Text="OPORTUNIDAD POR PACIENTE POR RADIOLOGIA" Value="9" />
						                                                <ext:ListItem Text="OPORTUNIDAD POR PACIENTE POR LABORATORIO CLINICO" Value="10" />						
					                                                </Items>
				                                                </ext:ComboBox>
			                                                </Items>
		                                                </ext:Container>
                                                    </Items>
                                                </ext:Container>
                                            </Items>
                                        </ext:Panel>
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container11" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".15" >
                                    <Items>
                                        <ext:Panel runat="server" ID="panel12" Padding="10" >
                                            <Items>
                                                <ext:Button runat="server" ID="btnAgeDiariaxMed" Text="Agenda Diaria x Med/Esp" Width="150" /> 
                                                <ext:Button runat="server" ID="btnAgeXProc" Text="Citas X Procedimiento" Width="150" Disabled="true" />
                                                <ext:Button runat="server" ID="btnAgeXPax" Text="Citas X Paciente" Width="150" />
                                                <ext:Button runat="server" ID="btnAgeXEsp" Text="Citas X Especialidad" Width="150" />
                                                <ext:Button runat="server" ID="btnAgeCanc" Text="Citas Canceladas" Width="150" />
                                                <ext:Button runat="server" ID="btnGeneralCitas" Text="General de Citas" Width="150" Disabled="true"/>
                                                <ext:Button runat="server" ID="BtnAgeMed" Text="Agenda Medica" Width="150" />
                                                <ext:Button runat="server" ID="btnReporte" Text="Exportar" Icon="PageExcel" Width="150" >
                                                    <DirectEvents>
                                                        <Click OnEvent="btnReporte_DirectClick" AutoDataBind="true" FormID="Form1" Buffer="300" IsUpload="true" Method="GET"
                                                            Success="Ext.net.DirectMethods.Download();">
                                                            </Click>                                    
                                                    </DirectEvents>
                                                </ext:Button>
                                            </Items>
                                        </ext:Panel>
                                    </Items>
                                </ext:Container>    
                            </Items>
                        </ext:Container>
                    </Items>
                </ext:Panel>
                <ext:GridPanel
                    ID="grdExcel"
                    runat="server"
                    StoreId="stExcel"
                    Border="false"
                    Height="400">
                    <ColumnModel ID="ColumnModel3" runat="server">
                        <Columns>
                        </Columns>
                    </ColumnModel>
                    <LoadMask ShowMask="true" Msg="Loading...." />
                    <Buttons>
                        <ext:Button runat="Server" ID="btnExporta" Text="Exportar" AutoPostBack="true" OnClick="ToExcel" Icon="PageExcel">
                            <Listeners>
                                <Click Fn="saveData" />
                            </Listeners>
                        </ext:Button>
                    </Buttons>                                       
                </ext:GridPanel>
            </Items>
        </ext:TabPanel>
    </form>
</body>
</html>

