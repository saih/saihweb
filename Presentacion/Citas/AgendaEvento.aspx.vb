﻿Imports Ext.Net

Public Class AgendaEvento
    Inherits System.Web.UI.Page

    Protected Property Accion As String
        Get
            Return ViewState("Accion")
        End Get
        Set(value As String)
            ViewState.Add("Accion", value)
        End Set
    End Property

    Protected Property IdAge As String
        Get
            Return ViewState("IdAge")
        End Get
        Set(value As String)
            ViewState.Add("IdAge", value)
        End Set
    End Property

    Protected Property numhis As String
        Get
            Return ViewState("numhis")
        End Get
        Set(value As String)
            ViewState.Add("numhis", value)
        End Set
    End Property


    Protected Property manual As String
        Get
            Return ViewState("manual")
        End Get
        Set(value As String)
            ViewState.Add("manual", value)
        End Set
    End Property

    Protected Property codigo As String
        Get
            Return ViewState("codigo")
        End Get
        Set(value As String)
            ViewState.Add("codigo", value)
        End Set
    End Property


    Protected Property Estado As String
        Get
            Return ViewState("Estado")
        End Get
        Set(value As String)
            ViewState.Add("Estado", value)
        End Set
    End Property

    Protected Property manEspecialidad As String
        Get
            Return ViewState("manEspecialidad")
        End Get
        Set(value As String)
            ViewState.Add("manEspecialidad", value)
        End Set
    End Property

    Protected Property codEspecialidad As String
        Get
            Return ViewState("codEspecialidad")
        End Get
        Set(value As String)
            ViewState.Add("codEspecialidad", value)
        End Set
    End Property

    Protected Property habilita_transaccion As String
        Get
            Return ViewState("habilita_transaccion")
        End Get
        Set(value As String)
            ViewState.Add("habilita_transaccion", value)
        End Set
    End Property

    Protected Property codMedico As String
        Get
            Return ViewState("codMedico")
        End Get
        Set(value As String)
            ViewState.Add("codMedico", value)
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim seg As New Seguridad(Me, Session("PERFIL"))
            manEspecialidad = "0"
            habilita_transaccion = "0"
            Dim negPas As New Negocio.Negociosaih_pas
            Dim negEmpresa As New Negocio.Negociosaih_emp
            stPAsistencial.DataSource = negPas.Obtenersaih_pasbyWhere(" where estado = 'A'")
            stPAsistencial.DataBind()

            Dim negPrm As New Negocio.Negociosaih_prm
            stSede.DataSource = negPrm.Obtenersaih_prm
            stSede.DataBind()
            cbSede.Value = Session("Sede")

            Dim negDiasFestivos As New Negocio.Negociosaih_dfes
            Response.Write("<style type='text/css'>")
            For Each diaFestivo As Datos.saih_dfes In negDiasFestivos.Obtenersaih_dfes
                Response.Write("#CalendarPanel1-month-day-" & Presentacion.Funciones.Fecha(diaFestivo.dia) & "{ background-color: #F68F99;}")
            Next
            Response.Write("</style>")

        End If
    End Sub

    <DirectMethod()>
    Public Function nuevoEvento(fecha As String)
        fecha = fecha.Substring(1, fecha.Length - 2)
        txtFecha.Text = Funciones.FechaCalendar(fecha)
        txtHoraInicio.Text = Funciones.HoraCalendar(fecha)
        txtHoraFinal.Text = Funciones.HoraCalendar(fecha)
        'vntAgeEvento.Show()
        Return True
    End Function

    <DirectMethod()>
    Public Function editaEvento(id As String)
        Accion = "Edit"
        manual = ""
        codigo = ""
        Dim spFunc As New Datos.spFunciones
        Dim negAge As New Negocio.Negociosaih_age
        Dim age As New Datos.saih_age
        age = negAge.Obtenersaih_ageById(id)
        IdAge = id
        Dim negMedico As New Negocio.Negociosaih_pas
        Dim medico As New Datos.saih_pas
        Dim negTab As New Negocio.Negociosaih_tab
        Dim tab As New Datos.saih_tab
        medico = negMedico.Obtenersaih_pasById(age.codpas)
        tab = negTab.Obtenersaih_tabById(Datos.ConstantesUtil.DOMINIO_ESPECIALIDADES, medico.cod_especialidad)

        Dim js As String
        js = "limpiarImpresion('WebImpresion.aspx');"
        Ext.Net.X.Js.AddScript(js)

        presentacionUtil.limpiarObjetos(vntAgeEvento)

        lblMedico.Text = medico.nombres
        lblEspecialW.Text = tab.nombre
        txtFecha.Text = Funciones.FechaWindow(age.fecha_cita)
        txtHoraInicio.Text = Funciones.HoraWindow(age.fecha_cita)
        txtHoraFinal.Text = Funciones.HoraWindow(age.fecha_fincita)
        txtConsultorio.Text = age.consultorio
        txtTelefono.Text = age.telefono
        txtObservaciones.Text = age.observaciones
        lblNroAgenda.Text = age.nroage

        stTipServicio.DataSource = spFunc.executaSelect("select b.codser,b.nombre from saih_paser a inner " & _
                                    "join saih_ser b on a.codser = b.codser where a.codpas = '" & age.codpas & "'")
        stTipServicio.DataBind()
        cbTipServicio.Value = age.codser
        stProcedimiento.DataSource = spFunc.executaSelect("select a.manual+'-'+a.codigo codigo,a.codigo+' M('+a.manual+') ' + a.nombre nombre from saih_pcd a " & _
                                     "inner join saih_paser b on a.codser = b.codser where b.codpas = '" & age.codpas & "' and a.codser = '" & age.codser & "'")
        stProcedimiento.DataBind()

        btnGuardaAge.Hidden = False

        Estado = age.estado
        habilita_transaccion = "1"

        If age.estado <> "Pendiente" Then
            btnCancelarAge.Hidden = False
            btnImprimeAge.Hidden = False
            Dim negHis As New Negocio.Negociosaih_his
            cbTipServicio.Value = age.codser
            cbProcedimiento.Value = age.manual & "-" & age.codpcd
            txtCodigoPcd.Text = age.codpcd
            manual = age.manual
            codigo = age.codpcd
            cargaDatosPaciente(age.numhis, "Ingreso")
            ConCancelar.Hidden = False
            CbPaciente.Disabled = True
            vntAgeEvento.Show()
        Else
            Dim ctDiferencia As Integer = Datos.deTablas.difFechas("hh", Funciones.fechaHoraDB(age.fecha_cita), Funciones.FechaActual)
            vntAgeEvento.Show()
            If ctDiferencia > 0 Then
                Ext.Net.X.Msg.Alert("Información", "La fecha de la cita es menor a la actual").Show()
                btnGuardaAge.Hidden = True
            End If
            btnCancelarAge.Hidden = True
            btnImprimeAge.Hidden = True
            ConCancelar.Hidden = True
            CbPaciente.Disabled = False
        End If

        Return True
    End Function

    Private Sub CbPaciente_DirectSelect(sender As Object, e As Ext.Net.DirectEventArgs) Handles CbPaciente.DirectSelect
        cargaDatosPaciente(CbPaciente.Value, "")
        numhis = CbPaciente.Value

        If manEspecialidad = "1" Then

            Dim id_familia As String

            Dim negFamiliaEsp As New Negocio.Negociofamilia_esp
            Dim familiaEsp As New Datos.familia_esp

            Dim negMedico As New Negocio.Negociosaih_pas
            Dim medico As Datos.saih_pas


            id_familia = Datos.deTablas.datoTabla("id_familia", "familia_his", " where numhis = '" & numhis & "'")
            If id_familia = "" Then
                habilita_transaccion = "0"
                Ext.Net.X.Msg.Alert("Información", "El Paciente no posee grupo familiar asignado").Show()
                Return
            Else
                familiaEsp = negFamiliaEsp.Obtenerfamilia_espById(id_familia, codEspecialidad)
                If familiaEsp Is Nothing Then
                    habilita_transaccion = "0"
                    Ext.Net.X.Msg.Alert("Información", "No se ha asignado medico y Especialidad para el grupo Familiar").Show()
                Else
                    medico = negMedico.Obtenersaih_pasById(familiaEsp.codpas)

                    If medico.codigo <> codMedico Then
                        habilita_transaccion = "0"
                        Ext.Net.X.Msg.Alert("Información", "El Medico asignado para el grupo familiar y especialidad es (" + medico.nombres + ")").Show()
                        Return
                    End If
                End If
            End If
        End If

        Dim negAge As New Negocio.Negociosaih_age
        Dim age As New Datos.saih_age
        age = negAge.Obtenersaih_ageById(IdAge)

        Dim horasAgenda As Integer = System.Web.Configuration.WebConfigurationManager.AppSettings("horasAgendamiento").ToString
        
        Dim citasPaciente As Integer

        citasPaciente = Datos.deTablas.datoCont("saih_age", "where ((fecha_cita between " & _
                            "dateadd(hh,-" & horasAgenda & ",'" & Funciones.fechaHoraDB(age.fecha_cita) & "') " & _
                            "and '" & Funciones.fechaHoraDB(age.fecha_cita) & "') or (fecha_cita between " & _
                            "'" & Funciones.fechaHoraDB(age.fecha_cita) & "' and dateadd(hh,-" & _
                            horasAgenda & ",'" & Funciones.fechaHoraDB(age.fecha_cita) & "') " & _
                            ")) and numhis = '" & numhis & "' and estado <> 'Cancelado'")

        If citasPaciente > 0 Then
            Ext.Net.X.Msg.Alert("Información", "Se encuentra Asignación en un rango de (" & horasAgenda & ") horas").Show()
        End If

    End Sub

    Private Function cargaDatosPaciente(numhis As String, tipo As String)

        Try
            Dim negHis As New Negocio.Negociosaih_his
            Dim paciente As New Datos.saih_his
            paciente = negHis.Obtenersaih_hisById(numhis)
            Dim negTab As New Negocio.Negociosaih_tab
            Dim tab As New Datos.saih_tab
            tab = negTab.Obtenersaih_tabById(Datos.ConstantesUtil.DOMINIO_ENTIDADES_SALUD, paciente.codemp)
            lblEntidad.Text = "(" & tab.codigo & ")  " & tab.nombre
            lblNumHistoria.Text = paciente.numhis
            lblIdentificacion.Text = paciente.tipo_identificacion & " " & paciente.identificacion
            lblSexo.Text = paciente.sexo
            lblEdad.Text = Datos.deTablas.edadPaciente(paciente.numhis)
            If tipo = "Ingreso" Then
                CbPaciente.Text = paciente.razonsocial
            Else
                If paciente.observaciones <> "" Then
                    Ext.Net.X.Msg.Alert("Información", paciente.observaciones).Show()
                End If
            End If
            Me.numhis = numhis
        Catch ex As Exception
            Ext.Net.X.Msg.Alert("Información", "Se presento un error en la información, Por favor comunicarse con el administrador").Show()
        End Try
        
        Return True
    End Function

    Protected Sub btnBuscar_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        cargarCalendar()
    End Sub

    Private Function cargarCalendar()

        Try
            Dim sWhere As String = " where id <> 0 and estado <> 'Cancelado' "
            Dim servicio As New DatosCalendario

            If cbSede.Value <> "" Then
                sWhere &= " and codpre = '" & cbSede.Value & "'"
            End If

            If cbPAsistencial.Value <> "" Then
                sWhere &= " and codpas = '" & cbPAsistencial.Value & "'"
            Else
                Ext.Net.X.Msg.Alert("Información", "Debe Seleccionar un Medico").Show()
                Return False
            End If

            If cbEstado.Value <> "" Then
                sWhere &= " and estado = '" & cbEstado.Value & "'"
            End If

            If txtNroPme.Text <> "" Then
                sWhere &= " and nropme = '" & txtNroPme.Text & "'"
            End If

            CalendarPanel1.EventStore.DataSource = servicio.getEvents(sWhere)
            CalendarPanel1.EventStore.DataBind()
        Catch ex As Exception
            Dim negLog As New Negocio.Negociolog_error
            Dim log As New Datos.log_error
            log.tipo = "Agenda"
            log.descripcion = ex.Message
            log.fecha = Funciones.FechaActual
            log.usuario = Session("Usuario")
            negLog.Altalog_error(log)
            Ext.Net.X.Msg.Alert("Información", "Se presento un Error, Por favor comuniquese con el administrador").Show()
        End Try

        

        Return True
    End Function

    <DirectMethod()>
    Public Function txtCodigoPcd_Blur()

        Dim dato As Integer = Datos.deTablas.datoSql("select count(*) from saih_pcd a inner join " & _
                                    "saih_paser b on a.codser = b.codser where b.codpas = '" &
                                    cbPAsistencial.Value & "' and a.codigo = '" & txtCodigoPcd.Text & "'")

        If dato > 0 Then
            cbProcedimiento.Value = txtCodigoPcd.Text
            selecServicio()
        Else
            cbProcedimiento.Clear()
        End If

        Return True
    End Function

    Private Function selecServicio()
        Dim negPcd As New Negocio.Negociosaih_pcd
        Dim pcd As New Datos.saih_pcd
        pcd = negPcd.Obtenersaih_pcdByCodigo(cbProcedimiento.Value)
        cbTipServicio.Value = pcd.codser
        Return True
    End Function

    Private Sub cbTipServicio_DirectSelect(sender As Object, e As Ext.Net.DirectEventArgs) Handles cbTipServicio.DirectSelect
        Dim spFunc As New Datos.spFunciones

        cbProcedimiento.Clear()
        txtCodigoPcd.Clear()

        stProcedimiento.DataSource = spFunc.executaSelect("select a.manual+'-'+a.codigo codigo,a.codigo+' M('+a.manual+') ' + a.nombre nombre from saih_pcd a " & _
                                         "inner join saih_paser b on a.codser = b.codser where b.codpas = '" & _
                                         cbPAsistencial.Value & "' and b.codser = '" & cbTipServicio.Value & "'")
        stProcedimiento.DataBind()
    End Sub

    Private Sub cbProcedimiento_DirectSelect(sender As Object, e As Ext.Net.DirectEventArgs) Handles cbProcedimiento.DirectSelect
        Dim datos As String()
        datos = cbProcedimiento.Value.ToString.Split("-")
        manual = datos(0)
        codigo = datos(1)
    End Sub

    Protected Sub btnGuardaAge_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        Dim negAge As New Negocio.Negociosaih_age
        Dim age As New Datos.saih_age
        Dim spFunc As New Datos.spFunciones

        If habilita_transaccion = "0" Then
            Ext.Net.X.Msg.Alert("Información", "Se encontro un inconveniente, por favor comuniquese con el administrador").Show()
            Return
        End If

        If Accion = "Edit" Then
            age = negAge.Obtenersaih_ageById(IdAge)
            If age.estado = "Pendiente" Then

                If age.estado <> Estado Then
                    Ext.Net.X.Msg.Alert("Información", "Se encontro un inconveniente, por favor comuniquese con el administrador").Show()
                    Return
                End If

                'Dim horasAgenda As Integer = System.Web.Configuration.WebConfigurationManager.AppSettings("horasAgendamiento").ToString
                Dim nro As Integer = 0
                Dim msg As New Datos.mensaje
                'Dim citasPaciente As Integer

                'citasPaciente = Datos.deTablas.datoCont("saih_age", "where ((fecha_cita between " & _
                '                    "dateadd(hh,-" & horasAgenda & ",'" & Funciones.fechaHoraDB(age.fecha_cita) & "') " & _
                '                    "and '" & Funciones.fechaHoraDB(age.fecha_cita) & "') or (fecha_cita between " & _
                '                    "'" & Funciones.fechaHoraDB(age.fecha_cita) & "' and dateadd(hh,-" & _
                '                    horasAgenda & ",'" & Funciones.fechaHoraDB(age.fecha_cita) & "') " & _
                '                    ")) and numhis = '" & numhis & "' and estado <> 'Cancelado'")

                'If citasPaciente > 0 Then
                '    Ext.Net.X.Msg.Alert("Información", "Se encuentra Asignación en un rango de (" & horasAgenda & ") horas").Show()
                '    Return
                'End If

                msg = spFunc.consecutivoPRM(age.codpre, Datos.ConstantesUtil.AGENDA)

                If msg.Codigo = "" Or msg.Codigo = "0" Then
                    nro = msg.Valor
                End If

                If nro <> 0 Then
                    Dim negHis As New Negocio.Negociosaih_his
                    Dim paciente As New Datos.saih_his
                    paciente = negHis.Obtenersaih_hisById(numhis)
                    age.fecha_adicion = Funciones.fechaHoraDB(age.fecha_adicion)
                    age.fecha_cita = Funciones.fechaHoraDB(age.fecha_cita)
                    age.fecha_fincita = Funciones.fechaHoraDB(age.fecha_fincita)
                    age.fecha_modificacion = Funciones.FechaActual
                    age.usuario_modifico = Session("Usuario")
                    age.estado = "Programada"
                    age.codser = cbTipServicio.Value
                    age.codpcd = codigo
                    age.manual = manual
                    age.numhis = paciente.numhis
                    age.codemp = paciente.codemp
                    age.telefono = txtTelefono.Text
                    age.nroage = nro
                    age.observaciones = txtObservaciones.Text
                    If negAge.Editasaih_age(age) Then
                        spFunc.executUpdate("update saih_his set telefono = '" & txtTelefono.Text & "' where numhis = '" & numhis & "'")
                        spFunc.guardarAgeDFB(age.id)
                        impCitaMedica(age.id)
                        Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                        cargarCalendar()
                        vntAgeEvento.Hide()
                    End If
                End If
            Else
                'Dim horasAgenda As Integer = System.Web.Configuration.WebConfigurationManager.AppSettings("horasAgendamiento").ToString
                'Dim citasPaciente As Integer

                'citasPaciente = Datos.deTablas.datoCont("saih_age", "where ((fecha_cita between " & _
                '                    "dateadd(hh,-" & horasAgenda & ",'" & Funciones.fechaHoraDB(age.fecha_cita) & "') " & _
                '                    "and '" & Funciones.fechaHoraDB(age.fecha_cita) & "') or (fecha_cita between " & _
                '                    "'" & Funciones.fechaHoraDB(age.fecha_cita) & "' and dateadd(hh,-" & _
                '                    horasAgenda & ",'" & Funciones.fechaHoraDB(age.fecha_cita) & "') " & _
                '                    ")) and numhis = '" & numhis & "' and estado <> 'Cancelado' and id <> " & IdAge)

                'If citasPaciente > 0 Then
                '    Ext.Net.X.Msg.Alert("Información", "Se encuentra Asignación en un rango de (" & horasAgenda & ") horas").Show()
                '    Return
                'End If

                If age.estado <> Estado Then
                    Ext.Net.X.Msg.Alert("Información", "La Cita ya se encuentra en Uso, Por favor asigne un horario o medico Diferente").Show()
                    Return
                End If

                Dim negHis As New Negocio.Negociosaih_his
                Dim paciente As New Datos.saih_his
                paciente = negHis.Obtenersaih_hisById(numhis)
                age.fecha_adicion = Funciones.fechaHoraDB(age.fecha_adicion)
                age.fecha_cita = Funciones.fechaHoraDB(age.fecha_cita)
                age.fecha_fincita = Funciones.fechaHoraDB(age.fecha_fincita)
                age.fecha_modificacion = Funciones.FechaActual
                age.usuario_modifico = Session("Usuario")
                age.codser = cbTipServicio.Value
                age.codpcd = codigo
                age.manual = manual
                age.numhis = paciente.numhis
                age.codemp = paciente.codemp
                age.telefono = txtTelefono.Text
                age.observaciones = txtObservaciones.Text
                If negAge.Editasaih_age(age) Then
                    spFunc.executUpdate("update saih_his set telefono = '" & txtTelefono.Text & "' where numhis = '" & numhis & "'")
                    Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                    cargarCalendar()
                    vntAgeEvento.Hide()
                End If
            End If
        End If
    End Sub

    Private Function impCitaMedica(id As String)
        Dim urlJasper As String = System.Web.Configuration.WebConfigurationManager.AppSettings("urlJasper").ToString
        Dim url As String
        Dim rpt As String = "rptCitaMedica"
        urlJasper = urlJasper.Replace("#", "&")
        url = urlJasper & "&IdAge=" & id & "&Usuario=" & Session("Usuario") & "&reportUnit=%2Freports%2FSAIH%2F" & rpt & "#page=1&zoom=auto,-146,792"
        Dim js As String
        js = "impresionReporte('" & url & "');"
        Ext.Net.X.Js.AddScript(js)
        Return True
    End Function


    Protected Sub btnCancelarAge_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        Dim spFunc As New Datos.spFunciones
        Dim negAge As New Negocio.Negociosaih_age
        Dim age As New Datos.saih_age
        Dim agenew As New Datos.saih_age
        Dim negapas As New Negocio.Negociosaih_apas
        Dim apas As New Datos.saih_apas

        If txtCodCancelacion.Text = "" Then
            Ext.Net.X.Msg.Alert("Información", "Debe Ingresar el codigo de la cancelación").Show()
            Return
        End If

        If txtCodCancelacion.Text <> Session("Usuario") Then
            Ext.Net.X.Msg.Alert("Información", "El Codigo no coincide con el Usuario Actual").Show()
            Return
        End If

        age = negAge.Obtenersaih_ageById(IdAge)
        spFunc.executUpdate("update saih_age set estado = 'Cancelado',cod_cancelacion='" & txtCodCancelacion.Text & "' where id = " & IdAge)

        apas = negapas.Obtenersaih_apasById(age.codpre, age.nropme)

        If apas.estado = "Cancelando" Then
            Dim contAge As Integer = Datos.deTablas.datoCont("saih_age", " where estado = 'Programada' and codpre = '" & _
                                    age.codpre & "' and nropme = '" & age.nropme & "'")

            If contAge = 0 Then

                If apas.estado = "Cancelando" Then
                    spFunc.executUpdate("update saih_apas set estado = 'Cancelado' where codpre = '" & _
                                        age.codpre & "' and nropme = '" & age.nropme & "'")
                End If
            End If
            Ext.Net.X.Msg.Alert("Información", "Cita Cancelada Satisfactoriamente").Show()
        Else
            agenew.consultorio = age.consultorio
            agenew.fecha_cita = Funciones.fechaHoraDB(age.fecha_cita)
            agenew.fecha_fincita = Funciones.fechaHoraDB(age.fecha_fincita)
            agenew.codser = age.codser
            agenew.usuario_creo = Session("Usuario")
            agenew.usuario_modifico = Session("Usuario")
            agenew.codpre = age.codpre
            agenew.nropme = age.nropme
            agenew.codpas = age.codpas
            agenew.fecha_adicion = Funciones.FechaActual
            agenew.fecha_modificacion = Funciones.FechaActual
            agenew.es_nueva = 0
            agenew.estado = "Pendiente"
            agenew.manual = ""
            agenew.codpcd = ""
            agenew.numhis = ""
            agenew.telefono = ""
            agenew.nroage = ""
            agenew.observaciones = ""
            agenew.cod_cancelacion = ""
            agenew.codemp = ""
            If negAge.Altasaih_age(agenew) Then
                Ext.Net.X.Msg.Alert("Información", "Cita Cancelada Satisfactoriamente, Nuevo Registro Creado").Show()
            End If
        End If

        cargarCalendar()
        vntAgeEvento.Hide()

    End Sub

    Private Sub btnCerrarAge_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnCerrarAge.DirectClick
        vntAgeEvento.Hide()
    End Sub

    Private Sub cbPAsistencial_DirectSelect(sender As Object, e As Ext.Net.DirectEventArgs) Handles cbPAsistencial.DirectSelect
        Dim negMedico As New Negocio.Negociosaih_pas
        Dim medico As New Datos.saih_pas
        Dim negTab As New Negocio.Negociosaih_tab
        Dim tab As New Datos.saih_tab
        medico = negMedico.Obtenersaih_pasById(cbPAsistencial.Value)
        tab = negTab.Obtenersaih_tabById(Datos.ConstantesUtil.DOMINIO_ESPECIALIDADES, medico.cod_especialidad)
        lblEspecialidadMedico.Text = tab.nombre
        codEspecialidad = tab.codigo
        codMedico = medico.codigo
        If tab.valor = Datos.ConstantesUtil.VALOR_ESPECIALID_GRUPO_FAMILIAR Then
            manEspecialidad = "1"
        Else
            manEspecialidad = "0"
        End If
    End Sub
    
    Private Sub btnImprimeAge_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnImprimeAge.DirectClick
        impCitaMedica(IdAge)
    End Sub
End Class