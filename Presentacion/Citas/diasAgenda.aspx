﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="diasAgenda.aspx.vb" Inherits="Presentacion.diasAgenda" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="./../public/css/redmond/jquery-ui-1.9.2.custom.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="./../public/css/bootstrap.min.css" />
    <script type="text/javascript" src="./../public/js/jquery-1.9.0.js"></script>
    <script type="text/javascript" src="./../public/js/jquery-ui-1.9.2.custom.js"></script>
	<script type="text/javascript" src="./../public/js/jquery-ui.multidatespicker.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#simpliest-usage').multiDatesPicker({
                altField: '#altField',
                minDate: 0,
                maxDate: 60
            });            
        });
	</script>
</head>
<body>
    
    <form id="form1" runat="server">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div id="simpliest-usage" class="box"></div> </br>
                    <asp:Button ID="btnValida" runat="server" Text="Cargar Fechas" class="btn btn-primary" /> </br>
                    <asp:TextBox ID="altField" runat="server" ></asp:TextBox>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
