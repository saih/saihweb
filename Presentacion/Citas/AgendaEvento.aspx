﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AgendaEvento.aspx.vb" Inherits="Presentacion.AgendaEvento" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>

    
    <link href="../public/css/estilos.css" type="text/css" rel="stylesheet" />
    <link href="../public/css/Calendar.css" type="text/css" rel="stylesheet" />
    <script src="../public/js/jquery-1.9.0.js" type="text/javascript"></script>
    <script type="text/javascript" src="./../public/js/app.js"></script>
    
    <script type="text/javascript">
        jQuery(document).ready(function () {
            $('#cbProfesional').change(function () {
                $('#GroupStore1').empty();
            });
        });
    </script>

    <script type="text/javascript">
        function impresionReporte(url) {
            var ventana = parent.Ext.getCmp('VentanaImpresion');
            ventana.setTitle('Impresión Cíta Medica');
            ventana.loadIFrame({ url: url })
            ventana.show();
        };

        function limpiarImpresion(url) {
            var ventana = parent.Ext.getCmp('VentanaImpresion');
            ventana.setTitle('Impresión Documento');
            ventana.loadIFrame({ url: url })
        };

    </script>

    <script type="text/javascript">
        var datoPaciente;
        var datoProfesional;

        var CompanyX = {
            getCalendar: function () { return CompanyX.CalendarPanel1; },
            getStore: function () { return CompanyX.EventStore1; },
            getWindow: function () { return CompanyX.EventEditWindow1; },
            customWindow: function () {
                // event edit window localization  
                var win = this.getWindow();
                var form = win.formPanel;
                var titleItem = form.get('title');
                dateRangeItem = form.get('date-range');
                calendarItem = form.get('calendar');
                detailsLnk = win.fbar.get(0);
                saveBtn = win.fbar.get(2);
                deleteBtn = win.fbar.get(3);
                cancelBtn = win.fbar.get(4);
                titleItem.fieldLabel = 'Titulo'; // Title   
                dateRangeItem.fieldLabel = 'Cuando';   // When
                dateRangeItem.toText = 'a';       // to
                dateRangeItem.allDayText = 'Es Todo el Día';  // Is all day
                calendarItem.fieldLabel = 'Atención';  // Calendar group                                
                detailsLnk.text = '<a href="#" id="tblink">Imprimir...</a>';    // Edit Details                
                saveBtn.text = 'Guardar';        // Save button
                deleteBtn.text = 'Borrar';        // Delete button
                cancelBtn.text = 'Cancelar';       // Cancel button

                // date-range date format
                win.get(0).get(1).on('render', function () {
                    this.startDate.format = 'd/m/Y';
                    this.endDate.format = 'd/m/Y';
                });
            },
            updateTitle: function (startDt, endDt) {
                var msg = '';
                if (startDt.clearTime().getTime() == endDt.clearTime().getTime()) {
                    msg = startDt.format('F j, Y');
                }
                else if (startDt.getFullYear() == endDt.getFullYear()) {
                    if (startDt.getMonth() == endDt.getMonth()) {
                        msg = startDt.format('F j') + ' - ' + endDt.format('j, Y');
                    }
                    else {
                        msg = startDt.format('F j') + ' - ' + endDt.format('F j, Y');
                    }
                }
                else {
                    msg = startDt.format('F j, Y') + ' - ' + endDt.format('F j, Y');
                }
                this.Panel1.setTitle(msg);
            },
            setStartDate: function (picker, date) {
                this.getCalendar().setStartDate(date);
            },
            rangeSelect: function (cal, dates, callback) {
                this.record.show(cal, dates);
                this.getWindow().on('hide', callback, cal, { single: true });
            },
            dayClick: function (cal, dt, allDay, el) {
                //alert(dt);
                Ext.net.DirectMethods.nuevoEvento(dt);                
            },
            record: {
                add: function (win, rec) {
                    win.hide();
                    rec.data.IsNew = false;
                    // save the event to database and update the event store
                    CompanyX.record.save(rec);
                    CompanyX.ShowMsg('Event ' + rec.data.Title + ' was added');
                },
                update: function (win, rec) {
                    win.hide();
                    rec.commit();
                    // update the event in the database
                    CompanyX.record.upd(rec);
                    CompanyX.ShowMsg('Event ' + rec.data.Title + ' was updated');
                },
                remove: function (win, rec) {
                    CompanyX.record.del(rec);
                    // remove the event from the database
                    this.getStore().remove(rec);
                    win.hide();
                    CompanyX.ShowMsg('Event ' + rec.data.Title + ' was deleted');
                },
                editFormAdd: function (win, rec) {
                    // called from the eventeditform EventAdd  
                    // save the event to database and update the event store
                    //alert('Acaaa Fue');
                    CompanyX.record.save(rec);
                },
                editFormUpdate: function (win, rec) {
                    // called from the eventeditform EventUpdate
                    // update the event in the database
                    CompanyX.record.upd(rec);
                },
                editFormDelete: function (win, rec) {
                    // called from the eventeditform EventDelete
                    // delete the event from database
                    CompanyX.record.del(rec);
                },
                edit: function (win, rec) {
                    win.hide();
                    CompanyX.getCalendar().showEditForm(rec);
                },
                resize: function (cal, rec) {
                    rec.commit();
                    CompanyX.ShowMsg('Event ' + rec.data.Title + ' was updated');
                },
                move: function (cal, rec) {

                   // Ext.net.DirectMethods.copiaEvento(rec.data.EventId, rec.data.StartDate);


                },
                show: function (cal, rec, el) {
                    var datos;
                    Ext.net.DirectMethods.editaEvento(rec.data.EventId);


                },
                save: function (rec) {

                    Ext.net.DirectMethod.request({
                        url: "../Funciones/DatosCalendario.asmx/Save",
                        json: true,
                        cleanRequest: true,
                        params: {
                            e: rec.data,
                            usuario: document.getElementById("hdUsuario").value
                        },
                        success: function (result) {
                            // update record with db-assigned id                   
                            rec.data.EventId = result.EventId;
                            // add to store
                            CompanyX.getStore().add(rec);
                        }
                    });
                },
                upd: function (rec) {
                    // update event data in the database
                    // event data row is found in the database using EventId
                    Ext.net.DirectEvent.request({
                        url: "../Funciones/DatosCalendario.asmx/Update",
                        json: true,
                        cleanRequest: true,
                        extraParams: {
                            e: rec.data,
                            usuario: document.getElementById("hdUsuario").value
                        }
                    });
                },
                del: function (rec) {
                    Ext.net.DirectEvent.request({
                        url: "../Funciones/DatosCalendario.asmx/Delete",
                        json: true,
                        cleanRequest: true,
                        extraParams: { e: rec.data },
                        success: function (result) {

                            Ext.net.DirectMethods.Refrescar();
                        }
                    });
                }
            }
        };

        var calendar_beforeRender = function (calendar, store) {
            
        };
        


        

    </script>
</head>
<body>
    <form id="form1" runat="server">

    <ext:ResourceManager ID="ResourceManager1" runat="server" Namespace="CompanyX" Locale="es" />   
  
        <ext:Viewport ID="Viewport1" runat="server" Layout="fit">       
            <Items>                    
                <ext:Panel 
                    ID="Panel2" 
                    runat="server" 
                    Title="..." 
                    Layout="Border" 
                    Region="Center" 
                    Cls="app-center">
                    <Items>
                        <ext:Panel ID="Panel3" 
                            runat="server" 
                            Width="200" 
                            Region="West" 
                            Border="false" 
                            Cls="app-west">
                            <Items>
                                <ext:DatePicker 
                                    ID="DatePicker1" 
                                    runat="server" 
                                    Cls="ext-cal-nav-picker" AnchorHorizontal="99%"                                   
                                    >
                                    <Listeners>
                                        <Select Fn="CompanyX.setStartDate" Scope="CompanyX" />
                                    </Listeners>
                                </ext:DatePicker>
                                <ext:Panel ID="Panel1" runat="server" Frame="true" Width="200" Layout="Form" HideLabels="true">
                                    <Items>
                                        <ext:Label runat="server" ID="lbl12" Text="Medico" />
                                        <ext:ComboBox     
	                                        ID="cbPAsistencial"                                                                              
	                                        runat="server" 
	                                        Shadow="Drop" 
	                                        Mode="Local" 
	                                        TriggerAction="All" 
	                                        ForceSelection="true"
	                                        DisplayField="nombres"
	                                        ValueField="codigo"	
	                                        EmptyText="Seleccione Profesional"
	                                        AnchorHorizontal="99%"
	                                        FieldLabel="Medico"
                                            AllowBlank="false"
                                            DataIndex="codpas"
	                                        >
	                                        <Store>
		                                        <ext:Store ID="stPAsistencial" runat="server" AutoLoad="true">
			                                        <Reader>
				                                        <ext:JsonReader IDProperty="codigo">
					                                        <Fields>
						                                        <ext:RecordField Name="codigo" />
						                                        <ext:RecordField Name="nombres" />
					                                        </Fields>
				                                        </ext:JsonReader>
			                                        </Reader>            
		                                        </ext:Store>    
	                                        </Store>    
                                        </ext:ComboBox>
                                        <ext:Label runat="server" ID="lblEspecialidadMedico" Text="" />
                                        <ext:Label runat="server" ID="Label1" Text="Sede" />                                        
                                        <ext:ComboBox     
	                                        ID="cbSede"
	                                        runat="server" 
	                                        Shadow="Drop" 
	                                        Mode="Local" 
	                                        TriggerAction="All" 
	                                        ForceSelection="true"
	                                        DisplayField="lugar"
	                                        ValueField="codpre"	
	                                        EmptyText="Seleccione Sede"
	                                        AnchorHorizontal="99%"
                                            >
	                                        <Store>
		                                        <ext:Store ID="stSede" runat="server" AutoLoad="true">
			                                        <Reader>
				                                        <ext:JsonReader IDProperty="codpre">
					                                        <Fields>
						                                        <ext:RecordField Name="codpre" />
						                                        <ext:RecordField Name="lugar" />
					                                        </Fields>
				                                        </ext:JsonReader>
			                                        </Reader>            
		                                        </ext:Store>    
	                                        </Store>    
                                        </ext:ComboBox>
                                        <ext:Label runat="server" ID="Label2" Text="Estado" />
                                        <ext:ComboBox ID="cbEstado" runat="server"  AnchorHorizontal="100%" >
                                            <Items>
                                                <ext:ListItem Text="Todos" Value="" />
                                                <ext:ListItem Text="Pendiente" Value="Pendiente" />
                                                <ext:ListItem Text="Programada" Value="Programada" />
                                            </Items>
                                        </ext:ComboBox>
                                        <ext:Label runat="server" ID="Label3" Text="Num. Agenda Medico" />
                                        <ext:TextField runat="server" ID="txtNroPme" />
                                        <ext:Button runat="server" ID="btnBuscar" Text="Buscar" Icon="FilmGo" >
                                            <DirectEvents>
                                                <Click OnEvent="btnBuscar_DirectClick">
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>
                                    </Items>
                                </ext:Panel>
                                
                            </Items>
                            <TopBar>
                                <ext:Toolbar ID="Toolbar2" runat="server">
                                    <Items>
                                        <ext:Button 
                                            ID="Button1"
                                            runat="server" 
                                            Text="Save All Events" 
                                            Icon="Disk" 
                                            OnClientClick="CompanyX.record.saveAll();" Hidden="true"
                                            />
                                    </Items>
                                </ext:Toolbar>
                            </TopBar>
                        </ext:Panel>           
                    <ext:CalendarPanel 
                        ID="CalendarPanel1" runat="server" Height="500" MonthText="MES"
                        DayText="DIA" WeekText="SEMANA" Region="Center"                                                
                        >
                        <EventStore ID="EventStore1"
                                    runat="server"
                                    IgnoreExtraFields="false">
                        </EventStore>
                        <Content>
                            <ext:Toolbar ID="Toolbar1" runat="server">
                                <Items>
                                                                
                                </Items>                        
                            </ext:Toolbar>
                        </Content>
                        <GroupStore ID="GroupStore1" runat="server">
                            <Groups>
                                <ext:Group CalendarId="0" Title="__" />                                              
                            </Groups>
                        </GroupStore>               
                        <Listeners>                   
                            <EventClick Fn="CompanyX.record.show" Scope="CompanyX" />                   
                            <DayClick Fn="CompanyX.dayClick" Scope="CompanyX" />                   
                            <RangeSelect Fn="CompanyX.rangeSelect" Scope="CompanyX" />                   
                            <EventMove Fn="CompanyX.record.move" Scope="CompanyX" />                   
                            <EventResize Fn="CompanyX.record.resize" Scope="CompanyX" />                   
                            <BeforeRender Handler="calendar_beforeRender(this, #{CustomStore});" />   
                            <EventAdd Fn="CompanyX.record.editFormAdd" Scope="CompanyX" />                        
                            <EventUpdate Fn="CompanyX.record.editFormUpdate" Scope="CompanyX" />
                            <EventDelete Fn="CompanyX.record.editFormDelete" Scope="CompanyX" />
                        </Listeners>           
                    </ext:CalendarPanel>
                    </Items>
                </ext:Panel>       
                </Items>   
            </ext:Viewport>
    
        <ext:Window 
        ID="vntAgeEvento"
        runat="server"
        Icon="ApplicationFormEdit"
        Width="900"
        Height="350"
        Hidden="true" 
        Modal="true"            
        Title="Agenda Cita Medica"
        Constrain="true"
        
        >
        <Items>
            <ext:FormPanel ID="FormaEvento" runat="server" Padding="10" Height="320">
                <Items>
                    <ext:Container ID="Container20" runat="server" Layout="ColumnLayout" Height="30" >
                        <Items>
                            <ext:Container ID="Container21" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".35" >
                                <Items>
                                    <ext:Label runat="server" ID="lblMedico" FieldLabel="Medico" />
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container22" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".35" >
                                <Items>
                                    <ext:Label runat="server" ID="lblEspecialW" FieldLabel="Especialidad" />
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container25" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3" >
                                <Items>
                                    <ext:Label runat="server" ID="lblNroAgenda" FieldLabel="Nro.Agenda" />
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:Container>
                    <ext:Container ID="Container4" runat="server" Layout="ColumnLayout" Height="30" >
                        <Items>
                            <ext:Container ID="Container6" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                <Items>
                                    <ext:ComboBox
                                        ID="CbPaciente"
                                        FieldLabel="Paciente"
                                        runat="server"
                                        DisplayField="razonsocial"
                                        ValueField="numhis"
                                        TypeAhead="false"
                                        LoadingText="Buscando...."
                                        Width="300"
                                        PageSize="10"
                                        HideTrigger="true"
                                        ItemSelector="div.search-item"
                                        MinChars="5"
                                        AllowBlank="false"                                            
                                        >
                                        <Store>
                                            <ext:Store runat="server" AutoLoad="false" ID="stCbPaciente">
                                                <Proxy>
                                                    <ext:HttpProxy Method="POST" Url=".././Funciones/Paciente.ashx" />
                                                </Proxy>
                                                <Reader>
                                                    <ext:JsonReader Root="paciente" TotalProperty="total">
                                                        <Fields>
                                                            <ext:RecordField Name="numhis" />
                                                            <ext:RecordField Name="tipo_identificacion" />
                                                            <ext:RecordField Name="identificacion" />
                                                            <ext:RecordField Name="razonsocial" />                                                                
                                                        </Fields>                                                        
                                                    </ext:JsonReader>
                                                </Reader>                                                
                                            </ext:Store>
                                        </Store>
                                        <Template runat="server" ID="ctl1788">
                                            <Html>
                                                <tpl for=".">
                                                    <div class="search-item">
							                            <h3><span>{tipo_identificacion}-{identificacion}</span>{razonsocial}</h3>							                               
						                            </div>                                                        
                                                </tpl>                                                    
                                            </Html>                                            
                                        </Template>
                                    </ext:ComboBox>
                                </Items>
                            </ext:Container>                                
                            <ext:Container ID="Container9" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25" >
                                <Items>
                                    <ext:Label ID="lblIdentificacion" runat="server" FieldLabel="Doc.Identidad" />
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container10" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25" >
                                <Items>
                                    <ext:Label ID="lblNumHistoria" runat="server" FieldLabel="Nro.Historia" />
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:Container> 
                    <ext:Container ID="Container2" runat="server" Layout="ColumnLayout" Height="30" >
                        <Items>
                            <ext:Container ID="Container3" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25" >
                                <Items>
                                    <ext:Label ID="lblEdad" runat="server" FieldLabel="Edad" />
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container5" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25" >
                                <Items>
                                    <ext:Label ID="lblSexo" runat="server" FieldLabel="Sexo" />
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container12" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25" >
                                <Items>
                                    <ext:TextField runat="server" ID="txtTelefono" DataIndex="telefono" AnchorHorizontal="99%" FieldLabel="Telefono" AllowBlank="false" />
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:Container>
                    <ext:Container ID="Container13" runat="server" Layout="ColumnLayout" Height="30" >
                        <Items>
                            <ext:Container ID="Container14" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".4" >
                                <Items>
                                    <ext:Label ID="lblEntidad" runat="server" FieldLabel="Entidad" />
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:Container>
                    <ext:Container ID="Container7" runat="server" Layout="ColumnLayout" Height="30" >
                        <Items>
                            <ext:Container ID="Container8" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3" >
                                <Items>
                                    <ext:DateField runat="server" ID="txtFecha" FieldLabel="Fecha Agenda" AllowBlank="false" Disabled="true" AnchorHorizontal="95%" />
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container1" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25" >
                                <Items>
                                    <ext:TimeField runat="server" ID="txtHoraInicio" FieldLabel="H.Inicial" AllowBlank="false" AnchorHorizontal="95%" Disabled="true"  />
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container11" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25" >
                                <Items>
                                    <ext:TimeField runat="server" ID="txtHoraFinal" FieldLabel="H.Final" AllowBlank="false" AnchorHorizontal="95%" Disabled="true"/>
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container19" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".2" >
                                <Items>
                                    <ext:TextField ID="txtConsultorio" runat="server" FieldLabel="Consultorio" Disabled="true" AnchorHorizontal="95%" />
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:Container>
                    <ext:Container ID="Container15" runat="server" Layout="ColumnLayout" Height="30" >
                        <Items>
                            <ext:Container ID="Container16" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".4" >
                                <Items>
                                    <ext:ComboBox     
	                                    ID="cbTipServicio"                                                                              
	                                    runat="server" 
	                                    Shadow="Drop" 
	                                    Mode="Local" 
	                                    TriggerAction="All" 
	                                    ForceSelection="true"
	                                    DisplayField="nombre"
	                                    ValueField="codser"	
	                                    EmptyText="Seleccione Tipo de Servicio"
	                                    AnchorHorizontal="99%"
	                                    AllowBlank="false"
                                        FieldLabel="Servicio"
	                                    >
	                                    <Store>
		                                    <ext:Store ID="stTipServicio" runat="server" AutoLoad="true">
			                                    <Reader>
				                                    <ext:JsonReader IDProperty="codser">
					                                    <Fields>
						                                    <ext:RecordField Name="codser" />
						                                    <ext:RecordField Name="nombre" />
					                                    </Fields>
				                                    </ext:JsonReader>
			                                    </Reader>            
		                                    </ext:Store>    
	                                    </Store>    
                                    </ext:ComboBox>
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container17" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".01" >
                                <Items>
                                    <ext:TextField runat="server" ID="txtCodigoPcd" FieldLabel="Proc. o Cons." AnchorHorizontal="99%" Hidden="true" >
                                        <Listeners>
                                            <Blur Handler="Ext.net.DirectMethods.txtCodigoPcd_Blur()"  />
                                        </Listeners>
                                    </ext:TextField>
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container18" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".59" >
                                <Items>
                                    <ext:ComboBox     
	                                    ID="cbProcedimiento"                                                                              
	                                    runat="server" 
	                                    Shadow="Drop" 
	                                    Mode="Local" 
	                                    TriggerAction="All" 
	                                    ForceSelection="true"
	                                    DisplayField="nombre"
	                                    ValueField="codigo"
	                                    EmptyText="Seleccione Procedimiento o Consulta"
	                                    AnchorHorizontal="99%"
	                                    AllowBlank="false"
                                        FieldLabel="Proc. o Cons."
	                                    >
	                                    <Store>
		                                    <ext:Store ID="stProcedimiento" runat="server" AutoLoad="true">
			                                    <Reader>
				                                    <ext:JsonReader IDProperty="codigo">
					                                    <Fields>
						                                    <ext:RecordField Name="codigo" />
                                                            <ext:RecordField Name="manual" />
						                                    <ext:RecordField Name="nombre" />
					                                    </Fields>
				                                    </ext:JsonReader>
			                                    </Reader>            
		                                    </ext:Store>    
	                                    </Store>    
                                    </ext:ComboBox>
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:Container>
                    <ext:Container ID="Container23" runat="server" Layout="ColumnLayout" Height="60" >
                        <Items>
                            <ext:Container ID="Container24" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth="1" >
                                <Items>
                                    <ext:TextArea runat="server" FieldLabel="Observaciones" ID="txtObservaciones" DataIndex="observaciones" AnchorHorizontal="100%" Height="50" />
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:Container>
                    <ext:Container ID="ConCancelar" runat="server" Layout="ColumnLayout" Height="30" >
                        <Items>
                            <ext:Container ID="Container26" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth="1" >
                                <Items>
                                    <ext:TextField runat="server" ID="txtCodCancelacion" FieldLabel="Cod.Cancelación" AnchorHorizontal="100%" DataIndex="cod_cancelacion" MaxLength="20" />
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:Container>
                </Items>
            </ext:FormPanel>
        </Items>
        <Buttons>
            <ext:Button ID="btnGuardaAge" runat="server" Text="Guardar" Icon="Disk">
                <DirectEvents>
                    <Click 
                        OnEvent="btnGuardaAge_DirectClick" 
                        Before="return obligatorios('FormaEvento');">
                        <EventMask 
                            ShowMask="true" 
                            MinDelay="1000"                             
                            />
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button runat="server" ID="btnCancelarAge" Text="Cancelar" Icon="Stop" >
                <DirectEvents>
                    <Click OnEvent="btnCancelarAge_DirectClick">
                        <EventMask ShowMask="true" />
                    </Click>
                </DirectEvents>
            </ext:Button>
            <ext:Button runat="server" ID="btnImprimeAge" Text="Imprimir" Icon="Printer" />
            <ext:Button runat="server" ID="btnCerrarAge" Text="Cerrar" Icon="Delete" />
        </Buttons>
        <BottomBar>
                    
        </BottomBar>
    </ext:Window> 
    


    </form>    
</body>
</html>


