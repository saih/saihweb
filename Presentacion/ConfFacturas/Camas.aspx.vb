﻿Imports Negocio
Imports Ext.Net

Public Class Camas
    Inherits System.Web.UI.Page


    Protected Property Accion As String
        Get
            Return ViewState("Accion")
        End Get
        Set(value As String)
            ViewState.Add("Accion", value)
        End Set
    End Property

    Protected Property codigo_area As String
        Get
            Return ViewState("codigo_area")
        End Get
        Set(value As String)
            ViewState.Add("codigo_area", value)
        End Set
    End Property

    Protected Property codigo As String
        Get
            Return ViewState("codigo")
        End Get
        Set(value As String)
            ViewState.Add("codigo", value)
        End Set
    End Property

    Protected Property codpcd As String
        Get
            Return ViewState("codpcd")
        End Get
        Set(value As String)
            ViewState.Add("codpcd", value)
        End Set
    End Property

    Protected Property codser As String
        Get
            Return ViewState("codser")
        End Get
        Set(value As String)
            ViewState.Add("codser", value)
        End Set
    End Property

    Protected Property manual As String
        Get
            Return ViewState("manual")
        End Get
        Set(value As String)
            ViewState.Add("manual", value)
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim seg As New Seguridad(Me, Session("PERFIL"))
            cargarArea()
        End If
    End Sub

    Private Function cargarArea()
        Funciones.cargaStoreDominio(stTipSerCama, Datos.ConstantesUtil.DOMINIO_PABELLONES)
        stTipSerCama.DataBind()
        Return True

    End Function

    Protected Sub btnBuscarTar_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs)
        cargarCama()
    End Sub

    Private Function cargarProcedimientos()
        Dim sWhere As String = " where codigo <> ''"
        Dim negProcedimiento As New Negocio.Negociosaih_pcd
        stProd.DataSource = negProcedimiento.Obtenersaih_pcdbyWhere(sWhere)
        stProd.DataBind()

        Return (True)
    End Function

    Protected Sub rowSelectPro(ByVal sender As Object, ByVal e As DirectEventArgs)
        Dim negProcedimiento As New Negocio.Negociosaih_pcd
        Dim procedimiento As New Datos.saih_pcd
        manual = e.ExtraParams("manual")
        codigo = e.ExtraParams("codigo")
        procedimiento = negProcedimiento.Obtenersaih_pcdById(manual, codigo)
    End Sub

    Private Function cargarCombos()
        Dim spFunc As New Datos.spFunciones
        Dim negSer As New Negocio.Negociosaih_ser
        Dim sWhere As String = " where codser <> ''"
        stCamaServicio.DataSource = negSer.Obtenersaih_serbyWhere(sWhere)
        stCamaServicio.DataBind()

        Funciones.cargaStoreDominio(stManual, Datos.ConstantesUtil.DOMINIO_MANUAL_TARIFARIO)
        stManual.DataBind()

        stProcedimiento.DataSource = spFunc.executaSelect("select a.codigo codigo,a.codigo+' M('+a.manual+') ' + a.nombre nombre from dbsaih.dbo.saih_pcd a")
        stProcedimiento.DataBind()

        Funciones.cargaStoreDominio(stServi, Datos.ConstantesUtil.DOMINIO_PABELLONES)
        stServi.DataBind()

        Return True
    End Function

    Protected Sub rowSelect(ByVal sender As Object, ByVal e As DirectEventArgs)
        codigo = e.ExtraParams("ID")
    End Sub

    Private Sub btnNuevoCama_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnNuevoCama.DirectClick
        Accion = "Insert"
        ' presentacionUtil.limpiarObjetos(vtnCama)
        cargarCama()
        cargarCombos()
        vtnCama.Show()
    End Sub

    Protected Sub btnGuardaCama_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs)
        Dim negCamas As New Negocio.Negociocama
        Dim camaGuardar As New Datos.cama
        Dim negCamasPro As New Negocio.Negociocama_procedimiento
        Dim camaProGuar As New Datos.cama_procedimiento

        If Accion = "Insert" Then
            Reflexion.valoresForma(camaGuardar, vtnCama)
            If negCamas.Altacama(camaGuardar) Then
                camaProGuar.codigo_cama = camaGuardar.codigo
                camaProGuar.codpcd = camaGuardar.codpcd
                camaProGuar.manual = camaGuardar.manual
                negCamasPro.Altacama_procedimiento(camaProGuar)
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                vtnCama.Hide()
                cargarCama()
            End If
        Else
            camaGuardar = negCamas.ObtenercamaById(codigo)
            Reflexion.valoresForma(camaGuardar, vtnCama)
            If negCamas.Editacama(camaGuardar) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                vtnCama.Hide()
                cargarCama()
            End If
        End If
    End Sub

    Private Sub btnCerrarCama_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnCerrarCama.DirectClick
        vtnCama.Hide()
    End Sub

    Private Function cargarCama()
        Dim negCa As New Negocio.Negociocama
        Dim sWhere As String = " where codigo <> '' "

        If txtCodiA.Text <> "" Then
            sWhere &= " and codigo = '" & txtCodiA.Text & "'"

        End If
        stCam.DataSource = negCa.ObtenercamabyWhere(sWhere)
        stCam.DataBind()

        Return True
    End Function

    Private Sub btnEditaCama_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnEditaCama.DirectClick
        Accion = "Edit"
        Dim negCama As New Negocio.Negociocama
        Dim camaEditar As New Datos.cama
        camaEditar = negCama.ObtenercamaById(codigo)
        Reflexion.formaValores(camaEditar, vtnCama)
        vtnCama.Show()
    End Sub

    Private Sub btnEliminaCama_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnEliminaCama.DirectClick
        Dim js As String = "Ext.MessageBox.confirm('Confirmación', '¿Esta seguro de eliminar?', " & _
           "function (btn) { " & _
                " if (btn == 'yes') " & _
                "   Ext.net.DirectMethods.eliminarCama(); " & _
                " });"
        Ext.Net.X.Js.AddScript(js)
    End Sub

    <DirectMethod()>
    Public Function eliminarCama()
        Dim neCamaPro As New Negocio.Negociocama_procedimiento
        Dim negCamaElim As New Negocio.Negociocama
        neCamaPro.Eliminacama_procedimiento(codigo, manual, codpcd)
        negCamaElim.Eliminacama(codigo)
        cargarCama()
        Return True
    End Function

    Private Function stPCam() As Object
        Throw New NotImplementedException
    End Function

End Class