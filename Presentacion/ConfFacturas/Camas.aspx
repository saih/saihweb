﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Camas.aspx.vb" Inherits="Presentacion.Camas" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script src="../public/js/jquery-1.9.0.js" type="text/javascript"></script>
    <script type="text/javascript" src="./../public/js/app.js"></script>


    <script type="text/javascript">
        var enterKeyPressHandler = function (f, e) {

            var val = parseInt(e.getKey());
            console.log(val);
            if ((val >= 65 && val <= 90) || (val === 32) || (val === 16)) {

            } else {
                var objExt = Ext.getCmp(f.id);
                var palabra = f.getValue();
                f.setValue(palabra.substring(0, palabra.length - 1))
            }
        }    </script>
</head>
<body>
    <form id="form1" runat="server">
        <ext:ResourceManager ID="ResourceManager1" runat="server" />
        
        <ext:Store 
            ID="stCam" 
            runat="server" 
            AutoLoad="true">
            <Reader>
                <ext:JsonReader IDProperty="codigo">
                    <Fields>
                        <ext:RecordField Name="codigo" />
                        <ext:RecordField Name="codigo_area" />
                        <ext:RecordField Name="num_admision" />
                        <ext:RecordField Name="manual" />
                        <ext:RecordField Name="codpcd" />
                        <ext:RecordField Name="codser" />
                    </Fields>
                </ext:JsonReader>
        </Reader>
    </ext:Store>
    <ext:Store ID="stProd"
            runat="server" AutoLoad="true">
            <Reader>
                <ext:JsonReader>
                    <Fields>
                        <ext:RecordField Name="manual" />
                        <ext:RecordField Name="codigo" />
                        <ext:RecordField Name="nombre" />
                    </Fields>
                </ext:JsonReader>
            </Reader>
    </ext:Store>

    <ext:Panel ID="PanelCama" runat="server" Frame="true" PaddingSummary="5px 5px 0" AutoWidth="true" Height="650" ButtonAlign="Center" Style="text-align: left" Title="Tablas">
        <Items>
            <ext:FieldSet ID="FieldSetB" runat="server" Title="Campos de Busqueda" Padding="10" Height="60">
                <Items>
                    <ext:Container ID="ContainerCamBus" runat="server" Layout="ColumnLayout" Height="30">
                        <Items> 
                            <ext:Container ID="ContUno" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25">
                                <Items>
                                    <ext:TextField ID="txtCodiA" runat="server" FieldLabel="Código"/>
                                </Items>
                            </ext:Container>
                            <ext:Container ID="Container2" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25">
                                <Items>
                                     <ext:ComboBox     
	                                        ID="cbTipSerC"                                                                              
	                                        runat="server" 
	                                        Shadow="Drop" 
	                                        Mode="Local" 
	                                        TriggerAction="All" 
	                                        ForceSelection="true"
	                                        DisplayField="nombre"
	                                        ValueField="codser"	
	                                        EmptyText="Seleccione Tipo de Servicio"
	                                        AnchorHorizontal="99%"
	                                        AllowBlank="false"
                                            FieldLabel="Area"
                                            DataIndex="codigo_area"
	                                        >
	                                        <Store>
		                                        <ext:Store ID="stTipSerCama" runat="server" AutoLoad="true">
			                                        <Reader>
				                                        <ext:JsonReader IDProperty="codigo_area">
					                                        <Fields>
						                                        <ext:RecordField Name="codigo" />
						                                        <ext:RecordField Name="nombre" />
					                                        </Fields>
				                                        </ext:JsonReader>
			                                        </Reader>            
		                                        </ext:Store>    
	                                        </Store>    
                                        </ext:ComboBox>
                                </Items>
                            </ext:Container>
                             <ext:Container ID="Containerta" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25">
                                <Items>
                                    <ext:Button ID="btnBuscarTar" runat="server" Text="Buscar" Icon="ApplicationGo">
                                         <DirectEvents>
                                                <Click OnEvent="btnBuscarTar_DirectClick" Before="">
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                         </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:Container> 
                </Items>
            </ext:FieldSet >                        
            <ext:GridPanel ID="grdCam"
                 runat="server"                                            
                    Margins="0 0 5 5"
                    Icon="TransmitGo"
                    Frame="true"
                    Height="400"
                    StoreId="stCam"
                    Title="Tarifas">
                    <ColumnModel ID="ColumnModelCama" runat="server">
                        <Columns>                        
                            <ext:Column Header="Manual" DataIndex="codigo" />
                            <ext:Column Header="Manual" DataIndex="codigo_area" />
                            <ext:Column Header="Manual" DataIndex="num_admision" />
                            <ext:Column Header="Manual" DataIndex="manual" />
                            <ext:Column Header="Codigo" DataIndex="codpcd" />
                            <ext:Column Header="Codigo" DataIndex="codser" />
                        </Columns>                                
                    </ColumnModel>
                      <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModelCama" runat="server" SingleSelect="true" >
                            <DirectEvents>
                                <RowSelect OnEvent="rowSelect" Buffer="100">
                                    <ExtraParams>                                    
                                        <ext:Parameter Name="ID" Value="this.getSelected().id" Mode="Raw" />
                                    </ExtraParams>
                                </RowSelect>
                            </DirectEvents>
                        </ext:RowSelectionModel>
                    </SelectionModel>
                     <BottomBar>
                        <ext:StatusBar runat="server" ID="StatusBarCa">
                            <Items>                                                        
                                <ext:Button runat="server" Text="Nueva Cama" ID="btnNuevoCama" Icon="Add" />
                                <ext:Button runat="server" Text="Editar Cama" ID="btnEditaCama" Icon="ApplicationEdit" />
                                <ext:Button runat="server" Text="Eliminar Cama" ID="btnEliminaCama" Icon="ApplicationDelete"/>
                            </Items>
                        </ext:StatusBar>
                    </BottomBar>
            </ext:GridPanel>
         </Items>
    </ext:Panel >
    <ext:Window ID="vtnCama" 
            runat="server" 
            Icon="ApplicationFormEdit"
            Width="850"
            Height="510"
            Hidden="true" 
            Modal="true"            
            Title="Registro Cama"
            Constrain="true">
            <Items> 
                <ext:Panel ID="PanelCam" 
                        runat="server" 
                        Title=""
                        AnchorHorizontal="100%"
                        Height="480"
                        Layout="Fit">
                        <Items>
                            <ext:FormPanel ID="StatusFormCam" 
                                runat="server"
                                LabelWidth="75"
                                ButtonAlign="Right"
                                Border="false"
                                PaddingSummary="15px 15px 15px">
                                     <Defaults>
                                         <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                         <ext:Parameter Name="SelectOnFocus" Value="true" Mode="Raw" />
                                         <ext:Parameter Name="MsgTarget" Value="side" />
                                      </Defaults> 
                                      <Items>
                                        <ext:FieldSet ID="FieldCama" runat="server" Title="Cama" Padding="10" Height="150">
                                             <Items>
                                                <ext:Container ID="ConCo" runat="server" Layout="ColumnLayout" Height="30">   
                                                    <Items>
                                                     <ext:Container ID="ConCod" runat="server"  Layout="ColumnLayout" Height=".25">
                                                            <Items>
                                                                <ext:TextField id="txtCodH" runat="server" FieldLabel="Codigo" AnchorHorizontal="90%" AllowBlank="false" MinLength="2" MaxLength="4" DataIndex="codigo"  EnableKeyEvents="true">
                                                                    <Listeners>
                                                                       <KeyUp Fn="enterKeyPressHandler" />                                                                
                                                                    </Listeners>
                                                                </ext:TextField >
                                                            </Items>
                                                        </ext:Container>
                                                        <ext:Container ID="ContUnoArea" runat="server"  Layout="ColumnLayout" Height=".25">
                                                            <Items>
                                                             <ext:ComboBox     
	                                                                        ID="CB"                                                                              
	                                                                        runat="server" 
	                                                                        Shadow="Drop" 
	                                                                        Mode="Local" 
	                                                                        TriggerAction="All" 
	                                                                        ForceSelection="true"
	                                                                        DisplayField="nombre"
	                                                                        ValueField="codigo"	
	                                                                        EmptyText="Seleccione Tipo de Servicio"
	                                                                        AnchorHorizontal="99%"
	                                                                        AllowBlank="false"
                                                                            FieldLabel="Area"
                                                                            DataIndex="codigo_area"
	                                                                        >
	                                                                        <Store>
		                                                                        <ext:Store ID="stServi" runat="server" AutoLoad="true">
			                                                                        <Reader>
				                                                                        <ext:JsonReader IDProperty="codigo_area">
					                                                                        <Fields>
						                                                                        <ext:RecordField Name="codigo" />
						                                                                        <ext:RecordField Name="nombre" />
					                                                                        </Fields>
				                                                                        </ext:JsonReader>
			                                                                        </Reader>            
		                                                                        </ext:Store>    
	                                                                        </Store>    
                                                                       </ext:ComboBox>
                                                            </Items>
                                                        </ext:Container>
                                                    </Items>
                                                 </ext:Container> 
                                                <ext:Container ID="contarinaer" runat="server"  Layout="ColumnLayout" Height="30">
                                                    <Items>
                                                        <ext:Container ID="CotariNumAd" runat="server" Layout="ColumnLayout" Height=".25">
                                                            <Items>
                                                                <ext:TextField ID="txtNumAdm" runat="server" FieldLabel="Num. Adminsion" AnchorHorizontal="90%" AllowBlank="false" MinLength="2" MaxLength="4" DataIndex="num_admision"  EnableKeyEvents="true">
                                                                     <Listeners>
                                                                       <KeyUp Fn="enterKeyPressHandler" />                                                                
                                                                    </Listeners>
                                                                </ext:TextField>
                                                            </Items>
                                                        </ext:Container>
                                                       <ext:Container ID="ContaiManu" runat="server" Layout="ColumnLayout" Height=".25">
                                                          <Items>
                                                            <ext:ComboBox ID="cbManual"                                                                              
	                                                                runat= "server"
	                                                                Shadow="Drop" 
	                                                                Mode="Local" 
	                                                                TriggerAction="All" 
	                                                                ForceSelection="true"
	                                                                DisplayField="nombre"
	                                                                ValueField="codigo"	
	                                                                EmptyText="Seleccione Tipo de Manual"
	                                                                AnchorHorizontal="99%"
	                                                                AllowBlank="false"
                                                                    FieldLabel="Manual"
                                                                    DataIndex="manual">
                                                                    <Store>
		                                                                <ext:Store ID="stManual" runat="server" AutoLoad="true">
			                                                                <Reader>
				                                                                <ext:JsonReader IDProperty="codigo">
					                                                                <Fields>
						                                                                <ext:RecordField Name="codigo" />
						                                                                <ext:RecordField Name="nombre" />
					                                                                </Fields>
				                                                                </ext:JsonReader>
			                                                                </Reader>            
		                                                                </ext:Store>    
	                                                                </Store>    
                                                            </ext:ComboBox>
                                                          </Items>
                                                        </ext:Container>
                                                    </Items>
                                                </ext:Container>
                                                <ext:Container ID="Container3" runat="server" LabelAlign="Right" Layout="ColumnLayout" Height="30">
                                                    <Items>
                                                        <ext:Container ID="Container1" runat="server" LabelAlign="Right" Layout="ColumnLayout" Height=".25">
                                                            <Items>
                                                                <ext:ComboBox ID="cbProcedimiento"                                                                              
	                                                            runat="server" 
	                                                            Shadow="Drop" 
	                                                            Mode="Local" 
	                                                            TriggerAction="All" 
	                                                            ForceSelection="true"
	                                                            DisplayField="nombre"
	                                                            ValueField="codigo"	
	                                                            EmptyText="Seleccione Tipo de Procedimiento"
	                                                            AnchorHorizontal="99%"
	                                                            AllowBlank="false"
                                                                FieldLabel="Procedimiento"
                                                                DataIndex="codpcd">
                                                                    <Store>
		                                                                <ext:Store ID="stProcedimiento" runat="server" AutoLoad="true">
			                                                                <Reader>
				                                                                <ext:JsonReader IDProperty="codigo">
					                                                                <Fields>
						                                                                <ext:RecordField Name="codigo" />
						                                                                <ext:RecordField Name="nombre" />
					                                                                </Fields>
				                                                                </ext:JsonReader>
			                                                                </Reader>            
		                                                                </ext:Store>    
	                                                                </Store>    
                                                            </ext:ComboBox>
                                                          </Items>
                                                        </ext:Container> 
                                                        <ext:Container ID="Cont6" runat="server" LabelAlign="Right" Layout="ColumnLayout" Height=".25">
                                                            <Items>
                                                                <ext:ComboBox ID="cbServi"
                                                                        runat="server"
                                                                        Shadow="Drop" 
	                                                                    Mode="Local" 
	                                                                    TriggerAction="All" 
	                                                                    ForceSelection="true"
	                                                                    DisplayField="nombre"
	                                                                    ValueField="codser"	
	                                                                    EmptyText="Seleccione Tipo de Servicio"
	                                                                    AnchorHorizontal="99%"
	                                                                    AllowBlank="false"
                                                                        FieldLabel="Servicio"
                                                                        DataIndex="codser" >
                                                                        <Store>
		                                                                <ext:Store ID="stCamaServicio" runat="server" AutoLoad="true">
			                                                                 <Reader>
				                                                                    <ext:JsonReader IDProperty="codser">
					                                                                    <Fields>
						                                                                    <ext:RecordField Name="codser" />
						                                                                    <ext:RecordField Name="nombre" />
					                                                                    </Fields>
				                                                                    </ext:JsonReader>
			                                                                    </Reader>            
		                                                                    </ext:Store>  
	                                                                     </Store>    
                                                                </ext:ComboBox>
                                                            </Items>
                                                        </ext:Container>
                                                    </Items>
                                                </ext:Container> 
                                             </Items>  
                                        </ext:FieldSet> 
                                     </Items>
                                     <Buttons>
                                                <ext:Button ID="btnGuardaCama" runat="server" Text="Guardar" Icon="Disk">
                                                    <DirectEvents>
                                                        <Click 
                                                            OnEvent="btnGuardaCama_DirectClick" 
                                                            Before="var valid= #{StatusFormCam}.getForm().isValid();">
                                                            <EventMask 
                                                                ShowMask="true" 
                                                                MinDelay="1000" 
                                                                Target="CustomTarget" 
                                                                CustomTarget="={#{StatusFormCam}.getEl()}" 
                                                                />
                                                        </Click>
                                                    </DirectEvents>
                                                </ext:Button>
                                                <ext:Button runat="server" ID="btnCerrarCama" Text="Cerrar" />
                                            </Buttons>
                            </ext:FormPanel>
                        </Items>
                </ext:Panel>
            </Items>
    </ext:Window>
    </form>
</body>
</html>
