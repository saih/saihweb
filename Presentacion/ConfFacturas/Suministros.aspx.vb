﻿Imports Ext.Net

Public Class Suministros
    Inherits System.Web.UI.Page

#Region "Variables ViewState"

    Protected Property idTar As String
        Get
            Return ViewState("idTar")
        End Get
        Set(value As String)
            ViewState.Add("idTar", value)
        End Set
    End Property

    Protected Property codigo As String
        Get
            Return ViewState("codigo")
        End Get
        Set(value As String)
            ViewState.Add("codigo", value)
        End Set
    End Property

    Protected Property Accion As String
        Get
            Return ViewState("Accion")
        End Get
        Set(value As String)
            ViewState.Add("Accion", value)
        End Set
    End Property

    Protected Property AccionTar As String
        Get
            Return ViewState("AccionTar")
        End Get
        Set(value As String)
            ViewState.Add("AccionTar", value)
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim seg As New Seguridad(Me, Session("PERFIL"))
        End If
    End Sub

    Protected Sub btnBuscar_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        cargarSuministros()
    End Sub

#Region "Suministros"
    Protected Sub rowSelect(ByVal sender As Object, ByVal e As DirectEventArgs)
        codigo = e.ExtraParams("codigo")
    End Sub

    Protected Sub btnGuardaSum_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs)
        Dim negSuministros As New Negocio.Negociosuministro
        Dim suministro As New Datos.suministro
        If Accion = "Insert" Then
            Reflexion.valoresForma(suministro, vntSuministros)
            If negSuministros.Altasuministro(suministro) Then
                Ext.Net.X.Msg.Alert("Información", "Registros Guardado Correctamente").Show()
                cargarSuministros()
            End If
        Else
            Reflexion.valoresForma(suministro, vntSuministros)
            If negSuministros.Editasuministro(suministro) Then
                Ext.Net.X.Msg.Alert("Información", "Registros Guardado Correctamente").Show()
                cargarSuministros()
            End If
        End If
    End Sub

    Private Sub btnNuevoSuministro_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnNuevoSuministro.DirectClick
        Accion = "Insert"
        codigo = ""
        cargarCombosSuministros()
        cargaCombosTarifas()
        cargarTarifa()
        presentacionUtil.limpiarObjetos(vntSuministros)
        vntSuministros.Show()
    End Sub

    Private Sub btnEditaSuministro_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEditaSuministro.DirectClick
        Accion = "Edit"
        Dim negSuministros As New Negocio.Negociosuministro
        Dim suministro As New Datos.suministro
        cargarCombosSuministros()
        cargaCombosTarifas()
        cargarTarifa()
        presentacionUtil.limpiarObjetos(vntSuministros)
        suministro = negSuministros.ObtenersuministroById(codigo)
        Reflexion.formaValores(suministro, vntSuministros)
        vntSuministros.Show()
    End Sub

    <DirectMethod()>
    Public Function eliminarSum()
        Dim negSuministros As New Negocio.Negociosuministro
        negSuministros.Eliminasuministro(codigo)
        cargarSuministros()
        Return True
    End Function

    Private Sub btnEliminaSuministro_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnEliminaSuministro.DirectClick
        Dim js As String = "Ext.MessageBox.confirm('Confirmación', '¿Esta seguro de eliminar?', " & _
           "function (btn) { " & _
                " if (btn == 'yes') " & _
                "   Ext.net.DirectMethods.eliminarSum(); " & _
                " });"
        Ext.Net.X.Js.AddScript(js)
    End Sub

    Private Function cargarCombosSuministros()
        Dim negUnf As New Negocio.Negociosaih_unf
        stUnidadFuncional.DataSource = negUnf.Obtenersaih_unf
        stUnidadFuncional.DataBind()
        Funciones.cargaStoreDominioCodNom(stCod_centrocosto, Datos.ConstantesUtil.DOMINIO_CENTRO_COSTO)
        Funciones.cargaStoreDominioCodNom(stConcentracion, Datos.ConstantesUtil.DOMINIO_CONCENTRACION)
        Funciones.cargaStoreDominioCodNom(stUnd_concentracion, Datos.ConstantesUtil.DOMINIO_UNIDAD_CONCENTRACION)
        Funciones.cargaStoreDominioCodNom(stClase, Datos.ConstantesUtil.DOMINIO_CLASE_SUMINISTRO)
        Return True
    End Function

    Private Function cargarSuministros()

        Dim negSuministros As New Negocio.Negociosuministro
        Dim sWhere As String = " where codigo <> '' "

        If txtNombreBsq.Text <> "" Then
            sWhere &= " and nombre like '%" & txtNombreBsq.Text & "%'"
        End If

        If txtCodigoBsq.Text <> "" Then
            sWhere &= " and codigo = '" & txtCodigoBsq.Text & "'"
        End If

        stSuministros.DataSource = negSuministros.ObtenersuministrobyWhere(sWhere)
        stSuministros.DataBind()

        Return True
    End Function

    Private Sub btnCerrarSum_DirectClick(sender As Object, e As Ext.Net.DirectEventArgs) Handles btnCerrarSum.DirectClick
        vntSuministros.Hide()
    End Sub

#End Region

#Region "Tarifas"
    Private Function cargaCombosTarifas()
        Funciones.cargaStoreDominioCodNom(stManual, Datos.ConstantesUtil.DOMINIO_MANUAL_TARIFARIO)
        Funciones.cargaStoreDominioCodNom(stPlan, Datos.ConstantesUtil.DOMINIO_PLANES)
        Return True
    End Function


    <DirectMethod()>
    Public Function cancelarTarifa()
        cargarTarifa()
        Return True
    End Function

    <DirectMethod()>
    Public Function eliminarTarifa(manual As String, plan_tar As String)
        Dim js As String = "Ext.MessageBox.confirm('Confirmación', '¿Esta seguro de eliminar?', " & _
           "function (btn) { " & _
                " if (btn == 'yes') " & _
                "   Ext.net.DirectMethods.eliminaTarifa('" & manual & "','" & plan_tar & "'); " & _
                " });"
        Ext.Net.X.Js.AddScript(js)

        Return True
    End Function

    <DirectMethod()>
    Public Function eliminaTarifa(manual As String, plan_tar As String)
        Dim negTarifa As New Negocio.Negociosuministro_tar
        negTarifa.Eliminasuministro_tar(codigo, manual, plan_tar)
        cargarTarifa()
        Return True
    End Function

    <DirectMethod()>
    Public Function editarTarifa(manual As String, plan_tar As String, valor As String, es_nueva As String)
        Dim Tarifa As New Datos.suministro_tar
        Dim negTarifa As New Negocio.Negociosuministro_tar
        Tarifa.cod_suministro = codigo
        Tarifa.manual = manual
        Tarifa.valor = valor
        Tarifa.plan_tar = plan_tar

        If es_nueva = "1" Then
            If negTarifa.Altasuministro_tar(Tarifa) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                cargarTarifa()
            End If
        Else
            If negTarifa.Editasuministro_tar(Tarifa) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                cargarTarifa()
            End If
        End If

        Return True
    End Function

    Private Function cargarTarifa()
        Dim negTarifa As New Negocio.Negociosuministro_tar
        stTarifa.DataSource = negTarifa.Obtenersuministro_tarbyWhere(" where cod_suministro = '" & codigo & "'")
        stTarifa.DataBind()
        Return True
    End Function


#End Region

    
End Class