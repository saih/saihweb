﻿Imports Ext.Net

Public Class Cajeros
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim seg As New Seguridad(Me, Session("PERFIL"))

        End If
    End Sub

    Protected Property Accion As String
        Get
            Return ViewState("Accion")
        End Get
        Set(ByVal value As String)
            ViewState.Add("Accion", value)
        End Set
    End Property

    Protected Property codigo_cajero As String
        Get
            Return ViewState("codigo_cajero")
        End Get
        Set(ByVal value As String)
            ViewState.Add("codigo_cajero", value)
        End Set
    End Property

    Protected Property usuario As String
        Get
            Return ViewState("usuario")
        End Get
        Set(ByVal value As String)
            ViewState.Add("usuario", value)
        End Set
    End Property

    Protected Sub btnBuscarCajero_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs)
        cargaCajero()
    End Sub

    Private Function cargaCajero()
        Dim negHis As New Negocio.Negociocajero
        Dim sWhere As String = " where codigo_cajero <> '' "

        If txtCajero.Text <> "" Then
            sWhere &= " and codigo_cajero = '" & txtCajero.Text & "'"

        End If

        If txtUsuario.Text <> "" Then
            sWhere &= " and usuario = '" & txtUsuario.Text & "'"

        End If

        stCaj.DataSource = negHis.ObtenercajerobyWhere(sWhere)
        stCaj.DataBind()

        Return True
    End Function

    Protected Sub rowSelect(ByVal sender As Object, ByVal e As DirectEventArgs)
        codigo_cajero = e.ExtraParams("ID")
    End Sub

    Private Sub btnNuevoCajero_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnNuevoCajero.DirectClick
        Accion = "Insert"
        presentacionUtil.limpiarObjetos(vntCaje)
        vntCaje.Show()
    End Sub

    Protected Sub btnGuardaCajero_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs)
        Dim negCaj As New Negocio.Negociocajero
        Dim cajeroGuardar As New Datos.cajero

        If Accion = "Insert" Then
            Reflexion.valoresForma(cajeroGuardar, vntCaje)
                If negCaj.Altacajero(cajeroGuardar) Then
                    Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                vntCaje.Hide()
                    cargaCajero()
                End If
        Else
            cajeroGuardar = negCaj.ObtenercajeroById(codigo_cajero)
            Reflexion.valoresForma(cajeroGuardar, vntCaje)
            If negCaj.Editacajero(cajeroGuardar) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                vntCaje.Hide()
                cargaCajero()
            End If

        End If

    End Sub

    Private Sub btnCerrarCajero_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnCerrarCajero.DirectClick
        vntCaje.Hide()
    End Sub

    Private Sub btnEditaCajero_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnEditaCajero.DirectClick
        Accion = "Edit"
        Dim negCajero As New Negocio.Negociocajero
        Dim cajeroEditar As New Datos.cajero
        cajeroEditar = negCajero.ObtenercajeroById(codigo_cajero)
        Reflexion.formaValores(cajeroEditar, vntCaje)
        vntCaje.Show()
    End Sub

    Private Sub btnEliminaCajero_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnEliminaCajero.DirectClick
        Dim js As String = "Ext.MessageBox.confirm('Confirmación', '¿Esta seguro de eliminar?', " & _
           "function (btn) { " & _
                " if (btn == 'yes') " & _
                "   Ext.net.DirectMethods.eliminarCajero(); " & _
                " });"
        Ext.Net.X.Js.AddScript(js)
    End Sub

    <DirectMethod()>
    Public Function eliminarCajero()
        Dim negCajeroElim As New Negocio.Negociocajero
        negCajeroElim.Eliminacajero(codigo_cajero)
        cargaCajero()
        Return True
    End Function

End Class