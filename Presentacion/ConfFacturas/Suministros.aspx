﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Suministros.aspx.vb" Inherits="Presentacion.Suministros" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <ext:XScript runat="server" ID="XScript1">
        <script type="text/javascript">
            var addTarifa = function () {
                var grid = #{grdTarifa};
                grid.getRowEditor().stopEditing();
                
                grid.insertRecord(0, {
                    manual  : "",
                    valor : "",
                    plan_tar   : "",                    
                    es_nueva : "1"
                });
                
                grid.getView().refresh();
                grid.getSelectionModel().selectRow(0);
                grid.getRowEditor().startEditing(0);
            }

            var fmtPlan = function(val, params, record){		        
                val = record.get('plan_tar')
		        var opts = stPlan.data.items;
		        for(var i = 0, len = opts.length;i < len; i++){                    
			        if (opts[i].data.codigo === record.get('plan_tar')){				        
				        return opts[i].data.nombre;
			        }
		        }
		        return val;
	        };

            var fmtManual = function(val, params, record){		        
                val = record.get('manual')
		        var opts = stManual.data.items;
		        for(var i = 0, len = opts.length;i < len; i++){                    
			        if (opts[i].data.codigo === record.get('manual')){				        
				        return opts[i].data.nombre;
			        }
		        }
		        return val;
	        };
            

            var cancelTarifa = function(){
                Ext.net.DirectMethods.cancelarTarifa();
            }
            
            var removeTarifa = function () {
                
                var grid = #{grdTarifa};
                grid.getRowEditor().stopEditing();
                
                var s = grid.getSelectionModel().getSelections();
                
                for (var i = 0, r; r = s[i]; i++) {                    
                    Ext.net.DirectMethods.eliminarTarifa(r.data.manual,r.data.plan_tar);
                }
            }

            var editaTarifa = function(val, params, record){
                Ext.net.DirectMethods.editarTarifa(record.get('manual'),record.get('plan_tar'),record.get('valor'),record.get('es_nueva'));
            }
        </script>
    </ext:XScript>
</head>
<body>
    <form id="form1" runat="server">
       <ext:ResourceManager ID="ResourceManager1" runat="server" />

       <ext:Store 
            ID="stSuministros" 
            runat="server" 
            AutoLoad="true">
            <Reader>
                <ext:JsonReader>
                    <Fields>                        
                        <ext:RecordField Name="codigo" />                        
                        <ext:RecordField Name="nombre" />                        
                    </Fields>
                </ext:JsonReader>
            </Reader>
        </ext:Store>

        <ext:Panel ID="Panel3" runat="server" Frame="true" PaddingSummary="5px 5px 0" AutoWidth="true" Height="650" ButtonAlign="Center" Style="text-align: left" Title="Suministros">
            <Items>
                <ext:FieldSet ID="FieldSet3" runat="server" Title="Campos de Busqueda" Padding="10" Height="60">
                    <Items>
                        <ext:Container ID="Container10" runat="server" Layout="ColumnLayout" Height="30">
                            <Items>
                                <ext:Container ID="Container8" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25">
                                    <Items>
                                        <ext:TextField runat="server" ID="txtCodigoBsq" FieldLabel="Codigo" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container1" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3">
                                    <Items>
                                        <ext:TextField runat="server" ID="txtNombreBsq" FieldLabel="Nombre" AnchorHorizontal="99%" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container2" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".2">
                                    <Items>
                                        <ext:Button runat="server" ID="btnBuscar" Text="Buscar" Icon="ApplicationGo" >
                                        <DirectEvents>
                                            <Click OnEvent="btnBuscar_DirectClick">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                    </Items>
                </ext:FieldSet>
                <ext:GridPanel
                    ID="grdSuministros"
                    runat="server"                                            
                    Margins="0 0 5 5"
                    Icon="VectorAdd"
                    Frame="true"
                    Height="400"
                    StoreId="stSuministros"
                    Title="Suministros"
                    >
                    <ColumnModel ID="ColumnModel3" runat="server">
                        <Columns>                                                        
                            <ext:Column Header="Codigo" DataIndex="codigo" />                            
                            <ext:Column Header="Nombre" DataIndex="nombre" Width="380"/>
                        </Columns>                                
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" SingleSelect="true" >
                            <DirectEvents>
                                <RowSelect OnEvent="rowSelect" Buffer="100">
                                    <ExtraParams>
                                        <ext:Parameter Name="codigo" Value="this.getSelected().get('codigo')" Mode="Raw" />
                                    </ExtraParams>
                                </RowSelect>
                            </DirectEvents>
                        </ext:RowSelectionModel>
                    </SelectionModel>
                    <LoadMask ShowMask="true" Msg="Loading...." />
                    <BottomBar>
                        <ext:StatusBar runat="server" ID="StatusBar2">
                            <Items>                                                        
                                <ext:Button runat="server" Text="Nueva Suministro" ID="btnNuevoSuministro" Icon="Add" />
                                <ext:Button runat="server" Text="Editar Suministro" ID="btnEditaSuministro" Icon="ApplicationEdit" />
                                <ext:Button runat="server" Text="Eliminar Suministro" ID="btnEliminaSuministro" Icon="ApplicationDelete" />
                            </Items>
                        </ext:StatusBar>
                    </BottomBar>
                </ext:GridPanel>
            </Items>
        </ext:Panel>


        <ext:Store ID="stManual" runat="server" AutoLoad="true">
			<Reader>
				<ext:JsonReader IDProperty="codigo">
					<Fields>
						<ext:RecordField Name="codigo" />
						<ext:RecordField Name="nombre" />
					</Fields>
				</ext:JsonReader>
			</Reader>            
		</ext:Store>


        <ext:Store ID="stPlan" runat="server" AutoLoad="true">
			<Reader>
				<ext:JsonReader IDProperty="codigo">
					<Fields>
						<ext:RecordField Name="codigo" />
						<ext:RecordField Name="nombre" />
					</Fields>
				</ext:JsonReader>
			</Reader>            
		</ext:Store>

        <ext:Store ID="stTarifa" runat="server" AutoLoad="true">
			<Reader>
				<ext:JsonReader IDProperty="codigo">
					<Fields>
						<ext:RecordField Name="manual" />
						<ext:RecordField Name="plan_tar" />
                        <ext:RecordField Name="valor" />
                        <ext:RecordField Name="es_nueva" />
					</Fields>
				</ext:JsonReader>
			</Reader>            
		</ext:Store>

        <ext:Window 
        ID="vntSuministros"
        runat="server"
        Icon="ApplicationFormEdit"
        Width="900"
        Height="600"
        Hidden="true" 
        Modal="true"            
        Title="Medicamentos y Suministros"
        Constrain="true">
        <Items>
            <ext:Panel ID="Panel1" 
            runat="server" 
            Title=""
            AnchorHorizontal="100%"
            Height="570"
            >
            <Items>
                <ext:FormPanel 
                    ID="StatusForm" 
                    runat="server"
                    LabelWidth="75"
                    ButtonAlign="Right"
                    Border="false"
                    PaddingSummary="10px 10px 10px">
                    <Defaults>                        
                        <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                        <ext:Parameter Name="SelectOnFocus" Value="true" Mode="Raw" />
                        <ext:Parameter Name="MsgTarget" Value="side" />
                    </Defaults>
                    <Items>
                        <ext:Container ID="Container3" runat="server" Layout="ColumnLayout" Height="30" >
                            <Items>
                                <ext:Container ID="Container4" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:Label runat="server" ID="lblProcedimiento" Text="Medicamentos y Suministros" />
                                    </Items>
                                </ext:Container>                                
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container6" runat="server" Layout="ColumnLayout" Height="30" >
                            <Items>
                                <ext:Container ID="Container7" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:TextField runat="server" id="txtCodigo" FieldLabel="Código" AnchorHorizontal="95%" AllowBlank="false" MinLength="4" MaxLength="10" DataIndex="codigo" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container5" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:TextField runat="server" id="txtCodAna" FieldLabel="Cod. Anarfamologico" AnchorHorizontal="60%" AllowBlank="true" DataIndex="codana" />
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>                      
                        <ext:Container ID="Container9" runat="server" Layout="ColumnLayout" Height="30" >
                            <Items>
                                <ext:Container ID="Container11" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".99" >
                                    <Items>
                                        <ext:TextField runat="server" id="txtNombre" FieldLabel="Nombre" AnchorHorizontal="95%" AllowBlank="false" MinLength="6" MaxLength="80" DataIndex="nombre" />
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container12" runat="server" Layout="ColumnLayout" Height="30" >
                            <Items>
                                <ext:Container ID="Container13" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".4" >
                                    <Items>
                                        <ext:TextField runat="server" id="txtCod_generico" FieldLabel="Código Generico" AnchorHorizontal="95%" AllowBlank="false" MinLength="4" MaxLength="15" DataIndex="cod_generico" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container14" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".6" >
                                    <Items>
                                        <ext:TextField runat="server" id="txtNom_generico" FieldLabel="Nombre Generico" AnchorHorizontal="95%" AllowBlank="false" MinLength="6" MaxLength="100" DataIndex="nom_generico" />
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container15" runat="server" Layout="ColumnLayout" Height="30" >
                            <Items>
                                <ext:Container ID="Container16" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3" >
                                    <Items>
                                        <ext:TextField runat="server" id="txtPresentacion" FieldLabel="Presentación" AnchorHorizontal="99%" AllowBlank="true" DataIndex="presentacion" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container17" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".4" >
                                    <Items>
                                        <ext:ComboBox     
                                            ID="cbUnd_concentracion"                                                                                   
                                            runat="server" 
                                            Shadow="Drop"
                                            Mode="Local" 
                                            TriggerAction="All" 
                                            ForceSelection="true"
                                            DisplayField="nombre"
                                            ValueField="codigo"
                                            FieldLabel="Und. Concentración"
                                            EmptyText="Seleccione Und de Concentración..."
                                            DataIndex="und_concentracion"
                                            AnchorHorizontal="99%"
                                            AllowBlank="false"
                                            >
                                            <Store>
                                                <ext:Store ID="stUnd_concentracion" runat="server" AutoLoad="true">
                                                    <Reader>
                                                        <ext:JsonReader IDProperty="codigo">
                                                            <Fields>
                                                                <ext:RecordField Name="codigo" />
                                                                <ext:RecordField Name="nombre" />
                                                            </Fields>
                                                        </ext:JsonReader>
                                                    </Reader>            
                                                </ext:Store>    
                                            </Store>    
                                        </ext:ComboBox>
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container18" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3" >
                                    <Items>
                                        <ext:ComboBox     
                                            ID="cbConcentracion"                                                                                   
                                            runat="server" 
                                            Shadow="Drop" 
                                            Mode="Local" 
                                            TriggerAction="All" 
                                            ForceSelection="true"
                                            DisplayField="nombre"
                                            ValueField="codigo"
                                            FieldLabel="Concentración"
                                            EmptyText="Seleccione Concentración..."
                                            DataIndex="concentracion"
                                            AnchorHorizontal="99%"
                                            AllowBlank="false"
                                            >
                                            <Store>
                                                <ext:Store ID="stConcentracion" runat="server" AutoLoad="true">
                                                    <Reader>
                                                        <ext:JsonReader IDProperty="codigo">
                                                            <Fields>
                                                                <ext:RecordField Name="codigo" />
                                                                <ext:RecordField Name="nombre" />
                                                            </Fields>
                                                        </ext:JsonReader>
                                                    </Reader>            
                                                </ext:Store>    
                                            </Store>    
                                        </ext:ComboBox>
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container19" runat="server" Layout="ColumnLayout" Height="30" >
                            <Items>
                                <ext:Container ID="Container20" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".99" >
                                    <Items>
                                        <ext:TextField runat="server" id="txtPoso" FieldLabel="Posología" AnchorHorizontal="95%" AllowBlank="true" MaxLength="200" DataIndex="poso" />
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container21" runat="server" Layout="ColumnLayout" Height="30" >
                            <Items>
                                <ext:Container ID="Container22" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:ComboBox     
                                            ID="cbCod_centrocosto"                                                                                   
                                            runat="server" 
                                            Shadow="Drop" 
                                            Mode="Local" 
                                            TriggerAction="All" 
                                            ForceSelection="true"
                                            DisplayField="nombre"
                                            ValueField="codigo"
                                            FieldLabel="Centro de Costo"
                                            DataIndex="cod_centrocosto"
                                            EmptyText="Seleccione Centro de Costo..."
                                            AnchorHorizontal="99%"
                                            AllowBlank="false"
                                            >
                                            <Store>
                                                <ext:Store ID="stCod_centrocosto" runat="server" AutoLoad="true">
                                                    <Reader>
                                                        <ext:JsonReader IDProperty="codigo">
                                                            <Fields>
                                                                <ext:RecordField Name="codigo" />
                                                                <ext:RecordField Name="nombre" />
                                                            </Fields>
                                                        </ext:JsonReader>
                                                    </Reader>            
                                                </ext:Store>    
                                            </Store>    
                                        </ext:ComboBox>
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container25" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                    <Items>
                                        <ext:ComboBox     
                                            ID="cbClase"                                                                                   
                                            runat="server" 
                                            Shadow="Drop" 
                                            Mode="Local" 
                                            TriggerAction="All" 
                                            ForceSelection="true"
                                            DisplayField="nombre"
                                            ValueField="codigo"
                                            FieldLabel="Clase"
                                            DataIndex="clase"
                                            EmptyText="Seleccione Clase..."
                                            AnchorHorizontal="99%"
                                            AllowBlank="false"
                                            >
                                            <Store>
                                                <ext:Store ID="stClase" runat="server" AutoLoad="true">
                                                    <Reader>
                                                        <ext:JsonReader IDProperty="codigo">
                                                            <Fields>
                                                                <ext:RecordField Name="codigo" />
                                                                <ext:RecordField Name="nombre" />
                                                            </Fields>
                                                        </ext:JsonReader>
                                                    </Reader>            
                                                </ext:Store>    
                                            </Store>    
                                        </ext:ComboBox>
                                    </Items>
                                </ext:Container>                                
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container28" runat="server" Layout="ColumnLayout" Height="30" >
                            <Items>                     
                                <ext:Container ID="Container29" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".7" >
                                    <Items>                                        
                                        <ext:ComboBox     
	                                        ID="cbUnidadFuncional"                                                                              
	                                        runat="server" 
	                                        Shadow="Drop" 
	                                        Mode="Local" 
	                                        TriggerAction="All" 
	                                        ForceSelection="true"
	                                        DisplayField="nombre"
	                                        ValueField="codunf"	
	                                        EmptyText="Seleccione Unidad Funcional"
	                                        AnchorHorizontal="99%"
	                                        AllowBlank="false"    
                                            DataIndex="cod_undfunc"
                                            FieldLabel="Und.Funcional"
	                                        >
	                                        <Store>
		                                        <ext:Store ID="stUnidadFuncional" runat="server" AutoLoad="true">
			                                        <Reader>
				                                        <ext:JsonReader IDProperty="codunf">
					                                        <Fields>
						                                        <ext:RecordField Name="codunf" />
						                                        <ext:RecordField Name="nombre" />
					                                        </Fields>
				                                        </ext:JsonReader>
			                                        </Reader>            
		                                        </ext:Store>    
	                                        </Store>    
                                        </ext:ComboBox>
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                        <ext:Container ID="Container24" runat="server" Layout="ColumnLayout" Height="30" >
                            <Items>                     
                                <ext:Container ID="Container23" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".2" >
                                    <Items>
                                        <ext:TextField runat="server" id="txtCod_material" FieldLabel="Cod.Inventario" AnchorHorizontal="99%" AllowBlank="true" MaxLength="10" DataIndex="cod_material" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container34" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3" >
                                    <Items>
                                        <ext:ComboBox ID="cbTipo_imp" runat="server"  FieldLabel="Tipo Impuesto" AllowBlank="false" AnchorHorizontal="95%" DataIndex="tipo_imp">
                                            <Items>
                                                <ext:ListItem Text="Gravado" Value="g" />
                                                <ext:ListItem Text="No Gravado" Value="n" />
                                                <ext:ListItem Text="Excento" Value="e" />
                                            </Items>
                                        </ext:ComboBox>
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container26" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25" >
                                    <Items>
                                        <ext:NumberField ID="txtValor" FieldLabel="Vlr.Actual" runat="server" DataIndex="valor" AllowBlank="false" AnchorHorizontal="95%" />
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container27" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25" >
                                    <Items>
                                        <ext:NumberField ID="txtIva" FieldLabel="Iva" runat="server" DataIndex="iva" AllowBlank="false" AnchorHorizontal="95%" />
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>
                        <ext:TabPanel ID="TabSuministro" runat="server" ActiveTabIndex="0" Border="false" Title="Center" ResizeTabs="true">
                            <Items>
                                <ext:Panel ID="PTarifas" runat="server" Closable="False" Title="Tarifas" Hidden="False" >
                                    <Items>
                                        <ext:GridPanel 
                                            ID="grdTarifa" 
                                            runat="server"
                                            Width="790"
                                            Height="200"
                                            AutoExpandColumn="manual"
                                            Title="" 
                                            StoreID="stTarifa" 
                                            >
                                            <Plugins>
                                                <ext:RowEditor ID="RowEditor2" runat="server" SaveText="Guardar" CancelText="Cancelar"  >                            
                                                    <Listeners>
                                                        <CancelEdit Fn="cancelTarifa" />
                                                        <AfterEdit Fn="editaTarifa" />                                
                                                    </Listeners>
                                                </ext:RowEditor>
                                            </Plugins>
                                            <View>
                                                <ext:GridView ID="GridView2" runat="server" MarkDirty="false" />
                                            </View>
                                            <TopBar>
                                                <ext:Toolbar ID="Toolbar2" runat="server">
                                                    <Items>
                                                        <ext:Button ID="btnAddTarifa" runat="server" Text="Nuevo" Icon="UserAdd">
                                                            <Listeners>
                                                                <Click Fn="addTarifa" />
                                                            </Listeners>
                                                        </ext:Button>
                                                        <ext:Button ID="btnEliminaTarifa" runat="server" Text="Eliminar" Icon="UserDelete" >                                    
                                                            <Listeners>
                                                                <Click Fn="removeTarifa" />
                                                            </Listeners>
                                                        </ext:Button>
                                                    </Items>
                                                </ext:Toolbar>
                                            </TopBar>
                                            <SelectionModel>
                                                <ext:RowSelectionModel ID="RowSelectionModel4" runat="server" />
                                            </SelectionModel>
                                            <ColumnModel>
                                                <Columns>
                                                    <ext:RowNumbererColumn />     
                                                    <ext:Column DataIndex="es_nueva" Hidden="true" />
                                                    <ext:Column 
                                                        ColumnID="plan_tar" 
                                                        Header="Plan" 
                                                        DataIndex="plan_tar" 
                                                        Width="150"
                                                        >
                                                        <Editor>
                                                            <ext:ComboBox    
                                                                ID="cbPlan"  	                                                                                  
	                                                            runat="server" 
	                                                            Shadow="Drop" 
	                                                            Mode="Local" 
	                                                            TriggerAction="All" 
	                                                            ForceSelection="true"
	                                                            DisplayField="nombre"
	                                                            ValueField="codigo"	                                    
	                                                            EmptyText="Seleccione Plan..."
	                                                            AnchorHorizontal="99%"
	                                                            AllowBlank="false"
                                                                StoreID="stPlan" />	                                    
                                                        </Editor>
                                                        <Renderer Fn="fmtPlan" />
                                                    </ext:Column>
                                                    <ext:Column 
                                                        ColumnID="manual" 
                                                        Header="Manual" 
                                                        DataIndex="manual" 
                                                        Width="150"                                
                                                        Sortable="true">
                                                        <Editor>
                                                            <ext:ComboBox    
                                                                ID="cbTarManual"  	                                                                                  
	                                                            runat="server" 
	                                                            Shadow="Drop" 
	                                                            Mode="Local" 
	                                                            TriggerAction="All" 
	                                                            ForceSelection="true"
	                                                            DisplayField="nombre"
	                                                            ValueField="codigo"	                                    
	                                                            EmptyText="Seleccione Manual..."
	                                                            AnchorHorizontal="99%"
	                                                            AllowBlank="false"
                                                                StoreID="stManual" />	                                    
                                                        </Editor>
                                                        <Renderer Fn="fmtManual" />
                                                    </ext:Column>                                                        
                                                    <ext:Column Header="Valor" DataIndex="valor" Width="100">
                                                        <Editor>
                                                            <ext:NumberField ID="txtValorTar" runat="server" AllowBlank="false" />
                                                        </Editor>
                                                    </ext:Column>                                                                                                                                                                      
                                                </Columns>
                                            </ColumnModel>
                                        </ext:GridPanel>
                                    </Items>
                                </ext:Panel>
                            </Items>
                        </ext:TabPanel>
                        
                    </Items>
                        <Buttons>
                            <ext:Button ID="btnGuardaSum" runat="server" Text="Guardar" Icon="Disk">
                                <DirectEvents>
                                    <Click 
                                        OnEvent="btnGuardaSum_DirectClick" 
                                        Before="var valid= #{StatusForm}.getForm().isValid(); if (valid) {#{FormStatusBar}.showBusy('Guardando Formulario...');} return valid;">
                                        <EventMask 
                                            ShowMask="true" 
                                            MinDelay="1000" 
                                            Target="CustomTarget" 
                                            CustomTarget="={#{StatusForm}.getEl()}" 
                                            />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button runat="server" ID="btnCerrarSum" Text="Cerrar" />
                        </Buttons>
                    </ext:FormPanel>
                </Items>
                <BottomBar>
                    <ext:StatusBar ID="FormStatusBar" runat="server" DefaultText="Ready">
                        <Plugins>
                            <ext:ValidationStatus ID="ValidationStatus1" 
                                runat="server" 
                                FormPanelID="StatusForm" 
                                ValidIcon="Accept" 
                                ErrorIcon="Exclamation" 
                                ShowText="El Formulario tiene Errores (Click para ver Detalles)"
                                HideText="Click para Ocultar Errores"
                                />
                        </Plugins>
                    </ext:StatusBar>
                </BottomBar>
        </ext:Panel>
        </Items>
    </ext:Window>

    </form>
</body>
</html>
