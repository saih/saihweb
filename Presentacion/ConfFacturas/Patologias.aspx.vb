﻿Imports Ext.Net

Public Class Patologias
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim seg As New Seguridad(Me, Session("PERFIL"))
        End If
    End Sub
    Protected Property Accion As String
        Get
            Return ViewState("Accion")
        End Get
        Set(ByVal value As String)
            ViewState.Add("Accion", value)
        End Set
    End Property

    Protected Property codigo As String
        Get
            Return ViewState("codigo")
        End Get
        Set(ByVal value As String)
            ViewState.Add("codigo", value)
        End Set
    End Property

    Protected Property nombre As String
        Get
            Return ViewState("nombre")
        End Get
        Set(ByVal value As String)
            ViewState.Add("nombre", value)
        End Set
    End Property
    Protected Sub btnBuscarPatologia_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs)
        cargaPatologia()
    End Sub
    Private Function cargaPatologia()
        Dim negPat As New Negocio.Negociopatologia
        Dim sWhere As String = " where codigo <> '' "

        If txtCod.Text <> "" Then
            sWhere &= " and codigo = '" & txtCod.Text & "'"

        End If

        If TxtNombre.Text <> "" Then
            sWhere &= " and usuario = '" & TxtNombre.Text & "'"

        End If

        stPat.DataSource = negPat.ObtenerpatologiabyWhere(sWhere)
        stPat.DataBind()
        Return True
    End Function

    Private Sub btnNuevoPatologia_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnNuevoPatologia.DirectClick
        Accion = "Insert"
        presentacionUtil.limpiarObjetos(vntPat)
        vntPat.Show()
    End Sub

    Protected Sub rowSelect(ByVal sender As Object, ByVal e As DirectEventArgs)
        codigo = e.ExtraParams("ID")
    End Sub
    Protected Sub btnGuardarPatologia_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs)
        Dim negPat As New Negocio.Negociopatologia
        Dim patologiaGuardar As New Datos.patologia

        If Accion = "Insert" Then
            Reflexion.valoresForma(patologiaGuardar, vntPat)
            If negPat.Altapatologia(patologiaGuardar) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                vntPat.Hide()
                cargaPatologia()
            End If
        Else
            patologiaGuardar = negPat.ObtenerpatologiaById(codigo)
            Reflexion.valoresForma(patologiaGuardar, vntPat)
            If negPat.Editapatologia(patologiaGuardar) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                vntPat.Hide()
                cargaPatologia()
            End If

        End If

    End Sub
    Private Sub btnCerrarPatologia_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnCerrarPatologia.DirectClick
        vntPat.Hide()
    End Sub
    Private Sub btnEditaPatologia_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnEditaPatologia.DirectClick
        Accion = "Edit"
        Dim negPatolo As New Negocio.Negociopatologia
        Dim patologiaEditar As New Datos.patologia
        patologiaEditar = negPatolo.ObtenerpatologiaById(codigo)
        Reflexion.formaValores(patologiaEditar, vntPat)
        vntPat.Show()
    End Sub
    Private Sub btnEliminaPatologia_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnEliminaPatologia.DirectClick
        Dim js As String = "Ext.MessageBox.confirm('Confirmación', '¿Esta seguro de eliminar?', " & _
           "function (btn) { " & _
                " if (btn == 'yes') " & _
                "   Ext.net.DirectMethods.eliminarPatologia(); " & _
                " });"
        Ext.Net.X.Js.AddScript(js)
    End Sub
    <DirectMethod()>
    Public Function eliminarPatologia()
        Dim negPatologiaElim As New Negocio.Negociopatologia
        negPatologiaElim.Eliminapatologia(codigo)
        cargaPatologia()
        Return True
    End Function
End Class