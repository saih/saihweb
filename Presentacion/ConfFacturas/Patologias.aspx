﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Patologias.aspx.vb" Inherits="Presentacion.Patologias" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../public/js/jquery-1.9.0.js" type="text/javascript"></script>
    <script type="text/javascript" src="./../public/js/app.js"></script>


    <script type="text/javascript">
        var enterKeyPressHandler = function (f, e) {

            var val = parseInt(e.getKey());
            console.log(val);
            if ((val >= 65 && val <= 90) || (val === 32) || (val === 16)) {

            } else {
                var objExt = Ext.getCmp(f.id);
                var palabra = f.getValue();
                f.setValue(palabra.substring(0, palabra.length - 1))
            }
        }   
      </script>
</head>
<body>
    <form id="form1" runat="server">
        
    <ext:ResourceManager ID="ResourceManager1" runat="server"/>
    <ext:Store ID ="stPat"
        runat="server" 
        AutoLoad="true">
        <Reader>
            <ext:JsonReader IDProperty="codigo">
                 <Fields>
                    <ext:RecordField Name="codigo" />
                    <ext:RecordField Name="nombre" />
                    <ext:RecordField Name="aplica_sexo" />
                    <ext:RecordField Name="edad_inicial" />
                    <ext:RecordField Name="edad_final" />
                    <ext:RecordField Name="ind_patep" />
                </Fields>
            </ext:JsonReader>
        </Reader>
    </ext:Store>
    <ext:Panel ID="panel1" runat="server" Frame="true" PaddingSummary="5px 5px 0" AutoWidth="true" Height="650" ButtonAlign="Center" Style="text-align: left" Title="Tablas">
        <Items>
            <ext:FieldSet ID="FieldSetP" runat="server" Title="Campos de Busqueda" Padding="10" Height="60">
                <Items>
                    <ext:Container ID="ContP1" runat="server" Layout="ColumnLayout" Height="30">
                        <Items>
                            <ext:Container ID="ContPCod" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25">
                                <Items>
                                    <ext:TextField ID="txtCod" runat="server" FieldLabel="Codigo Pat"></ext:TextField>
                                </Items>
                            </ext:Container>
                            <ext:Container ID="ContPNom" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25">
                                <Items>
                                    <ext:TextField ID="TxtNombre" runat="server" FieldLabel="Nombre Pat"></ext:TextField>
                                </Items>
                            </ext:Container>
                            <ext:Container ID="ContBusPat" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".2">
                                <Items>
                                    <ext:Button ID="btnBuscarPatologia" runat="server" Text="Buscar" Icon="ApplicationGo">
                                        <DirectEvents>
                                                <Click OnEvent="btnBuscarPatologia_DirectClick" Before="">
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                    </ext:Button>
                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:Container>
                </Items>
            </ext:FieldSet>
            <ext:GridPanel  ID="grdCaj"
                    runat="server"                                            
                    Margins="0 0 5 5"
                    Icon="TransmitGo"
                    Frame="true"
                    Height="400"
                    StoreId="stPat"
                    Title="Patologia">
                     <ColumnModel ID="ColumnModelPat" runat="server">
                        <Columns>                        
                            <ext:Column Header="Código" DataIndex="codigo" />
                            <ext:Column Header="Nombre" DataIndex="nombre" />
                            <ext:Column Header="Aplica Sexo" DataIndex="aplica_sexo" />                        
                            <ext:Column Header="Edad Inicial" DataIndex="edad_inicial"/>
                            <ext:Column Header="Edad Final" DataIndex="edad_final"/>
                            <ext:Column Header="Indicardo Patologia" DataIndex="ind_patep"/>
                        </Columns>                                
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModelPat" runat="server" SingleSelect="true" >
                            <DirectEvents>
                                <RowSelect OnEvent="rowSelect" Buffer="100">
                                    <ExtraParams>                                    
                                        <ext:Parameter Name="ID" Value="this.getSelected().id" Mode="Raw" />
                                    </ExtraParams>
                                </RowSelect>
                            </DirectEvents>
                        </ext:RowSelectionModel>
                    </SelectionModel>
                     <BottomBar>
                        <ext:StatusBar runat="server" ID="StatusBarPat">
                            <Items>                                                        
                                <ext:Button runat="server" Text="Nueva Patologia" ID="btnNuevoPatologia" Icon="Add" />
                                <ext:Button runat="server" Text="Editar Patologia" ID="btnEditaPatologia" Icon="ApplicationEdit" />
                                <ext:Button runat="server" Text="Eliminar Patologia" ID="btnEliminaPatologia" Icon="ApplicationDelete" />
                            </Items>
                        </ext:StatusBar>
                    </BottomBar>
            </ext:GridPanel>
        </Items>
    </ext:Panel>
    <ext:Window ID="vntPat"
            runat="server"
            Icon="ApplicationFormEdit"
            Width="850"
            Height="510"
            Hidden="true" 
            Modal="true"            
            Title="Registro Patologia"
            Constrain="true">
            <Items>
                <ext:Panel ID="PanelPatologia"
                        runat="server" 
                        Title=""
                        AnchorHorizontal="100%"
                        Height="480"
                        Layout="Fit">
                        <Items>
                             <ext:FormPanel 
                                ID="StatusFormP" 
                                runat="server"
                                LabelWidth="75"
                                ButtonAlign="Right"
                                Border="false"
                                PaddingSummary="10px 10px 10px">
                                    <Defaults>
                                        <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                        <ext:Parameter Name="SelectOnFocus" Value="true" Mode="Raw" />
                                        <ext:Parameter Name="MsgTarget" Value="side" />
                                    </Defaults> 
                                    <Items>
                                        <ext:FieldSet ID="FieldSet1C" runat="server" Title="Cajero" Padding="10" Height="120">
                                            <Items>
                                                <ext:Container ID="Container3P" runat="server" Layout="ColumnLayout" Height="30">
                                                    <Items>
                                                         <ext:Container ID="Container2P" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25">
                                                            <Items>
                                                                <ext:NumberField runat="server" FieldLabel="Codigo" ID="txtCodPat" DataIndex="codigo" AnchorHorizontal="99%" AllowBlank="false" MinLength="2" MaxLength="5"></ext:NumberField >
                                                            </Items> 
                                                         </ext:Container>   
                                                         <ext:Container ID="Container4P" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3">
                                                            <Items>
                                                                <ext:TextField runat="server" ID="txtNombrePat" FieldLabel="Nombre Patologia" AnchorHorizontal="90%" AllowBlank="false" MinLength="2" MaxLength="150" DataIndex="nombre"  EnableKeyEvents="true">
                                                                <Listeners>
                                                                        <KeyUp Fn="enterKeyPressHandler" />                                                                
                                                                </Listeners>
                                                                </ext:TextField>
                                                            </Items>
                                                         </ext:Container> 
                                                         <ext:Container ID="Container6P" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5">
                                                            <Items>
                                                                <ext:ComboBox ID="cbSexoPat" runat="server"  FieldLabel="Aplica Sexo" AllowBlank="false" AnchorHorizontal="95%" DataIndex="aplica_sexo" >
                                                                    <Items>
                                                                        <ext:ListItem Text="SI" Value="S" />
                                                                        <ext:ListItem Text="NO" Value="N" />
                                                                    </Items>
                                                                </ext:ComboBox>
                                                            </Items>
                                                          </ext:Container>
                                                    </Items>
                                                </ext:Container> 
                                                <ext:Container ID="Container5P" runat="server" Layout="ColumnLayout" Height="30">
                                                    <Items>
                                                            <ext:Container ID="Container7P" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".35" >
                                                                    <Items>
                                                                        <ext:NumberField runat="server" ID="txtEdadInicial" FieldLabel="Edad Inicial"  AnchorHorizontal="90%" AllowBlank="false" MinLength="1" MaxLength="3" DataIndex="edad_inicial"  EnableKeyEvents="true">
                                                                            <Listeners>
                                                                                <KeyUp Fn="enterKeyPressHandler" />                                                                
                                                                            </Listeners>
                                                                        </ext:NumberField> 
                                                                    </Items>
                                                            </ext:Container>
                                                            <ext:Container ID="Container8P" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".35" >
                                                                    <Items>
                                                                        <ext:NumberField runat="server" ID="txtEdadFinal" FieldLabel="Edad Final"  AnchorHorizontal="90%" AllowBlank="false" MinLength="1" MaxLength="3" DataIndex="edad_final"  EnableKeyEvents="true">
                                                                            <Listeners>
                                                                                <KeyUp Fn="enterKeyPressHandler" />                                                                
                                                                            </Listeners>
                                                                        </ext:NumberField> 
                                                                    </Items>
                                                            </ext:Container>
                                                            <ext:Container ID="Container9P" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".35" >
                                                                    <Items>
                                                                        <ext:NumberField runat="server" ID="txtTipPat" FieldLabel="Tipo Patologia"  AnchorHorizontal="90%" AllowBlank="false" MinLength="1" MaxLength="3" DataIndex="ind_patep"  EnableKeyEvents="true">
                                                                            <Listeners>
                                                                                <KeyUp Fn="enterKeyPressHandler" />                                                                
                                                                            </Listeners>
                                                                        </ext:NumberField> 
                                                                    </Items>
                                                            </ext:Container>
                                                       </Items> 
                                                </ext:Container> 
                                            </Items>
                                        </ext:FieldSet> 
                                    </Items> 
                                    <Buttons>
                                        <ext:Button ID="btnGuardarPatologia" runat="server" Text="Guardar" Icon="Disk">
                                            <DirectEvents>
                                                <Click 
                                                OnEvent="btnGuardarPatologia_DirectClick" 
                                                Before="var valid= #{StatusFormP}.getForm().isValid();">
                                                <EventMask 
                                                    ShowMask="true" 
                                                    MinDelay="1000" 
                                                    Target="CustomTarget" 
                                                    CustomTarget="={#{StatusFormP}.getEl()}" 
                                                    />
                                            </Click>
                                            </DirectEvents> 
                                        </ext:Button>
                                         <ext:Button runat="server" ID="btnCerrarPatologia" Text="Cerrar" />
                                   </Buttons>
                            </ext:FormPanel>
                        </Items>
                </ext:Panel>
            </Items>
    </ext:Window>
    </form>
</body>
</html>
