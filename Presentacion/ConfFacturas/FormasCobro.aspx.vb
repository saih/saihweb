﻿Imports Ext.Net
Public Class FormasCobro
    Inherits System.Web.UI.Page

    Protected Property Accion As String
        Get
            Return ViewState("Accion")
        End Get
        Set(ByVal value As String)
            ViewState.Add("Accion", value)
        End Set
    End Property

    Protected Property manual As String
        Get
            Return ViewState("manual")
        End Get
        Set(ByVal value As String)
            ViewState.Add("manual", value)
        End Set
    End Property

    Protected Property codigo As String
        Get
            Return ViewState("codigo")
        End Get
        Set(ByVal value As String)
            ViewState.Add("codigo", value)
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Funciones.cargaStoreDominio(stFormadeCobro, Datos.ConstantesUtil.DOMINIO_MANUAL_TARIFARIO)
    End Sub
    Protected Sub rowSelect(ByVal sender As Object, ByVal e As DirectEventArgs)
        Dim negFormCob As New Negocio.Negocioexc_quirurjicas
        Dim formCob As New Datos.exc_quirurjicas
        manual = e.ExtraParams("manual")
        codigo = e.ExtraParams("codigo")
        formCob = negFormCob.Obtenerexc_quirurjicasById(manual, codigo)
    End Sub

    Protected Sub btnBuscar_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs)
        cargaFormas()
    End Sub
    Protected Sub btnLimpiar_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs)
        limpiarCampos()
    End Sub
    Protected Sub btnAsignar_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs)

    End Sub

    Protected Sub btnGuardaForCob_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs)
        Dim negForCob As New Negocio.Negocioexc_quirurjicas
        Dim forCob As New Datos.exc_quirurjicas

        If Accion = "Insert" Then
            Reflexion.valoresForma(forCob, vntForCob)

            If negForCob.Altaexc_quirurjicas(forCob) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                vntForCob.Hide()
                cargaFormas()
            End If
        Else
            forCob = negForCob.Obtenerexc_quirurjicasById(manual, codigo)
            Reflexion.valoresForma(forCob, vntForCob)
            If negForCob.Editaexc_quirurjicas(forCob) Then
                Ext.Net.X.Msg.Alert("Información", "Registro Guardado Correctamente").Show()
                vntForCob.Hide()
                cargaFormas()
            End If
        End If
    End Sub
    Private Function cargaFormas()
        Dim negForm As New Negocio.Negocioexc_quirurjicas
        Dim sWhere As String = " where manual <> '' "
        Dim count As Integer

        count = 0

        If cbxManual.Text <> "" Then
            sWhere &= " and manual = '" & cbxManual.Text & "'"
            count = count + 1
        End If

        If count = 0 Then
            Ext.Net.X.Msg.Alert("Información", "Por favor ingresar por lo menos un filtro de búsqueda").Show()
            Return True
        End If

        stForCob.DataSource = negForm.Obtenerexc_quirurjicasbyWhere(sWhere)
        stForCob.DataBind()

        Return True
    End Function

    <DirectMethod()>
    Public Function eliminarForCob()
        Dim negForCob As New Negocio.Negocioexc_quirurjicas
        negForCob.Eliminaexc_quirurjicas(manual, codigo)
        cargaFormas()
        Return True
    End Function

    Private Sub btnEliminaForCob_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnEliminaForma.DirectClick
        Dim js As String = "Ext.MessageBox.confirm('Confirmación', '¿Esta seguro de eliminar?', " & _
           "function (btn) { " & _
                " if (btn == 'yes') " & _
                "   Ext.net.DirectMethods.eliminarForCob(); " & _
                " });"
        Ext.Net.X.Js.AddScript(js)
    End Sub

    Private Sub btnNuevoForCob_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnNuevoForma.DirectClick
        Accion = "Insert"
        cargaCombos()
        presentacionUtil.limpiarObjetos(vntForCob)
        vntForCob.Show()
    End Sub

    Private Sub btnEditaForCob_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnEditaForma.DirectClick
        Accion = "Edit"
        Dim negForCob As New Negocio.Negocioexc_quirurjicas
        Dim ForCob As New Datos.exc_quirurjicas
        ForCob = negForCob.Obtenerexc_quirurjicasById(manual, codigo)
        cargaCombos()
        Reflexion.formaValores(ForCob, vntForCob)
        vntForCob.Show()
    End Sub

    Private Function cargaCombos()
        Dim negUsu As New Negocio.NegocioUsuario
        Dim sWhere As String = " where Usuario <> '' "
        Dim negSede As New Negocio.Negociosaih_prm
        Dim sWhere1 As String = " where codpre <> '' "

        Funciones.cargaStoreDominio(stManual, Datos.ConstantesUtil.DOMINIO_MANUAL_TARIFARIO)

        Return True
    End Function
    Private Function limpiarCampos()
        cbxManual.Clear()
        Return True
    End Function



    Private Sub btnCerrarForCob_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnCerrarForCob.DirectClick
        vntForCob.Hide()
    End Sub
End Class