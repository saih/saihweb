﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Cajeros.aspx.vb" Inherits="Presentacion.Cajeros" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../public/js/jquery-1.9.0.js" type="text/javascript"></script>
    <script type="text/javascript" src="./../public/js/app.js"></script>


    <script type="text/javascript">
        var enterKeyPressHandler = function (f, e) {

            var val = parseInt(e.getKey());
            console.log(val);
            if ((val >= 65 && val <= 90) || (val === 32) || (val === 16)) {

            } else {
                var objExt = Ext.getCmp(f.id);
                var palabra = f.getValue();
                f.setValue(palabra.substring(0, palabra.length - 1))
            }
        }    </script>
</head>
<body>
    <form id="form1" runat="server">
    
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
    
    <ext:Store 
        ID="stCaj" 
        runat="server" 
        AutoLoad="true">
        <Reader>
            <ext:JsonReader IDProperty="codigo_cajero">
                <Fields>
                    <ext:RecordField Name="codigo_cajero" />
                    <ext:RecordField Name="usuario" />
                    <ext:RecordField Name="base" />
                    <ext:RecordField Name="fecha_apertura" />
                    <ext:RecordField Name="estado" />
                </Fields>
            </ext:JsonReader>
        </Reader>
    </ext:Store>

    <ext:Panel ID="Panel3" runat="server" Frame="true" PaddingSummary="5px 5px 0" AutoWidth="true" Height="650" ButtonAlign="Center" Style="text-align: left" Title="Tablas">
        <Items>
            <ext:FieldSet ID="FieldSet3" runat="server" Title="Campos de Busqueda" Padding="10" Height="60">
                 <Items>
                    <ext:Container ID="ContainerCajeros" runat="server" Layout="ColumnLayout" Height="30">
                            <Items>
                                <ext:Container ID="ContainerCaj" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25">
                                    <Items>
                                        <ext:TextField runat="server" id="txtCajero" FieldLabel="Cajero" />
                                    </Items>    
                                </ext:Container>
                                <ext:Container ID="ContainerUser" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25">
                                    <Items>
                                         <ext:TextField runat="server" id="txtUsuario" FieldLabel="Usuario" />                                       
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="ContainerBuscar" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".2">
                                    <Items>
                                        <ext:Button runat="server" ID="btnBuscarCajero" Text="Buscar" Icon="ApplicationGo" >
                                            <DirectEvents>
                                                <Click OnEvent="btnBuscarCajero_DirectClick" Before="">
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>
                                    </Items>
                                </ext:Container>
                            </Items>
                        </ext:Container>   
                 </Items> 
            </ext:FieldSet> 
            <ext:GridPanel 
                    ID="grdCaj"
                    runat="server"                                            
                    Margins="0 0 5 5"
                    Icon="TransmitGo"
                    Frame="true"
                    Height="400"
                    StoreId="stCaj"
                    Title="Cajero">
                    <ColumnModel ID="ColumnModelCaj" runat="server">
                        <Columns>                        
                            <ext:Column Header="Código" DataIndex="codigo_cajero" />
                            <ext:Column Header="Usuario" DataIndex="usuario" />
                            <ext:Column Header="Fecha Apertura" DataIndex="fecha_apertura" />                        
                            <ext:Column Header="Estado" DataIndex="estado" Width="380"/>
                        </Columns>                                
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModelCaje" runat="server" SingleSelect="true" >
                            <DirectEvents>
                                <RowSelect OnEvent="rowSelect" Buffer="100">
                                    <ExtraParams>                                    
                                        <ext:Parameter Name="ID" Value="this.getSelected().id" Mode="Raw" />
                                    </ExtraParams>
                                </RowSelect>
                            </DirectEvents>
                        </ext:RowSelectionModel>
                    </SelectionModel>
                     <BottomBar>
                        <ext:StatusBar runat="server" ID="StatusBar2">
                            <Items>                                                        
                                <ext:Button runat="server" Text="Nuevo Cajero" ID="btnNuevoCajero" Icon="Add" />
                                <ext:Button runat="server" Text="Editar Cajero" ID="btnEditaCajero" Icon="ApplicationEdit" />
                                <ext:Button runat="server" Text="Eliminar Cajero" ID="btnEliminaCajero" Icon="ApplicationDelete" />
                            </Items>
                        </ext:StatusBar>
                    </BottomBar>
            </ext:GridPanel>
        </Items>
    </ext:Panel> 
    <ext:Window 
            ID="vntCaje"
            runat="server"
            Icon="ApplicationFormEdit"
            Width="850"
            Height="510"
            Hidden="true" 
            Modal="true"            
            Title="Registro Cajero"
            Constrain="true">
            <Items>
                <ext:Panel 
                        ID="PanelC" 
                        runat="server" 
                        Title=""
                        AnchorHorizontal="100%"
                        Height="480"
                        Layout="Fit">
                        <Items>
                            <ext:FormPanel 
                                ID="StatusFormC" 
                                runat="server"
                                LabelWidth="75"
                                ButtonAlign="Right"
                                Border="false"
                                PaddingSummary="10px 10px 10px">
                                <Defaults>
                                    <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                    <ext:Parameter Name="SelectOnFocus" Value="true" Mode="Raw" />
                                    <ext:Parameter Name="MsgTarget" Value="side" />
                                </Defaults> 
                                <Items>
                                     <ext:FieldSet ID="FieldSet1C" runat="server" Title="Cajero" Padding="10" Height="120">
                                        <Items>
                                            <ext:Container ID="Container3C" runat="server" Layout="ColumnLayout" Height="30">
                                                 <Items>
                                                    <ext:Container ID="Container2C" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25">
                                                        <Items>
                                                            <ext:NumberField runat="server" FieldLabel="Codigo Cajero" ID="txtCCaj" DataIndex="codigo_cajero" AnchorHorizontal="99%" AllowBlank="false" MinLength="2" MaxLength="5"></ext:NumberField >
                                                        </Items> 
                                                    </ext:Container>   
                                                    <ext:Container ID="Container4C" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3">
                                                        <Items>
                                                            <ext:TextField runat="server" ID="txtUsus" FieldLabel="Usuario" AnchorHorizontal="90%" AllowBlank="false" MinLength="2" MaxLength="50" DataIndex="usuario"  EnableKeyEvents="true">
                                                            <Listeners>
                                                                    <KeyUp Fn="enterKeyPressHandler" />                                                                
                                                            </Listeners>
                                                            </ext:TextField>
                                                        </Items>
                                                    </ext:Container>                                    
                                                </Items> 
                                            </ext:Container> 
                                            <ext:Container ID="Container5C" runat="server" Layout="ColumnLayout" Height="30">
                                                <Items>
                                                    <ext:Container ID="Container6C" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5">
                                                        <Items>
                                                            <ext:NumberField runat="server" ID="txtBaseC" FieldLabel="Base" AnchorHorizontal="99%" AllowBlank="false" MinLength="2" MaxLength="20" DataIndex="base" EnableKeyEvents="true">
                                                            <Listeners>
                                                                    <KeyUp Fn="enterKeyPressHandler" />                                                                
                                                            </Listeners>
                                                            </ext:NumberField>
                                                        </Items>
                                                    </ext:Container>
                                                    <ext:Container ID="Container7C" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".35" >
                                                            <Items>
                                                                <ext:DateField runat="server" ID="txtFechaApertura" FieldLabel="F. Apertura" AnchorHorizontal="95%" DataIndex="fecha_apertura" />
                                                            </Items>
                                                    </ext:Container>
                                                </Items>                                                
                                            </ext:Container>
                                            <ext:Container ID="Container8C" runat="server" Layout="ColumnLayout" Height="30">
                                                <Items>
                                                    <ext:Container ID="Container9C" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5">
                                                        <Items>
                                                            <ext:NumberField runat="server" ID="txtSaldo" FieldLabel="Saldo" AnchorHorizontal="99%" AllowBlank="false" MinLength="2" MaxLength="20" DataIndex="saldo" EnableKeyEvents="true">
                                                            <Listeners>
                                                                    <KeyUp Fn="enterKeyPressHandler" />                                                                
                                                            </Listeners>
                                                            </ext:NumberField>
                                                        </Items>
                                                    </ext:Container>
                                                    <ext:Container ID="Container10C" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".35" >
                                                            <Items>
                                                                <ext:ComboBox ID="cbEstado" runat="server"  FieldLabel="Estado" AllowBlank="false" AnchorHorizontal="95%" DataIndex="estado" >
                                                                    <Items>
                                                                        <ext:ListItem Text="ACTIVO" Value="A" />
                                                                        <ext:ListItem Text="INACTIVO" Value="I" />
                                                                    </Items>
                                                                </ext:ComboBox>
                                                            </Items>
                                                    </ext:Container>
                                                </Items>                                                
                                            </ext:Container>
                                        </Items> 
                                     </ext:FieldSet> 
                                </Items>
                                <Buttons>
                                    <ext:Button ID="btnGuardaCajero" runat="server" Text="Guardar" Icon="Disk">
                                        <DirectEvents>
                                            <Click 
                                                OnEvent="btnGuardaCajero_DirectClick" 
                                                Before="var valid= #{StatusFormC}.getForm().isValid();">
                                                <EventMask 
                                                    ShowMask="true" 
                                                    MinDelay="1000" 
                                                    Target="CustomTarget" 
                                                    CustomTarget="={#{StatusFormC}.getEl()}" 
                                                    />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button runat="server" ID="btnCerrarCajero" Text="Cerrar" />
                                </Buttons>
                            </ext:FormPanel> 
                        </Items> 
                        
                </ext:Panel>
            </Items> 
    </ext:Window> 
    </form>
</body>
</html>
