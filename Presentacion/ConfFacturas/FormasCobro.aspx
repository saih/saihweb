﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FormasCobro.aspx.vb" Inherits="Presentacion.FormasCobro" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <ext:ResourceManager ID="ResourceManager1" runat="server" />
           <ext:Store 
            ID="stForCob" 
            runat="server" 
            AutoLoad="true">
            <Reader>
                <ext:JsonReader IDProperty="UsuarioSede">
                    <Fields>
                        <ext:RecordField Name="manual" />
                        <ext:RecordField Name="codigo" />
                        <ext:RecordField Name="nombre" />
                        <ext:RecordField Name="tipo_porcentaje" />
                        <ext:RecordField Name="porc_honmed" />
                        <ext:RecordField Name="porc_honane" />
                        <ext:RecordField Name="porc_honayu" />
                        <ext:RecordField Name="porc_dersal" />
                        <ext:RecordField Name="porc_dersalesp" />
                        <ext:RecordField Name="porc_matsum" />
                        <ext:RecordField Name="porc_honper" />
                        <ext:RecordField Name="porc_paqquir" />
                    </Fields>
                </ext:JsonReader>
            </Reader>
        </ext:Store>
        <ext:Panel ID="Panel03" runat="server" Frame="true" PaddingSummary="5px 5px 0" AutoWidth="true" Height="650" ButtonAlign="Center" Style="text-align: left" Title="Forma de Cobro">
            <Items>
                <ext:FieldSet ID="FieldSet03" runat="server" Title="Campos de Busqueda" Padding="10" Height="60">
                    <Items>
                        <ext:Container ID="Container10" runat="server" Layout="ColumnLayout" Height="30">
                            <Items>
                                <ext:Container ID="Container08" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".25">
                                    <Items>
                                           <ext:ComboBox     
	                                                                ID="cbxManual"                                                                              
	                                                                runat="server" 
	                                                                Shadow="Drop" 
	                                                                Mode="Local" 
	                                                                TriggerAction="All" 
	                                                                ForceSelection="true"
	                                                                DisplayField="nombre"
	                                                                ValueField="codigo"	
	                                                                EmptyText="Seleccione Manual"
	                                                                AnchorHorizontal="95%"
	                                                                AllowBlank="false"
                                                                    FieldLabel="Manual"
                                                                    DataIndex="manual"
	                                                                >
	                                                                <Store>
		                                                                <ext:Store ID="stFormadeCobro" runat="server" AutoLoad="true">
			                                                                <Reader>
				                                                        <ext:JsonReader IDProperty="codigo">
					                                                        <Fields>
						                                                        <ext:RecordField Name="codigo" />
						                                                        <ext:RecordField Name="nombre" />
					                                                        </Fields>
				                                                        </ext:JsonReader>
			                                                                </Reader>            
		                                                                </ext:Store>    
	                                                                </Store>    
                                                                </ext:ComboBox>
                                    </Items>    
                                </ext:Container>
                                <ext:Container ID="Container5" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".2">
                                    <Items>
                                        <ext:Button runat="server" ID="btnBuscar" Text="Buscar" Icon="ApplicationGo" >
                                            <DirectEvents>
                                                <Click OnEvent="btnBuscar_DirectClick" Before="">
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>
                                    </Items>
                                </ext:Container>
                                <ext:Container ID="Container1" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".2">
                                    <Items>
                                        <ext:Button runat="server" ID="btnLimpiar" Text="Limpiar Campos" Icon="ApplicationGo" >
                                            <DirectEvents>
                                                <Click OnEvent="btnLimpiar_DirectClick" Before="">
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>
                                    </Items>
                                </ext:Container>

                            </Items>
                        </ext:Container>                        
                    </Items>
                </ext:FieldSet>
                <ext:GridPanel
                ID="grdForCob"
                runat="server"                                            
                Margins="0 0 5 5"
                Icon="TransmitGo"
                Frame="true"
                Height="400"
                StoreId="stForCob"
                Title="Forma de Cobro"
                >
                <ColumnModel ID="ColumnModel03" runat="server">
                    <Columns>                        
                        <ext:Column Header="Manual" DataIndex="manual" />
                        <ext:Column Header="Nombre" DataIndex="nombre" />                        
                    </Columns>                                
                </ColumnModel>
                <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" SingleSelect="true" >
                            <DirectEvents>
                                <RowSelect OnEvent="rowSelect" Buffer="100">
                                    <ExtraParams>                                                                            
                                        <ext:Parameter Name="manual" Value="this.getSelected().get('manual')" Mode="Raw" />
                                        <ext:Parameter Name="codigo" Value="this.getSelected().get('codigo')" Mode="Raw" />
                                    </ExtraParams>
                                </RowSelect>
                            </DirectEvents>
                        </ext:RowSelectionModel>
                    </SelectionModel>
                 <LoadMask ShowMask="true" Msg="Loading...." />
                <BottomBar>
                    <ext:StatusBar runat="server" ID="StatusBar2">
                        <Items>                                                        
                            <ext:Button runat="server" Text="Nueva Forma" ID="btnNuevoForma" Icon="Add" />
                            <ext:Button runat="server" Text="Editar Forma" ID="btnEditaForma" Icon="ApplicationEdit" />
                            <ext:Button runat="server" Text="Eliminar Forma" ID="btnEliminaForma" Icon="ApplicationDelete" />
                        </Items>
                    </ext:StatusBar>
                </BottomBar>
            </ext:GridPanel>
            </Items>
        </ext:Panel>

        <ext:Window 
            ID="vntForCob"
            runat="server"
            Icon="ApplicationFormEdit"
            Width="850"
            Height="480"
            Hidden="true" 
            Modal="true"            
            Title="Registro FormaCobro"
            Constrain="true">
            <Items>
                <ext:Panel ID="Panel1" 
                    runat="server" 
                    Title=""
                    AnchorHorizontal="100%"
                    Height="450"
                    Layout="Fit">
                    <Items>
                        <ext:FormPanel 
                            ID="StatusForm" 
                            runat="server"
                            LabelWidth="75"
                            ButtonAlign="Right"
                            Border="false"
                            PaddingSummary="10px 10px 10px">
                            <Defaults>                        
                                <ext:Parameter Name="AllowBlank" Value="false" Mode="Raw" />
                                <ext:Parameter Name="SelectOnFocus" Value="true" Mode="Raw" />
                                <ext:Parameter Name="MsgTarget" Value="side" />
                            </Defaults>
                            <Items>
            
                                <ext:TabPanel ID="TabForCob" runat="server" ActiveTabIndex="0" Border="false" Title="Center" ResizeTabs="true">
                                    <Items>
                                        <ext:Panel ID="PGeneral" runat="server" Closable="False" Title="Datos Basicos" Hidden="False" >
                                            <Items>                                          
                                                 <ext:Container ID="Container34" runat="server" Layout="ColumnLayout" Height="30" >
                                                    <Items>
                                                       <ext:Container ID="Container35" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                                            <Items>
                                                                <ext:ComboBox     
	                                                        ID="cbManual"                                                                              
	                                                        runat="server" 
	                                                        Shadow="Drop" 
	                                                        Mode="Local" 
	                                                        TriggerAction="All" 
	                                                        ForceSelection="true"
	                                                        DisplayField="nombre"
	                                                        ValueField="codigo"	
	                                                        EmptyText="Seleccione Manual"
	                                                        AnchorHorizontal="99%"
	                                                        AllowBlank="false"
                                                            FieldLabel="Manual"
                                                            DataIndex="manual"
	                                                        >
	                                                        <Store>
		                                                        <ext:Store ID="stManual" runat="server" AutoLoad="true">
			                                                        <Reader>
				                                                        <ext:JsonReader IDProperty="codigo">
					                                                        <Fields>
						                                                        <ext:RecordField Name="codigo" />
						                                                        <ext:RecordField Name="nombre" />
					                                                        </Fields>
				                                                        </ext:JsonReader>
			                                                        </Reader>            
		                                                        </ext:Store>    
	                                                        </Store>    
                                                        </ext:ComboBox>
                                                             </Items>
                                                        </ext:Container>
                                                        <ext:Container ID="Container12" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3">
                                                            <Items>                                                      
                                                            <ext:TextField runat="server" FieldLabel="Codigo de la Forma" ID="txtCodFor" AllowBlank="false" MinLength="3" MaxLength="15" AnchorHorizontal="90%" DataIndex="codigo" SelectOnFocus="true" />
                                                            </Items>
                                                        </ext:Container>
                                                    </Items>
                                                 </ext:Container>


                                                 <ext:Container ID="Container20" runat="server" Layout="ColumnLayout" Height="30" >
                                                    <Items>
                                                       <ext:Container ID="Container21" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                                            <Items>
                                                              <ext:TextField runat="server" FieldLabel="Descripcion" ID="txtDes" AllowBlank="false" MinLength="3" MaxLength="15" AnchorHorizontal="90%" DataIndex="nombre" SelectOnFocus="true" />                                                            
                                                             </Items>
                                                        </ext:Container>
                                                        <ext:Container ID="Container22" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3">
                                                            <Items>
                                                            <ext:NumberField runat="server" FieldLabel="Porcentaje Honorarios Medicos" ID="txtHonMed" DataIndex="porc_honmed" AnchorHorizontal="99%" AllowBlank="false" MinLength="2" MaxLength="5"></ext:NumberField >                                                                                                                  
                                                            </Items>
                                                        </ext:Container>
                                                    </Items>
                                                 </ext:Container>
                                                 

                                                 <ext:Container ID="Container2" runat="server" Layout="ColumnLayout" Height="30" >
                                                    <Items>
                                                       <ext:Container ID="Container3" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                                            <Items>
                                                            <ext:NumberField runat="server" FieldLabel="Porcentaje Honorarios Anestesiologo" ID="txtHonAnest" DataIndex="porc_honane" AnchorHorizontal="99%" AllowBlank="false" MinLength="2" MaxLength="5"></ext:NumberField >                                                                                                                                                                              
                                                             </Items>
                                                        </ext:Container>
                                                        <ext:Container ID="Container15" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3">
                                                            <Items>
                                                            <ext:NumberField runat="server" FieldLabel="Porcentaje Honorarios Ayudante Quirur" ID="txtHonAyQ" DataIndex="porc_honayu" AnchorHorizontal="99%" AllowBlank="false" MinLength="2" MaxLength="5"></ext:NumberField >                                                                                                                                                                                                                                           
                                                            </Items>
                                                        </ext:Container>
                                                    </Items>
                                                 </ext:Container>

                                                

                                                 <ext:Container ID="Container4" runat="server" Layout="ColumnLayout" Height="30" >
                                                    <Items>
                                                       <ext:Container ID="Container6" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                                            <Items>
                                                        <ext:NumberField runat="server" FieldLabel="Porcentaje Honorarios Perfusionista" ID="txtHonPer" DataIndex="porc_honper" AnchorHorizontal="99%" AllowBlank="false" MinLength="2" MaxLength="5"></ext:NumberField >                                                                                                                                                                                                                                      
                                                             </Items>
                                                        </ext:Container>
                                                        <ext:Container ID="Container16" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3">
                                                            <Items>
                                                        <ext:NumberField runat="server" FieldLabel="Porcentaje Derechos de Sala" ID="txtDerSal" DataIndex="porc_dersal" AnchorHorizontal="99%" AllowBlank="false" MinLength="2" MaxLength="5"></ext:NumberField >                                                                                                                                                                                                                                      
                                                            </Items>
                                                        </ext:Container>
                                                    </Items>
                                                 </ext:Container>




                                                 <ext:Container ID="Container7" runat="server" Layout="ColumnLayout" Height="30" >
                                                    <Items>
                                                       <ext:Container ID="Container9" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                                            <Items>
                                                        <ext:NumberField runat="server" FieldLabel="Porcentaje Derechos de Sala Especial" ID="txtDerSalEsp" DataIndex="porc_dersalesp" AnchorHorizontal="99%" AllowBlank="false" MinLength="2" MaxLength="5"></ext:NumberField >                                                                                                                                                                                                                                      
                                                             </Items>
                                                        </ext:Container>
                                                        <ext:Container ID="Container17" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".3">
                                                            <Items>
                                                            <ext:NumberField runat="server" FieldLabel="Porcentaje Materiales y Suministros" ID="txtMat" DataIndex="porc_matsum" AnchorHorizontal="99%" AllowBlank="false" MinLength="2" MaxLength="5"></ext:NumberField >                                                                                                                                                                              
                                                            </Items>
                                                        </ext:Container>
                                                    </Items>
                                                 </ext:Container>



                                                 <ext:Container ID="Container11" runat="server" Layout="ColumnLayout" Height="30" >
                                                    <Items>
                                                       <ext:Container ID="Container13" runat="server" LabelAlign="Right" Layout="FormLayout" ColumnWidth=".5" >
                                                            <Items>
                                                            <ext:NumberField runat="server" FieldLabel="Porcentaje Para Paquetes Quirurjicos" ID="txtPaQui" DataIndex="porc_paqquir" AnchorHorizontal="99%" AllowBlank="false" MinLength="2" MaxLength="5"></ext:NumberField >                                                                                                                                                                              
                                                             </Items>
                                                        </ext:Container>
                                                    </Items>
                                                 </ext:Container>

                                                    
                                            </Items>
                                        </ext:Panel>
                                        
                                    </Items>
                                </ext:TabPanel>
                            </Items>
                            <Buttons>
                            <ext:Button ID="btnGuardaForCob" runat="server" Text="Guardar" Icon="Disk">
                                <DirectEvents>
                                    <Click 
                                        OnEvent="btnGuardaForCob_DirectClick" 
                                        Before="var valid= #{StatusForm}.getForm().isValid(); if (valid) {#{FormStatusBar}.showBusy('Guardando Formulario...');} return valid;">
                                        <EventMask 
                                            ShowMask="true" 
                                            MinDelay="1000" 
                                            Target="CustomTarget" 
                                            CustomTarget="={#{StatusForm}.getEl()}" 
                                            />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button runat="server" ID="btnCerrarForCob" Text="Cerrar" />
                        </Buttons>
                    </ext:FormPanel>
                </Items>
                    <BottomBar>
                        <ext:StatusBar ID="FormStatusBar" runat="server" DefaultText="Ready">
                            <Plugins>
                                <ext:ValidationStatus ID="ValidationStatus1" 
                                    runat="server" 
                                    FormPanelID="StatusForm" 
                                    ValidIcon="Accept" 
                                    ErrorIcon="Exclamation" 
                                    ShowText="El Formulario tiene Errores (Click para ver Detalles)"
                                    HideText="Click para Ocultar Errores"
                                    />
                            </Plugins>
                        </ext:StatusBar>
                    </BottomBar>
                </ext:Panel>
            </Items>
        </ext:Window>

    </form>
</body>
</html>
