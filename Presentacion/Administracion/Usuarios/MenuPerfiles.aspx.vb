﻿Imports Ext.Net

Public Class MenuPerfiles
    Inherits System.Web.UI.Page

    Shared estItem As Integer = 0

    Public Shared listMenuPerfil As New List(Of menu)

    Protected Property esData As Integer
        Get
            Return Me.ViewState("esData")
        End Get
        Set(ByVal value As Integer)
            Me.ViewState.Add("esData", value)
        End Set
    End Property

    Protected Property idPerfil As String
        Get
            Return Me.ViewState("idPerfil")
        End Get
        Set(ByVal value As String)
            Me.ViewState.Add("idPerfil", value)
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsPostBack = True Then
            
            If esData <> 1 Then
                listMenuPerfil.Clear()
                esData = 1
                Dim Query As String

                Query = "select a.id_menu,a.descripcion,a.icono,a.tipo,a.padre_menu,a.orden,a.accion,a.nivel," & _
                        "case when b.idperfil is null then 0 else 1 end escoje " & _
                        "from menu_prin as a left join menu_perfil_control as b on a.id_menu = b.id_menu and b.idperfil = '" & cbPerfil.Value & "' " & _
                        "where a.padre_menu = 'PMenu' order by orden"


                Dim listMenu As New List(Of menu)
                Dim cadenaCon As String = ConfigurationManager.ConnectionStrings("ConexionLocal").ConnectionString
                Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(cadenaCon)
                conn.Open()
                Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(Query, conn)
                Dim reader As SqlClient.SqlDataReader = cmd.ExecuteReader
                'listMenuPerfil.Clear()

                While (reader.Read)
                    listMenu.Add(New menu(reader("id_menu"), reader("descripcion"), reader("icono"), reader("tipo"), reader("padre_menu"), reader("orden"), "", reader("escoje")))
                    listMenuPerfil.Add(New menu(reader("id_menu"), reader("descripcion"), reader("icono"), reader("tipo"), reader("padre_menu"), reader("orden"), "", reader("nivel"), reader("escoje")))
                End While
                conn.Close()

                For Each menus As menu In listMenu
                    Dim PanelMenu As New Ext.Net.Panel()
                    PanelMenu.Title = menus.Descripcion
                    PanelMenu.Icon = menus.Icono
                    Me.PMenu.Items.Add(PanelMenu)
                    recorreMenu(menus.Id_menu, menus.Tipo, PanelMenu, New Ext.Net.TreeNode)
                Next
                Dim FuncDat As New Datos.spFunciones
                Dim queryPerfiles As String = "select idperfil, descripcion from menu_perfil"
                Dim ds As DataSet
                ds = FuncDat.executaSelect(queryPerfiles)
                stPerfil.DataSource = ds
                stPerfil.DataBind()
                cbPerfil.Value = idPerfil
            End If
        Else
            Dim FuncDat As New Datos.spFunciones
            Dim queryPerfiles As String = "select idperfil, descripcion from menu_perfil"
            Dim ds As DataSet
            ds = FuncDat.executaSelect(queryPerfiles)
            stPerfil.DataSource = ds
            stPerfil.DataBind()
        End If

    End Sub


    Public Function recorreMenu(ByVal idMenuPadre As String, ByVal tipoPadre As String, ByVal control As Control, ByVal padreNode As Ext.Net.TreeNode) As Boolean
        Dim Query As String


        'Query = "select a.id_menu,a.descripcion,a.icono,a.tipo,a.padre_menu,a.accion,a.orden,a.nivel " & _
        '                     "from menu_prin as a where a.padre_menu = '" & idMenuPadre & "' order by orden"

        Query = "select a.id_menu,a.descripcion,a.icono,a.tipo,a.padre_menu,a.accion,a.orden,a.nivel," & _
        "case when b.idperfil is null then 0 else 1 end escoje from menu_prin as a " & _
        "left join menu_perfil_control as b on a.id_menu = b.id_menu and b.idperfil = '" & cbPerfil.Value & "' " & _
        "where a.padre_menu = '" & idMenuPadre & "' order by orden "




        Dim listMenu As New List(Of menu)
        Dim cadenaCon As String = ConfigurationManager.ConnectionStrings("ConexionLocal").ConnectionString
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(cadenaCon)
        conn.Open()
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(Query, conn)
        Dim reader As SqlClient.SqlDataReader = cmd.ExecuteReader
        Dim root As New Ext.Net.TreeNode()
        While (reader.Read)
            listMenu.Add(New menu(reader("id_menu"), reader("descripcion"), reader("icono"), reader("tipo"), reader("padre_menu"), reader("orden"), reader("accion"), reader("escoje")))
            listMenuPerfil.Add(New menu(reader("id_menu"), reader("descripcion"), reader("icono"), reader("tipo"), reader("padre_menu"), reader("orden"), reader("accion"), reader("nivel"), reader("escoje")))
        End While
        conn.Close()


        For Each menus As menu In listMenu
            If tipoPadre = "Panel" Then
                If menus.Tipo = "TreePanel" Then
                    Dim TreePanel As New Ext.Net.TreePanel
                    TreePanel.ID = menus.Id_menu
                    TreePanel.Width = 297
                    TreePanel.RootVisible = False
                    TreePanel.AutoScroll = True
                    TreePanel.Border = False
                    CType(control, Ext.Net.Panel).Items.Add(TreePanel)
                    recorreMenu(menus.Id_menu, menus.Tipo, TreePanel, New Ext.Net.TreeNode)
                    estItem = 0
                End If
            End If
            If tipoPadre = "TreePanel" Then
                If menus.Tipo = "TreeNode" Then
                    If estItem = 0 Then
                        root.Expanded = True
                        CType(control, Ext.Net.TreePanel).Root.Add(root)
                        estItem = 1
                    End If
                    Dim Node As New Ext.Net.TreeNode()
                    Node.Icon = menus.Icono
                    Node.Text = menus.Descripcion
                    Node.SingleClickExpand = False
                    root.Nodes.Add(Node)
                    recorreMenu(menus.Id_menu, menus.Tipo, New Control, Node)
                    If menus.Accion <> "" Then
                        Node.Leaf = True
                        If menus.Escojer = 1 Then
                            Node.Checked = Ext.Net.ThreeStateBool.True
                        Else
                            Node.Checked = Ext.Net.ThreeStateBool.False
                        End If
                        Node.Listeners.CheckChange.Handler = "node.getUI()[checked ? 'addClass' : 'removeClass']('complete'); Ext.net.DirectMethods.CheckNode('" & menus.Id_menu & "');"
                        'Node.Listeners.Click.Handler = "addTab(#{TabMain}, '" & menus.Id_menu & "', '" & menus.Accion & "?TipoDoc=" & Session("Csc_Usuario") & "', '" & menus.Descripcion & "');"
                    End If
                End If
            End If
            If tipoPadre = "TreeNode" Then
                If menus.Tipo = "TreeNode" Then
                    Dim Node As New Ext.Net.TreeNode()
                    Node.Icon = menus.Icono
                    Node.Text = menus.Descripcion
                    Node.SingleClickExpand = False
                    padreNode.Nodes.Add(Node)
                    If menus.Accion <> "" Then
                        Node.Leaf = True

                        If menus.Escojer = 1 Then
                            Node.Checked = Ext.Net.ThreeStateBool.True
                        Else
                            Node.Checked = Ext.Net.ThreeStateBool.False
                        End If


                        Node.Listeners.CheckChange.Handler = "node.getUI()[checked ? 'addClass' : 'removeClass']('complete'); Ext.net.DirectMethods.CheckNode('" & menus.Id_menu & "');"
                        'Node.Listeners.Click.Handler = "addTab(#{TabMain}, '" & menus.Id_menu & "', '" & menus.Accion & "?TipoDoc=" & Session("Csc_Usuario") & "', '" & menus.Descripcion & "');"
                    End If
                End If
            End If

        Next
        Return True
    End Function

    <DirectMethod()>
    Public Function CheckNode(ByVal id_menu As String)
        For Each menus As menu In listMenuPerfil
            If menus.Id_menu = id_menu Then
                If menus.Escojer = 0 Then
                    menus.Escojer = 1
                Else
                    menus.Escojer = 0
                End If
                escojeMenuPadre(menus.Padre_menu, menus.Escojer, menus.Id_menu)
            End If
        Next
        Return True
    End Function

    Private Function escojeMenuPadre(ByVal idMenu As String, ByVal escojer As Integer, ByVal menuId As String)
        For Each menus As menu In listMenuPerfil
            If menus.Id_menu = idMenu Then
                If menus.Padre_menu <> "" Then
                    If escojer = 1 Then
                        menus.Escojer = 1
                        escojeMenuPadre(menus.Padre_menu, escojer, menus.Id_menu)
                    Else
                        If menusPadre(menus.Id_menu, menuId) = 0 Then
                            menus.Escojer = 0
                            escojeMenuPadre(menus.Padre_menu, escojer, menus.Id_menu)
                        End If
                    End If
                End If
            End If
        Next

        Return True
    End Function

    Private Function menusPadre(ByVal idMenuPadre As String, ByVal idmenu As String) As Integer
        Dim ct As Integer = 0
        For Each menus As menu In listMenuPerfil
            If menus.Padre_menu = idMenuPadre Then
                If menus.Id_menu <> idmenu Then
                    If menus.Escojer = 1 Then
                        ct += 1
                    End If
                End If
                
            End If
        Next
        Return ct
    End Function

    Private Function borrarMenu()
        Dim Query As String = "delete from menu_perfil_control where idperfil = '" & cbPerfil.Value & "'"
        Dim cadenaCon As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("ConexionLocal").ConnectionString
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(cadenaCon)
        conn.Open()
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(Query, conn)
        cmd.ExecuteNonQuery()
        conn.Close()
        Return True
    End Function

    Private Function insertaMenu(ByVal id_menu As String)
        Dim Query As String = "insert into menu_perfil_control(idperfil,id_menu) values('" & cbPerfil.Value & "','" & id_menu & "')"
        Dim cadenaCon As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("ConexionLocal").ConnectionString
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(cadenaCon)
        conn.Open()
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(Query, conn)
        cmd.ExecuteNonQuery()
        conn.Close()
        Return True
    End Function

    Private Sub btnEjecutar_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnEjecutar.DirectClick
        borrarMenu()
        Dim menuMarca As String = ""
        For Each menus As menu In listMenuPerfil
            If menus.Escojer = 1 Then
                insertaMenu(menus.Id_menu)
                'menuMarca &= " - " & menus.Id_menu
            End If
        Next

        Ext.Net.X.Msg.Alert("Información", "Menu Generado").Show()

    End Sub

    Class menu
        Private _id_menu As String
        Private _descripcion As String
        Private _icono As Integer
        Private _tipo As String
        Private _padre_menu As String
        Private _orden As String
        Private _accion As String
        Private _nivel As Integer
        Private _escojer As Integer
        Public Property Accion() As String
            Get
                Return _accion
            End Get
            Set(ByVal value As String)
                _accion = value
            End Set
        End Property
        Public Property Descripcion() As String
            Get
                Return _descripcion
            End Get
            Set(ByVal value As String)
                _descripcion = value
            End Set
        End Property
        Public Property Escojer() As Integer
            Get
                Return _escojer
            End Get
            Set(ByVal value As Integer)
                _escojer = value
            End Set
        End Property
        Public Property Icono() As Integer
            Get
                Return _icono
            End Get
            Set(ByVal value As Integer)
                _icono = value
            End Set
        End Property
        Public Property Id_menu() As String
            Get
                Return _id_menu
            End Get
            Set(ByVal value As String)
                _id_menu = value
            End Set
        End Property
        Public Property Nivel() As Integer
            Get
                Return _nivel
            End Get
            Set(ByVal value As Integer)
                _nivel = value
            End Set
        End Property
        Public Property Orden() As String
            Get
                Return _orden
            End Get
            Set(ByVal value As String)
                _orden = value
            End Set
        End Property
        Public Property Padre_menu() As String
            Get
                Return _padre_menu
            End Get
            Set(ByVal value As String)
                _padre_menu = value
            End Set
        End Property
        Public Property Tipo() As String
            Get
                Return _tipo
            End Get
            Set(ByVal value As String)
                _tipo = value
            End Set
        End Property

        Public Sub New(ByVal id_menu As String,
            ByVal descripcion As String,
            ByVal icono As Integer,
            ByVal tipo As String,
            ByVal padre_menu As String,
            ByVal orden As String,
            ByVal accion As String, ByVal escojer As Integer)
            Me.Id_menu = id_menu
            Me.Descripcion = descripcion
            Me.Icono = icono
            Me.Tipo = tipo
            Me.Padre_menu = padre_menu
            Me.Orden = orden
            Me.Accion = accion
            Me.Escojer = escojer
        End Sub

        Public Sub New(ByVal id_menu As String,
            ByVal descripcion As String,
            ByVal icono As Integer,
            ByVal tipo As String,
            ByVal padre_menu As String,
            ByVal orden As String,
            ByVal accion As String,
            ByVal nivel As Integer, ByVal escojer As Integer)
            Me.Id_menu = id_menu
            Me.Descripcion = descripcion
            Me.Icono = icono
            Me.Tipo = tipo
            Me.Padre_menu = padre_menu
            Me.Orden = orden
            Me.Accion = accion
            Me.Nivel = nivel
            Me.Escojer = escojer
        End Sub
    End Class

    
    Private Sub cbPerfil_DirectSelect(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles cbPerfil.DirectSelect
        esData = 0
        idPerfil = cbPerfil.Value
    End Sub
End Class