﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="actClave.aspx.vb" Inherits="Presentacion.actClave" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body background="../imagenes/bkg-blu.jpg">
    <form id="form1" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" />    
    <ext:Window ID="ActClave" runat="server" Closable="false" Resizable="false"
        Height="340" Icon="Lock" Title="Actualizacíón de Clave" Draggable="false" Width="355"
        Modal="true" BodyPadding="5" Layout="AnchorLayout">
        <Items>
            <ext:FormPanel ID="Window2" runat="server" Layout="AnchorLayout" Border="false" >
                <Items>
                <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:Image ID="Image1" runat="server" ImageUrl="../imagenes/Logo-B3d1.png" AutoHeight="true"
                        AutoWidth="true" >
                        <ResizeConfig ID="ResizeConfig1" runat="server" Adjustments="0, 0" EnableViewState="true"
                            Visible="False">
                        </ResizeConfig>
                    </ext:Image>
                    </Items>
                 </ext:Toolbar>   
                </Items>
            </ext:FormPanel>            
            <ext:FieldSet runat="server" Border="false" AutoHeight="true" AutoWidth="False" X="0"
                Y="0" LabelPad="0" MonitorResize="True" Frame="True" Padding="20" LabelWidth="100"
                ID="ctl38">
                <Items>
                    <ext:TextField runat="server" ID="txtPassActual" FieldLabel="Clave Actual" InputType="Password" />
                    <ext:TextField runat="server" ID="txtPassNew" FieldLabel="Nueva Clave" InputType="Password" />
                    <ext:TextField runat="server" ID="txtPassNew2" FieldLabel="Repetir Clave" InputType="Password" /> 
                    <ext:TextField runat="server" ID="txtEmail" FieldLabel="Correo Electronico" />                   
                </Items>
            </ext:FieldSet>
        </Items>
        <Buttons>
            <ext:Button ID="btnEjecutar" runat="server" Text="Ejecutar Cambios" Icon="Star" />
        </Buttons>
    </ext:Window>
        
    </form>
</body>
</html>
