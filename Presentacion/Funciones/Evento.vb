﻿Public Class Evento
    Public Property EventId() As Integer
        Get
            Return m_EventId
        End Get
        Set(ByVal value As Integer)
            m_EventId = value
        End Set
    End Property
    Private m_EventId As Integer
    Public Property CalendarId() As Integer
        Get
            Return m_CalendarId
        End Get
        Set(ByVal value As Integer)
            m_CalendarId = value
        End Set
    End Property
    Private m_CalendarId As Integer
    Public Property EndDate() As DateTime
        Get
            Return m_EndDate
        End Get
        Set(ByVal value As DateTime)
            m_EndDate = value
        End Set
    End Property
    Private m_EndDate As DateTime
    Public Property StartDate() As DateTime
        Get
            Return m_StartDate
        End Get
        Set(ByVal value As DateTime)
            m_StartDate = value
        End Set
    End Property
    Private m_StartDate As DateTime
    Public Property IsAllDay() As Boolean
        Get
            Return m_IsAllDay
        End Get
        Set(ByVal value As Boolean)
            m_IsAllDay = value
        End Set
    End Property
    Private m_IsAllDay As Boolean
    Public Property IsNew() As Boolean
        Get
            Return m_IsNew
        End Get
        Set(ByVal value As Boolean)
            m_IsNew = value
        End Set
    End Property
    Private m_IsNew As Boolean
    Public Property Location() As String
        Get
            Return m_Location
        End Get
        Set(ByVal value As String)
            m_Location = value
        End Set
    End Property
    Private m_Location As String
    Public Property Notes() As String
        Get
            Return m_Notes
        End Get
        Set(ByVal value As String)
            m_Notes = value
        End Set
    End Property
    Private m_Notes As String
    Public Property Reminder() As String
        Get
            Return m_Reminder
        End Get
        Set(ByVal value As String)
            m_Reminder = value
        End Set
    End Property
    Private m_Reminder As String
    Public Property Title() As String
        Get
            Return m_Title
        End Get
        Set(ByVal value As String)
            m_Title = value
        End Set
    End Property
    Private m_Title As String
    Public Property Url() As String
        Get
            Return m_Url
        End Get
        Set(ByVal value As String)
            m_Url = value
        End Set
    End Property
    Private m_Url As String
    Public Property MyCustomField1() As Integer
        Get
            Return m_MyCustomField1
        End Get
        Set(ByVal value As Integer)
            m_MyCustomField1 = value
        End Set
    End Property
    Private m_MyCustomField1 As Integer
    Public Property Csc_Paciente() As Integer
        Get
            Return m_Csc_Paciente
        End Get
        Set(ByVal value As Integer)
            m_Csc_Paciente = value
        End Set
    End Property
    Private m_Csc_Paciente As Integer


    'Public Sub New(ByVal EventId As Integer, ByVal CalendarId As Integer, ByVal EndDate As DateTime, ByVal StartDate As DateTime, ByVal IsAllDay As Boolean, ByVal IsNew As Boolean,
    '               ByVal Location As Boolean, ByVal Notes As String, ByVal Reminder As String, ByVal Title As String, ByVal Url As String, ByVal MyCustomField1 As Integer)
    '    Me.EventId = EventId
    '    Me.CalendarId = CalendarId
    '    Me.EndDate = EndDate
    '    Me.StartDate = StartDate
    '    Me.IsAllDay = IsAllDay
    '    Me.IsNew = IsNew
    '    Me.Location = Location
    '    Me.Notes = Notes
    '    Me.Reminder = Reminder
    '    Me.Title = Title
    '    Me.Url = Url
    '    Me.MyCustomField1 = MyCustomField1


    'End Sub

End Class
