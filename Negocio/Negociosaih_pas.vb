Public Class Negociosaih_pas

  Public Function Altasaih_pas(
      ByVal codigo As string,
      ByVal identificacion As string,
      ByVal id_lugarexp As integer,
      ByVal num_registro_med As string,
      ByVal nombres As string,
      ByVal direccion As string,
      ByVal telefono As string,
      ByVal cod_clase As string,
      ByVal cod_especialidad As string,
      ByVal direccion_off As string,
      ByVal telefono_off As string,
      ByVal num_citas_dia As integer,
      ByVal estado As string,
      ByVal duracion As integer,
      ByVal foto As string,
      ByVal firma As string,
      ByVal huella As string
    )
      Dim saih_pas As New Datos.saih_pas(codigo,identificacion,id_lugarexp,num_registro_med,nombres,direccion,telefono,cod_clase,cod_especialidad,direccion_off,telefono_off,num_citas_dia,estado,duracion,foto,firma,huella)
        Dim dtsaih_pas As New Datos.Datosaih_pas
        Return dtsaih_pas.Insertar(saih_pas)
    End Function

  Public Function AltaRetsaih_pas(
      ByVal codigo As string,
      ByVal identificacion As string,
      ByVal id_lugarexp As integer,
      ByVal num_registro_med As string,
      ByVal nombres As string,
      ByVal direccion As string,
      ByVal telefono As string,
      ByVal cod_clase As string,
      ByVal cod_especialidad As string,
      ByVal direccion_off As string,
      ByVal telefono_off As string,
      ByVal num_citas_dia As integer,
      ByVal estado As string,
      ByVal duracion As integer,
      ByVal foto As string,
      ByVal firma As string,
      ByVal huella As string
    ) As Integer
      Dim saih_pas As New Datos.saih_pas(codigo,identificacion,id_lugarexp,num_registro_med,nombres,direccion,telefono,cod_clase,cod_especialidad,direccion_off,telefono_off,num_citas_dia,estado,duracion,foto,firma,huella)
        Dim dtsaih_pas As New Datos.Datosaih_pas
        Return dtsaih_pas.InsertarRet(saih_pas)
    End Function

  Public Function Editasaih_pas(
      ByVal codigo As string,
      ByVal identificacion As string,
      ByVal id_lugarexp As integer,
      ByVal num_registro_med As string,
      ByVal nombres As string,
      ByVal direccion As string,
      ByVal telefono As string,
      ByVal cod_clase As string,
      ByVal cod_especialidad As string,
      ByVal direccion_off As string,
      ByVal telefono_off As string,
      ByVal num_citas_dia As integer,
      ByVal estado As string,
      ByVal duracion As integer,
      ByVal foto As string,
      ByVal firma As string,
      ByVal huella As string
    )
      Dim saih_pas As New Datos.saih_pas(codigo,identificacion,id_lugarexp,num_registro_med,nombres,direccion,telefono,cod_clase,cod_especialidad,direccion_off,telefono_off,num_citas_dia,estado,duracion,foto,firma,huella)
        Dim dtsaih_pas As New Datos.Datosaih_pas
        Return dtsaih_pas.Editar(saih_pas)
    End Function

 Public Function Eliminasaih_pas(
      ByVal codigo As string)
        Dim dtsaih_pas As New Datos.Datosaih_pas
        Return dtsaih_pas.Borrar(codigo)
    End Function

  Public Function Obtenersaih_pasById(
      ByVal codigo As string) As Datos.saih_pas
        Dim saih_pas As Datos.saih_pas = Nothing
        Dim dtsaih_pas As New Datos.Datosaih_pas
        For Each saih_pas In dtsaih_pas.Select_ById_saih_pas(codigo)
        Next
        Return saih_pas
    End Function

  Public Function Obtenersaih_pas() As List(Of Datos.saih_pas)
        Dim dtsaih_pas As New Datos.Datosaih_pas
        Return dtsaih_pas.Select_saih_pas()
    End Function

  Public Function Obtenersaih_pasbyWhere(ByVal sWhere as String) As List(Of Datos.saih_pas)
        Dim dtsaih_pas As New Datos.Datosaih_pas
        Return dtsaih_pas.Select_Where_saih_pas(sWhere)
    End Function
End Class