Public Class Negociousuario_log

  Public Function Altausuario_log(
      ByVal id As integer,
      ByVal Usuario As string,
      ByVal fecha As string,
      ByVal ip As string,
      ByVal host As string,
      ByVal mac As string,
      ByVal tipo As string
    )
      Dim usuario_log As New Datos.usuario_log(id,Usuario,fecha,ip,host,mac,tipo)
        Dim dtusuario_log As New Datos.Datousuario_log
        Return dtusuario_log.Insertar(usuario_log)
    End Function

  Public Function AltaRetusuario_log(
      ByVal id As integer,
      ByVal Usuario As string,
      ByVal fecha As string,
      ByVal ip As string,
      ByVal host As string,
      ByVal mac As string,
      ByVal tipo As string
    ) As Integer
      Dim usuario_log As New Datos.usuario_log(id,Usuario,fecha,ip,host,mac,tipo)
        Dim dtusuario_log As New Datos.Datousuario_log
        Return dtusuario_log.InsertarRet(usuario_log)
    End Function

  Public Function Editausuario_log(
      ByVal id As integer,
      ByVal Usuario As string,
      ByVal fecha As string,
      ByVal ip As string,
      ByVal host As string,
      ByVal mac As string,
      ByVal tipo As string
    )
      Dim usuario_log As New Datos.usuario_log(id,Usuario,fecha,ip,host,mac,tipo)
        Dim dtusuario_log As New Datos.Datousuario_log
        Return dtusuario_log.Editar(usuario_log)
    End Function

 Public Function Eliminausuario_log(
      ByVal id As integer)
        Dim dtusuario_log As New Datos.Datousuario_log
        Return dtusuario_log.Borrar(id)
    End Function

  Public Function Obtenerusuario_logById(
      ByVal id As integer) As Datos.usuario_log
        Dim usuario_log As Datos.usuario_log = Nothing
        Dim dtusuario_log As New Datos.Datousuario_log
        For Each usuario_log In dtusuario_log.Select_ById_usuario_log(id)
        Next
        Return usuario_log
    End Function

  Public Function Obtenerusuario_log() As List(Of Datos.usuario_log)
        Dim dtusuario_log As New Datos.Datousuario_log
        Return dtusuario_log.Select_usuario_log()
    End Function

  Public Function Obtenerusuario_logbyWhere(ByVal sWhere as String) As List(Of Datos.usuario_log)
        Dim dtusuario_log As New Datos.Datousuario_log
        Return dtusuario_log.Select_Where_usuario_log(sWhere)
    End Function
End Class