Public Class Negociocama

  Public Function Altacama(
      ByVal codigo As string,
      ByVal codigo_area As string,
      ByVal num_admision As string,
      ByVal manual As string,
      ByVal codpcd As string,
      ByVal codser As string
    )
      Dim cama As New Datos.cama(codigo,codigo_area,num_admision,manual,codpcd,codser)
        Dim dtcama As New Datos.Datocama
        Return dtcama.Insertar(cama)
    End Function

  Public Function Altacama(
      ByVal cama As Datos.cama    
)
        Dim dtcama As New Datos.Datocama
        Return dtcama.Insertar(cama)
    End Function

  Public Function AltaRetcama(
      ByVal codigo As string,
      ByVal codigo_area As string,
      ByVal num_admision As string,
      ByVal manual As string,
      ByVal codpcd As string,
      ByVal codser As string
    ) As Integer
      Dim cama As New Datos.cama(codigo,codigo_area,num_admision,manual,codpcd,codser)
        Dim dtcama As New Datos.Datocama
        Return dtcama.InsertarRet(cama)
    End Function

  Public Function AltaRetcama(
      ByVal cama As Datos.cama    
)
        Dim dtcama As New Datos.Datocama
        Return dtcama.InsertarRet(cama)
    End Function

  Public Function Editacama(
      ByVal codigo As string,
      ByVal codigo_area As string,
      ByVal num_admision As string,
      ByVal manual As string,
      ByVal codpcd As string,
      ByVal codser As string
    )
      Dim cama As New Datos.cama(codigo,codigo_area,num_admision,manual,codpcd,codser)
        Dim dtcama As New Datos.Datocama
        Return dtcama.Editar(cama)
    End Function

  Public Function Editacama(
      ByVal cama As Datos.cama    
)
        Dim dtcama As New Datos.Datocama
        Return dtcama.Editar(cama)
    End Function

 Public Function Eliminacama(
      ByVal codigo As string)
        Dim dtcama As New Datos.Datocama
        Return dtcama.Borrar(codigo)
    End Function

  Public Function ObtenercamaById(
      ByVal codigo As string) As Datos.cama
        Dim cama As Datos.cama = Nothing
        Dim dtcama As New Datos.Datocama
        For Each cama In dtcama.Select_ById_cama(codigo)
        Next
        Return cama
    End Function

  Public Function Obtenercama() As List(Of Datos.cama)
        Dim dtcama As New Datos.Datocama
        Return dtcama.Select_cama()
    End Function

  Public Function ObtenercamabyWhere(ByVal sWhere as String) As List(Of Datos.cama)
        Dim dtcama As New Datos.Datocama
        Return dtcama.Select_Where_cama(sWhere)
    End Function
End Class