Public Class Negociocama_procedimiento

  Public Function Altacama_procedimiento(
      ByVal codigo_cama As string,
      ByVal manual As string,
      ByVal codpcd As string
    )
      Dim cama_procedimiento As New Datos.cama_procedimiento(codigo_cama,manual,codpcd)
        Dim dtcama_procedimiento As New Datos.Datocama_procedimiento
        Return dtcama_procedimiento.Insertar(cama_procedimiento)
    End Function

  Public Function Altacama_procedimiento(
      ByVal cama_procedimiento As Datos.cama_procedimiento    
)
        Dim dtcama_procedimiento As New Datos.Datocama_procedimiento
        Return dtcama_procedimiento.Insertar(cama_procedimiento)
    End Function

  Public Function AltaRetcama_procedimiento(
      ByVal codigo_cama As string,
      ByVal manual As string,
      ByVal codpcd As string
    ) As Integer
      Dim cama_procedimiento As New Datos.cama_procedimiento(codigo_cama,manual,codpcd)
        Dim dtcama_procedimiento As New Datos.Datocama_procedimiento
        Return dtcama_procedimiento.InsertarRet(cama_procedimiento)
    End Function

  Public Function AltaRetcama_procedimiento(
      ByVal cama_procedimiento As Datos.cama_procedimiento    
)
        Dim dtcama_procedimiento As New Datos.Datocama_procedimiento
        Return dtcama_procedimiento.InsertarRet(cama_procedimiento)
    End Function

  Public Function Editacama_procedimiento(
      ByVal codigo_cama As string,
      ByVal manual As string,
      ByVal codpcd As string
    )
      Dim cama_procedimiento As New Datos.cama_procedimiento(codigo_cama,manual,codpcd)
        Dim dtcama_procedimiento As New Datos.Datocama_procedimiento
        Return dtcama_procedimiento.Editar(cama_procedimiento)
    End Function

  Public Function Editacama_procedimiento(
      ByVal cama_procedimiento As Datos.cama_procedimiento    
)
        Dim dtcama_procedimiento As New Datos.Datocama_procedimiento
        Return dtcama_procedimiento.Editar(cama_procedimiento)
    End Function

 Public Function Eliminacama_procedimiento(
      ByVal codigo_cama As string,
      ByVal manual As string,
      ByVal codpcd As string)
        Dim dtcama_procedimiento As New Datos.Datocama_procedimiento
        Return dtcama_procedimiento.Borrar(codigo_cama,manual,codpcd)
    End Function

  Public Function Obtenercama_procedimientoById(
      ByVal codigo_cama As string,
      ByVal manual As string,
      ByVal codpcd As string) As Datos.cama_procedimiento
        Dim cama_procedimiento As Datos.cama_procedimiento = Nothing
        Dim dtcama_procedimiento As New Datos.Datocama_procedimiento
        For Each cama_procedimiento In dtcama_procedimiento.Select_ById_cama_procedimiento(codigo_cama,manual,codpcd)
        Next
        Return cama_procedimiento
    End Function

  Public Function Obtenercama_procedimiento() As List(Of Datos.cama_procedimiento)
        Dim dtcama_procedimiento As New Datos.Datocama_procedimiento
        Return dtcama_procedimiento.Select_cama_procedimiento()
    End Function

  Public Function Obtenercama_procedimientobyWhere(ByVal sWhere as String) As List(Of Datos.cama_procedimiento)
        Dim dtcama_procedimiento As New Datos.Datocama_procedimiento
        Return dtcama_procedimiento.Select_Where_cama_procedimiento(sWhere)
    End Function
End Class