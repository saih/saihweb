Public Class Negociocajero

  Public Function Altacajero(
      ByVal codigo_cajero As string,
      ByVal usuario As string,
      ByVal base As decimal,
      ByVal fecha_apertura As string,
      ByVal saldo As decimal,
      ByVal estado As string
    )
      Dim cajero As New Datos.cajero(codigo_cajero,usuario,base,fecha_apertura,saldo,estado)
        Dim dtcajero As New Datos.Datocajero
        Return dtcajero.Insertar(cajero)
    End Function

  Public Function Altacajero(
      ByVal cajero As Datos.cajero    
)
        Dim dtcajero As New Datos.Datocajero
        Return dtcajero.Insertar(cajero)
    End Function

  Public Function AltaRetcajero(
      ByVal codigo_cajero As string,
      ByVal usuario As string,
      ByVal base As decimal,
      ByVal fecha_apertura As string,
      ByVal saldo As decimal,
      ByVal estado As string
    ) As Integer
      Dim cajero As New Datos.cajero(codigo_cajero,usuario,base,fecha_apertura,saldo,estado)
        Dim dtcajero As New Datos.Datocajero
        Return dtcajero.InsertarRet(cajero)
    End Function

  Public Function AltaRetcajero(
      ByVal cajero As Datos.cajero    
)
        Dim dtcajero As New Datos.Datocajero
        Return dtcajero.InsertarRet(cajero)
    End Function

  Public Function Editacajero(
      ByVal codigo_cajero As string,
      ByVal usuario As string,
      ByVal base As decimal,
      ByVal fecha_apertura As string,
      ByVal saldo As decimal,
      ByVal estado As string
    )
      Dim cajero As New Datos.cajero(codigo_cajero,usuario,base,fecha_apertura,saldo,estado)
        Dim dtcajero As New Datos.Datocajero
        Return dtcajero.Editar(cajero)
    End Function

  Public Function Editacajero(
      ByVal cajero As Datos.cajero    
)
        Dim dtcajero As New Datos.Datocajero
        Return dtcajero.Editar(cajero)
    End Function

 Public Function Eliminacajero(
      ByVal codigo_cajero As string)
        Dim dtcajero As New Datos.Datocajero
        Return dtcajero.Borrar(codigo_cajero)
    End Function

  Public Function ObtenercajeroById(
      ByVal codigo_cajero As string) As Datos.cajero
        Dim cajero As Datos.cajero = Nothing
        Dim dtcajero As New Datos.Datocajero
        For Each cajero In dtcajero.Select_ById_cajero(codigo_cajero)
        Next
        Return cajero
    End Function

  Public Function Obtenercajero() As List(Of Datos.cajero)
        Dim dtcajero As New Datos.Datocajero
        Return dtcajero.Select_cajero()
    End Function

  Public Function ObtenercajerobyWhere(ByVal sWhere as String) As List(Of Datos.cajero)
        Dim dtcajero As New Datos.Datocajero
        Return dtcajero.Select_Where_cajero(sWhere)
    End Function
End Class