Public Class Negociofamilia_esp

  Public Function Altafamilia_esp(
      ByVal id_familia As integer,
      ByVal cod_especialidad As string,
      ByVal codpas As string,
      ByVal fecha_adicion As string,
      ByVal usuario_creo As string,
      ByVal fecha_modificacion As string,
      ByVal usuario_modifico As string
    )
      Dim familia_esp As New Datos.familia_esp(id_familia,cod_especialidad,codpas,fecha_adicion,usuario_creo,fecha_modificacion,usuario_modifico)
        Dim dtfamilia_esp As New Datos.Datofamilia_esp
        Return dtfamilia_esp.Insertar(familia_esp)
    End Function

  Public Function Altafamilia_esp(
      ByVal familia_esp As Datos.familia_esp    
)
        Dim dtfamilia_esp As New Datos.Datofamilia_esp
        Return dtfamilia_esp.Insertar(familia_esp)
    End Function

  Public Function AltaRetfamilia_esp(
      ByVal id_familia As integer,
      ByVal cod_especialidad As string,
      ByVal codpas As string,
      ByVal fecha_adicion As string,
      ByVal usuario_creo As string,
      ByVal fecha_modificacion As string,
      ByVal usuario_modifico As string
    ) As Integer
      Dim familia_esp As New Datos.familia_esp(id_familia,cod_especialidad,codpas,fecha_adicion,usuario_creo,fecha_modificacion,usuario_modifico)
        Dim dtfamilia_esp As New Datos.Datofamilia_esp
        Return dtfamilia_esp.InsertarRet(familia_esp)
    End Function

  Public Function AltaRetfamilia_esp(
      ByVal familia_esp As Datos.familia_esp    
)
        Dim dtfamilia_esp As New Datos.Datofamilia_esp
        Return dtfamilia_esp.InsertarRet(familia_esp)
    End Function

  Public Function Editafamilia_esp(
      ByVal id_familia As integer,
      ByVal cod_especialidad As string,
      ByVal codpas As string,
      ByVal fecha_adicion As string,
      ByVal usuario_creo As string,
      ByVal fecha_modificacion As string,
      ByVal usuario_modifico As string
    )
      Dim familia_esp As New Datos.familia_esp(id_familia,cod_especialidad,codpas,fecha_adicion,usuario_creo,fecha_modificacion,usuario_modifico)
        Dim dtfamilia_esp As New Datos.Datofamilia_esp
        Return dtfamilia_esp.Editar(familia_esp)
    End Function

  Public Function Editafamilia_esp(
      ByVal familia_esp As Datos.familia_esp    
)
        Dim dtfamilia_esp As New Datos.Datofamilia_esp
        Return dtfamilia_esp.Editar(familia_esp)
    End Function

 Public Function Eliminafamilia_esp(
      ByVal id_familia As integer,
      ByVal cod_especialidad As string)
        Dim dtfamilia_esp As New Datos.Datofamilia_esp
        Return dtfamilia_esp.Borrar(id_familia,cod_especialidad)
    End Function

  Public Function Obtenerfamilia_espById(
      ByVal id_familia As integer,
      ByVal cod_especialidad As string) As Datos.familia_esp
        Dim familia_esp As Datos.familia_esp = Nothing
        Dim dtfamilia_esp As New Datos.Datofamilia_esp
        For Each familia_esp In dtfamilia_esp.Select_ById_familia_esp(id_familia,cod_especialidad)
        Next
        Return familia_esp
    End Function

  Public Function Obtenerfamilia_esp() As List(Of Datos.familia_esp)
        Dim dtfamilia_esp As New Datos.Datofamilia_esp
        Return dtfamilia_esp.Select_familia_esp()
    End Function

  Public Function Obtenerfamilia_espbyWhere(ByVal sWhere as String) As List(Of Datos.familia_esp)
        Dim dtfamilia_esp As New Datos.Datofamilia_esp
        Return dtfamilia_esp.Select_Where_familia_esp(sWhere)
    End Function
End Class