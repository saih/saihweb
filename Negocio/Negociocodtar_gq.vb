Public Class Negociocodtar_gq

  Public Function Altacodtar_gq(
      ByVal manual As string,
      ByVal codigo As string,
      ByVal nombre As string,
      ByVal honmed As string,
      ByVal honane As string,
      ByVal dersal As string,
      ByVal dersalesp As string,
      ByVal dersalend As string,
      ByVal matsum As string,
      ByVal mataneloc As string,
      ByVal honayu As string,
      ByVal honper As string
    )
      Dim codtar_gq As New Datos.codtar_gq(manual,codigo,nombre,honmed,honane,dersal,dersalesp,dersalend,matsum,mataneloc,honayu,honper)
        Dim dtcodtar_gq As New Datos.Datocodtar_gq
        Return dtcodtar_gq.Insertar(codtar_gq)
    End Function

  Public Function Altacodtar_gq(
      ByVal codtar_gq As Datos.codtar_gq    
)
        Dim dtcodtar_gq As New Datos.Datocodtar_gq
        Return dtcodtar_gq.Insertar(codtar_gq)
    End Function

  Public Function AltaRetcodtar_gq(
      ByVal manual As string,
      ByVal codigo As string,
      ByVal nombre As string,
      ByVal honmed As string,
      ByVal honane As string,
      ByVal dersal As string,
      ByVal dersalesp As string,
      ByVal dersalend As string,
      ByVal matsum As string,
      ByVal mataneloc As string,
      ByVal honayu As string,
      ByVal honper As string
    ) As Integer
      Dim codtar_gq As New Datos.codtar_gq(manual,codigo,nombre,honmed,honane,dersal,dersalesp,dersalend,matsum,mataneloc,honayu,honper)
        Dim dtcodtar_gq As New Datos.Datocodtar_gq
        Return dtcodtar_gq.InsertarRet(codtar_gq)
    End Function

  Public Function AltaRetcodtar_gq(
      ByVal codtar_gq As Datos.codtar_gq    
)
        Dim dtcodtar_gq As New Datos.Datocodtar_gq
        Return dtcodtar_gq.InsertarRet(codtar_gq)
    End Function

  Public Function Editacodtar_gq(
      ByVal manual As string,
      ByVal codigo As string,
      ByVal nombre As string,
      ByVal honmed As string,
      ByVal honane As string,
      ByVal dersal As string,
      ByVal dersalesp As string,
      ByVal dersalend As string,
      ByVal matsum As string,
      ByVal mataneloc As string,
      ByVal honayu As string,
      ByVal honper As string
    )
      Dim codtar_gq As New Datos.codtar_gq(manual,codigo,nombre,honmed,honane,dersal,dersalesp,dersalend,matsum,mataneloc,honayu,honper)
        Dim dtcodtar_gq As New Datos.Datocodtar_gq
        Return dtcodtar_gq.Editar(codtar_gq)
    End Function

  Public Function Editacodtar_gq(
      ByVal codtar_gq As Datos.codtar_gq    
)
        Dim dtcodtar_gq As New Datos.Datocodtar_gq
        Return dtcodtar_gq.Editar(codtar_gq)
    End Function

 Public Function Eliminacodtar_gq(
      ByVal manual As string,
      ByVal codigo As string)
        Dim dtcodtar_gq As New Datos.Datocodtar_gq
        Return dtcodtar_gq.Borrar(manual,codigo)
    End Function

  Public Function Obtenercodtar_gqById(
      ByVal manual As string,
      ByVal codigo As string) As Datos.codtar_gq
        Dim codtar_gq As Datos.codtar_gq = Nothing
        Dim dtcodtar_gq As New Datos.Datocodtar_gq
        For Each codtar_gq In dtcodtar_gq.Select_ById_codtar_gq(manual,codigo)
        Next
        Return codtar_gq
    End Function

  Public Function Obtenercodtar_gq() As List(Of Datos.codtar_gq)
        Dim dtcodtar_gq As New Datos.Datocodtar_gq
        Return dtcodtar_gq.Select_codtar_gq()
    End Function

  Public Function Obtenercodtar_gqbyWhere(ByVal sWhere as String) As List(Of Datos.codtar_gq)
        Dim dtcodtar_gq As New Datos.Datocodtar_gq
        Return dtcodtar_gq.Select_Where_codtar_gq(sWhere)
    End Function
End Class