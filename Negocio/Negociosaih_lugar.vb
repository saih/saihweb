Public Class Negociosaih_lugar

  Public Function Altasaih_lugar(
      ByVal Id As integer,
      ByVal Tipo As integer,
      ByVal Descripcion As string,
      ByVal Id_Lugar As integer,
      ByVal Codigo As string
    )
      Dim saih_lugar As New Datos.saih_lugar(Id,Tipo,Descripcion,Id_Lugar,Codigo)
        Dim dtsaih_lugar As New Datos.Datosaih_lugar
        Return dtsaih_lugar.Insertar(saih_lugar)
    End Function

  Public Function AltaRetsaih_lugar(
      ByVal Id As integer,
      ByVal Tipo As integer,
      ByVal Descripcion As string,
      ByVal Id_Lugar As integer,
      ByVal Codigo As string
    ) As Integer
      Dim saih_lugar As New Datos.saih_lugar(Id,Tipo,Descripcion,Id_Lugar,Codigo)
        Dim dtsaih_lugar As New Datos.Datosaih_lugar
        Return dtsaih_lugar.InsertarRet(saih_lugar)
    End Function

  Public Function Editasaih_lugar(
      ByVal Id As integer,
      ByVal Tipo As integer,
      ByVal Descripcion As string,
      ByVal Id_Lugar As integer,
      ByVal Codigo As string
    )
      Dim saih_lugar As New Datos.saih_lugar(Id,Tipo,Descripcion,Id_Lugar,Codigo)
        Dim dtsaih_lugar As New Datos.Datosaih_lugar
        Return dtsaih_lugar.Editar(saih_lugar)
    End Function

 Public Function Eliminasaih_lugar(
      ByVal Id As integer)
        Dim dtsaih_lugar As New Datos.Datosaih_lugar
        Return dtsaih_lugar.Borrar(Id)
    End Function

  Public Function Obtenersaih_lugarById(
      ByVal Id As integer) As Datos.saih_lugar
        Dim saih_lugar As Datos.saih_lugar = Nothing
        Dim dtsaih_lugar As New Datos.Datosaih_lugar
        For Each saih_lugar In dtsaih_lugar.Select_ById_saih_lugar(Id)
        Next
        Return saih_lugar
    End Function

  Public Function Obtenersaih_lugar() As List(Of Datos.saih_lugar)
        Dim dtsaih_lugar As New Datos.Datosaih_lugar
        Return dtsaih_lugar.Select_saih_lugar()
    End Function

  Public Function Obtenersaih_lugarbyWhere(ByVal sWhere as String) As List(Of Datos.saih_lugar)
        Dim dtsaih_lugar As New Datos.Datosaih_lugar
        Return dtsaih_lugar.Select_Where_saih_lugar(sWhere)
    End Function
End Class