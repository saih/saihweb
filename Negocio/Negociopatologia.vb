Public Class Negociopatologia

  Public Function Altapatologia(
      ByVal codigo As string,
      ByVal nombre As string,
      ByVal aplica_sexo As string,
      ByVal edad_inicial As integer,
      ByVal edad_final As integer,
      ByVal ind_patep As integer
    )
      Dim patologia As New Datos.patologia(codigo,nombre,aplica_sexo,edad_inicial,edad_final,ind_patep)
        Dim dtpatologia As New Datos.Datopatologia
        Return dtpatologia.Insertar(patologia)
    End Function

  Public Function Altapatologia(
      ByVal patologia As Datos.patologia    
)
        Dim dtpatologia As New Datos.Datopatologia
        Return dtpatologia.Insertar(patologia)
    End Function

  Public Function AltaRetpatologia(
      ByVal codigo As string,
      ByVal nombre As string,
      ByVal aplica_sexo As string,
      ByVal edad_inicial As integer,
      ByVal edad_final As integer,
      ByVal ind_patep As integer
    ) As Integer
      Dim patologia As New Datos.patologia(codigo,nombre,aplica_sexo,edad_inicial,edad_final,ind_patep)
        Dim dtpatologia As New Datos.Datopatologia
        Return dtpatologia.InsertarRet(patologia)
    End Function

  Public Function AltaRetpatologia(
      ByVal patologia As Datos.patologia    
)
        Dim dtpatologia As New Datos.Datopatologia
        Return dtpatologia.InsertarRet(patologia)
    End Function

  Public Function Editapatologia(
      ByVal codigo As string,
      ByVal nombre As string,
      ByVal aplica_sexo As string,
      ByVal edad_inicial As integer,
      ByVal edad_final As integer,
      ByVal ind_patep As integer
    )
      Dim patologia As New Datos.patologia(codigo,nombre,aplica_sexo,edad_inicial,edad_final,ind_patep)
        Dim dtpatologia As New Datos.Datopatologia
        Return dtpatologia.Editar(patologia)
    End Function

  Public Function Editapatologia(
      ByVal patologia As Datos.patologia    
)
        Dim dtpatologia As New Datos.Datopatologia
        Return dtpatologia.Editar(patologia)
    End Function

 Public Function Eliminapatologia(
      ByVal codigo As string)
        Dim dtpatologia As New Datos.Datopatologia
        Return dtpatologia.Borrar(codigo)
    End Function

  Public Function ObtenerpatologiaById(
      ByVal codigo As string) As Datos.patologia
        Dim patologia As Datos.patologia = Nothing
        Dim dtpatologia As New Datos.Datopatologia
        For Each patologia In dtpatologia.Select_ById_patologia(codigo)
        Next
        Return patologia
    End Function

  Public Function Obtenerpatologia() As List(Of Datos.patologia)
        Dim dtpatologia As New Datos.Datopatologia
        Return dtpatologia.Select_patologia()
    End Function

  Public Function ObtenerpatologiabyWhere(ByVal sWhere as String) As List(Of Datos.patologia)
        Dim dtpatologia As New Datos.Datopatologia
        Return dtpatologia.Select_Where_patologia(sWhere)
    End Function
End Class