Public Class NegocioPaginaWeb

    Public Function AltaPaginaWeb(
        ByVal id As Integer,
        ByVal nombre As String
      )
        Dim PaginaWeb As New Datos.PaginaWeb(id, nombre)
        Dim dtPaginaWeb As New Datos.DatoPaginaWeb
        Return dtPaginaWeb.Insertar(PaginaWeb)
    End Function

    Public Function AltaRetPaginaWeb(
        ByVal id As Integer,
        ByVal nombre As String
      ) As Integer
        Dim PaginaWeb As New Datos.PaginaWeb(id, nombre)
        Dim dtPaginaWeb As New Datos.DatoPaginaWeb

        Return dtPaginaWeb.InsertarRet(PaginaWeb)
    End Function

    'Public Function alteRetPaginaWeg(ByVal id As Integer, ByVal nombre As String
    '                                )

    'End Function

    Public Function EditaPaginaWeb(
        ByVal id As Integer,
        ByVal nombre As String
      )
        Dim PaginaWeb As New Datos.PaginaWeb(id, nombre)
        Dim dtPaginaWeb As New Datos.DatoPaginaWeb
        dtPaginaWeb.Editar(PaginaWeb)
        Return True
    End Function

    Public Function ObtenerPaginaWebById(
        ByVal id As Integer) As Datos.PaginaWeb
        Dim PaginaWeb As Datos.PaginaWeb = Nothing
        Dim dtPaginaWeb As New Datos.DatoPaginaWeb
        For Each PaginaWeb In dtPaginaWeb.Select_ById_PaginaWeb(id)
        Next
        Return PaginaWeb
    End Function

    Public Function ObtenerPaginaWeb() As List(Of Datos.PaginaWeb)
        Dim dtPaginaWeb As New Datos.DatoPaginaWeb
        Return dtPaginaWeb.Select_PaginaWeb()
    End Function

    Public Function ObtenerPaginaWebbyWhere(ByVal sWhere As String) As List(Of Datos.PaginaWeb)
        Dim dtPaginaWeb As New Datos.DatoPaginaWeb
        Return dtPaginaWeb.Select_Where_PaginaWeb(sWhere)
    End Function
End Class