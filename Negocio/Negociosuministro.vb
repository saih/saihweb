Public Class Negociosuministro

  Public Function Altasuministro(
      ByVal codigo As string,
      ByVal nombre As string,
      ByVal codana As string,
      ByVal presentacion As string,
      ByVal und_concentracion As string,
      ByVal concentracion As string,
      ByVal clase As string,
      ByVal codser As string,
      ByVal valor As decimal,
      ByVal cod_centrocosto As string,
      ByVal cod_undfunc As string,
      ByVal cod_material As string,
      ByVal iva As decimal,
      ByVal cod_generico As string,
      ByVal nom_generico As string,
      ByVal tipo_imp As string,
      ByVal poso As string
    )
      Dim suministro As New Datos.suministro(codigo,nombre,codana,presentacion,und_concentracion,concentracion,clase,codser,valor,cod_centrocosto,cod_undfunc,cod_material,iva,cod_generico,nom_generico,tipo_imp,poso)
        Dim dtsuministro As New Datos.Datosuministro
        Return dtsuministro.Insertar(suministro)
    End Function

  Public Function Altasuministro(
      ByVal suministro As Datos.suministro    
)
        Dim dtsuministro As New Datos.Datosuministro
        Return dtsuministro.Insertar(suministro)
    End Function

  Public Function AltaRetsuministro(
      ByVal codigo As string,
      ByVal nombre As string,
      ByVal codana As string,
      ByVal presentacion As string,
      ByVal und_concentracion As string,
      ByVal concentracion As string,
      ByVal clase As string,
      ByVal codser As string,
      ByVal valor As decimal,
      ByVal cod_centrocosto As string,
      ByVal cod_undfunc As string,
      ByVal cod_material As string,
      ByVal iva As decimal,
      ByVal cod_generico As string,
      ByVal nom_generico As string,
      ByVal tipo_imp As string,
      ByVal poso As string
    ) As Integer
      Dim suministro As New Datos.suministro(codigo,nombre,codana,presentacion,und_concentracion,concentracion,clase,codser,valor,cod_centrocosto,cod_undfunc,cod_material,iva,cod_generico,nom_generico,tipo_imp,poso)
        Dim dtsuministro As New Datos.Datosuministro
        Return dtsuministro.InsertarRet(suministro)
    End Function

  Public Function AltaRetsuministro(
      ByVal suministro As Datos.suministro    
)
        Dim dtsuministro As New Datos.Datosuministro
        Return dtsuministro.InsertarRet(suministro)
    End Function

  Public Function Editasuministro(
      ByVal codigo As string,
      ByVal nombre As string,
      ByVal codana As string,
      ByVal presentacion As string,
      ByVal und_concentracion As string,
      ByVal concentracion As string,
      ByVal clase As string,
      ByVal codser As string,
      ByVal valor As decimal,
      ByVal cod_centrocosto As string,
      ByVal cod_undfunc As string,
      ByVal cod_material As string,
      ByVal iva As decimal,
      ByVal cod_generico As string,
      ByVal nom_generico As string,
      ByVal tipo_imp As string,
      ByVal poso As string
    )
      Dim suministro As New Datos.suministro(codigo,nombre,codana,presentacion,und_concentracion,concentracion,clase,codser,valor,cod_centrocosto,cod_undfunc,cod_material,iva,cod_generico,nom_generico,tipo_imp,poso)
        Dim dtsuministro As New Datos.Datosuministro
        Return dtsuministro.Editar(suministro)
    End Function

  Public Function Editasuministro(
      ByVal suministro As Datos.suministro    
)
        Dim dtsuministro As New Datos.Datosuministro
        Return dtsuministro.Editar(suministro)
    End Function

 Public Function Eliminasuministro(
      ByVal codigo As string)
        Dim dtsuministro As New Datos.Datosuministro
        Return dtsuministro.Borrar(codigo)
    End Function

  Public Function ObtenersuministroById(
      ByVal codigo As string) As Datos.suministro
        Dim suministro As Datos.suministro = Nothing
        Dim dtsuministro As New Datos.Datosuministro
        For Each suministro In dtsuministro.Select_ById_suministro(codigo)
        Next
        Return suministro
    End Function

  Public Function Obtenersuministro() As List(Of Datos.suministro)
        Dim dtsuministro As New Datos.Datosuministro
        Return dtsuministro.Select_suministro()
    End Function

  Public Function ObtenersuministrobyWhere(ByVal sWhere as String) As List(Of Datos.suministro)
        Dim dtsuministro As New Datos.Datosuministro
        Return dtsuministro.Select_Where_suministro(sWhere)
    End Function
End Class