Public Class Negocioemp_subsidio

  Public Function Altaemp_subsidio(
      ByVal codemp As string,
      ByVal estrato As string,
      ByVal subsidio As decimal
    )
      Dim emp_subsidio As New Datos.emp_subsidio(codemp,estrato,subsidio)
        Dim dtemp_subsidio As New Datos.Datoemp_subsidio
        Return dtemp_subsidio.Insertar(emp_subsidio)
    End Function

  Public Function Altaemp_subsidio(
      ByVal emp_subsidio As Datos.emp_subsidio    
)
        Dim dtemp_subsidio As New Datos.Datoemp_subsidio
        Return dtemp_subsidio.Insertar(emp_subsidio)
    End Function

  Public Function AltaRetemp_subsidio(
      ByVal codemp As string,
      ByVal estrato As string,
      ByVal subsidio As decimal
    ) As Integer
      Dim emp_subsidio As New Datos.emp_subsidio(codemp,estrato,subsidio)
        Dim dtemp_subsidio As New Datos.Datoemp_subsidio
        Return dtemp_subsidio.InsertarRet(emp_subsidio)
    End Function

  Public Function AltaRetemp_subsidio(
      ByVal emp_subsidio As Datos.emp_subsidio    
)
        Dim dtemp_subsidio As New Datos.Datoemp_subsidio
        Return dtemp_subsidio.InsertarRet(emp_subsidio)
    End Function

  Public Function Editaemp_subsidio(
      ByVal codemp As string,
      ByVal estrato As string,
      ByVal subsidio As decimal
    )
      Dim emp_subsidio As New Datos.emp_subsidio(codemp,estrato,subsidio)
        Dim dtemp_subsidio As New Datos.Datoemp_subsidio
        Return dtemp_subsidio.Editar(emp_subsidio)
    End Function

  Public Function Editaemp_subsidio(
      ByVal emp_subsidio As Datos.emp_subsidio    
)
        Dim dtemp_subsidio As New Datos.Datoemp_subsidio
        Return dtemp_subsidio.Editar(emp_subsidio)
    End Function

 Public Function Eliminaemp_subsidio(
      ByVal codemp As string,
      ByVal estrato As string)
        Dim dtemp_subsidio As New Datos.Datoemp_subsidio
        Return dtemp_subsidio.Borrar(codemp,estrato)
    End Function

  Public Function Obteneremp_subsidioById(
      ByVal codemp As string,
      ByVal estrato As string) As Datos.emp_subsidio
        Dim emp_subsidio As Datos.emp_subsidio = Nothing
        Dim dtemp_subsidio As New Datos.Datoemp_subsidio
        For Each emp_subsidio In dtemp_subsidio.Select_ById_emp_subsidio(codemp,estrato)
        Next
        Return emp_subsidio
    End Function

  Public Function Obteneremp_subsidio() As List(Of Datos.emp_subsidio)
        Dim dtemp_subsidio As New Datos.Datoemp_subsidio
        Return dtemp_subsidio.Select_emp_subsidio()
    End Function

  Public Function Obteneremp_subsidiobyWhere(ByVal sWhere as String) As List(Of Datos.emp_subsidio)
        Dim dtemp_subsidio As New Datos.Datoemp_subsidio
        Return dtemp_subsidio.Select_Where_emp_subsidio(sWhere)
    End Function
End Class