Public Class Negocioexc_quirurjicas

  Public Function Altaexc_quirurjicas(
      ByVal manual As string,
      ByVal codigo As string,
      ByVal nombre As string,
      ByVal tipo_porcentaje As string,
      ByVal porc_honmed As decimal,
      ByVal porc_honane As decimal,
      ByVal porc_honayu As decimal,
      ByVal porc_dersal As decimal,
      ByVal porc_dersalesp As decimal,
      ByVal porc_matsum As decimal,
      ByVal porc_honper As decimal,
      ByVal porc_paqquir As decimal
    )
      Dim exc_quirurjicas As New Datos.exc_quirurjicas(manual,codigo,nombre,tipo_porcentaje,porc_honmed,porc_honane,porc_honayu,porc_dersal,porc_dersalesp,porc_matsum,porc_honper,porc_paqquir)
        Dim dtexc_quirurjicas As New Datos.Datoexc_quirurjicas
        Return dtexc_quirurjicas.Insertar(exc_quirurjicas)
    End Function

  Public Function Altaexc_quirurjicas(
      ByVal exc_quirurjicas As Datos.exc_quirurjicas    
)
        Dim dtexc_quirurjicas As New Datos.Datoexc_quirurjicas
        Return dtexc_quirurjicas.Insertar(exc_quirurjicas)
    End Function

  Public Function AltaRetexc_quirurjicas(
      ByVal manual As string,
      ByVal codigo As string,
      ByVal nombre As string,
      ByVal tipo_porcentaje As string,
      ByVal porc_honmed As decimal,
      ByVal porc_honane As decimal,
      ByVal porc_honayu As decimal,
      ByVal porc_dersal As decimal,
      ByVal porc_dersalesp As decimal,
      ByVal porc_matsum As decimal,
      ByVal porc_honper As decimal,
      ByVal porc_paqquir As decimal
    ) As Integer
      Dim exc_quirurjicas As New Datos.exc_quirurjicas(manual,codigo,nombre,tipo_porcentaje,porc_honmed,porc_honane,porc_honayu,porc_dersal,porc_dersalesp,porc_matsum,porc_honper,porc_paqquir)
        Dim dtexc_quirurjicas As New Datos.Datoexc_quirurjicas
        Return dtexc_quirurjicas.InsertarRet(exc_quirurjicas)
    End Function

  Public Function AltaRetexc_quirurjicas(
      ByVal exc_quirurjicas As Datos.exc_quirurjicas    
)
        Dim dtexc_quirurjicas As New Datos.Datoexc_quirurjicas
        Return dtexc_quirurjicas.InsertarRet(exc_quirurjicas)
    End Function

  Public Function Editaexc_quirurjicas(
      ByVal manual As string,
      ByVal codigo As string,
      ByVal nombre As string,
      ByVal tipo_porcentaje As string,
      ByVal porc_honmed As decimal,
      ByVal porc_honane As decimal,
      ByVal porc_honayu As decimal,
      ByVal porc_dersal As decimal,
      ByVal porc_dersalesp As decimal,
      ByVal porc_matsum As decimal,
      ByVal porc_honper As decimal,
      ByVal porc_paqquir As decimal
    )
      Dim exc_quirurjicas As New Datos.exc_quirurjicas(manual,codigo,nombre,tipo_porcentaje,porc_honmed,porc_honane,porc_honayu,porc_dersal,porc_dersalesp,porc_matsum,porc_honper,porc_paqquir)
        Dim dtexc_quirurjicas As New Datos.Datoexc_quirurjicas
        Return dtexc_quirurjicas.Editar(exc_quirurjicas)
    End Function

  Public Function Editaexc_quirurjicas(
      ByVal exc_quirurjicas As Datos.exc_quirurjicas    
)
        Dim dtexc_quirurjicas As New Datos.Datoexc_quirurjicas
        Return dtexc_quirurjicas.Editar(exc_quirurjicas)
    End Function

 Public Function Eliminaexc_quirurjicas(
      ByVal manual As string,
      ByVal codigo As string)
        Dim dtexc_quirurjicas As New Datos.Datoexc_quirurjicas
        Return dtexc_quirurjicas.Borrar(manual,codigo)
    End Function

  Public Function Obtenerexc_quirurjicasById(
      ByVal manual As string,
      ByVal codigo As string) As Datos.exc_quirurjicas
        Dim exc_quirurjicas As Datos.exc_quirurjicas = Nothing
        Dim dtexc_quirurjicas As New Datos.Datoexc_quirurjicas
        For Each exc_quirurjicas In dtexc_quirurjicas.Select_ById_exc_quirurjicas(manual,codigo)
        Next
        Return exc_quirurjicas
    End Function

  Public Function Obtenerexc_quirurjicas() As List(Of Datos.exc_quirurjicas)
        Dim dtexc_quirurjicas As New Datos.Datoexc_quirurjicas
        Return dtexc_quirurjicas.Select_exc_quirurjicas()
    End Function

  Public Function Obtenerexc_quirurjicasbyWhere(ByVal sWhere as String) As List(Of Datos.exc_quirurjicas)
        Dim dtexc_quirurjicas As New Datos.Datoexc_quirurjicas
        Return dtexc_quirurjicas.Select_Where_exc_quirurjicas(sWhere)
    End Function
End Class