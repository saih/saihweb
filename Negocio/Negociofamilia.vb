Public Class Negociofamilia

  Public Function Altafamilia(
      ByVal id As integer,
      ByVal descripcion As string,
      ByVal fecha_adicion As string,
      ByVal usuario_creo As string
    )
      Dim familia As New Datos.familia(id,descripcion,fecha_adicion,usuario_creo)
        Dim dtfamilia As New Datos.Datofamilia
        Return dtfamilia.Insertar(familia)
    End Function

  Public Function Altafamilia(
      ByVal familia As Datos.familia    
)
        Dim dtfamilia As New Datos.Datofamilia
        Return dtfamilia.Insertar(familia)
    End Function

  Public Function AltaRetfamilia(
      ByVal id As integer,
      ByVal descripcion As string,
      ByVal fecha_adicion As string,
      ByVal usuario_creo As string
    ) As Integer
      Dim familia As New Datos.familia(id,descripcion,fecha_adicion,usuario_creo)
        Dim dtfamilia As New Datos.Datofamilia
        Return dtfamilia.InsertarRet(familia)
    End Function

  Public Function AltaRetfamilia(
      ByVal familia As Datos.familia    
)
        Dim dtfamilia As New Datos.Datofamilia
        Return dtfamilia.InsertarRet(familia)
    End Function

  Public Function Editafamilia(
      ByVal id As integer,
      ByVal descripcion As string,
      ByVal fecha_adicion As string,
      ByVal usuario_creo As string
    )
      Dim familia As New Datos.familia(id,descripcion,fecha_adicion,usuario_creo)
        Dim dtfamilia As New Datos.Datofamilia
        Return dtfamilia.Editar(familia)
    End Function

  Public Function Editafamilia(
      ByVal familia As Datos.familia    
)
        Dim dtfamilia As New Datos.Datofamilia
        Return dtfamilia.Editar(familia)
    End Function

 Public Function Eliminafamilia(
      ByVal id As integer)
        Dim dtfamilia As New Datos.Datofamilia
        Return dtfamilia.Borrar(id)
    End Function

  Public Function ObtenerfamiliaById(
      ByVal id As integer) As Datos.familia
        Dim familia As Datos.familia = Nothing
        Dim dtfamilia As New Datos.Datofamilia
        For Each familia In dtfamilia.Select_ById_familia(id)
        Next
        Return familia
    End Function

  Public Function Obtenerfamilia() As List(Of Datos.familia)
        Dim dtfamilia As New Datos.Datofamilia
        Return dtfamilia.Select_familia()
    End Function

  Public Function ObtenerfamiliabyWhere(ByVal sWhere as String) As List(Of Datos.familia)
        Dim dtfamilia As New Datos.Datofamilia
        Return dtfamilia.Select_Where_familia(sWhere)
    End Function
End Class