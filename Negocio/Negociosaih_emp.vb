Public Class Negociosaih_emp

    Public Function Altasaih_emp(
        ByVal codigo As String,
        ByVal nit As String,
        ByVal razonsocial As String,
        ByVal direccion As String,
        ByVal telefono As String,
        ByVal fax As String,
        ByVal contacto As String,
        ByVal tipo_fact As String,
        ByVal cod_contable As String,
        ByVal cod_empFctProc As String,
        ByVal cod_empSolCuentas As String
      )
        Dim saih_emp As New Datos.saih_emp(codigo, nit, razonsocial, direccion, telefono, fax, contacto, tipo_fact, cod_contable, cod_empFctProc, cod_empSolCuentas)
        Dim dtsaih_emp As New Datos.Datosaih_emp
        Return dtsaih_emp.Insertar(saih_emp)
    End Function

    Public Function Altasaih_emp(
        ByVal saih_emp As Datos.saih_emp
  )
        Dim dtsaih_emp As New Datos.Datosaih_emp
        Return dtsaih_emp.Insertar(saih_emp)
    End Function

    Public Function AltaRetsaih_emp(
        ByVal codigo As String,
        ByVal nit As String,
        ByVal razonsocial As String,
        ByVal direccion As String,
        ByVal telefono As String,
        ByVal fax As String,
        ByVal contacto As String,
        ByVal tipo_fact As String,
        ByVal cod_contable As String,
        ByVal cod_empFctProc As String,
        ByVal cod_empSolCuentas As String
      ) As Integer
        Dim saih_emp As New Datos.saih_emp(codigo, nit, razonsocial, direccion, telefono, fax, contacto, tipo_fact, cod_contable, cod_empFctProc, cod_empSolCuentas)
        Dim dtsaih_emp As New Datos.Datosaih_emp
        Return dtsaih_emp.InsertarRet(saih_emp)
    End Function

    Public Function AltaRetsaih_emp(
        ByVal saih_emp As Datos.saih_emp
  )
        Dim dtsaih_emp As New Datos.Datosaih_emp
        Return dtsaih_emp.InsertarRet(saih_emp)
    End Function

    Public Function Editasaih_emp(
        ByVal codigo As String,
        ByVal nit As String,
        ByVal razonsocial As String,
        ByVal direccion As String,
        ByVal telefono As String,
        ByVal fax As String,
        ByVal contacto As String,
        ByVal tipo_fact As String,
        ByVal cod_contable As String,
        ByVal cod_empFctProc As String,
        ByVal cod_empSolCuentas As String
      )
        Dim saih_emp As New Datos.saih_emp(codigo, nit, razonsocial, direccion, telefono, fax, contacto, tipo_fact, cod_contable, cod_empFctProc, cod_empSolCuentas)
        Dim dtsaih_emp As New Datos.Datosaih_emp
        Return dtsaih_emp.Editar(saih_emp)
    End Function

    Public Function Editasaih_emp(
        ByVal saih_emp As Datos.saih_emp
  )
        Dim dtsaih_emp As New Datos.Datosaih_emp
        Return dtsaih_emp.Editar(saih_emp)
    End Function

    Public Function Eliminasaih_emp(
         ByVal codigo As String)
        Dim dtsaih_emp As New Datos.Datosaih_emp
        Return dtsaih_emp.Borrar(codigo)
    End Function

    Public Function Obtenersaih_empById(
        ByVal codigo As String) As Datos.saih_emp
        Dim saih_emp As Datos.saih_emp = Nothing
        Dim dtsaih_emp As New Datos.Datosaih_emp
        For Each saih_emp In dtsaih_emp.Select_ById_saih_emp(codigo)
        Next
        Return saih_emp
    End Function

    Public Function Obtenersaih_emp() As List(Of Datos.saih_emp)
        Dim dtsaih_emp As New Datos.Datosaih_emp
        Return dtsaih_emp.Select_saih_emp()
    End Function

    Public Function Obtenersaih_empbyWhere(ByVal sWhere As String) As List(Of Datos.saih_emp)
        Dim dtsaih_emp As New Datos.Datosaih_emp
        Return dtsaih_emp.Select_Where_saih_emp(sWhere)
    End Function
End Class