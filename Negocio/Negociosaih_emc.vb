Public Class Negociosaih_emc

  Public Function Altasaih_emc(
      ByVal codemp As string,
      ByVal num_contrato As string,
      ByVal nombre As string,
      ByVal fecha As string,
      ByVal fecha_inicial As string,
      ByVal fecha_final As string,
      ByVal monto As decimal,
      ByVal estado As string,
      ByVal num_afiliados As integer,
      ByVal forma_pago As string,
      ByVal val_ejecutado As decimal
    )
      Dim saih_emc As New Datos.saih_emc(codemp,num_contrato,nombre,fecha,fecha_inicial,fecha_final,monto,estado,num_afiliados,forma_pago,val_ejecutado)
        Dim dtsaih_emc As New Datos.Datosaih_emc
        Return dtsaih_emc.Insertar(saih_emc)
    End Function

  Public Function Altasaih_emc(
      ByVal saih_emc As Datos.saih_emc    
)
        Dim dtsaih_emc As New Datos.Datosaih_emc
        Return dtsaih_emc.Insertar(saih_emc)
    End Function

  Public Function AltaRetsaih_emc(
      ByVal codemp As string,
      ByVal num_contrato As string,
      ByVal nombre As string,
      ByVal fecha As string,
      ByVal fecha_inicial As string,
      ByVal fecha_final As string,
      ByVal monto As decimal,
      ByVal estado As string,
      ByVal num_afiliados As integer,
      ByVal forma_pago As string,
      ByVal val_ejecutado As decimal
    ) As Integer
      Dim saih_emc As New Datos.saih_emc(codemp,num_contrato,nombre,fecha,fecha_inicial,fecha_final,monto,estado,num_afiliados,forma_pago,val_ejecutado)
        Dim dtsaih_emc As New Datos.Datosaih_emc
        Return dtsaih_emc.InsertarRet(saih_emc)
    End Function

  Public Function AltaRetsaih_emc(
      ByVal saih_emc As Datos.saih_emc    
)
        Dim dtsaih_emc As New Datos.Datosaih_emc
        Return dtsaih_emc.InsertarRet(saih_emc)
    End Function

  Public Function Editasaih_emc(
      ByVal codemp As string,
      ByVal num_contrato As string,
      ByVal nombre As string,
      ByVal fecha As string,
      ByVal fecha_inicial As string,
      ByVal fecha_final As string,
      ByVal monto As decimal,
      ByVal estado As string,
      ByVal num_afiliados As integer,
      ByVal forma_pago As string,
      ByVal val_ejecutado As decimal
    )
      Dim saih_emc As New Datos.saih_emc(codemp,num_contrato,nombre,fecha,fecha_inicial,fecha_final,monto,estado,num_afiliados,forma_pago,val_ejecutado)
        Dim dtsaih_emc As New Datos.Datosaih_emc
        Return dtsaih_emc.Editar(saih_emc)
    End Function

  Public Function Editasaih_emc(
      ByVal saih_emc As Datos.saih_emc    
)
        Dim dtsaih_emc As New Datos.Datosaih_emc
        Return dtsaih_emc.Editar(saih_emc)
    End Function

 Public Function Eliminasaih_emc(
      ByVal codemp As string,
      ByVal num_contrato As string)
        Dim dtsaih_emc As New Datos.Datosaih_emc
        Return dtsaih_emc.Borrar(codemp,num_contrato)
    End Function

  Public Function Obtenersaih_emcById(
      ByVal codemp As string,
      ByVal num_contrato As string) As Datos.saih_emc
        Dim saih_emc As Datos.saih_emc = Nothing
        Dim dtsaih_emc As New Datos.Datosaih_emc
        For Each saih_emc In dtsaih_emc.Select_ById_saih_emc(codemp,num_contrato)
        Next
        Return saih_emc
    End Function

  Public Function Obtenersaih_emc() As List(Of Datos.saih_emc)
        Dim dtsaih_emc As New Datos.Datosaih_emc
        Return dtsaih_emc.Select_saih_emc()
    End Function

  Public Function Obtenersaih_emcbyWhere(ByVal sWhere as String) As List(Of Datos.saih_emc)
        Dim dtsaih_emc As New Datos.Datosaih_emc
        Return dtsaih_emc.Select_Where_saih_emc(sWhere)
    End Function
End Class