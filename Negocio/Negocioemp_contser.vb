Public Class Negocioemp_contser

  Public Function Altaemp_contser(
      ByVal codemp As string,
      ByVal codser As string,
      ByVal manual As string,
      ByVal porcentaje As decimal,
      ByVal num_contrato As string,
      ByVal niv_complejidad As string
    )
      Dim emp_contser As New Datos.emp_contser(codemp,codser,manual,porcentaje,num_contrato,niv_complejidad)
        Dim dtemp_contser As New Datos.Datoemp_contser
        Return dtemp_contser.Insertar(emp_contser)
    End Function

  Public Function Altaemp_contser(
      ByVal emp_contser As Datos.emp_contser    
)
        Dim dtemp_contser As New Datos.Datoemp_contser
        Return dtemp_contser.Insertar(emp_contser)
    End Function

  Public Function AltaRetemp_contser(
      ByVal codemp As string,
      ByVal codser As string,
      ByVal manual As string,
      ByVal porcentaje As decimal,
      ByVal num_contrato As string,
      ByVal niv_complejidad As string
    ) As Integer
      Dim emp_contser As New Datos.emp_contser(codemp,codser,manual,porcentaje,num_contrato,niv_complejidad)
        Dim dtemp_contser As New Datos.Datoemp_contser
        Return dtemp_contser.InsertarRet(emp_contser)
    End Function

  Public Function AltaRetemp_contser(
      ByVal emp_contser As Datos.emp_contser    
)
        Dim dtemp_contser As New Datos.Datoemp_contser
        Return dtemp_contser.InsertarRet(emp_contser)
    End Function

  Public Function Editaemp_contser(
      ByVal codemp As string,
      ByVal codser As string,
      ByVal manual As string,
      ByVal porcentaje As decimal,
      ByVal num_contrato As string,
      ByVal niv_complejidad As string
    )
      Dim emp_contser As New Datos.emp_contser(codemp,codser,manual,porcentaje,num_contrato,niv_complejidad)
        Dim dtemp_contser As New Datos.Datoemp_contser
        Return dtemp_contser.Editar(emp_contser)
    End Function

  Public Function Editaemp_contser(
      ByVal emp_contser As Datos.emp_contser    
)
        Dim dtemp_contser As New Datos.Datoemp_contser
        Return dtemp_contser.Editar(emp_contser)
    End Function

 Public Function Eliminaemp_contser(
      ByVal codemp As string,
      ByVal codser As string,
      ByVal manual As string)
        Dim dtemp_contser As New Datos.Datoemp_contser
        Return dtemp_contser.Borrar(codemp,codser,manual)
    End Function

  Public Function Obteneremp_contserById(
      ByVal codemp As string,
      ByVal codser As string,
      ByVal manual As string) As Datos.emp_contser
        Dim emp_contser As Datos.emp_contser = Nothing
        Dim dtemp_contser As New Datos.Datoemp_contser
        For Each emp_contser In dtemp_contser.Select_ById_emp_contser(codemp,codser,manual)
        Next
        Return emp_contser
    End Function

  Public Function Obteneremp_contser() As List(Of Datos.emp_contser)
        Dim dtemp_contser As New Datos.Datoemp_contser
        Return dtemp_contser.Select_emp_contser()
    End Function

  Public Function Obteneremp_contserbyWhere(ByVal sWhere as String) As List(Of Datos.emp_contser)
        Dim dtemp_contser As New Datos.Datoemp_contser
        Return dtemp_contser.Select_Where_emp_contser(sWhere)
    End Function
End Class