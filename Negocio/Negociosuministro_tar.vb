Public Class Negociosuministro_tar

  Public Function Altasuministro_tar(
      ByVal cod_suministro As string,
      ByVal manual As string,
      ByVal plan_tar As string,
      ByVal valor As decimal
    )
      Dim suministro_tar As New Datos.suministro_tar(cod_suministro,manual,plan_tar,valor)
        Dim dtsuministro_tar As New Datos.Datosuministro_tar
        Return dtsuministro_tar.Insertar(suministro_tar)
    End Function

  Public Function Altasuministro_tar(
      ByVal suministro_tar As Datos.suministro_tar    
)
        Dim dtsuministro_tar As New Datos.Datosuministro_tar
        Return dtsuministro_tar.Insertar(suministro_tar)
    End Function

  Public Function AltaRetsuministro_tar(
      ByVal cod_suministro As string,
      ByVal manual As string,
      ByVal plan_tar As string,
      ByVal valor As decimal
    ) As Integer
      Dim suministro_tar As New Datos.suministro_tar(cod_suministro,manual,plan_tar,valor)
        Dim dtsuministro_tar As New Datos.Datosuministro_tar
        Return dtsuministro_tar.InsertarRet(suministro_tar)
    End Function

  Public Function AltaRetsuministro_tar(
      ByVal suministro_tar As Datos.suministro_tar    
)
        Dim dtsuministro_tar As New Datos.Datosuministro_tar
        Return dtsuministro_tar.InsertarRet(suministro_tar)
    End Function

  Public Function Editasuministro_tar(
      ByVal cod_suministro As string,
      ByVal manual As string,
      ByVal plan_tar As string,
      ByVal valor As decimal
    )
      Dim suministro_tar As New Datos.suministro_tar(cod_suministro,manual,plan_tar,valor)
        Dim dtsuministro_tar As New Datos.Datosuministro_tar
        Return dtsuministro_tar.Editar(suministro_tar)
    End Function

  Public Function Editasuministro_tar(
      ByVal suministro_tar As Datos.suministro_tar    
)
        Dim dtsuministro_tar As New Datos.Datosuministro_tar
        Return dtsuministro_tar.Editar(suministro_tar)
    End Function

 Public Function Eliminasuministro_tar(
      ByVal cod_suministro As string,
      ByVal manual As string,
      ByVal plan_tar As string)
        Dim dtsuministro_tar As New Datos.Datosuministro_tar
        Return dtsuministro_tar.Borrar(cod_suministro,manual,plan_tar)
    End Function

  Public Function Obtenersuministro_tarById(
      ByVal cod_suministro As string,
      ByVal manual As string,
      ByVal plan_tar As string) As Datos.suministro_tar
        Dim suministro_tar As Datos.suministro_tar = Nothing
        Dim dtsuministro_tar As New Datos.Datosuministro_tar
        For Each suministro_tar In dtsuministro_tar.Select_ById_suministro_tar(cod_suministro,manual,plan_tar)
        Next
        Return suministro_tar
    End Function

  Public Function Obtenersuministro_tar() As List(Of Datos.suministro_tar)
        Dim dtsuministro_tar As New Datos.Datosuministro_tar
        Return dtsuministro_tar.Select_suministro_tar()
    End Function

  Public Function Obtenersuministro_tarbyWhere(ByVal sWhere as String) As List(Of Datos.suministro_tar)
        Dim dtsuministro_tar As New Datos.Datosuministro_tar
        Return dtsuministro_tar.Select_Where_suministro_tar(sWhere)
    End Function
End Class