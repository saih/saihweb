Public Class Negociosaih_apas

  Public Function Altasaih_apas(
      ByVal codpre As string,
      ByVal nropme As string,
      ByVal codpas As string,
      ByVal fecha_adicion As string,
      ByVal fecha_modificacion As string,
      ByVal usuario_creo As string,
      ByVal usuario_modifico As string,
      ByVal fecha_inicial As string,
      ByVal fecha_final As string,
      ByVal consultorio As string,
      ByVal intervalo As integer,
      ByVal codser As string,
      ByVal estado As string,
      ByVal motivo_cancelacion As string
    )
      Dim saih_apas As New Datos.saih_apas(codpre,nropme,codpas,fecha_adicion,fecha_modificacion,usuario_creo,usuario_modifico,fecha_inicial,fecha_final,consultorio,intervalo,codser,estado,motivo_cancelacion)
        Dim dtsaih_apas As New Datos.Datosaih_apas
        Return dtsaih_apas.Insertar(saih_apas)
    End Function

  Public Function Altasaih_apas(
      ByVal saih_apas As Datos.saih_apas    
)
        Dim dtsaih_apas As New Datos.Datosaih_apas
        Return dtsaih_apas.Insertar(saih_apas)
    End Function

  Public Function AltaRetsaih_apas(
      ByVal codpre As string,
      ByVal nropme As string,
      ByVal codpas As string,
      ByVal fecha_adicion As string,
      ByVal fecha_modificacion As string,
      ByVal usuario_creo As string,
      ByVal usuario_modifico As string,
      ByVal fecha_inicial As string,
      ByVal fecha_final As string,
      ByVal consultorio As string,
      ByVal intervalo As integer,
      ByVal codser As string,
      ByVal estado As string,
      ByVal motivo_cancelacion As string
    ) As Integer
      Dim saih_apas As New Datos.saih_apas(codpre,nropme,codpas,fecha_adicion,fecha_modificacion,usuario_creo,usuario_modifico,fecha_inicial,fecha_final,consultorio,intervalo,codser,estado,motivo_cancelacion)
        Dim dtsaih_apas As New Datos.Datosaih_apas
        Return dtsaih_apas.InsertarRet(saih_apas)
    End Function

  Public Function AltaRetsaih_apas(
      ByVal saih_apas As Datos.saih_apas    
)
        Dim dtsaih_apas As New Datos.Datosaih_apas
        Return dtsaih_apas.InsertarRet(saih_apas)
    End Function

  Public Function Editasaih_apas(
      ByVal codpre As string,
      ByVal nropme As string,
      ByVal codpas As string,
      ByVal fecha_adicion As string,
      ByVal fecha_modificacion As string,
      ByVal usuario_creo As string,
      ByVal usuario_modifico As string,
      ByVal fecha_inicial As string,
      ByVal fecha_final As string,
      ByVal consultorio As string,
      ByVal intervalo As integer,
      ByVal codser As string,
      ByVal estado As string,
      ByVal motivo_cancelacion As string
    )
      Dim saih_apas As New Datos.saih_apas(codpre,nropme,codpas,fecha_adicion,fecha_modificacion,usuario_creo,usuario_modifico,fecha_inicial,fecha_final,consultorio,intervalo,codser,estado,motivo_cancelacion)
        Dim dtsaih_apas As New Datos.Datosaih_apas
        Return dtsaih_apas.Editar(saih_apas)
    End Function

  Public Function Editasaih_apas(
      ByVal saih_apas As Datos.saih_apas    
)
        Dim dtsaih_apas As New Datos.Datosaih_apas
        Return dtsaih_apas.Editar(saih_apas)
    End Function

 Public Function Eliminasaih_apas(
      ByVal codpre As string,
      ByVal nropme As string)
        Dim dtsaih_apas As New Datos.Datosaih_apas
        Return dtsaih_apas.Borrar(codpre,nropme)
    End Function

  Public Function Obtenersaih_apasById(
      ByVal codpre As string,
      ByVal nropme As string) As Datos.saih_apas
        Dim saih_apas As Datos.saih_apas = Nothing
        Dim dtsaih_apas As New Datos.Datosaih_apas
        For Each saih_apas In dtsaih_apas.Select_ById_saih_apas(codpre,nropme)
        Next
        Return saih_apas
    End Function

  Public Function Obtenersaih_apas() As List(Of Datos.saih_apas)
        Dim dtsaih_apas As New Datos.Datosaih_apas
        Return dtsaih_apas.Select_saih_apas()
    End Function

  Public Function Obtenersaih_apasbyWhere(ByVal sWhere as String) As List(Of Datos.saih_apas)
        Dim dtsaih_apas As New Datos.Datosaih_apas
        Return dtsaih_apas.Select_Where_saih_apas(sWhere)
    End Function
End Class